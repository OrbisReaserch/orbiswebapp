<?php
class Category_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function getMenuCategory()
	{
		$result=$this->db->query("select * from category where status='1'");
		return $result->result();
	}
	public function getFeaturedReports()
	{
		$result=$this->db->query("SELECT * FROM report AS r1 JOIN (SELECT CEIL(RAND() * (SELECT MAX(id)   FROM report)) AS id) AS r2 WHERE r1.id >= r2.id ORDER BY r1.id ASC LIMIT 0,3");
		return $result->result();
	}
	public function getClients()
	{
		$result=$this->db->query("select * from brand where status='1' limit 0,6");
		return $result->result();
	}
	public function getUsers()
	{
		$result=$this->db->query("select * from users");
		return $result->result();
	}
  public function getPublishers()
	{
		$result=$this->db->query("select * from users where user_type='Publisher' and status='1'");
		return $result->result();
	}
	public function getSubCategory()
	{
		$result=$this->db->query("select * from sub_category where status='1'");
		return $result->result();
	}
	public function getAdvertisement()
	{
		$result=$this->db->query("select * from advertisement where status='1'");
		return $result->result();
	}
	public function getBrand()
	{
		$result=$this->db->query("select * from brand where status='1'");
		return $result->result();
	}
	public function getNews()
	{
		$result=$this->db->query("select * from news where status='1'");
		return $result->result();
	}


	public function getOrderDetails($id)
	{
		$result=$this->db->query("select * from orders where status='1' and order_id='".$id."'");
		return $result->result();
	}
		public function getTestimonials()
	{
		$result=$this->db->query("select * from testimonials where status='1' order by id DESC limit 1");
		return $result->result();
	}
	
	public function getFooterNews()
	{
		$result=$this->db->query("select * from news where status='1' order by id desc limit 2");
		return $result->result();
	}
	public function getFooterReport()
	{
		$result=$this->db->query("select * from report where status='1' order by id desc limit 5");
		return $result->result();
	}
	

}
?>