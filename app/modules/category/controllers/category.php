<?php
class Category extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('category/category_model');
	}

	public function index()
	{
		$data['category'] = $this->category_model->getMenuCategory();		
		$data['sub_category'] = $this->category_model->getSubCategory();	
		$data['include'] = 'category/category';
		$this->load->view('frontend/container',$data);
	}
		public function publisher()
		{
				$data['users'] = $this->category_model->getUsers();
		$data['publisher'] = $this->category_model->getPublishers();
		$data['category'] = $this->category_model->getMenuCategory();
		$data['clients'] = $this->category_model->getClients();
		$data['sub_category'] = $this->category_model->getSubCategory();
		$data['advertisement'] = $this->category_model->getAdvertisement();
		$data['brand'] = $this->category_model->getBrand();
		$data['news'] = $this->category_model->getNews();
		$data['featured_reports'] = $this->category_model->getFeaturedReports();
		$data['testimonials'] = $this->category_model->getTestimonials();
		$data['footernews'] = $this->category_model->getFooterNews();
		$data['footerreport'] = $this->category_model->getFooterReport();
		$data['include'] = 'category/category';
		$data['publisherdiv'] = 'yes';
		$this->load->view('frontend/container',$data);

		}
	function clear_cache()
	{
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
	}

}