<div id="slider_wrapper2">
  <div class="wrap">
    <h1>Country</h1>
  </div>
</div>
<div class="clear"></div>
<div class="content">
  <div class="wrap">
  
  <?php 
  $i=1;
  for ($j = 0; $j< sizeof($country); $j++)
                  {
     if($i%2!=0)
    {      
    	$id_contry=$country[$j]->id;
        $que_report=$this->db->query("SELECT * FROM report WHERE status='1' AND country_id='$id_contry' ORDER BY id DESC LIMIT 2");
		$n=$que_report->num_rows();   	
    ?>
    <div class="one_half">
      <div class="heading"><span><?php echo $country[$j]->country_name?></span></div>
      <div class="hosting_features">
       <?php 
        
		if($n>0)
		{
        $row=$que_report->result();
		for ($a=0;$a<count($row);$a++)
		{
        ?>
        <div class="one_full">
          <ul>
            <li><span><i class="icon-plus"></i></span><a href="<?php echo base_url(); ?>reports/report/index/<?php echo $row[$a]->page_urls; ?>"><?php echo substr($row[$a]->report_name,0,50) ?></a>
              <p>
			  <?php 
			  $repo=$row[$a]->report_description;
			    $summary = str_replace("<span>", "", $repo);
                $summary1 = str_replace("</span>", "", $summary);
                $summary2 = str_replace("<div>", "", $summary1);
                $summary3 = str_replace("</div>", "", $summary2);
                $summary4 = str_replace("<p>", "", $summary3);
                $summary5 = str_replace("</p>", "", $summary4);
				 $summary6 = str_replace("\n", "", $summary5);
			  echo substr($summary6,0,100); 
			  ?>...
			  <a href="<?php echo base_url(); ?>reports/report/index/<?php echo $row[$a]->page_urls; ?>">Read More</a></p>
            </li>
          </ul>
        </div>
        
        <?php 
		}
      ?>
        
        <div class="one_full">
          <ul>
            <li><a class="button small" href="<?php echo base_url(); ?>reports/report/getCountryInfo/<?php echo $country[$j]->id; ?>">View all</a></li>
          </ul>
        </div>
        
          <?php 
		}
		else 
		{
      ?>
      
      
      
      <div class="one_full">
          <ul>
            <li>
              <p>No report avaliable for this country</p>
            </li>
          </ul>
        </div>
      <?php 
		}
      ?>
        
        <div class="clear"></div>
      </div>
    </div>
    <?php 
      }
   else 
   {
   	$id_contry=$country[$j]->id;
        $que_report=$this->db->query("SELECT * FROM report WHERE status='1' AND country_id='$id_contry' ORDER BY id DESC LIMIT 2");
		$n=$que_report->num_rows();
    ?>
    <div class="one_half last">
      <div class="heading"><span><?php echo $country[$j]->country_name;?></span></div>
      <div class="hosting_features">
        
        <?php 
        
		if($n>0)
		{
        $row=$que_report->result();
		for ($a=0;$a<count($row);$a++)
		{
        ?>
        
        <div class="one_full">
          <ul>
            <li><span><i class="icon-plus"></i></span><a href="<?php echo base_url(); ?>reports/report/index/<?php echo $row[$a]->page_urls; ?>"> <?php echo substr($row[$a]->report_name,0,50) ?> </a>
              <p>
			  <?php 
			  $repo=$row[$a]->report_description;
			    $summary = str_replace("<span>", "", $repo);
                $summary1 = str_replace("</span>", "", $summary);
                $summary2 = str_replace("<div>", "", $summary1);
                $summary3 = str_replace("</div>", "", $summary2);
                $summary4 = str_replace("<p>", "", $summary3);
                $summary5 = str_replace("</p>", "", $summary4);
				 $summary6 = str_replace("\n", "", $summary5);
			  echo substr($summary6,0,100);
			  ?>...
			 
			  <a href="<?php echo base_url(); ?>reports/report/index/<?php echo $row[$a]->page_urls; ?>">Read more</a></p>
            </li>
          </ul>
        </div>
      <?php 
		}
      ?>
      
        <div class="one_full">
          <ul>
            <li><a class="button small" href="<?php echo base_url(); ?>reports/report/getCountryInfo/<?php echo $country[$j]->id; ?>">View all</a></li>
          </ul>
        </div>
        <?php 
		}
		else 
		{
      ?>
      
      
      
      <div class="one_full">
          <ul>
            <li>
              <p>No report avaliable for this country</p>
            </li>
          </ul>
        </div>
      <?php 
		}
      ?>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
    <div class="space2"></div>
    
<?php 
    }
    $i=$i+1;
                  }
?>

  <div class="clear"></div>
    <div class="space2"></div>
  
  </div>
</div>