<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Order extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->model("category/category_model");
		$this->load->model('order_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	//==================== all page session check =====================

	public function Checklogin()
	{
		if ($this->session->userdata('user_id') == '')
		{
			redirect('home/');
		}
	}

	public function index()
	{
		$this->Checklogin();
		$user_id =  $this->session->userdata("user_id");
		$data['category'] = $this->category_model->getMenuCategory();
		$data['subcategory'] = $this->category_model->getSubCategory();
		//$data['user_info'] = $this->profile_model->getRecord($id);
		$data['featured'] = $this->category_model->getFeaturedReports();
		$data['result'] = $this->order_model->getUserOrder($user_id);
		$data['footernews'] = $this->order_model->getFooterNews();
		$data['footerreport'] = $this->order_model->getFooterReport();
		$data['include'] = 'order/order';
		// $data['admin_section'] = 'order';
		$this->load->view('frontend/container', $data);
	}

	public function view_order($id)
	{
		$this->Checklogin();
		$data['category'] = $this->category_model->getMenuCategory();
		$data['subcategory'] = $this->category_model->getSubCategory();
		//$data['user_info'] = $this->profile_model->getRecord($id);
		$data['featured'] = $this->category_model->getFeaturedReports();
		$data['result'] = $this->order_model->getOrder($id);
		$data['footernews'] = $this->order_model->getFooterNews();
		$data['footerreport'] = $this->order_model->getFooterReport();
		$data['include'] = 'order/view_order';
		$data['info'] = $this->order_model->getRecord($id);
		// $data['admin_section'] = 'order';
		$this->load->view('frontend/container', $data);
	}

	public function delete($id)
	{
		if ($this->advertisement_model->delete($id))
		{
			$this->session->set_flashdata('success', 'Record has been deleted successfully.');
			redirect(base_url() . 'siteadmin/advertisement');
		}
	}
}
