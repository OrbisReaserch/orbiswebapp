<?php
class Order_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getall()
	{
		$query = $this->db->get('orders');
		return $query->result();
	}
        
        public function getRecord($id)
	{
			
		$q= $this->db->get_where('orders',array('order_id'=>$id));
		return $q->row_array();
	}
        
        public function getOrder($id)
	{	
                $result=$this->db->query("select * from order_details where order_id='".$id."'");
		return $result->result();
	}
        public function getUserOrder($id)
	{	
                $result=$this->db->query("select * from orders where user_id='".$id."'");
		return $result->result();
	}
        
//	public function getMenuCategory()
//	{
//		$result=$this->db->query("select * from category where status='1'");
//		return $result->result();
//	}
	
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('order');
		return true;
	}
public function getFooterNews()
	{
		$result=$this->db->query("select * from news where status='1' order by id desc limit 2");
		return $result->result();
	}
	public function getFooterReport()
	{
		$result=$this->db->query("select * from report where status='1' order by id desc limit 5");
		return $result->result();
	
	}

}