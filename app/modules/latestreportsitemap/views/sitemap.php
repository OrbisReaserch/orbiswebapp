<?php header('Content-Type: application/xml');

$changefreq='<changefreq>always</changefreq>';
$priority='<priority>0.9</priority>';
$catfeq='<changefreq>always</changefreq>';
$catpriority='<priority>0.8</priority>';
$publfeq='<changefreq>weekly</changefreq>';
$publpriority='<priority>0.7</priority>';
$newsfeq='<changefreq>daily</changefreq>';
$newspriority='<priority>0.7</priority>';
$countryfeq='<changefreq>weekly</changefreq>';
$countrypriority='<priority>0.7</priority>';
?>

<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

<url><loc><?php echo str_replace('m/','m',base_url());?></loc>
	<changefreq>always</changefreq>
	<priority>1.0</priority></url>
	

	<?php $sitemappages='51'; 

	for($i=1;$i<=$sitemappages;$i++){
	?> 
	<url><loc><?php echo base_url();?>sitemap<?php echo $i ?>.xml</loc><?php echo $changefreq;  echo $priority;?></url>
	
	<?php }?>
	<url><loc><?php echo base_url();?>allcategorysitemap.xml</loc><?php echo $catfeq;  echo $catpriority;?></url>
	<url><loc><?php echo base_url();?>publishersitemap.xml</loc><?php echo $publfeq;  echo $publpriority;?></url>
	<url><loc><?php echo base_url();?>newssitemap.xml</loc><?php echo $newsfeq;  echo $newspriority;?></url>
	<url><loc><?php echo base_url();?>countrysitemap.xml</loc><?php echo $countryfeq;  echo $countrypriority;?></url>
	

</urlset>