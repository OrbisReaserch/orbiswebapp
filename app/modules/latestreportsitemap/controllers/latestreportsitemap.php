<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');
class latestreportsitemap extends CI_Controller
{

    function __construct ()
    {

        parent::__construct();

         $this->load->library('xml_writer');

    }



    public function sitemap(){

         $this->load->view('sitemap');
    }

    
    Public function latestreportsitemap1 ()
    {     
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 0,45000 ;');
		$u = $query->result_array();

	
		foreach($u as $url)
			{ 	
            //valid_date function define in constant.php	
					$xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();	
			}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
		 
    }
    Public function latestreportsitemap2 ()
    {       
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 45001,45000;');
		$u = $query->result_array();

	
		foreach($u as $url)
			{ 		
					$xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();	
			}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
		 
    }
	
	Public function latestreportsitemap3 ()
    {       
         
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 90001,45000;');
		$u = $query->result_array();

	
		foreach($u as $url)
			{ 		
					$xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();	
			}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
		 
    }

    Public function latestreportsitemap4 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 135001,45000;');
		$u = $query->result_array();

	
		foreach($u as $url)
			{ 		
					$xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();	
			}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
		 
    }

    Public function latestreportsitemap5 ()
    {
    	
         
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 180001,45000;');
		$u = $query->result_array();

	
		foreach($u as $url)
			{ 		
					$xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();	
			}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
		 
    }

    Public function latestreportsitemap6 ()
    {
    	
      $priority='0.9';
        $chengefeq='always';
      $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 225001,45000;');
		$u = $query->result_array();

	
		foreach($u as $url)
			{ 		
					$xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();	
			}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
		 
    }
	
    Public function latestreportsitemap7 ()
    {
    	
      $priority='0.9';
      $chengefeq='always';
      $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 270001,45000;');
		$u = $query->result_array();

	
		foreach($u as $url)
			{ 		
					$xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();	
			}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
		 
    }

    Public function latestreportsitemap8 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 315001,45000;');
		$u = $query->result_array();

	
		foreach($u as $url)
			{ 		
					$xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();	
			}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
		 
    }

    Public function latestreportsitemap9 ()
    {
    	
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 360001,45000;');
		$u = $query->result_array();

	
		foreach($u as $url)
			{ 		
					$xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();	
			}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
		 
    }

    Public function latestreportsitemap10 ()
    {
    	
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 405001,45000;');
		$u = $query->result_array();

	
		foreach($u as $url)
			{ 		
					$xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();	
			}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
		 
    }

    Public function latestreportsitemap11 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 450001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap12 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 495001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap13 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 540001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap14 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 585001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap15 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 630001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap16 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 675001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch(); 
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap17 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 720001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                   $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch(); 
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap18 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 765001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap19 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls FROM report ORDER BY id ASC LIMIT 
            810001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap20 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 855001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap21 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 900001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap22 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 945001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap23 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 990001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap24 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls FROM report ORDER BY id ASC LIMIT 
            1035001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap25 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1080001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap26 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1125001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap27 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1170001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap28 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1215001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap29 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1260001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap30 ()
    {

        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1305001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    public  function latestreportsitemap31 ()
    {
       $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();  

        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1350001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {       
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap32 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1395001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap33 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1440001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap34 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1485001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap35 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1530001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch(); 
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap36 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls FROM report ORDER BY id ASC LIMIT 
            1575001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap37 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1620001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap38 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls FROM report ORDER BY id ASC LIMIT 
            1665001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap39 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1710001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap40 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1755001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap41 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls FROM report ORDER BY id ASC LIMIT 
            1800001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }

    Public function latestreportsitemap42 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 1845001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap43 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls FROM report ORDER BY id ASC LIMIT 
            1935001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
					$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
					$xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap44 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls FROM report ORDER BY id ASC LIMIT 
            1980001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
                    $xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
                    $xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap45 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls FROM report ORDER BY id ASC LIMIT 
            2025001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
                    $xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
                    $xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap46 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 2070001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
                    $xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
                    $xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap47 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 2115001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
                    $xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
                    $xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap48 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 2160001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
                    $xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                   // $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
                    $xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap49 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 2205001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
                    $xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
                    $xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap50 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 2250001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
                    $xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
                    $xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    Public function latestreportsitemap51 ()
    {
        
        $priority='0.9';
        $chengefeq='always';
        $xml = new Xml_writer();
         
        $xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 2295001,45000;');
        $u = $query->result_array();

    
        foreach($u as $url)
            {     
                    $xml->startBranch('url');     
                    $xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    //$xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);    
                    $xml->endBranch();  
            }
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
         
    }
    // Public function latestreportsitemap52 ()
    // {
        
    //     $priority='0.9';
    //     $chengefeq='always';
    //     $xml = new Xml_writer();
         
    //     $xml->initiate();
    //     $xml->startBranch('urlset');
    //     $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 2340001,45000;');
    //     $u = $query->result_array();

    
    //     foreach($u as $url)
    //         {     
    //                 $xml->startBranch('url');     
    //                 $xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
    //                 //$xml->addNode('lastmod',$url['report_date'],true);
    //                 $xml->addNode('changefreq',$chengefeq,true); 
    //                 $xml->addNode('priority',$priority,true);    
    //                 $xml->endBranch();  
    //         }
    //     $xml->endBranch();
    //     $data = array();
    //     $data['xml'] = $xml->getXml(FALSE);
    //     $this->load->view('xml_new', $data);
         
    // }


   /* function index ()
    {
		$this->load->library('xml_writer');
        $priority='0.9';
        $chengefeq='always';
$xml = new Xml_writer();
        $xml->setRootName('Orbis-Reports');
        $xml->initiate();
        $xml->startBranch('reports');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 0,49000;');
		$u=$query->result_array();
		$i=1;
		foreach($u as $url)
		{ 		
			$xml->startBranch('url');      
			$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);     
			$xml->endBranch();		
			$i++;	
		}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml', $data);
    }
	Public function latestreportsitemap1 ()
    { 
        $this->load->library('xml_writer');
        $priority='0.9';
        $chengefeq='always';
$xml = new Xml_writer();
		// $xml->setXsltFilePath('newsitemap.xsl');
		$xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 0,49000;');
		$u=$query->result_array();
		 
		foreach($u as $url)
		{ 		
			$xml->startBranch('url');        
			$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);       
			$xml->endBranch();		
			 	
		}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
    }
	Public function latestreportsitemap2 ()
    {
         $this->load->library('xml_writer');
        $priority='0.9';
        $chengefeq='always';
$xml = new Xml_writer();
		// $xml->setXsltFilePath('newsitemap.xsl');
		$xml->initiate();
        $xml->startBranch('urlset');
        $query=$this->db->query('SELECT page_urls,report_date FROM report ORDER BY id ASC LIMIT 49000,49000;');
		$u=$query->result_array();
		 
		foreach($u as $url)
		{ 		
			$xml->startBranch('url');        
			$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true); 
                    $xml->addNode('lastmod',$url['report_date'],true);
                    $xml->addNode('changefreq',$chengefeq,true); 
                    $xml->addNode('priority',$priority,true);       
			$xml->endBranch();		
			 	
		}
        $xml->endBranch();
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml_new', $data);
    }*/
	
	
}
