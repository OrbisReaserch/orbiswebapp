<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class countrysitemap extends CI_Controller
{

    function __construct ()
    {
        parent::__construct();
        $this->load->library('xml_writer');
    }

    function index ()
    {        
        $priority='0.7';
        $lastmod='2015-02-02';
        $changefreq='daily'; 
        $xml = new Xml_writer();
        $xml->initiate();
        $xml->startBranch('urlset');
		
		$query=$this->db->query("SELECT country_name FROM country where status = '1';");
		 
		$u=$query->result_array();
		$i=1;
		foreach($u as $url)
	{ 		
	 $counry_name = base_url().strtolower(str_replace(" ","-",$url['country_name'])).'-market-reports.html';   
		$xml->startBranch('url');        
		$xml->addNode('loc',$counry_name,true);
        $xml->addNode('lastmod',$lastmod,true);
        $xml->addNode('changefreq',$changefreq,true);
        $xml->addNode('priority',$priority,true);     
		$xml->endBranch();		
		$i++;	
	}
        
        
        
        
        $xml->endBranch();
        
     
        
        
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml', $data);
    }
}
