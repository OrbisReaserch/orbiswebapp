<!-- Content -->
<?php $user_id =  $this->session->userdata("user_id"); ?>
<div class="row content">

    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="breadcrumbs">
            <p><a href="<?php echo base_url();?>home">Home</a> <i class="icons icon-right-dir"></i> Create an account</p>
        </div>
    </div>

    <!-- Main Content -->
    <section class="main-content col-lg-9 col-md-9 col-sm-9">



        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 register-account">

                <div class="carousel-heading no-margin">
                    <h4>Edit Personal Information</h4>
                </div>
                <form method='post' action='<?php echo base_url();?>user/edit_user/<?php echo $user_id;?>'>
                    <div class="page-content">
                        <div class="row">

                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <p><strong>Shopper information</strong></p>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>E-Mail*</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='email' id='email' placeholder="Email-Id" value="<?php echo $info[0]->email;?>" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}">
                            </div>	

                        </div>

                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>Username*</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='username' id='username' value="<?php echo $info[0]->username;?>" placeholder="Username" required>
                            </div>	

                        </div>

                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>Displayed name*</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='display_name' id='display_name' value="<?php echo $info[0]->display_name;?>" required placeholder="Displayed Name">
                            </div>	

                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <p><strong>Bill to</strong></p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>Company Name</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='company_name' id='company_name' value="<?php echo $info[0]->company_name;?>" placeholder="Company Name">
                            </div>	
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>First name</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='first_name' id='first_name' value="<?php echo $info[0]->first_name;?>" placeholder="First Name">
                            </div>	
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>Middle Name</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='middle_name' id='middle_name' value="<?php echo $info[0]->middle_name;?>" placeholder="Middle Name">
                            </div>	
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>Last Name</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='last_name' id='last_name' value="<?php echo $info[0]->last_name;?>" placeholder="Last Name">
                            </div>	
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>Address 1</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='address1' id='address1' value="<?php echo $info[0]->address1;?>" placeholder="Address">
                            </div>	
                        </div>
                       <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>Address 2</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='address2' id='address2' value="<?php echo $info[0]->address2;?>" placeholder="Address">
                            </div>	
                        </div>

                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>ZIP / Postal Code</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='zipcode' id='zipcode' value="<?php echo $info[0]->zipcode;?>" placeholder="Zip Code">
                            </div>	

                        </div>

                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>City</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='city' id='city' value="<?php echo $info[0]->city;?>" placeholder="City">
                            </div>	

                        </div>

                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>Country</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='country' id='country' value="<?php echo $info[0]->country;?>" placeholder="Country">
                            </div>	

                        </div>

                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>State / Province / Region</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='state' id='state' value="<?php echo $info[0]->state;?>" placeholder="State">
                            </div>	

                        </div>

                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>Phone</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='phone' id='phone' value="<?php echo $info[0]->phone;?>" placeholder="Phone">
                            </div>	

                        </div>

                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>Mobile Phone</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='mobile' id='mobile' value="<?php echo $info[0]->mobile;?>" placeholder="Mobile">
                            </div>	

                        </div>

                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <p>FAX</p>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <input type="text" name='fax' id='fax' value="<?php echo $info[0]->fax;?>" placeholder="Fax">
                            </div>	

                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <input class="big" type="submit" name="submit" value="Save">
                                <input class="big" type="reset" value="Cancel">
                            </div>

                        </div>
                    </div>
                </form>     
            </div>

        </div>


    </section>
    <!-- /Main Content -->


    <!-- Sidebar -->
    <aside class="sidebar col-lg-3 col-md-3 col-sm-3 right-sidebar">

        <!-- Categories -->
        <?php $this->load->view("frontend/category_menu");?>
        <!-- /Categories -->



        <!-- Compare Products -->
        <?php $this->load->view("frontend/compare");?>
        <!-- /Compare Products -->


        <!-- Carousel -->
        <?php $this->load->view("frontend/advertisement");?>
        <!-- /Carousel -->


        <!-- Bestsellers -->
        <?php $this->load->view("frontend/best_sellers");?>
        <!-- /Bestsellers -->


    </aside>
    <!-- /Sidebar -->

</div>
<!-- /Content -->
<!-- JavaScript -->
<script src="<?php echo theme_url(); ?>js/modernizr.min.js"></script>
<script src="<?php echo theme_url(); ?>js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo theme_url(); ?>js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo theme_url(); ?>js/jquery.raty.min.js"></script>

<!-- Scroll Bar -->
<script src="<?php echo theme_url(); ?>js/perfect-scrollbar.min.js"></script>

<!-- Cloud Zoom -->
<script src="<?php echo theme_url(); ?>js/zoomsl-3.0.min.js"></script>

<!-- FancyBox -->
<script src="<?php echo theme_url(); ?>js/jquery.fancybox.pack.js"></script>

<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="<?php echo theme_url(); ?>js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo theme_url(); ?>js/jquery.themepunch.revolution.min.js"></script>

<!-- FlexSlider -->
<script defer src="<?php echo theme_url(); ?>js/flexslider.min.js"></script>

<!-- IOS Slider -->
<script src = "<?php echo theme_url(); ?>js/jquery.iosslider.min.js"></script>

<!-- noUi Slider -->
<script src="<?php echo theme_url(); ?>js/jquery.nouislider.min.js"></script>

<!-- Owl Carousel -->
<script src="<?php echo theme_url(); ?>js/owl.carousel.min.js"></script>

<!-- Cloud Zoom -->
<script src="<?php echo theme_url(); ?>js/zoomsl-3.0.min.js"></script>

<!-- SelectJS -->
<script src="<?php echo theme_url(); ?>js/chosen.jquery.min.js" type="text/javascript"></script>

<!-- Main JS -->
<script defer src="<?php echo theme_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo theme_url(); ?>js/main-script.js"></script>
<script src="<?php echo theme_url(); ?>twitter/jquery.tweet.js" type="text/javascript"></script>
<script src="<?php echo theme_url(); ?>js/tinynav.min.js" type="text/javascript"></script>