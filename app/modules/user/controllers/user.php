<?php
class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('user/user_model');
		$this->load->library('user_agent');
	}

	public function index()
	{
		$this->load->model('home/home_model');
		$data['category'] = $this->home_model->getMenuCategory();
		$data['sub_category'] = $this->home_model->getSubCategory();
		$data['product_tags']=$this->home_model->getProductTags();
		$data['special_products']=$this->home_model->getSpecialProducts();
		$data['sub_cat'] = $this->home_model->getSubCategory();
		$data['advertisement'] = $this->home_model->getAdvertisement();
		$data['url']=$this->agent->referrer();
		$data['include'] = 'user/user';
		$this->load->view('frontend/container',$data);
	}
	public function check_login()
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		if(isset($_POST['username']) && isset($_POST['password']))
		{
			$k=$this->user_model->check_login($username,$password);
			if($k)
			{
				//echo "jhsc";exit;
				$data['res']='success';
			}
			else
			{
				//echo "djhbv";exit;
				$this->session->set_flashdata('error', 'Invalid Username / Password. Please try again.');
				redirect(base_url() . 'register/login');
				//$data['error_message_login'] = 'Invalid Username / Password. Please try again.';
				//$this->load->view('home/home',$data);
			}
			if(isset($_POST['url']))
			{
				redirect($_POST['url']);
			}
			else
			{
				if('' != $this->session->userdata('cart')){
					redirect(base_url().'reports/report/checkout');
				}else{
					redirect(base_url().'home');
				}
			}
		}
		else
		{
			$data['error_message'] = 'Invalid Username / Password. Please try again.';
			redirect($this->agent->referrer());
		}
	}
	function register()
	{
		if(isset($_POST['submit']))
		{
			$k=$this->user_model->register();
			if($k)
			{
				$data['res']='success';
				$this->load->view('user/register_success',$data);
			}
			else
			{
				$data['error_message'] = 'Error. Please Try Again.';
				$this->load->view('user/user',$data);
			}
			redirect('home/home');
		}
		else
		{
			$data['error_message'] = 'Error. Please Try Again.';
			//                redirect($this->agent->referrer());
			$this->load->view('user/register',$data);
		}

	}
	function register_success()
	{
		//echo "Successfully registered..!";exit;
		redirect('user/user/register_success');
	}

	function edit_user($id)
	{
		if(isset($_POST['submit']))
		{
			$edit = $this->user_model->edit_user();
			redirect('user/edit_user/'.$id);
		}
		else
		{
			$data['info'] = $this->user_model->getUserDetails($id);
			$this->load->model('home/home_model');
			$data['category'] = $this->home_model->getMenuCategory();
			$data['sub_category'] = $this->home_model->getSubCategory();
			$data['sub_cat'] = $this->home_model->getSubCategory();
			$data['special_products']=$this->home_model->getSpecialProducts();
			$data['sub_cat'] = $this->home_model->getSubCategory();
			$data['advertisement'] = $this->home_model->getAdvertisement();
			$data['include'] = 'user/edit_user';
			$this->load->view('frontend/container',$data);

		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
	function clear_cache()
	{
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
	}

}