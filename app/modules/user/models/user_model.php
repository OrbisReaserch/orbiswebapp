<?php
class User_Model extends CI_Model
{
    public function __construct()
    {
            parent::__construct();

    }
    public function check_login($username,$password)
    {
        $result=$this->db->query("select * from users where username='".$username."' and password='".$password."' and status='1'");
        $row=$result->result();
        if(count($row)==1)
        {
            $this->session->set_flashdata('message','You have successfully logged in');   
            $sessiondata=array('user_id'=>$row[0]->id,'email'=>$row[0]->email,'display_name'=>$row[0]->display_name);
            $this->session->set_userdata($sessiondata);           
            return true;
        }
        else
        {
            return false;
        }
    }
   
   public function register()
    {
        //print_r($_POST); exit;
        $email = $_POST['email'];
        $username = $_POST['username'];
        $display_name = $_POST['display_name'];
        $password = $_POST['password'];
        $company_name = $_POST['company_name'];
        $first_name = $_POST['first_name'];
        $middle_name = $_POST['middle_name'];
        $last_name = $_POST['last_name'];
        $address1 = $_POST['address1'];
        $address2 = $_POST['address2'];
        $zipcode = $_POST['zipcode'];
        $city = $_POST['city'];
        $country = $_POST['country'];
        $state = $_POST['state'];
        $phone = $_POST['phone'];
        $mobile = $_POST['mobile'];
        $fax = $_POST['fax'];
        $data = array('email'=>$email,'username'=>$username,'display_name'=>$display_name,'password'=>$password,'company_name'=>$company_name,
            'first_name'=>$first_name,'middle_name'=>$middle_name,'last_name'=>$last_name,'address1'=>$address1,'address2'=>$address2,'zipcode'=>$zipcode,
            'city'=>$city,'country'=>$country,'state'=>$state,'phone'=>$phone,'mobile'=>$mobile,'fax'=>$fax,'status'=>'1');
        //print_r($data);exit;
            $this->db->insert('users',$data);
            return $this->db->insert_id();
    }
    public function edit_user()
    {
        $user_id = $this->session->userdata("user_id");
        $email = $_POST['email'];
        $username = $_POST['username'];
        $display_name = $_POST['display_name'];
        $company_name = $_POST['company_name'];
        $first_name = $_POST['first_name'];
        $middle_name = $_POST['middle_name'];
        $last_name = $_POST['last_name'];
        $address1 = $_POST['address1'];
        $address2 = $_POST['address2'];
        $zipcode = $_POST['zipcode'];
        $city = $_POST['city'];
        $country = $_POST['country'];
        $state = $_POST['state'];
        $phone = $_POST['phone'];
        $mobile = $_POST['mobile'];
        $fax = $_POST['fax'];
        $data = array('email'=>$email,'username'=>$username,'display_name'=>$display_name,'company_name'=>$company_name,
            'first_name'=>$first_name,'middle_name'=>$middle_name,'last_name'=>$last_name,'address1'=>$address1,'address2'=>$address2,'zipcode'=>$zipcode,
            'city'=>$city,'country'=>$country,'state'=>$state,'phone'=>$phone,'mobile'=>$mobile,'fax'=>$fax,'status'=>'1');
            $this->db->where('id',$user_id);
            $this->db->update('users',$data);
        
    }
     public function getUserDetails($id)
    {
         $result=$this->db->query("select * from users where status='1' and id='".$id."'");
        return $result->result();
    }
    public function getUserData($id)
   {
      $q= $this->db->get_where('users',array('id'=>$id));
       $user = $q->row_array();
       if($user['is_login'] == 0 || $user['is_login'] == "0"){
           $this->db->where('id',$id);
           $this->db->update('users',array('is_login'=>1));
           redirect('user/logout');
       }
   }
}
?>