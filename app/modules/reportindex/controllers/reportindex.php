<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class reportindex extends CI_Controller
{

    function __construct ()
    {
        parent::__construct();
		$this->load->model('category/category_model');
    }

    function index()
    {
		$data['category'] = $this->category_model->getMenuCategory();
		$data['sub_category'] = $this->category_model->getSubCategory();
		$data['include'] = 'reportindex/report_index';
		$this->load->view('frontend/container',$data);
    }
	function feed($type,$id){ 
		$cond = $type."_id = ".$id;
        
         
        $this->load->library('xml_writer');
        
         
        $xml = new Xml_writer();
        $xml->setRootName('Orbis-Reports');
        $xml->initiate();
        

        
        $xml->startBranch('reports');
         
		
		$query=$this->db->query('SELECT page_urls FROM report where '.$cond.' ORDER BY id DESC;');
		 
		$u=$query->result_array();
		$i=1;
		foreach($u as $url)
	{ 		
		$xml->startBranch('url');        
		$xml->addNode('loc', base_url().'reports/index/'.$url['page_urls'],true);       
		$xml->endBranch();		
		$i++;	
	}
        
        
        
        
        $xml->endBranch();
        
     
        
        
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml', $data);
	}
}
