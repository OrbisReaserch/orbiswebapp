 <header class="main-header">
    <div class="container">
        <h1 class="page-title">Report Index </h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Report Index</li>
        </ol>
    </div>
</header>


<div class="container">
   <div class="row masonry-container">
   
    <?php 
for($i = 0; $i< sizeof($category); $i++)
				{
				$category_name =  str_replace(" ","-",strtolower($category[$i]->category_name));

					?>
                <div class="col-md-4 col-sm-4 col-xs-12 masonry-item">
                     <div class="panel panel-default sitemap">
                        <div class="panel-heading"><a href = "<?php echo base_url();?>market-reports/<?php echo $category_name ; ?>/feed" ><?php echo $category[$i]->category_name;?></a></div>
                        <div class="panel-body">
                          <ul>
					<?php	  
					for ($j = 0; $j< sizeof($sub_category); $j++)
					{
					  $sub_category_name =  str_replace(" ","-",strtolower($sub_category[$j]->sub_category_name));
						if($sub_category[$j]->category_id == $category[$i]->id)
							{
						?>
                            <li><a href="<?php echo base_url();?>market-reports/<?php echo $category_name;?>/<?php echo $sub_category_name ;?>/feed"><i class="fa fa-angle-right"></i> &nbsp; <?php echo $sub_category[$j]->sub_category_name?></a></li>
					<?php } 
					}
							?>
						  </ul>
                        </div>
                    </div>
                </div>
				<?php } ?>
                
                
                        
             
            
    </div>
</div>
