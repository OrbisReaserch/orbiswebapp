<?php
class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('home/home_model');
	}

	public function index()
	{
		
		$data['category'] = $this->home_model->getMenuCategory();
		$data['clients'] = $this->home_model->getClients();
		$data['featured'] = $this->home_model->getFeaturedReports();
		$data['news'] = $this->home_model->getNews();
		$data['press_release'] = $this->home_model->getPressrelease();
		
		$data['include'] = 'home/home';
		$this->load->view('frontend/container_home',$data);

	}
	public function discount(){

		$data['include'] = 'home/discount';
		$this->load->view('frontend/container',$data);
	}
public function requestsample(){

		$data['include'] = 'home/requestsample';
		$this->load->view('frontend/container',$data);
	}
	public function enquirybeforebuying(){

		$data['include'] = 'home/enquirybeforebuying';
		$this->load->view('frontend/container',$data);
	}

public function get_report_name()
	{
		$term=$_GET["term"];
        $query=$this->db->query("SELECT * FROM report WHERE report_name LIKE '%$term%';");             
		$a = $query->result();
		$json=array();
		for($i = 0; $i<count($a); $i++){
			$json[]=array(
                    'value'=> $a[$i]->report_name,
                    'label'=> $a[$i]->report_name
			);
		}
		echo json_encode($json);
	}

	function clear_cache()
	{
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
	}
	public function autocomplete_text(){
				//$keyword = '%'.$_POST['keyword'].'%';
				$r = $this->home_model->getAllReports();
				$json=array();
				for($i = 0; $i < count($r); $i++) {
					array_push($json,$r[$i]->report_name);
				}
				echo json_encode($json);
}
}