<style>
.newssection {
margin: 0px 0 0;
}

.newssection .demo1{
	height: 400px !important;
	}
.newssection .demo2{
	height: 400px !important;
	}
.newssection .demo3{
	height: 400px !important;
	}		
.newssection .panel-heading {
	   border-radius: 0;
    color: #333;
    font-size: 20px;
	text-align: center;
}
	
.newssection .panel-heading span {
}

.newssection .sectionbelowheading{
	border-bottom: 1px solid #e5e5e5;
	}
.newssection .sectionbelowheading a{
	font-size: 30px; color: rgba(0, 0, 0, 0.7);
	}
	
	
.newssection .panel-body {height: 428px;
}
.newssection .panel-body ul {
	padding: 0px;
	margin: 0px;
	list-style: none;
}
.newssection .panel-body ul li.news-item {
	padding: 10px 0;
	margin: 0px;
	border-bottom: 1px dotted #D6D6D6;
	height:100px;
}
.newssection .panel-body ul li.news-item img {
	margin-right: 15px;
}
.newssection .panel-body ul li.news-item p {
	margin-bottom:0px;
}
.newssection .panel-footer {
	background-color: #fff;
	padding: 0;
}
.newssection .panel-footer .pagination{
display:none;
}

.newssection .panel-footer a {
	font-size: 30px; color: rgba(0, 0, 0, 0.7);
}

</style>

      <div class="container">
	   <?php
	if($this->session->flashdata('success'))
	{
		// echo '<div style="width: 100%; border:1px solid gray;margin-top: 0%;margin-bottom: 2%;background-color: aliceblue;"> <center> <p style=" margin-top: 1%;  "> <font style="color:green; font-weight:bold;">'.$this->session->flashdata('success').'</font></p><center></center></center></div>';
    echo '<div  style="width: 80%; margin-left: 10%;  border:2px solid ;background-color: #66c5ff;  "> <center> <p> <font style="color:black; font-weight:bold; ">'.$this->session->flashdata('success').'</font></p></center></div></br>';
	}
	else if($this->session->flashdata('error'))
	{
		// echo '<div style="width: 100%; border:1px solid gray;margin-top: -2%;margin-bottom: 2%;background-color: aliceblue;"> <center> <p style=" margin-top: 1%;  "> <font style="color:green; font-weight:bold;">'.$this->session->flashdata('error').'</font></p><center></center></center></div>';
      echo '<div  style="width: 80%; margin-left: 10%;  border:2px solid ;background-color: #66c5ff;  "> <center> <p> <font style="color:black; font-weight:bold; ">'.$this->session->flashdata('error').'</font></p></center></div></br>';
	}
	?>
            <div class="row ourClient">
            
             <div class="col-md-2 col-sm-3 text-center">
                  <h3 class="section-title">Our Clients</h3>
             </div>
            
               <div class="col-md-10 col-sm-9">
                <div class="bx-wrapper" style="max-width:100%;">
                     <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: auto;">
                        <ul id="bx5" class="bxslider" style="width: 4215%; position: relative; left: -1337.55px;">
<?php
			for($j=0;$j<count($clients);$j++)
			{
		  ?>                          
						  <li style="float: left; list-style: outside none none; position: relative; width: 100%; margin-right: 10px;"> <a class="thumbnail"> <img alt="Image" src="<?php echo base_url();?>uploads/<?php echo $clients[$j]->brand_logo;?>" style = "height:70px;"> </a> </li>
                           <?php }
        ?>
                        </ul>
                     </div>
                  </div>
               </div>
               
            </div>
            <div class="row">
			<div class="col-md-4">
               <div class="panel panel-default newssection">
                  <div class="panel-heading"> Popular Categories  </div>
                  <div class="sectionbelowheading text-center"><a href="#" class="next"><span class="fa fa-angle-up"></span></a></div>
                  <div class="panel-body">
                     <div class="row">
                        <div class="col-xs-12">
                           <ul class="demo1">
						    <?php
          
							for($i=0;$i<count($category);$i++)
							{
							?>
                              <li class="news-item">
                                 <table cellpadding="4">
                                    <tr>
                                       <td><a href="<?php echo base_url();?>market-reports/<?php  $cat_name = str_replace(" ","-",strtolower($category[$i]->category_name)); echo $cat_name.'.html';?>"><img alt = "<?php echo $category[$i]->category_name; ?>" title = "<?php echo $category[$i]->category_name; ?>" src = "<?php echo base_url();?>uploads/category_icons/<?php echo $category[$i]->category_image;?>" width="65" /></a></td>
                                       <td><p><?php echo $category[$i]->category_name; ?></p>
                                          <a href="<?php echo base_url();?>market-reports/<?php  $cat_name = str_replace(" ","-",strtolower($category[$i]->category_name)); echo $cat_name.'.html';?>">View reports</a></td>
                                    </tr>
                                 </table>
                              </li>
                          <?php } ?>           
                               
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="panel-footer text-center"> <a href="#" class="prev"><span class="fa fa-angle-down"></span></a></div>
               </div>
            </div>
                        
						 <div class="col-md-4">
               <div class="panel panel-default newssection">
                  <div class="panel-heading">Featured Research Reports</div>
                    <div class="sectionbelowheading text-center"><a href="#" class="next"><span class="fa fa-angle-up"></span></a></div>
                  <div class="panel-body">
                     <div class="row">
                        <div class="col-xs-12">
                           <ul class="demo2">
                             
<?php
         
				for($i=0;$i<count($featured);$i++)
				{ 
				?>
							 <li class="news-item">
                                 <table cellpadding="4">
                                    <tr>
                                       <td><a href="<?php echo base_url();?>reports/index/<?php echo $featured[$i]->page_urls; ?>"><img alt = "<?php echo $featured[$i]->report_name; ?>" src="<?php echo base_url();?>uploads/category_report_image/<?php echo $featured[$i]->category_id.".jpg"; ?>" width="65"/></a></td>
                                       <td><p><?php echo substr($featured[$i]->report_name,0,60); ?></p>
                                          <a href="<?php echo base_url();?>reports/index/<?php echo $featured[$i]->page_urls; ?>">Read more...</a></td>
                                    </tr>
                                 </table>
                              </li>
                              <?php }
        ?>       
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="panel-footer text-center"> <a href="#" class="prev"><span class="fa fa-angle-down"></span></a></div>
               </div>
            </div>
				
                      <div class="col-md-4">
               <div class="panel panel-default newssection">
                  <div class="panel-heading"> Latest News </div>
                    <div class="sectionbelowheading text-center"><a href="#" class="next"><span class="fa fa-angle-up"></span></a></div>
                  <div class="panel-body">
                     <div class="row">
                        <div class="col-xs-12">
                           <ul class="demo3">
						     <?php
         
				for($i=0;$i<count($news);$i++)
				{
					$news_name =  str_replace(" ","-",strtolower($news[$i]->news_title));
				?>
                              <li class="news-item">
                                 <table cellpadding="4">
                                    <tr>
                                       <td><a href="<?php echo base_url(); ?>news/<?php echo $news_name; ?>"><img style = "height: 80px;" src="<?php echo base_url();?>uploads/news_image/<?php echo $news[$i]->news_image;?>" alt="<?php echo $news[$i]->news_title; ?>" title = "<?php echo $news[$i]->news_title; ?>" width="65" /></a></td>
                                       <td><p><?php echo $news[$i]->news_title; ?></p>
                                          <a href="<?php echo base_url(); ?>news/<?php echo $news_name; ?>">Read more...</a></td>
                                    </tr>
                                 </table>
                              </li>
                                <?php }
        ?>       
                           </ul>
                        </div>
                     </div>
                  </div>
                    <div class="panel-footer text-center"> <a href="#" class="prev"><span class="fa fa-angle-down"></span></a></div>
               </div>
            </div>
							 
                      
                       
               
            </div>
            
            
            
             <h3 class="section-title">Press Release</h3>
			  <?php
         
				for($k=0;$k<count($press_release);$k++)
				{
					 $news_name =  str_replace(" ","-",strtolower($press_release[$k]->news_title));
					 $news_name_2 =  str_replace(",","-",strtolower($news_name));
				?>
             <div class="row pressRelease">
                            <div class="col-lg-3 col-sm-4">
                              <div class="pressReleaseImg">
                                <img src="<?php echo base_url();?>uploads/news_image/<?php echo $press_release[$k]->news_image;?>" class="img-post img-responsive" alt="Image">
                              </div>
                            </div>
                            <div class="col-lg-9 col-sm-8 post-content">
                                <div class="pressReleaseContent">
                                  <h4><a href="<?php echo base_url().'press-release/'.$news_name_2; ?>"><?php echo $press_release[$k]->news_title; ?></a></h4>
                                  <p><i class="fa fa-calendar"></i> &nbsp; <?php echo date("d-M-Y", strtotime($press_release[$k]->news_date));?></p>
                                  <p><?php echo substr($press_release[$k]->news_description,0,350);  ?></p>
                                  <p><a href="<?php echo base_url().'press-release/'.$news_name_2; ?>">Read More...</a></p>
                                 </div>
                                
                            </div>
             </div>
   <?php
   }
        ?>       
      </div>
      
      
      
      
        

      