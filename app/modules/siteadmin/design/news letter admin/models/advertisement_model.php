<?php 
class Advertisement_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		
	}	
 	
	
	 public function getall()
	{
		$query = $this->db->get('advertisement');
		
		return $query->result();
		
	}
	
	public function add_advertisement()
	{
		$advertisement_title = $_POST['advertisement_title'];
		//$brand_description=$_POST['brand_description'];
		$data = array('advertisement_title'=>$advertisement_title,'status'=>'1');
		$this->db->insert('advertisement',$data);
		return $this->db->insert_id();
	}
        
	public function upload_advertisement_image($id,$filename)
	{
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
                {
                        $data = array('advertisement_image'=>$filename); 
                        $this->db->where('id',$id);
                        $this->db->update('advertisement',$data);
                        $returnmsg=true;
                }
		return $returnmsg;
	}
	
        public function edit_advertisement()
	{
		$advertisement_title = $_POST['advertisement_title'];
		//$advertisement_image=$_POST['advertisement_image'];
             //   $brand_description=$_POST['brand_description'];
                $advertisement_id=$_POST['advertisement_id'];
		
		$data = array('advertisement_title'=>$advertisement_title);
		$this->db->where('id',$advertisement_id);
		$this->db->update('advertisement',$data);
		//$this->db->insert('category',$data);
		return TRUE;
		
	}
public function update_advertisement_image($id,$filename)
	{
		 $advertisement_id=$_POST['advertisement_id'];
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
                {
                        $data = array('advertisement_image'=>$filename); 
                        $this->db->where('id',$advertisement_id);
                        $this->db->update('advertisement',$data);
                        $returnmsg=true;
                }
		return $returnmsg;
	}
        
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('advertisement');
		return true;
	
	}
	
	
	public function getRecord($id)
	{
           
		$q= $this->db->get_where('advertisement',array('id'=>$id));
               
		return $q->row_array();
	}
       
	public function activate_advertisement($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('advertisement',$data);	
	}
        

	public function deactivate_advertisement($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('advertisement',$data);	
	}
        
       
	public function getAllAdvertisement()
        {
            $result=$this->db->query("select * from advertisement where status='1'");
            return $result->result();
        }
        
}