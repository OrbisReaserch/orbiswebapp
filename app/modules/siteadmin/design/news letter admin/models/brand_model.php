<?php 
class Brand_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		
	}	
 	
	
	 public function getall()
	{
		$query = $this->db->get('brand');
		
		return $query->result();
		
	}
	
	public function add_brand()
	{
		$brandname = $_POST['brand_name'];
		$brand_description=$_POST['brand_description'];
		$data = array('brand_name'=>$brandname,'brand_description'=>$brand_description,'status'=>'1');
		$this->db->insert('brand',$data);
		return $this->db->insert_id();
	}
        
	public function upload_brand_image($id,$filename)
	{
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
                {
                        $data = array('brand_logo'=>$filename); 
                        $this->db->where('id',$id);
                        $this->db->update('brand',$data);
                        $returnmsg=true;
                }
		return $returnmsg;
	}
	
        public function edit_brand()
	{
		$brandname = $_POST['brand_name'];
                $brand_description=$_POST['brand_description'];
                $brand_id=$_POST['brand_id'];
		
		$data = array('brand_name'=>$brandname,'brand_description'=>$brand_description,'status'=>'1');
		$this->db->where('id',$brand_id);
		$this->db->update('brand',$data);
		//$this->db->insert('category',$data);
		return TRUE;
		
	}
        
        public function upload_edit_brand_image($id,$filename)
	{
            	$returnmsg=false;
		if($filename!="" && $filename!=NULL)
                {
                        $data = array('brand_logo'=>$filename); 
                        $this->db->where('id',$id);
                        $this->db->update('brand',$data);
                        $returnmsg=true;
                }
		return $returnmsg;
	}
        
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('brand');
		return true;
	
	}
	
	
	public function getRecord($id)
	{
           
		$q= $this->db->get_where('brand',array('id'=>$id));
               
		return $q->row_array();
	}
       
	public function activate_brand($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('brand',$data);	
	}
        

	public function deactivate_brand($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('brand',$data);	
	}
        
       
	public function getAllBrand()
        {
            $result=$this->db->query("select * from brand where status='1'");
            return $result->result();
        }
        
}