<?php
class contact_info_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();

	}


	public function getall()
	{
		//$query = $this->db->get('contact_us');
		$query=$this->db->query("select * from contact_info order by id desc");
		return $query->result();
	}

	public function add()
	{
		$address = $_POST['address'];
		$email=$_POST['email'];
		$phone=$_POST['phone'];
		$skype=$_POST['skype'];
		$twitter=$_POST['twitter'];
		$facebook=$_POST['facebook'];

		$data = array('address'=>$address,'email'=>$email,'phone'=>$phone,'skype'=>$skype,'twitter'=>$twitter,'facebook'=>$facebook);
		$this->db->insert('contact_info',$data);
		return $this->db->insert_id();
	}



	public function edit()
	{
		$address = $_POST['address'];
		$email=$_POST['email'];
		$phone=$_POST['phone'];
		$skype=$_POST['skype'];
		$twitter=$_POST['twitter'];
		$facebook=$_POST['facebook'];
		$contact_id  = $_POST['contact_id'];

		$data = array('address'=>$address,'email'=>$email,'phone'=>$phone,'skype'=>$skype,'twitter'=>$twitter,'facebook'=>$facebook);
//		print_r($data); die();
		$this->db->where('id',$contact_id);
		$this->db->update('contact_info',$data);
		//$this->db->insert('category',$data);
		return TRUE;

	}
	public function update_report_image($id,$filename)
	{
		$report_id=$_POST['report_id'];
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
		{
			$data = array('report_image'=>$filename);
			$this->db->where('id',$report_id);
			$this->db->update('report',$data);
			$returnmsg=true;
		}
		return $returnmsg;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('advertisement');
		return true;

	}


	public function getcategory($cat_id)
	{
		$que_category = $this->db->get_where('category',array('id'=>$cat_id));
		return $que_category->row_array();
	}
	public function getsubcategory($subcat)
	{
		$que_subcat=$this->db->get_where('sub_category',array('id'=>$subcat));
		return $que_subcat->row_array();
	}

	public function getvendor($vid)
	{
		$que_vendor = $this->db->get_where('users',array('id'=>$vid));
		return $que_vendor->row_array();
	}

	public function allcategory()
	{
		$cat=$this->db->query("select * from category where status='1'");
		return $cat->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}
	public function allsubcategory()
	{
		$cat=$this->db->query("select * from sub_category where status='1'");
		return $cat->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}


	public function allvendor()
	{
		$ven=$this->db->query("select * from users where status='1' and user_type='Publisher'");
		return $ven->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}

	public function allcountry()
	{
		$ven=$this->db->query("select * from country where status='1'");
		return $ven->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}


	public function deactivate($id)
	{
		/*	$q = $this->db->query("update report set status=0 where id='$rid'");
		 return $q->result(); */

		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('contact_info',$data);
	}
	public function activate($aid)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$aid);
		$this->db->update('contact_info',$data);
	}


	public function getRecord($id)
	{
		$q= $this->db->query("select * from contact_info where id='".$id."'");
		return $q->row_array();
	}

	public function getAllAdvertisement()
	{
		$result=$this->db->query("select * from advertisement where status='1'");
		return $result->result();
	}


}