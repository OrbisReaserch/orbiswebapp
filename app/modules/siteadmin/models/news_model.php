<?php 
class News_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		
	}	
 	
	
	 public function getall()
	{
		$query = $this->db->get('news');
		
		return $query->result();
		
	}
	
	public function add_news()
	{
		$news_title = $_POST['news_title'];
		$news_description=$_POST['news_description'];
		$meta_title=$_POST['meta_title'];
		$page_title=$_POST['page_title'];
		$meta_desc=$_POST['meta_desc'];
		$meta_tag='';
		$news_type=$_POST['news_type'];	
		$news_date=$_POST['news_date'];		
		$alt_tag=$_POST['alt_tag'];		
		$data = array('news_title'=>$news_title,
		'news_description'=>$news_description,
		'meta_title'=>$meta_title,
		'page_title'=>$page_title,
		'news_type'=>$news_type,
		'meta_desc'=>$meta_desc,
		'meta_tag'=>$meta_tag,
		'news_date'=>$news_date,
		'alt_tag' => $alt_tag,
		'status'=>'1');
            	
            	
            	$news_name =  str_replace(" ","-",strtolower($news_title));
            	$url = base_url().'news/'.$news_name;
            	CreateSitemap("newssitemap.xml",$url);
            	
            	
		$this->db->insert('news',$data);
		return $this->db->insert_id();
	}
        
	public function upload_news_image($id,$filename)
	{
            $returnmsg=false;
            if($filename!="" && $filename!=NULL)
            {
                    $data = array('news_image'=>$filename);
                    $this->db->where('id',$id);
                    $this->db->update('news',$data);
                    $returnmsg=true;
            }
            return $returnmsg;
	}
	
        public function edit_news()
	{
		$news_title = $_POST['news_title'];
		$news_description=$_POST['news_description'];
		$meta_title=$_POST['meta_title'];
		$page_title=$_POST['page_title'];
		$news_type=$_POST['news_type'];	
		$meta_desc=$_POST['meta_desc'];
		$meta_tag='';
		$news_date=$_POST['news_date'];
                $news_id=$_POST['news_id'];$alt_tag=$_POST['alt_tag'];
		
		$data = array(
		'news_title'=>$news_title,
		'news_description'=>$news_description,
		'news_date'=>$news_date,
		'meta_title'=>$meta_title,
		'page_title'=>$page_title,
		'news_type'=>$news_type,
		'meta_desc'=>$meta_desc,
		'meta_tag'=>$meta_tag,
		'alt_tag' => $alt_tag,
		'status'=>'1');
		$this->db->where('id',$news_id);
		$this->db->update('news',$data);
		
		return TRUE;
		
	}
        public function upload_news_image_update($id,$filename)
	{
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
                {
                        $data = array('news_image'=>$filename);
                        $this->db->where('id',$id);
                        $this->db->update('news',$data);
                        $returnmsg=true;
                }
		return $returnmsg;
	}
        
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('news');
		return true;
	
	}
	
	
	public function getRecord($id)
	{
           
		$q= $this->db->get_where('news',array('id'=>$id));
               
		return $q->row_array();
	}
       
	public function activate_news($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('news',$data);	
	}
        

	public function deactivate_news($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('news',$data);	
	}
        
       
	public function getAllNews()
        {
            $result=$this->db->query("select * from news where status='1' and news_type = 'news';");
            return $result->result();
        }
        
}