<?php
class Report_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();
		
		require_once 'PHPExcel.php';
		
	} 


	public function getall()
	{
		$query = $this->db->query('SELECT * FROM report ORDER BY id DESC LIMIT 0,200');
		return $query->result();
	}
	public function getReportsIdandUrl($limit)
	{
		$query = $this->db->query("SELECT id,page_urls FROM report ORDER BY id DESC LIMIT ".$limit);
		return $query->result();
	}
	public function getReportsDetailsFromId($from,$to)
	{
		$query = $this->db->query("SELECT id,page_urls FROM report where id between '".$from."' and '".$to."' ORDER BY id DESC ");
		return $query->result();
	}
	public function generate_pdf($report_id){
		error_reporting(E_ALL);
		
		require_once('PDF_CON_Astha/config/lang/eng.php');
        require_once('PDF_CON_Astha/tcpdf.php');

        ini_set('max_execution_time', 500);
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('ORBIS');
        $pdf->SetTitle('ORBIS');
        $pdf->SetSubject('ORBIS');
        $pdf->SetKeywords('TCPDF, PDF, ORBIS, ORBIS, ORBIS');

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 10);

        $pdf->AddPage();
		$data = $this->getRecord($report_id);
$aramexhtml = '<div style="">
<p>
<img src="http://www.orbisresearch.com/themes/backend/images/logo.png" alt="">
</p>
<p style = "text-align:center;font-size: x-large;color: blue;"><span style="font-weight: 400;">'.$data['report_name'].'</span></p>
<p style = "text-align:center;"><img height = "400" src="http://www.orbisresearch.com/uploads/category_report_image/'.$data['category_id'].'.jpg"/></p>
<div style="page-break-before: always;"></div>

<p><strong>No of Report Pages &ndash; </strong><span style="font-weight: 400;">'.$data['no_pages'].'</span></p>
<p><strong>Publishing Date &ndash; </strong><span style="font-weight: 400;">'.date("M d Y", strtotime($data['report_date'])).'</span></p>
<p><strong>Browse Complete Report Details at:</strong><span style = "color: blue;">'.base_url().'reports/index/'.$data['page_urls'].'</span></p>
<p><strong>Report Description</strong></p>
<p><span style="font-weight: 400;">'.str_replace('\n', '', $data['report_description']).'</span></p>
<p><strong>Get Discount on Report Purchase at: </strong><strong><span style = "color: blue;">'.base_url().'contacts/discount/'.$data['id'].'</span></strong></p>
<p><strong>Table of Contents:</strong></p>
<p><span style="font-weight: 400;">'.str_replace('\n', '', $data['report_content']).'</span></p> 
<br>
<strong>List Of Tables:</strong>
                    <p>';
                    
					$a = str_replace("<h1>","<h2>",$data['list_of_tables']);
                    $b = str_replace("</h1>","</h2>",$a);
                    $aramexhtml .= str_replace('\n', '', $b);
					
					$aramexhtml .= '</p>
<p><strong>For Enquiry and Customization of Report, Contact: </strong><span style = "color: blue;">'.base_url().'contacts/enquiry-before-buying/'.$data['id'].'</span></p>
<p><strong>Report Details &ndash;</strong></p>
<p><strong>Single User PDF License &ndash; </strong><strong>$'.$data['report_price'].'</strong></p>
<p><strong>Corporate License &ndash; </strong><strong>$'.$data['corporate_price'].'</strong></p>
<p><strong>Buy this Report Now at: </strong><span style = "color: blue;">'.base_url().'contact/purchase/'.$data['id'].'</span></p>
<p>&nbsp;</p>
<p><strong>About Us:</strong></p>
<p><strong>Orbis Research</strong><span style="font-weight: 400;"> is a single point aid for all your Market research requirements. We have vast database of reports from the leading publishers and authors across the globe. We specialize in delivering customised reports as per the requirements of our clients. We have complete information about our publishers and hence are sure about the accuracy of the industries and verticals of their specialisation. This helps our clients to map their needs and we produce the perfect required Market research study for our clients.</span></p>
<p><strong>Contact Us:</strong><strong><br /></strong><span style="font-weight: 400;">Hector Costello </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Senior Manager &ndash; Client Engagements</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">4144N Central Expressway, </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Suite 600, Dallas, </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Texas - 75204, U.S.A. </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Phone No.: +1 (214) 884-6817; +912064101019</span></p>
<p><strong>Email ID: </strong><a href="mailto:sales@orbisresearch.com"><strong>sales@orbisresearch.com</strong><strong><br /></strong></a><strong>Follow Us on LinkedIn: </strong><a href="https://www.linkedin.com/company/orbis-research"><span style="font-weight: 400;">https://www.linkedin.com/company/orbis-research</span></a></p>
</div>
';
      
	  $pdf->writeHTML($aramexhtml, true, false, true, false, '');
        $report_pdf = $data['report_name']."-" . time() . ".pdf";
        $pdf->Output('./uploads/report_pdf/' .$report_pdf, 'F');
		$label = base_url().'uploads/report_pdf/'.$report_pdf;
        header('location:'.$label);
		 
	}

	public function add_reports()
	{
		 $featured= $_POST['featured'];
		 if($featured==on)
		 {
		  $featured=1;
		 }
		 else
		 {
		   $featured=0;
		 }
		$report_title = $_POST['report_title'];
		$category=$_POST['category'];
		$subcategory=$_POST['subcategory'];
		$publisher_id = $_POST['vendor'];
		$report_summ= mysql_real_escape_string($_POST['report_summ']);
		$report_dec= mysql_real_escape_string($_POST['report_dec']);
		$report_content = mysql_real_escape_string($_POST['report_content']);
		$pub_day=$_POST['day'];
		$pub_month=$_POST['month'];
		$pub_year=$_POST['year'];
		$published_date=$pub_year.'-'.$pub_month.'-'.$pub_day;
		$list_of_tables= mysql_real_escape_string($_POST['list_of_tables']);
		$metatitle= mysql_real_escape_string($_POST['metatitle']);
		$meta_title= mysql_real_escape_string($_POST['meta_title']);
		$page_title= mysql_real_escape_string($_POST['page_title']);
		if($_POST['country_id'] == '0'){
					$country_id = $_POST['continent_id']; 
		}elseif($_POST['continent_id'] == '0'){
					$country_id = $_POST['country_id']; 
		}
		$report_status=$_POST['report_status'];
		$no_pages=$_POST['no_pages'];
	 	$user[] = $this->get_publisher($publisher_id);
		$first_name = $user[0][0]->display_name;
		$report_code_prefix =  strtoupper(substr($first_name, 0,2));
		//$maxdata = $this->get_max_code();
		$report_str =  str_pad(($maxdata['max'] + 1), 4, '0', STR_PAD_LEFT);
		$report_code = $report_code_prefix.$report_str;
		$single_price=$_POST['single_price'];
		$multiple_price=$_POST['multiple_price'];
		$global_price=$_POST['global_price'];
		$corporate_price=$_POST['corporate_price'];
		$d=date('y-m-d');
		$y=date("Y ", strtotime($d));
		$qy_meta_title = $_POST['qy_meta_title'];
		$qy_meta_desc = str_replace(["_x000D_", "\n"],"<br>", $_POST['qy_meta_desc']);
		$qy_meta_keyword = $_POST['qy_meta_keyword'];
		$qy_report_name = $_POST['qy_report_name'];
		$qy_page_urls = strtolower($this->clean($qy_meta_title));
		$search_keywords = $_POST['search_keywords'];
		$data = array('report_name'=>$report_title,'report_code'=>$report_code,'category_id'=>$category,'sub_category_id'=>$subcategory,'publisher_id'=>$publisher_id,'report_summery'=>$report_summ,'report_description'=>$report_dec,'report_content'=>$report_content,'report_price'=>$single_price,'multiple_price'=>$multiple_price,'global_price'=>$global_price,'corporate_price'=>$corporate_price,'report_date'=>$published_date,'report_year'=>$y,'featured_report'=>'0','metatitle'=>$metatitle,'meta_title'=>$meta_title,'page_title'=>$page_title,'report_status'=>$report_status,'list_of_tables'=>$list_of_tables,'status'=>'1','country_id'=>$country_id,'no_pages'=>$no_pages,'qy_meta_title'=>$qy_meta_title,'qy_meta_desc'=>$qy_meta_desc,'qy_meta_keyword'=>$qy_meta_keyword,'qy_report_name'=>$qy_report_name,'qy_page_urls'=>$qy_page_urls,'search_keywords'=>$search_keywords);

		$this->db->insert('report',$data);
		$reid= $this->db->insert_id();
		$abc = $this->clean($report_title);
		$data = array('page_urls'=>strtolower($abc));
		$this->db->where('id',$reid);
		$this->db->update('report',$data);
		return $reid;
	}
		function clean($string) {
		   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
			 $string = str_replace('--', '-', $string);
			 
		   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		}
	public function upload_report_image($id,$filename)
	{
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
		{
			$data = array('report_image'=>$filename);
			$this->db->where('id',$id);
			$this->db->update('report',$data);
			$returnmsg=true;
		}
		return $returnmsg;
	}

	public function get_publisher($publisher_id)
	{
		$query = $this->db->query("SELECT * FROM `users` where id='".$publisher_id."'");

		return $query->result();

	}

	public function getallpublishers()
	{
		$query = $this->db->query("SELECT * FROM `users` where user_type = 'Publisher'");

		return $query->result();

	}
	public function getallcountries()
	{
		$query = $this->db->query("SELECT * FROM `country`;");

		return $query->result();

	}

	/*public function get_max_code()
	{
		$a= $this->db->query("SELECT MAX(SUBSTR( report_code , 3, 6 ) ) as max FROM `report`");
		return $a->row_array();
	}

		public function upload_advertisement_image($id,$filename)
	 {
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
		{
		$data = array('advertisement_image'=>$filename);
		$this->db->where('id',$id);
		$this->db->update('advertisement',$data);
		$returnmsg=true;
		}
		return $returnmsg;
		}*/

	public function edit_report()
	{
		 $featured= $_POST['featured'];
		 if($featured==on)
		 {
		  $featured=1;
		 }
		 else
		 {
		   $featured=0;
		 }
		$report_title = $_POST['report_title'];
		$category=$_POST['category'];
		$subcategory=$_POST['subcategory'];
		$vendor=$_POST['vendor'];
		$report_summ= mysql_real_escape_string($_POST['report_summ']);
		$report_dec= mysql_real_escape_string($_POST['report_dec']);
		$report_content= mysql_real_escape_string($_POST['report_content']);
		$price=$_POST['price'];
		$report_id=$_POST['report_id'];
		$list_of_tables= mysql_real_escape_string($_POST['list_of_tables']);
		$metatitle= mysql_real_escape_string($_POST['metatitle']);
		$report_status=$_POST['report_status'];
		if($_POST['country_id'] == '0'){
					$country_id = $_POST['continent_id']; 
		}elseif($_POST['continent_id'] == '0'){
					$country_id = $_POST['country_id']; 
		}
		$no_pages=$_POST['no_pages'];
	 	$pub_day=$_POST['day'];
		$pub_month=$_POST['month'];
		$pub_year=$_POST['year'];
		$published_date=$pub_year.'-'.$pub_month.'-'.$pub_day;
		$single_price=$_POST['single_price'];
		$multiple_price=$_POST['multiple_price'];
		$global_price=$_POST['global_price'];
		$corporate_price=$_POST['corporate_price'];
		$meta_title= mysql_real_escape_string($_POST['meta_title']);
		$page_title= mysql_real_escape_string($_POST['page_title']);
		$qy_meta_title = $_POST['qy_meta_title'];
		$qy_meta_desc = str_replace(["_x000D_", "\n"],"<br>", $_POST['qy_meta_desc']);
		$qy_meta_keyword = $_POST['qy_meta_keyword'];
		$qy_report_name = $_POST['qy_report_name'];
		$search_keywords = $_POST['search_keywords'];
		$data = array('report_name'=>$report_title,'category_id'=>$category,'sub_category_id'=>$subcategory,'publisher_id'=>$vendor,'report_date'=>$published_date,'report_summery'=>$report_summ,'report_description'=>$report_dec,'report_content'=>$report_content,'report_price'=>$single_price,'multiple_price'=>$multiple_price,'global_price'=>$global_price,'corporate_price'=>$corporate_price,'list_of_tables'=>$list_of_tables,'country_id'=>$country_id,'metatitle'=>$metatitle,'meta_title'=>$meta_title,'page_title'=>$page_title,'report_status'=>$report_status,'no_pages'=>$no_pages,'qy_meta_title'=>$qy_meta_title,'qy_meta_desc'=>$qy_meta_desc,'qy_meta_keyword'=>$qy_meta_keyword,'qy_report_name'=>$qy_report_name,'search_keywords'=>$search_keywords);
		$this->db->where('id',$report_id);
		$this->db->update('report',$data);
		return TRUE;

	}
	public function update_report_image($id,$filename)
	{
		$report_id=$_POST['report_id'];
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
		{
			$data = array('report_image'=>$filename);
			$this->db->where('id',$report_id);
			$this->db->update('report',$data);
			$returnmsg=true;
		}
		return $returnmsg;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('advertisement');
		return true;

	}


	public function getcategory($cat_id)
	{
		$que_category = $this->db->get_where('category',array('id'=>$cat_id));
		return $que_category->row_array();
	}
	public function getsubcategory($subcat)
	{
		$que_subcat=$this->db->get_where('sub_category',array('id'=>$subcat));
		return $que_subcat->row_array();
	}

	public function getvendor($vid)
	{
		$que_vendor = $this->db->get_where('users',array('id'=>$vid));
		return $que_vendor->row_array();
	}

	public function allcategory()
	{
		$cat=$this->db->query("select * from category where status='1'");
		return $cat->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}
	public function allsubcategory()
	{
		$cat=$this->db->query("select * from sub_category where status='1'");
		return $cat->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}
	
	public function subcategory_by_id($id)
	{
	
	$cat=$this->db->query("select * from sub_category where category_id = '$id' and status='1'");
		return $cat->result();
	
	}
public function getReportsOfPublisher($id)
	{
		$report=$this->db->query("select * from  report where publisher_id = '".$id."' AND status='1'");
		return $report->result();
	}

	public function allvendor()
	{
		$ven=$this->db->query("select * from users where status='1' and user_type='Publisher'");
		return $ven->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}

	public function allcountry($isContinent)
	{
		$ven=$this->db->query("select * from country where status='1' AND isContinent = '".$isContinent."';");
		return $ven->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}
public function GetCountryById($country_id)
	{
		$ven=$this->db->query("select country_name from country where id = '".$country_id."'");
		$data = $ven->row_array();
		$countryName = "";
		if(empty($data)){
			$countryName = "Global";
		}else{
			$countryName = $data['country_name'];
		}
		 return $countryName;
	}

	public function delete_report($id)
	{
		/*	$q = $this->db->query("update report set status=0 where id='$rid'");
		 return $q->result(); */

		 $this->db->where('id',$id);
		$this->db->delete('report');
		//return true;
	}
	public function request_delete_report($aid)
	{
		$data = array('is_requested_for_delete'=>'1');
		$this->db->where('id',$aid);
		$this->db->update('report',$data);
	}
	public function cancel_request_delete_report($aid)
	{
		$data = array('is_requested_for_delete'=>'0');
		$this->db->where('id',$aid);
		$this->db->update('report',$data);
	}

	public function activate_report($aid)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$aid);
		$this->db->update('report',$data);
	}

	public function notfeatured_report($id)
	{
	// echo "update report set featured_report = '0' where id='$id'";  die(); 
		$q = $this->db->query("update report set featured_report = '0' where id='$id'");
		 return true;

		// $data = array('featured_report'=>'0');
		// $this->db->where('id',$id);
		// $this->db->update('report',$data);
	}

	public function featured_report($aid)
	{
	// echo "update report set featured_report='1' where id='$aid'"; die();
	$q = $this->db->query("update report set featured_report='1' where id='$aid'");
		 return true;
	}


	public function getRecord($id)
	{
		 
		$q= $this->db->query("select * from report where id='".$id."'");
		return $q->row_array();
	}

	public function getAllAdvertisement()
	{
		$result=$this->db->query("select * from advertisement where status='1'");
		return $result->result();
	}

	public function bulkUpload($file)
	{
		$categories = $this->allcategory();
		$subcategories = $this->allsubcategory();
		$publishers = $this->getallpublishers();
		$countries = $this->getallcountries();
		
		$inputFileType = PHPExcel_IOFactory::identify($file);

        $objReader = PHPExcel_IOFactory::createReader($inputFileType);  

        $objReader->setReadDataOnly(true);

        /**  Load $inputFileName to a PHPExcel Object  **/  
        $objPHPExcel = $objReader->load($file);

        $total_sheets=$objPHPExcel->getSheetCount(); 

        $allSheetName=$objPHPExcel->getSheetNames(); 
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0); 
        $highestRow = $objWorksheet->getHighestRow(); 
        $highestColumn = $objWorksheet->getHighestColumn();  
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);  
        $existreport_name=array();
        $totalcount=0;
        $validcount=0;
        $invalidcount=0;

        function fromExcelToLinux($excel_time) {
        	return date("y-m-d", (($excel_time-25569)*86400));
        }

        for ($row = 2; $row <= $highestRow; $row++) {   

        	$date = date('y-m-d');	
			$report_name = str_replace("_x000D_","",$objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
			$no_pages = str_replace("_x000D_","",$objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
			$report_price = str_replace("$","",$objWorksheet->getCellByColumnAndRow(2, $row)->getValue());  
			$multiple_price = str_replace("$","",$objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
			$global_price = str_replace("$","",$objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
			$corporate_price = str_replace("$","",$objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
			$report_date = str_replace("_x000D_","",$objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
			//This code commented by pravin 26-11-2019
			//$report_date = fromExcelToLinux($report_date);

			//this code added by pravin 16-11-2019
			$report_date = str_replace('/', '-', $report_date);
			$report_date = date('Y-m-d',strtotime($report_date));
			//this code end
			$report_description = str_replace(["_x000D_", "\n"],"<br>",$objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
			$report_content = str_replace(["_x000D_", "\n"],"<br>",$objWorksheet->getCellByColumnAndRow(8, $row)->getValue());
			$list_of_tables = str_replace(["_x000D_", "\n"],"<br>",$objWorksheet->getCellByColumnAndRow(9, $row)->getValue());
			
			 
			$categoryName = str_replace("_x000D_","",$objWorksheet->getCellByColumnAndRow(10, $row)->getValue());
			$subcategoryName = str_replace("_x000D_","",$objWorksheet->getCellByColumnAndRow(11, $row)->getValue());
			$publisherName = str_replace("_x000D_","",$objWorksheet->getCellByColumnAndRow(12, $row)->getValue());
			$countryName = str_replace("_x000D_","",$objWorksheet->getCellByColumnAndRow(13, $row)->getValue());
			$meta_title = str_replace("_x000D_","",$objWorksheet->getCellByColumnAndRow(15, $row)->getValue());//meta_title
			$metatitle = str_replace("_x000D_","",$objWorksheet->getCellByColumnAndRow(16, $row)->getValue());//keywords
			$report_summery = str_replace(["_x000D_", "\n"],"<br>",$objWorksheet->getCellByColumnAndRow(17, $row)->getValue());//desc
			$page_title = str_replace("_x000D_","",$objWorksheet->getCellByColumnAndRow(18, $row)->getValue());//page_title
			$search_keywords = str_replace("_x000D_","",$objWorksheet->getCellByColumnAndRow(19, $row)->getValue());

			for($m = 0; $m < count($categories); $m++) {
				if($categories[$m]->category_name == $categoryName) {
					$category_id = $categories[$m]->id;
					break;
				} else {
					$category_id = "1";
				}					 
			}
			
			for($n = 0; $n < count($subcategories); $n++) {
				if($subcategories[$n]->sub_category_name == $subcategoryName) {
					$sub_category_id = $subcategories[$n]->id;
					break;
				} else {
					$sub_category_id = "1";
				} 
			}

			for($o = 0; $o < count($publishers); $o++) {
				if($publishers[$o]->display_name == $publisherName) {
					$publisher_id = $publishers[$o]->id;
					 break;
				} else {
					$publisher_id = "1";
				}					 
			}

			for($p = 0; $p < count($countries); $p++) {
				if($countries[$p]->country_name == $countryName) {
					$country_id = $countries[$p]->id;
					 break;
				} else {
					$country_id = "261";
				}	 
			}
										 
			//$report_newdate = explode("-",$report_date); /*MM-dd-yyyy*/
			//$report_date = $report_newdate[2]."-".$report_newdate[0]."-".$report_newdate[1];
			 
			// $report_code_prefix =  strtoupper(substr($report_name, 0,2));
			// $maxdata = $this->get_max_code();
			// $cnt = $maxdata['max'];
			// $report_str =  str_pad(($cnt + 1), 4, '0', STR_PAD_LEFT);
			
			// if(strpos($report_code_prefix,' ') !== false) {
			// 	$report_code = "AB".$report_str;
			// } else {
			// 	$report_code = $report_code_prefix.$report_str;
			// }

			$abc =  $this->clean($report_name);
							  
			$data = array(
				'report_name'=>$report_name,
				'report_code'=>'',
				'category_id'=>$category_id,
				'sub_category_id'=>$sub_category_id,
				'publisher_id'=>$publisher_id,
				'country_id'=>$country_id,
				'no_pages'=>$no_pages,
				'report_price'=>$report_price,
				'multiple_price'=>$multiple_price,
				'global_price'=>$global_price,
				'corporate_price'=>$corporate_price,
				'report_date'=>$report_date,
				'report_status'=>'1',
				'list_of_tables'=>$list_of_tables,
				'report_content'=>$report_content,
				'report_description'=>$report_description,
				'meta_title'=>$meta_title,
				'page_title'=>$page_title,
				'metatitle'=>$metatitle,
				'report_summery'=>$report_summery,
				'page_urls'=>strtolower($abc),
				'search_keywords'=>$search_keywords,
				'featured_report'=>'0',
				'status'=>'1'
			);
		
			if($report_name  != ""){
				$check_title_name_exist=$this->report_model->checkReportNameExist($report_name);
				if($check_title_name_exist==0){
					$this->db->insert('report',$data);
					$validcount++;
				}else{
					$existreport_name[]=$report_name;
					$invalidcount++;
				}
			
			}
			$totalcount++;	
		}
		$existreport_name['success']=true;
		$existreport_name['totalcount']=$totalcount;
		$existreport_name['validcount']=$validcount;
		$existreport_name['invalidcount']=$invalidcount;	
		return $existreport_name;
	}

		public function bulkDownload($result){
		
		//ini_set('max_execution_time', 0);
		$requests  = $result;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                        ->setLastModifiedBy("Maarten Balliauw")
                        ->setTitle("Office 2007 XLSX Test Document")
                        ->setSubject("Office 2007 XLSX Test Document")
                        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                        ->setKeywords("office 2007 openxml php")
                        ->setCategory("Test result file");
				$ch = "A";	
				
				$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'User Name');$ch++;
				
			    $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Email');$ch++;
				  $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Contact');$ch++;
				  $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Country');$ch++;
				  $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Region');$ch++;
				  $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report Name');$ch++;
				  $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Date');$ch++;
				  $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Request Type');$ch++;
				  $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Publisher');$ch++;
				  $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Message');$ch++;
				  $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Category');$ch++;
				  $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Company');$ch++;
				  $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Designation');
			  $count = 3;
				
					for($a = 0; $a < count($requests); $a++){ 
							$reportData = $this->getRecord($requests[$a]->report_id);
							$categoryData = $this->getCategoryRecord($reportData['category_id']);
							$publisherData = $this->getPublisherRecord($reportData['publisher_id']);
							$ch1 = "A";		 
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->username);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->email);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->contact_no);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->country);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->region);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportData['report_name']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->lead_date);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->request_type);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $publisherData['display_name']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->messages);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $categoryData['category_name']);$ch1++;
							$company_name = $requests[$a]->company_name;
							$designtion = $requests[$a]->designation;

							if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $company_name))
								{
									$company_name = "Demo";
								}	
								
								
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count,$company_name);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count,$designtion);$ch1++;
							$count++;
				   }
	     $objPHPExcel->getActiveSheet()->setTitle('Simple');
     $objPHPExcel->setActiveSheetIndex(0);
     header('Content-Encoding: UTF-8');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Leads.csv"');
        header('Cache-Control: max-age=0');

        $objWriter = new PHPExcel_Writer_CSV($objPHPExcel);
        $objWriter->save('php://output');


        //exit;

	
		}
	public function GetAllRequests()
		{
			$query = $this->db->query('SELECT r.*,rp.report_name FROM request r INNER JOIN report rp on(r.report_id = rp.id) ORDER BY request_type');
			return $query->result();
		}
		public function getCategoryRecord($category_id){
			$query = $this->db->query("SELECT category_name FROM category where id = '".$category_id."';");
			return $query->row_array();
		}
		public function getPublisherRecord($publisher_id){
			$query = $this->db->query("SELECT display_name FROM users where id = '".$publisher_id."';");
			return $query->row_array();
		}
public function getReportsLimit($limit)
	{
		$result=$this->db->query("select * from report where status='1' order by report_date DESC ".$limit);
		return $result->result();
	}
	public function getallReportsCnt()
	{
		$result=$this->db->query("select count(id) as cnt from report where status='1';");
		return $result->result();
	}

	public function downloadPublisherReports($publisherResult) {
		$this->bulkDownloadReports($publisherResult);
	}

	public function bulkDownloadReports($result)
	{
		$requests  = $result;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                        ->setLastModifiedBy("Maarten Balliauw")
                        ->setTitle("Office 2007 XLSX Test Document")
                        ->setSubject("Office 2007 XLSX Test Document")
                        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                        ->setKeywords("office 2007 openxml php")
                        ->setCategory("Test result file");
		$ch = "A";	
				
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report ID');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report name');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report code');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Country');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Category');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Sub-category');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Publisher');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report date');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report summary');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report description');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report content');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'List of tables');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'list of figures');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report price');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Multiple price');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Global price');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Corporate price');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Metatitle');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Meta title');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Page title');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report status');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Pages');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Page URL');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Featured report');$ch++;
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Search keywords');
		$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Status');
		$count = 3;
				
		for($a = 0; $a < count($requests); $a++) { 

			$countryName = $this->GetCountryById($requests[$a]->country_id);
			$categoryName = $this->getCategoryRecord($requests[$a]->category_id);
			$subCategoryName = $this->getsubcategory($requests[$a]->sub_category_id);
			$publisherName = $this->getPublisherNamebyId($requests[$a]->publisher_id);
			$reportDescription = $this->parseHtmlToGenerateExcel($requests[$a]->report_description);
			$reportSummary = $this->parseHtmlToGenerateExcel($requests[$a]->report_summery);
			$reportContent = $this->parseHtmlToGenerateExcel($requests[$a]->report_content);
			$listOfTables = $this->parseHtmlToGenerateExcel($requests[$a]->list_of_tables);
			$listOfFigures = $this->parseHtmlToGenerateExcel($requests[$a]->list_of_figures);
			$PageUrl = base_url().'reports/index/'.$requests[$a]->page_urls;
						
			$ch1 = "A";

			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->id);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->report_name);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->report_code);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $countryName);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $categoryName['category_name']);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $subCategoryName['sub_category_name']);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $publisherName['display_name']);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->report_date);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportSummary);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportDescription);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportContent);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $listOfTables);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $listOfFigures);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, '$ '.$requests[$a]->report_price);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, '$ '.$requests[$a]->multiple_price);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, '$ '.$requests[$a]->global_price);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, '$ '.$requests[$a]->corporate_price);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->metatitle);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->meta_title);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->page_title);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->report_status);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->no_pages);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $PageUrl);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->featured_report);$ch1++;
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->search_keywords);
			$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $requests[$a]->status);

			$count++;
	   	}

	    $objPHPExcel->getActiveSheet()->setTitle('Reports');
     	$objPHPExcel->setActiveSheetIndex(0);
     	header('Content-Encoding: UTF-8');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Reports.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save('php://output');
	}

	public function getAllReportsOfPublisher($publisherId)
	{
		$publisherId = (int)$publisherId;
		$report=$this->db->query("select * from  report where publisher_id = ".$publisherId );
		return $report->result();
	}

	public function parseHtmlToGenerateExcel($htmlStr)
	{
		$stripedStr = strip_tags( 
						preg_replace(
							"/\\\+n/i",
							"\n",
							str_replace(
								["<br>", "<br />"], 
								"\n", 
								html_entity_decode($htmlStr)
							)
						)
					);

		return preg_replace("/[\n]+/i", "\n", $stripedStr);
	}
	
	public function getPublisherNamebyId($publisher_id){
		$query = $this->db->query("SELECT display_name FROM users where id = '".$publisher_id."';");
		return $query->row_array();
	}
	
	public function deletBulkReportData($bulkid){
		$result=$this->db->query("delete from  report where id IN($bulkid);");
	}
	public function checkReportNameExist($reportname){
		$ltrim_reportname=ltrim($reportname);
		$result=$this->db->query("select id from report where report_name ='$ltrim_reportname';");		
		return $result->num_rows();
	}
	public function checkEditReportNameExist($reportname,$id){
		$ltrim_reportname=ltrim($reportname);
		$result=$this->db->query("select id from report where report_name ='$ltrim_reportname' and id!='$id';");
		return $result->num_rows();	
	}

	public function downloadReportIdandUrls($limit){
		
		$result = $this->getReportsIdandUrl($limit);
		
		$requests  = $result;

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                        ->setLastModifiedBy("Maarten Balliauw")
                        ->setTitle("Office 2007 XLSX Test Document")
                        ->setSubject("Office 2007 XLSX Test Document")
                        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                        ->setKeywords("office 2007 openxml php")
                        ->setCategory("Test result file");
				$ch = "A";	
				
				$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Sr.No');$ch++;
				$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report Id');$ch++;
				$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report Name');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report Url');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Category');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Single User Price');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Publisher');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report Description');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'List Of Tables');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'List Of figures');
				 
			  $count = 2;
				$sr_no=1;
					for($a = 0; $a < count($requests); $a++){ 

						//print_r($requests[$a]);exit;
							$reportData = $this->getRecord($requests[$a]->id);
							//print_r($reportData);exit;
							$report_name=$reportData['report_name'];
							$page_url=$reportData['page_urls'];
							$publisher_id=$reportData['publisher_id'];
							$report_price=$reportData['report_price'];
							$category_id=$reportData['category_id'];
							$category_name=$this->getcategory($category_id);

							$publisher_name=$this->get_publisher($publisher_id);
							$page_url=$reportData['page_urls'];
							$url=base_url().'reports/index/'.$page_url;
							
							$ch1 = "A";		 
						$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $sr_no);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportData['id']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $report_name);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $url);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $category_name['category_name']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $report_price);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $publisher_name[0]->display_name);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportData['report_description']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportData['report_content']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportData['list_of_tables']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportData['list_of_figures']);
							
							$count++;
							$sr_no++;
				   }
	     $objPHPExcel->getActiveSheet()->setTitle('Simple');
     $objPHPExcel->setActiveSheetIndex(0);
     header('Content-Encoding: UTF-8');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Reports.csv"');
        header('Cache-Control: max-age=0');

        $objWriter = new PHPExcel_Writer_CSV($objPHPExcel);
       $objWriter->save('php://output');

        // redirect('siteadmin/report');
        //exit;

	
}
		public function downloadReportDetailsFromId($from,$to){
		
		$result = $this->getReportsDetailsFromId($from,$to);
		// echo "<pre>";
		// print_r($result);exit;
		
		$requests  = $result;

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                        ->setLastModifiedBy("Maarten Balliauw")
                        ->setTitle("Office 2007 XLSX Test Document")
                        ->setSubject("Office 2007 XLSX Test Document")
                        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                        ->setKeywords("office 2007 openxml php")
                        ->setCategory("Test result file");
				$ch = "A";	
				
				$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Sr.No');$ch++;
				$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report Id');$ch++;
				$objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report Name');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report Url');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Category');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Single User Price');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Publisher');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'Report Description');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'List Of Tables');$ch++;
				 $objPHPExcel->getActiveSheet()->setCellValue($ch.'1', 'List Of figures');
				 
			  $count = 2;
				$sr_no=1;
					for($a = 0; $a < count($requests); $a++){ 

						//print_r($requests[$a]);exit;
							$reportData = $this->getRecord($requests[$a]->id);
							//print_r($reportData);exit;
							$report_name=$reportData['report_name'];
							$page_url=$reportData['page_urls'];
							$publisher_id=$reportData['publisher_id'];
							$report_price=$reportData['report_price'];
							$category_id=$reportData['category_id'];
							$category_name=$this->getcategory($category_id);

							$publisher_name=$this->get_publisher($publisher_id);
							$page_url=$reportData['page_urls'];
							$url=base_url().'reports/index/'.$page_url;
							
							$ch1 = "A";		 
						$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $sr_no);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportData['id']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $report_name);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $url);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $category_name['category_name']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $report_price);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $publisher_name[0]->display_name);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportData['report_description']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportData['report_content']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportData['list_of_tables']);$ch1++;
							$objPHPExcel->getActiveSheet()->setCellValue($ch1.$count, $reportData['list_of_figures']);
							
							$count++;
							$sr_no++;
				   }
	     $objPHPExcel->getActiveSheet()->setTitle('Simple');
     $objPHPExcel->setActiveSheetIndex(0);
     header('Content-Encoding: UTF-8');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Reports.csv"');
        header('Cache-Control: max-age=0');

        $objWriter = new PHPExcel_Writer_CSV($objPHPExcel);
       $objWriter->save('php://output');

        // redirect('siteadmin/report');
        //exit;

	
		}
}		

		
			

