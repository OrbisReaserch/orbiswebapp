<?php 
class Faq_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		
	}	
 	
	 public function getall()
	 {
		$query = $this->db->get('faq');
		return $query->result();
		
	 }
	
	public function add()
	{
            
	 
            $data = array('question'=>$_POST['question'],
                        'answer'=>$_POST['answer'],
                         'status'=>'1');
            $this->db->insert('faq',$data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
	}
       
       
	public function edit()
	{
	        $data = array('question'=>$_POST['question'],
                        'answer'=>$_POST['answer']);
	        //print_r($_POST['faq_id']);exit;
                   $this->db->where('id',$_POST['faq_id']);
               $this->db->update('faq',$data);

               
               return TRUE;
	}
	
	
	
	
	public function getRecord($id)
	{
		$q= $this->db->get_where('faq',array('id'=>$id));
		return $q->row_array();
	}	
	
	
	
	public function activate($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('faq',$data);	
	}

	public function deactivate($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('faq',$data);	
	}
	
  }
