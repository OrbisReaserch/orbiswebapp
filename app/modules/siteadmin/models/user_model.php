<?php
class User_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();

	}


	public function getall()
	{
		$query = $this->db->get('users');

		return $query->result();

	}

	public function add_user()
	{
		$username = $_POST['user_name'];
		$display_name = $_POST['display_name'];
		$password = $_POST['password'];
		$user_type = $_POST['user_type'];
		$meta_title = $_POST['meta_title'];
		$meta_desc = $_POST['meta_desc'];
		$meta_tag = '';
		$email = $_POST['email'];
		$company_name = $_POST['company_name'];
		$first_name = $_POST['first_name'];
		$middle_name = $_POST['middle_name'];
		$last_name = $_POST['last_name'];
		$address1 = $_POST['address1'];
		$address2 = $_POST['address2'];
		$zipcode = $_POST['zipcode'];
		$city = $_POST['city'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$mobile = $_POST['mobile'];
		$phone = $_POST['phone'];
		$fax = $_POST['fax'];
		$website='';
		$production_details='';
		$specialisation='';
		$message='';
		$cost_range='';

		$date=date("Y-m-d h-i-s");
		$data = array('username'=>$username,'display_name'=>$display_name,'password'=>$password,'user_type'=>$user_type,'meta_tag'=>$meta_tag,'meta_desc'=>$meta_desc,'meta_title'=>$meta_title,
				'email'=>$email,'company_name'=>$company_name,'first_name'=>$first_name,'middle_name'=>$middle_name,
				'last_name'=>$last_name,'address1'=>$address1,'address2'=>$address2,'zipcode'=>$zipcode,
				'city'=>$city,'country'=>$country,'state'=>$state,'mobile'=>$mobile
		,'phone'=>$phone,'fax'=>$fax,'join_date'=>$date,'website'=>$website,'production_details'=>$production_details,'specialisation'=>$specialisation,'message'=>$message,'cost_range'=>$cost_range,'status'=>'1');
		$this->db->insert('users',$data);
		return $this->db->insert_id();
	}

	public function edit_user()
	{
		$user = $_POST['user_id'];
		$username = $_POST['user_name'];
		$display_name = $_POST['display_name'];
		$password = $_POST['password'];
		$user_type = $_POST['user_type'];
		$meta_title = $_POST['meta_title'];
		$meta_desc = $_POST['meta_desc'];
		$meta_tag = '';
		$email = $_POST['email'];
		$company_name = $_POST['company_name'];
		$first_name = $_POST['first_name'];
		$middle_name = $_POST['middle_name'];
		$last_name = $_POST['last_name'];
		$address1 = $_POST['address1'];
		$address2 = $_POST['address2'];
		$zipcode = $_POST['zipcode'];
		$city = $_POST['city'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$mobile = $_POST['mobile'];
		$phone = $_POST['phone'];
		$fax = $_POST['fax'];
		
		$getdetail=$this->user_model->getuserpassworddetail($user);
		
		if($getdetail['password']!=$password){
			$is_login_flag=0;
		}else{
			$is_login_flag=1;
		}
		
		$data = array('username'=>$username,'display_name'=>$display_name,'password'=>$password,'user_type'=>$user_type,'meta_tag'=>$meta_tag,'meta_desc'=>$meta_desc,'meta_title'=>$meta_title,
				'email'=>$email,'company_name'=>$company_name,'first_name'=>$first_name,'middle_name'=>$middle_name,
				'last_name'=>$last_name,'address1'=>$address1,'address2'=>$address2,'zipcode'=>$zipcode,
				'city'=>$city,'country'=>$country,'state'=>$state,'mobile'=>$mobile
		,'phone'=>$phone,'fax'=>$fax,'status'=>'1','is_login'=>$is_login_flag);
		$this->db->where('id',$user);
		$this->db->update('users',$data);

		return TRUE;

	}


	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('users');
		return true;

	}


	public function getRecord($id)
	{
		 
		$q= $this->db->get_where('users',array('id'=>$id));
		return $q->row_array();
	}
	public function getUserRecord($id)
    {
        $q= $this->db->get_where('users',array('id'=>$id));
        $user = $q->row_array();
        if($user['is_login'] == 0 || $user['is_login'] == "0"){
            $this->db->where('id',$id);
            $this->db->update('users',array('is_login'=>1));
            redirect('siteadmin/logout');
        }
    }
	 
	public function activate_user($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('users',$data);
	}


	public function deactivate_user($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('users',$data);
	}

	public function getVendors()
	{
		$result=$this->db->query("select * from users where status='1' and user_type = 'Vendor'");
		return $result->result();
	}

	public function getAllUsers()
	{
		$result=$this->db->query("select * from users where status='1'");
		return $result->result();
	}
	public function getuserpassworddetail($id){
		$result=$this->db->query("select id,username,password from users where id=$id");
		return $result->row_array();

	}
	

}