<?php
class Report_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();

	}


	public function getall()
	{
		$query = $this->db->get('report');

		return $query->result();

	}

	public function add_reports()
	{
		$report_title = $_POST['report_title'];
		$category=$_POST['category'];
		$subcategory=$_POST['subcategory'];
		$publisher_id = $_POST['vendor'];
		$report_dec=$_POST['report_dec'];
		$report_content = $_POST['report_content'];
		$featured= $_POST['featured'];
		$list_of_tables=$_POST['list_of_tables'];
		$list_of_figures=$_POST['list_of_figures'];
		$country_id=$_POST['country_id'];
		
		//$first_name =$user['user_data'][$i]->email;
		
		$user[] = $this->get_publisher($publisher_id);
		$first_name = $user[0][0]->display_name;
		
		$report_code_prefix =  strtoupper(substr($first_name, 0,2));
		$maxdata = $this->get_max_code();
		$report_str =  str_pad(($maxdata['max'] + 1), 4, '0', STR_PAD_LEFT);
    	$report_code = $report_code_prefix.$report_str;

		//echo $report_code; die();
		
		$price=$_POST['price'];
		$d=date('y-m-d');


		$data = array('report_name'=>$report_title,'report_code'=>$report_code,'category_id'=>$category,'sub_category_id'=>$subcategory,'publisher_id'=>$publisher_id,'report_description'=>$report_dec,'report_content'=>$report_content,'report_price'=>$price,'report_date'=>$d,'featured_report'=>$featured,'list_of_figures'=>$list_of_figures,'list_of_tables'=>$list_of_tables,'status'=>'1','country_id'=>$country_id);
		$this->db->insert('report',$data);
		return $this->db->insert_id();
	}
	
	public function upload_report_image($id,$filename)
	{
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
                {
                        $data = array('report_image'=>$filename); 
                        $this->db->where('id',$id);
                        $this->db->update('report',$data);
                        $returnmsg=true;
                }
		return $returnmsg;
	}
	
	public function get_publisher($publisher_id)
	{
		$query = $this->db->query("SELECT * FROM `users` where id='".$publisher_id."'");
		
		return $query->result();
		
	}
	
	public function get_max_code()
	{
		$a= $this->db->query("SELECT MAX(SUBSTR( report_code , 3, 6 ) ) as max FROM `report`");
		return $a->row_array();
	}
	
/*	public function upload_advertisement_image($id,$filename)
	{
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
		{
			$data = array('advertisement_image'=>$filename);
			$this->db->where('id',$id);
			$this->db->update('advertisement',$data);
			$returnmsg=true;
		}
		return $returnmsg;
	}*/

	public function edit_report()
	{
		$report_title = $_POST['report_title'];
		$category=$_POST['category'];
		$subcategory=$_POST['subcategory'];
		$vendor=$_POST['vendor'];
		$report_dec=$_POST['report_dec'];
		$report_content=$_POST['report_content'];
		$price=$_POST['price'];
		$report_id=$_POST['report_id'];
		$featured=$_POST['featured'];
		$list_of_tables=$_POST['list_of_tables'];
		$list_of_figures=$_POST['list_of_figures'];
		$country_id=$_POST['country_id'];
		
		$data = array('report_name'=>$report_title,'category_id'=>$category,'sub_category_id'=>$subcategory,'publisher_id'=>$vendor,'report_description'=>$report_dec,'report_content'=>$report_content,'report_price'=>$price,'featured_report'=>$featured,'list_of_figures'=>$list_of_figures,'list_of_tables'=>$list_of_tables,'country_id'=>$country_id);
		$this->db->where('id',$report_id);
		$this->db->update('report',$data);
		//$this->db->insert('category',$data);
		return TRUE;

	}
	public function update_report_image($id,$filename)
	{
		$report_id=$_POST['report_id'];
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
		{
			$data = array('report_image'=>$filename);
			$this->db->where('id',$report_id);
			$this->db->update('report',$data);
			$returnmsg=true;
		}
		return $returnmsg;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('advertisement');
		return true;

	}


	public function getcategory($cat_id)
	{
		$que_category = $this->db->get_where('category',array('id'=>$cat_id));
		return $que_category->row_array();
	}
	public function getsubcategory($subcat)
	{
	$que_subcat=$this->db->get_where('sub_category',array('id'=>$subcat));
	return $que_subcat->row_array();
	}
	
	public function getvendor($vid)
	{
		$que_vendor = $this->db->get_where('users',array('id'=>$vid));
		return $que_vendor->row_array();
	}

	public function allcategory()
	{
		$cat=$this->db->query("select * from category where status='1'");
		return $cat->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}
	public function allsubcategory()
	{
		$cat=$this->db->query("select * from sub_category where status='1'");
		return $cat->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}


	public function allvendor()
	{
		$ven=$this->db->query("select * from users where status='1' and user_type='Publisher'");
		return $ven->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}
	
	public function allcountry()
	{
		$ven=$this->db->query("select * from country where status='1'");
		return $ven->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}


	public function deactivate_report($id)
	{
		/*	$q = $this->db->query("update report set status=0 where id='$rid'");
		 return $q->result(); */

		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('report',$data);
	}
	public function activate_report($aid)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$aid);
		$this->db->update('report',$data);
	}


	public function getRecord($id)
	{
		///$r = $_POST['report_title'];	
		//$q= $this->db->get_where('report',array('id'=>$id));
		$q= $this->db->query("select * from report where id='".$id."'");
		return $q->row_array();
	}

	public function getAllAdvertisement()
	{
		$result=$this->db->query("select * from advertisement where status='1'");
		return $result->result();
	}
        public function bulkUpload($file)
        {
            
                $upfile=  base_url()."uploads/report_file/".$file;
                $fp = fopen($upfile, "r");
                
                $n=0;
                while ($line1 = fgets($fp))
                {
                    $date = date('y-m-d');
                    $line = str_getcsv($line1, ",", '"');
                    $report_name = $line[0];
                    $category_id = $line[1];
                    $sub_cat_id = $line[2];
                    $publisher_id = $line[3];
                    $country_id = $line[4];
                    $report_desc = $line[5];
                    $report_content = $line[6];
                    $list_of_tables = $line[7];
                    $list_of_figures = $line[8];
                    $report_price = $line[9];
                    
                    echo $report_name;
                
                    
                    if($n>=1)
                    {
                    	
                        $data = array('report_name'=>$report_name,'country_id'=>$country_id,'category_id'=>$category_id,'sub_category_id'=>$sub_cat_id,'publisher_id'=>$publisher_id,'report_date'=>$date,'report_description'=>$report_desc, 'report_content'=>$report_content, 'list_of_tables'=>$list_of_tables,'list_of_figures'=>$list_of_figures,'report_price'=>$report_price);
                        $this->db->insert('report',$data);
                    }
                 $n++;  
                } 
                    die();
                $ftp_server = base_url();
                $conn_id = ftp_connect($ftp_server);
                
                return true;
        }

}