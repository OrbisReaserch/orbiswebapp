<?php
class Country_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();

	}


	public function getall()
	{
		$query = $this->db->get('country');

		return $query->result();

	}

	public function add()
	{
 
		$country_name = $_POST['country_name'];
		$country_dec=$_POST['country_dec'];
		$page_title=$_POST['page_title'];
		$meta_title=$_POST['meta_title'];
		$meta_desc=$_POST['meta_desc'];
		$meta_tag=$_POST['meta_tag'];
			$qy_meta_title = $_POST['qy_meta_title'];
		$qy_meta_desc = $_POST['qy_meta_desc'];
		$qy_meta_keyword = $_POST['qy_meta_keyword'];
		$d=date('y-m-d');
		if('' != $_FILES['flag_image']['name']){
								$flag_image = time().$_FILES['flag_image']['name']; 
								$path = "./uploads/country_images/". $flag_image;
								move_uploaded_file($_FILES['flag_image']["tmp_name"],$path);
							}else{
								$flag_image = '';
							}

		$data = array('country_name'=>$country_name,'country_description'=>$country_dec,'meta_title'=>$meta_title,'page_title'=>$page_title,'meta_desc'=>$meta_desc,'meta_tag'=>$meta_tag,'flag_image'=>$flag_image,'status'=>'1','qy_meta_title'=>$qy_meta_title,'qy_meta_desc'=>$qy_meta_desc,'qy_meta_keyword'=>$qy_meta_keyword);
		$this->db->insert('country',$data);
		return $this->db->insert_id();
	}
	
 

	public function edit()
	{
		$country_name = $_POST['country_name'];
		$country_dec=$_POST['country_dec'];
		$id=$_POST['country_id'];
		$meta_title=$_POST['meta_title'];
		$page_title=$_POST['page_title'];
		$meta_desc=$_POST['meta_desc'];
		$meta_tag=$_POST['meta_tag'];
			$qy_meta_title = $_POST['qy_meta_title'];
		$qy_meta_desc = $_POST['qy_meta_desc'];
		$qy_meta_keyword = $_POST['qy_meta_keyword'];
if('' != $_FILES['flag_image']['name']){
								$flag_image = time().$_FILES['flag_image']['name']; 
								$path = "./uploads/country_images/". $flag_image;
								move_uploaded_file($_FILES['flag_image']["tmp_name"],$path);
							}else{
								$flag_image = $_POST['flag_image_old'];
							}
		$data = array('country_name'=>$country_name,'country_description'=>$country_dec,'meta_title'=>$meta_title,'page_title'=>$page_title,'meta_desc'=>$meta_desc,'meta_tag'=>$meta_tag,'flag_image' => $flag_image,'qy_meta_title'=>$qy_meta_title,'qy_meta_desc'=>$qy_meta_desc,'qy_meta_keyword'=>$qy_meta_keyword);
		$this->db->where('id',$id);
		$this->db->update('country',$data);
		//$this->db->insert('category',$data);
		return TRUE;

	}
	public function update_report_image($id,$filename)
	{
		$report_id=$_POST['report_id'];
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
		{
			$data = array('report_image'=>$filename);
			$this->db->where('id',$report_id);
			$this->db->update('report',$data);
			$returnmsg=true;
		}
		return $returnmsg;
	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('advertisement');
		return true;

	}


	public function getcategory($cat_id)
	{
		$que_category = $this->db->get_where('category',array('id'=>$cat_id));
		return $que_category->row_array();
	}
	public function getsubcategory($subcat)
	{
	$que_subcat=$this->db->get_where('sub_category',array('id'=>$subcat));
	return $que_subcat->row_array();
	}
	
	public function getvendor($vid)
	{
		$que_vendor = $this->db->get_where('users',array('id'=>$vid));
		return $que_vendor->row_array();
	}

	public function allcategory()
	{
		$cat=$this->db->query("select * from category where status='1'");
		return $cat->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}
	public function allsubcategory()
	{
		$cat=$this->db->query("select * from sub_category where status='1'");
		return $cat->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}


	public function allvendor()
	{
		$ven=$this->db->query("select * from users where status='1' and user_type='Publisher'");
		return $ven->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}
	
	public function allcountry()
	{
		$ven=$this->db->query("select * from country where status='1'");
		return $ven->result();
		/*$que_category = $this->db->get_where('category');
		 return $que_category->row_array();*/
	}


	public function deactivate($id)
	{
		/*	$q = $this->db->query("update report set status=0 where id='$rid'");
		 return $q->result(); */

		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('country',$data);
	}
	public function activate($aid)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$aid);
		$this->db->update('country',$data);
	}


	public function getRecord($id)
	{
		///$r = $_POST['report_title'];	
		//$q= $this->db->get_where('report',array('id'=>$id));
		$q= $this->db->query("select * from country where id='".$id."'");
		return $q->row_array();
	}

	public function getAllAdvertisement()
	{
		$result=$this->db->query("select * from advertisement where status='1'");
		return $result->result();
	}
	

}