<?php
class Product_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();

	}


	public function getall()
	{
		$query = $this->db->get('product');
		return $query->result();

	}

	public function getcategory()
	{
		/* $query = $this->db->get('category');
		 $this->db->where('status','1');
		 //return $query->result();

		 $category = array();

		 if($query->result())
		 {
		 foreach ($query->result() as $cat) {
		 $category[$cat->category_id] = $cat->category_name;
		 }
		 }
		 return $category;   */


		$this->db->from('category');
		$this->db->where('status','1');
		$result = $this->db->get();
		$return = array();
		if($result->num_rows() > 0) {
			foreach($result->result_array() as $row) {
				$return[$row['id']] = $row['category_name'];
			}
		}
		return $return;

	}

	public function getAllCategory()
	{
		$result=$this->db->query("select * from category where status='1'");
		return $result->result();
	}

	public function getsubcategory()
	{
		$query = $this->db->get('sub_category');
		$this->db->where('status','1');
		return $query->result();
	}

	public function getsubsubcategory()
	{
		$query = $this->db->get('sub_sub_category');
		$this->db->where('status','1');
		return $query->result();
	}
	public function getProductReviews()
	{
		$result=$this->db->query("select * from product_review");
		return $result->result();
	}

	public function add()
	{
		$file_name = "";
		$config['upload_path'] = './uploads/banner_images/';
		$config['upload_path_product'] = './uploads/product_images/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '2048';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';

		$this->load->library('image_lib', $config);
		$this->load->library('upload', $config);
		$i=0;
		if(isset($_FILES))
		{
			foreach($_FILES as $file=>$field)
			{
				if($this->upload->do_upload($file))
				{
					$data = $this->upload->data();
					$file_name = $data['file_name'];
				}
			}
		}
			
		$product_name = $_POST['product_name'];
		$vendor_id = $_POST['vendor_id'];
		$product_description=$_POST['product_description'];
		$product_type=$_POST['product_type'];
		$price=$_POST['price'];
		$discounted_price = $_POST['discounted_price'];
		$product_width=$_POST['product_width'];
		$product_weight=$_POST['product_weight'];
		$product_height=$_POST['product_height'];
		$product_length=$_POST['product_length'];
		$quantity=$_POST['quantity'];
		$manufacturer=$_POST['manufacturer'];
		$data= $this->get_vendor_name($vendor_id);
		$first_name =  $data['first_name'];
		$product_code_prefix =  strtoupper(substr($first_name, 0,2));
		$maxdata = $this->get_max_code();
		$product_str =  str_pad(($maxdata['max'] + 1), 4, '0', STR_PAD_LEFT);
		$product_code = $product_code_prefix.$product_str;
		$special_product=$_POST['special_product'];
		$featured_product=$_POST['featured_product'];
		$top_section=$_POST['top_section'];
		$product_tag=$_POST['product_tag'];
		$banner_section=$_POST['banner_section'];
		$category=$_POST['category'];
		$sub_category=$_POST['subcategory'];
		$sub_sub_category=$_POST['subsubcategory'];
		$banner_title = $_POST['banner_title'];
		$banner_description = $_POST['banner_description'];
		if($banner_section==1)
		{
			$data = array('product_name'=>$product_name,'vendor_id'=>$vendor_id,'product_description'=>$product_description,'product_type'=>$product_type,'price'=>$price,'discounted_price'=>$discounted_price,'product_width'=>$product_width,'product_height'=>$product_height,'product_length'=>$product_length,'product_weight'=>$product_weight,'quantity'=>$quantity,'manufacturer'=>$manufacturer,'product_code'=>$product_code,'special_product'=>$special_product,'featured_product'=>$featured_product,'top_section'=>$top_section,'product_tag'=>$product_tag,'banner_section'=>$banner_section,'banner_image'=>$file_name,'banner_title'=>$banner_title,'banner_description'=>$banner_description,'category'=>$category,'sub_category'=>$sub_category,'sub_sub_category'=>$sub_sub_category,'status'=>'1');
			$this->db->insert('product',$data);
		}
		else
		{
			$data = array('product_name'=>$product_name,'vendor_id'=>$vendor_id,'product_description'=>$product_description,'product_type'=>$product_type,'price'=>$price,'discounted_price'=>$discounted_price,'product_width'=>$product_width,'product_height'=>$product_height,'product_length'=>$product_length,'product_weight'=>$product_weight,'quantity'=>$quantity,'manufacturer'=>$manufacturer,'product_code'=>$product_code,'special_product'=>$special_product,'featured_product'=>$featured_product,'top_section'=>$top_section,'product_tag'=>$product_tag,'banner_section'=>$banner_section,'category'=>$category,'sub_category'=>$sub_category,'sub_sub_category'=>$sub_sub_category,'status'=>'1');
			$this->db->insert('product',$data);
		}


		$insert_id = $this->db->insert_id();
		if($insert_id)
		{
			for ($i = 0; $i < sizeof($_POST['dep_product_name']); $i++ )
			{
				$path = './uploads/product_images/';
				if ($_FILES['dep_product_image']['size'][$i] == 0) {
					$dep_product_image = "";
				} else {
					$dep_product_image = time() . $_FILES['dep_product_image']['name'][$i];
					move_uploaded_file($_FILES["dep_product_image"]["tmp_name"][$i], $path . $dep_product_image);
				}

				$dep_product_name = $_POST['dep_product_name'][$i];
				$dep_product_price = $_POST['dep_product_price'][$i];

				$data_mandatory = array('product_id'=>$insert_id,'product_code'=>$product_code,'product_name'=>$dep_product_name,'product_image'=>$dep_product_image,'price'=>$dep_product_price,'status'=>"1");
				print_r($data_mandatory);
				$this->db->insert('mandatory_products',$data_mandatory);
			}
		}
		return $this->db->insert_id();
	}


	public function get_max_code()
	{
		$a= $this->db->query("SELECT MAX(SUBSTR( product_code , 3, 6 ) ) as max FROM `product`");
		return $a->row_array();
	}

	public function get_vendor_name($vendor_id)
	{
		$a= $this->db->get_where('users',array('id'=>$vendor_id));
		return $a->row_array();
	}
	/* public function upload_product_image($id,$filearray)
	 {
	 //$returnmsg=false;
	 for($i==0;$i<count($filearray);$i++)
	 {
	 if($filearray[$i]!="" && $filearray[$i]!=NULL)
	 {
	 $data = array('product_image'=>$filearray[$i],'product_id'=>$id);
	 $this->db->insert('product_images',$data);
	 }
	 }
	 return true;
	 } */


	public function edit()
	{
		$file_name = "";
		$config['upload_path'] = './uploads/banner_images/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '2048';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';
		$this->load->library('image_lib', $config);
		$this->load->library('upload', $config);
		$i=0;
		if(isset($_FILES))
		{
			foreach($_FILES as $file=>$field)
			{
				if($this->upload->do_upload($file))
				{
					$data = $this->upload->data();
					$file_name = $data['file_name'];
				}
			}
		}
		$product_name = $_POST['product_name'];
		$product_description=$_POST['product_description'];
		$product_type=$_POST['product_type'];
		$price=$_POST['price'];
		$discounted_price = $_POST['discounted_price'];
		$product_width=$_POST['product_width'];
		$product_height=$_POST['product_height'];
		$product_weight=$_POST['product_weight'];
		$product_length=$_POST['product_length'];
		$quantity=$_POST['quantity'];
		$manufacturer=$_POST['manufacturer'];
		$product_code = $_POST['product_code'];
		$special_product=$_POST['special_product'];
		$featured_product=$_POST['featured_product'];
		$top_section=$_POST['top_section'];
		$product_tag=$_POST['product_tag'];
		$banner_section=$_POST['banner_section'];
		$category=$_POST['category'];
		$sub_category=$_POST['subcategory'];
		$sub_sub_category=$_POST['subsubcategory'];

		if($banner_section==1 )
		{
			$banner_title = $_POST['banner_title'];
			$banner_description = $_POST['banner_description'];
			if($file_name!="" && $file_name!=NULL)
			{
				$data = array('product_name'=>$product_name,'product_description'=>$product_description,'product_type'=>$product_type,'price'=>$price,'discounted_price'=>$discounted_price,'product_width'=>$product_width,'product_height'=>$product_height,'product_length'=>$product_length,'product_weight'=>$product_weight,'quantity'=>$quantity,'manufacturer'=>$manufacturer,'product_code'=>$product_code,'special_product'=>$special_product,'featured_product'=>$featured_product,'top_section'=>$top_section,'product_tag'=>$product_tag,'banner_section'=>$banner_section,'banner_image'=>$file_name,'banner_title'=>$banner_title,'banner_description'=>$banner_description,'category'=>$category,'sub_category'=>$sub_category,'sub_sub_category'=>$sub_sub_category,'status'=>'1');
				$this->db->where('id',$_POST['product_id']);
				$this->db->update('product',$data);
			}
			else
			{
				$data = array('product_name'=>$product_name,'product_description'=>$product_description,'product_type'=>$product_type,'price'=>$price,'discounted_price'=>$discounted_price,'product_width'=>$product_width,'product_height'=>$product_height,'product_length'=>$product_length,'product_weight'=>$product_weight,'quantity'=>$quantity,'manufacturer'=>$manufacturer,'product_code'=>$product_code,'special_product'=>$special_product,'featured_product'=>$featured_product,'top_section'=>$top_section,'product_tag'=>$product_tag,'banner_section'=>$banner_section,'banner_title'=>$banner_title,'banner_description'=>$banner_description,'category'=>$category,'sub_category'=>$sub_category,'sub_sub_category'=>$sub_sub_category,'status'=>'1');
				$this->db->where('id',$_POST['product_id']);
				$this->db->update('product',$data);
			}


		}
		else
		{
			$data = array('product_name'=>$product_name,'product_description'=>$product_description,'product_type'=>$product_type,'price'=>$price,'discounted_price'=>$discounted_price,'product_width'=>$product_width,'product_weight'=>$product_weight,'product_length'=>$product_length,'product_height'=>$product_height,'quantity'=>$quantity,'manufacturer'=>$manufacturer,'product_code'=>$product_code,'special_product'=>$special_product,'featured_product'=>$featured_product,'top_section'=>$top_section,'product_tag'=>$product_tag,'banner_section'=>$banner_section,'category'=>$category,'sub_category'=>$sub_category,'sub_sub_category'=>$sub_sub_category,'status'=>'1');
			$this->db->where('id',$_POST['product_id']);
			$this->db->update('product',$data);
		}

		$delete_color = mysql_query("delete from product_color where product_id = '".$_POST['product_id']."'");
		if($delete_color)
		{
			for($i=0;$i<count($_POST['product_color']);$i++)
			{
				$product_color = $_POST['product_color'][$i];
				$color_price = $_POST['color_price'][$i];
				$data_color = array('product_id'=>$_POST['product_id'],'product_color'=>$product_color,'color_price'=>$color_price,'status'=>'1');
				$this->db->insert('product_color',$data_color);
			}
		}
		//$this->db->insert('category',$data);
		return TRUE;

	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('category');
		return true;

	}
	public function getRecord($id)
	{
		$q= $this->db->get_where('product',array('id'=>$id));
		return $q->row_array();
	}
	public function getProductcolor($id)
	{
		$result=$this->db->query("select * from product_color where product_id='".$id."'");
		return $result->result();
	}
	public function setbanner($id)
	{
		$data = array('banner_section'=>'1');
		$this->db->where('id',$id);
		$this->db->update('product',$data);
	}
	public function unsetbanner($id)
	{
		$data = array('banner_section'=>'0');
		$this->db->where('id',$id);
		$this->db->update('product',$data);
	}
	public function setfeatured($id)
	{
		$data = array('featured_product'=>'1');
		$this->db->where('id',$id);
		$this->db->update('product',$data);
	}
	public function setunfeatured($id)
	{
		$data = array('featured_product'=>'0');
		$this->db->where('id',$id);
		$this->db->update('product',$data);
	}
	public function activate($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('product',$data);
	}
	public function deactivate($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('product',$data);
	}
	public function deleteimage($id)
	{
		$this->db->query("delete from product_images where id = '".$id."'");
	}
	public function getproduct_images($id)
	{
		$query = $this->db->query("select * from product_images where product_id = '".$id."'");
		// $this->db->where('id',$id);
		return $query->result();
	}
	/* public function add_image()
	 {
	 $product_id = $_POST['id'];
	 //$brand_description=$_POST['brand_description'];
	 $data = array('product_id'=>$product_id,'status'=>'1');
	 $this->db->insert('product_images',$data);
	 return $this->db->insert_id();
	 } */
	public function upload_product_image($id,$filename)
	{
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
		{
			$data = array('product_id'=>$id,'product_image'=>$filename,'status'=>'1');
			$this->db->insert('product_images',$data);
			$returnmsg=true;
		}
		return $returnmsg;
	}

	public function activate_image($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('product_images',$data);
	}


	public function deactivate_image($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('product_images',$data);
	}
	public function getProductname($id)
	{
		$result=$this->db->query("select * from product where id='".$id."'");
		return $result->result();
	}
	public function activate_review($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('product_review',$data);
	}

	public function deactivate_review($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('product_review',$data);
	}

}