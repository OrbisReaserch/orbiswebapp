<?php
class Category_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();

	}


	public function getall()
	{
		$query = $this->db->get('category');
		return $query->result();

	}
	public function getsub_subcategory()
	{
		$query = $this->db->get('sub_sub_category');
		$this->db->where('status','1');
		return $query->result();
	}

	public function add()
	{
		
		$name = $_POST['name'];
		$category_description=$_POST['category_description'];
		$category_tag=$_POST['category_tag'];
		$meta_title=$_POST['meta_title'];
		$page_title=$_POST['page_title'];
		$qy_meta_title = $_POST['qy_meta_title'];
		$qy_meta_desc = $_POST['qy_meta_desc'];
		$qy_meta_keyword = $_POST['qy_meta_keyword'];
		$short_desc = $_POST['short_desc'];
		if (isset($_POST['set_home']))
		{
		$data = array('category_name'=>$name,'category_description'=>$category_description,'category_tag'=>$category_tag,'meta_title'=>$meta_title,'page_title'=>$page_title,'status'=>'1','set_on_home'=>'1','qy_meta_title'=>$qy_meta_title,'qy_meta_desc'=>$qy_meta_desc,'qy_meta_keyword'=>$qy_meta_keyword,'short_desc'=>$short_desc);
		$this->db->insert('category',$data);
		}
		else 
		{
		$data = array('category_name'=>$name,'category_description'=>$category_description,'category_tag'=>$category_tag,'meta_title'=>$meta_title,'page_title'=>$page_title,'status'=>'1','set_on_home'=>'0','qy_meta_title'=>$qy_meta_title,'qy_meta_desc'=>$qy_meta_desc,'qy_meta_keyword'=>$qy_meta_keyword,'short_desc'=>$short_desc);
		$this->db->insert('category',$data);	
		}
		$category_name =  str_replace(" ","-",strtolower($_POST['name']));
		CreateSitemap("allcategorysitemap.xml","http://www.orbisresearch.com/market-reports/".$category_name.".html");
		return $this->db->insert_id();
	}

	public function upload_category_image($id,$filename)
	{
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
		{
			$data = array('category_image'=>$filename);
			$this->db->where('id',$id);
			$this->db->update('category',$data);
			$returnmsg=true;
		}
		return $returnmsg;
	}


	public function add_sub_category()
	{
		$categoryid = $_POST['category'];
		$subcategory = $_POST['sub_cat_name'];
		$page_title=$_POST['page_title'];
		$sub_category_description = $_POST['sub_category_description'];
		$sub_category_tag=$_POST['sub_category_tag'];$meta_title=$_POST['meta_title'];
		$qy_meta_title = $_POST['qy_meta_title'];
		$qy_meta_desc = $_POST['qy_meta_desc'];
		$qy_meta_keyword = $_POST['qy_meta_keyword'];
		$short_desc = $_POST['short_desc'];
		$data = array('sub_category_name'=>$subcategory,'sub_category_description'=>$sub_category_description,'sub_category_tag'=>$sub_category_tag,'category_id'=>$categoryid,'meta_title'=>$meta_title,'page_title'=>$page_title,'status'=>'1','qy_meta_title'=>$qy_meta_title,'qy_meta_desc'=>$qy_meta_desc,'qy_meta_keyword'=>$qy_meta_keyword,'short_desc'=>$short_desc);
		$this->db->insert('sub_category',$data);
		return $this->db->insert_id();
	}
	public function add_sub_sub_category()
	{
		$categoryid = $_POST['category'];
		$sub_categoryid = $_POST['sub_category'];
		$subcategory = $_POST['name'];
		$sub_category_description = $_POST['category_description'];
		$sub_category_tag=$_POST['category_tag'];
		$data = array('sub_sub_category_name'=>$subcategory,'sub_sub_category_description'=>$sub_category_description,'sub_sub_category_tag'=>$sub_category_tag,'category_id'=>$categoryid,'sub_category_id'=>$sub_categoryid,'status'=>'1');
		$this->db->insert('sub_sub_category',$data);
		return $this->db->insert_id();
	}

	public function upload_cat_image($id,$filearray)
	{
		
		$returnmsg	=	false;
		foreach($filearray as $filename)
		{
			if($filename)
			{
				//$fullfilename	=	'admin/contractor_img/'.$filename;
				$data = array('image'=>$filename);
				$this->db->where('id',$category_id);
				$this->db->update('category',$data);
				$returnmsg	=	true;
			}
		}
		return $returnmsg;
	}


	public function edit_sub_category()
	{
		$subcategory_name = $_POST['sub_cat_name'];
		$subcategory_description = $_POST['sub_category_description'];
		$subcategory_tag = $_POST['sub_category_tag'];
		$category_id=$_POST['category'];
		$page_title=$_POST['page_title'];
		$subcategory_id=$_POST['subcategory_id'];$meta_title=$_POST['meta_title'];
		$qy_meta_title = $_POST['qy_meta_title'];
		$qy_meta_desc = $_POST['qy_meta_desc'];
		$qy_meta_keyword = $_POST['qy_meta_keyword'];
		$short_desc = $_POST['short_desc'];
		$data = array('sub_category_name'=>$subcategory_name,'sub_category_description'=>$subcategory_description,'sub_category_tag'=>$subcategory_tag,'category_id'=>$category_id,'page_title'=>$page_title,'meta_title'=>$meta_title,'status'=>'1','qy_meta_title'=>$qy_meta_title,'qy_meta_desc'=>$qy_meta_desc,'qy_meta_keyword'=>$qy_meta_keyword,'short_desc'=>$short_desc);
		$this->db->where('id',$_POST['subcategory_id']);
		$this->db->update('sub_category',$data);
	 
		return TRUE;

	}

	public function edit_sub_sub_category()
	{
		$subcategory_name = $_POST['sub_cat_name'];
		$subcategory_description = $_POST['sub_category_description'];
		$subcategory_tag = $_POST['sub_category_tag'];
		$category_id=$_POST['category'];
		$sub_category_id=$_POST['sub_category'];
		$data = array('sub_sub_category_name'=>$subcategory_name,'sub_sub_category_description'=>$subcategory_description,'sub_sub_category_tag'=>$subcategory_tag,'category_id'=>$category_id,'sub_category_id'=>$sub_category_id,'status'=>'1');
		$this->db->where('id',$_POST['sub_subcategory_id']);
		$this->db->update('sub_sub_category',$data);
		//$this->db->insert('category',$data);
		return TRUE;

	}

	public function edit()
	{
		$category_name = $_POST['name'];
		$category_description = $_POST['category_description'];
		$category_tag = $_POST['category_tag'];
		$category_id=$_POST['category_id'];
		$page_title=$_POST['page_title'];
		$meta_title=$_POST['meta_title'];
		$qy_meta_title = $_POST['qy_meta_title'];
		$qy_meta_desc = $_POST['qy_meta_desc'];
		$qy_meta_keyword = $_POST['qy_meta_keyword'];
		$short_desc = $_POST['short_desc'];
	if (isset($_POST['set_home']))
		{
		$data = array('category_name'=>$category_name,'category_description'=>$category_description,'category_tag'=>$category_tag,'meta_title'=>$meta_title,'page_title'=>$page_title,'set_on_home'=>'1','qy_meta_title'=>$qy_meta_title,'qy_meta_desc'=>$qy_meta_desc,'qy_meta_keyword'=>$qy_meta_keyword,'short_desc'=>$short_desc);
		$this->db->where('id',$_POST['category_id']);
		$this->db->update('category',$data);
		}
		else 
		{
		$data = array('category_name'=>$category_name,'category_description'=>$category_description,'category_tag'=>$category_tag,'meta_title'=>$meta_title,'page_title'=>$page_title,'set_on_home'=>'0','qy_meta_title'=>$qy_meta_title,'qy_meta_desc'=>$qy_meta_desc,'qy_meta_keyword'=>$qy_meta_keyword,'short_desc'=>$short_desc);
		$this->db->where('id',$_POST['category_id']);
		$this->db->update('category',$data);	
		}
		
		//$this->db->insert('category',$data);
		return TRUE;

	}

	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('category');
		return true;

	}

	public function getVendors()
	{
		$result=$this->db->query("select * from users where status='1' and user_type = 'Vendor'");
		return $result->result();
	}


	public function getRecord($id)
	{
			
		$q= $this->db->get_where('category',array('id'=>$id));
			
		return $q->row_array();
	}

	public function getRecordSubCategory($id)
	{
			
		$q= $this->db->get_where('sub_category',array('id'=>$id));
			
		return $q->row_array();
	}

	public function getRecordSubtoSubCategory($id)
	{
			
		$q= $this->db->get_where('sub_sub_category',array('id'=>$id));
			
		return $q->row_array();
	}

	public function activate($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('category',$data);
	}

	public function activate_sub_category($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('sub_category',$data);
	}

	public function activate_sub_sub_category($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('sub_sub_category',$data);
	}

	public function deactivate($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('category',$data);
	}

	public function deactivate_sub_category($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('sub_category',$data);
	}

	public function deactivate_sub_sub_category($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('sub_sub_category',$data);
	}

	public function getAllCategory()
	{
		$result=$this->db->query("select * from category where status='1'");
		return $result->result();
	}
	public function getAllSubCategory()
	{
		$result=$this->db->query("select * from sub_category");
		return $result->result();
	}

	public function getAllSubtoSubCategory()
	{
		$result=$this->db->query("select * from sub_sub_category where status='1'");
		return $result->result();
	}

}