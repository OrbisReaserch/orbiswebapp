<?php 
class Content_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		
	}	
        
	public function getall()
	{
            $query = $this->db->get('content');	
            return $query->result();
	}
	
	public function add_content()
	{
            $this->page_name=$_POST['page_name'];
            $this->page_subject=$_POST['page_subject'];
            $this->page_description=$_POST['page_description'];
//            $data = array('page_name'=>$page_name,'page_subject'=>$page_subject,'page_description'=>$page_description);
            //print_r($data);
            $this->db->insert('content',$this);
            return $this->db->insert_id();
	}
        
	public function upload_content_image($id,$filename)
	{
            $returnmsg=false;
            if($filename!="" && $filename!=NULL)
            {
                $data = array('page_image'=>$filename);
                $this->db->where('id',$id);
                $this->db->update('content',$data);
                $returnmsg=true;
            }
            return $returnmsg;
	}
	
        public function edit_content()
	{
            $this->page_name=$_POST['page_name'];
            $this->page_subject=$_POST['page_subject'];
            $this->page_description=$_POST['page_description'];
            
            $this->db->update('content',$this,array('id' => $_POST['content_id']));
            return TRUE;		
	}
        public function upload_content_image_update($id,$filename)
	{
            $returnmsg=false;
            if($filename!="" && $filename!=NULL)
            {
                $data = array('page_image'=>$filename);
                $this->db->where('id',$id);
                $this->db->update('content',$data);
                $returnmsg=true;
            }
            return $returnmsg;
	}
        
	public function delete($id)
	{
            $this->db->where('id',$id);
            $this->db->delete('content');
            return true;
	}
	
	public function getRecord($id)
	{
            $q= $this->db->get_where('content',array('id'=>$id));
            return $q->row_array();
	}
       
	public function activate_content($id)
	{
            $data = array('status'=>'1');
            $this->db->where('id',$id);
            $this->db->update('content',$data);	
	}
        
	public function deactivate_content($id)
	{
            $data = array('status'=>'0');
            $this->db->where('id',$id);
            $this->db->update('content',$data);	
	}
        
	public function getAllContent()
        {
            $result=$this->db->query("select * from content where status='1'");
            return $result->result();
        }
}