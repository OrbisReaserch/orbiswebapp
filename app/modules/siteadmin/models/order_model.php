<?php
class Order_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();

	}


	public function getall()
	{
		$query = $this->db->query('SELECT o.*,od.* FROM orders o INNER JOIN order_details od ON(o.order_id = od.order_id)');
		return $query->result();
	}
	public function getallGuestOrders()
	{
		$query = $this->db->query('SELECT g.* , go.*  FROM guest g INNER JOIN guest_order go ON ( g.order_no = go.order_no )');
		return $query->result();
	}
	public function getallGuestOrdersLimit($limit)
	{
		$query = $this->db->query('SELECT g.*, go.*  FROM guest g INNER JOIN guest_order go ON ( g.order_no = go.order_no ) ORDER BY g.id desc '.$limit);
		return $query->result();
	}
	public function getallGuestOrdersCnt()
	{
		$query = $this->db->query('SELECT count(id) as cnt FROM guest_order;');
		return $query->result();
	}
	public function getallOrdersLimit()
	{
		$query = $this->db->query('SELECT o.*,od.* FROM orders o INNER JOIN order_details od ON(o.order_id = od.order_id) ORDER BY o.id desc '.$limit);
		return $query->result();
	}
	public function getallOrdersCnt()
	{
		$query = $this->db->query('SELECT count(id) as cnt FROM orders;');
		return $query->result();
	}
	public function getUsername($userId)
	{
		$query = $this->db->query("SELECT * FROM users WHERE id = '".$userId."'");
		return $query->result();
	}
		public function getReport($id)
	{
		$query = $this->db->query("SELECT * FROM `report` where id='".$id."'");
		return $query->result();
	}

	public function deleverReport($aid)
	{
		$data = array('order_status'=>'delivered');
		$this->db->where('order_id',$aid);
		$this->db->update('orders',$data);
	}
	public function deleverGuestReport($aid)
	{
		$data = array('order_status'=>'1');
		$this->db->where('order_no',$aid);
		$this->db->update('guest',$data);
	}

	
}