<?php
class request_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
	public function getall()
	{
		$query = $this->db->query('SELECT * FROM request ORDER BY id DESC LIMIT 0, 1000;');
		//$query = $this->db->query('SELECT * FROM request ORDER BY id DESC;');
		return $query->result();
	}
	public function getallByLimit($condition,$limit){
		if("" == $condition){
			$query = $this->db->query('SELECT * FROM request ORDER BY id DESC '.$limit);
		}else{
			$cond =  str_replace("~","'",urldecode($condition));
			$query = $this->db->query('SELECT * FROM request where '.str_replace("|","%",$cond).' ORDER BY id DESC '.$limit);
		} 
		return $query->result();
	}
	public function getallByCond($condition){
		$cond =  str_replace("~","'",urldecode($condition));
		$query = $this->db->query('SELECT * FROM request where '.str_replace("|","%",$cond).' ORDER BY id DESC limit 0,1000');
		return $query->result();
	}
	public function getallByDate(){
		//$cond =  str_replace("~","'",urldecode($condition));
		$query = $this->db->query("SELECT username, company_name, country, region, designation, email, contact_no, report_id,request_type, messages, publisher_id, lead_date FROM `request` WHERE lead_date BETWEEN '2018-01-01' AND '2018-06-07' order by id DESC");
		return $query->result();
	}
	public function getallByCondDateWise($condition,$defaultCondition){
		$cond =  str_replace("~","'",urldecode($condition));
		$query = $this->db->query('SELECT * FROM request where '.str_replace("|","%",$cond).' AND '.str_replace("~","'",urldecode($defaultCondition)).' ORDER BY id DESC');
		return $query->result();
	}
	public function GetCountries(){
	$query = $this->db->query('SELECT country as country_name FROM request WHERE country != "NULL" GROUP BY country;');
		return $query->result();
	
	}
	public function add()
	{
		$username = $_POST['username'];
		$full_name= $_POST['full_name'];
		$email = $_POST['email'];
		$contact_no = $_POST['contact_no'];
		$report_code = $_POST['report_code'];
		$publisher_id = $_POST['publisher_id'];
		$data = array('username'=>$username,'full_name'=>$full_name,'email'=>$email,'contact_no'=>$contact_no,'report_code'=>$report_code,'publisher_id'=>$publisher_id);
		$this->db->insert('request',$data);
		return $this->db->insert_id();
	}

	public function edit()
	{
		$request_id= $_POST['request_id'];
		$username = $_POST['username'];
		$full_name= $_POST['full_name'];
		$email = $_POST['email'];
		$contact_no = $_POST['contact_no'];
		$report_code = $_POST['report_code'];
		$publisher_id = $_POST['publisher_id'];
		$data = array('username'=>$username,'full_name'=>$full_name,'email'=>$email,'contact_no'=>$contact_no,'report_code'=>$report_code,'publisher_id'=>$publisher_id);
		$this->db->where('id',$request_id);
		$this->db->update('request',$data);
		return TRUE;
	}


	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('request');
		return true;
	}
	public function getRequestsOfPublisher($id)
	{
		$request=$this->db->query("select * from  request where publisher_id = '".$id."' AND status='1'");
		return $request->result();
	}


	public function getRecord($id)
	{
			
		$q= $this->db->get_where('request',array('id'=>$id));
		return $q->row_array();
	}

	public function activate($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('request',$data);
	}
	public function deactivate($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('request',$data);
	}

	public function getAllRequests()
	{
		$result=$this->db->query("select * from request where status='1'");
		return $result->result();
	}
	public function getAllPublishers()
	{
		$result=$this->db->query("select * from users where user_type = 'Publisher' AND status='1'");
		return $result->result();
	}

	public function getAllReports()
	{
		$result=$this->db->query("select * from reports where status='1'");
		return $result->result();
	}
	public function getEmailId($publisher_id)
	{
		$result=$this->db->query("select email from users where id = '".$publisher_id."'");
		return $result->result();
	}

}