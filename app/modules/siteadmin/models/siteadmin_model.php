<?php

class Siteadmin_Model extends CI_Model
{

	public function checkLogin($user,$pass)
	{
		$this->db->select("*");
		$this->db->from('users');
		$this->db->where('username',$user);
		$this->db->where('password',$pass); 
	 
		$query = $this->db->get();
		
		if($query->num_rows() == 1)
		{
			$row = $query->row();
			if($row->user_type!='Client' && $row->user_type!='Publisher')
			{
			$data = array('id'=>$row->id,'username'=>$row->username,'admin_email'=>$row->email,'user_type'=>$row->user_type);
			$this->session->set_flashdata('message','You have successfully logged in');
			$this->session->set_userdata($data);
			
			// valid login
			return TRUE;
			}
			else
			{
				// invalid login
				return FALSE;
			}
			 
		}
		else
		{
			// invalid login
			return FALSE;
		}
		 
	}
	 
	public function forgot_password()
	{

		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('status','Y');

		$query = $this->db->get();

		$row = $query->row();
		//echo $row->email.'======';;

		send_mail($row->name,$row->email,'admin_password_help',$row->pass);
		if($query->num_rows() == 1)
		return true;
		else
		return false;

	}

	public function admin_setting()
	{

		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_type','admin');

		$query = $this->db->get();

		$row = $query->row();

		$data = array('user'=>$row->username,'email'=>$row->username,'pass'=>$row->password);
		//print_r($data);
		return $data;

	}

	public function update_setting($user,$email,$pass)
	{
		//echo $this->session->userdata('admin_id').'===============';
		$data = array('user'=>$user,'email'=>$email,'pass'=>$pass);
		$this->db->where('id',$this->session->userdata('admin_id'));
		$this->db->update('admin',$data);
		return true;
	}

}