<?php 
class Weight_range_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		
	}	
 	
	
	 public function getall()
	{
		$query = $this->db->get('weight_range');
		
		return $query->result();
		
	}
	
	public function add_weight_range()
	{
		$weight_from = $_POST['weight_from'];
		$weight_to=$_POST['weight_to'];
		$rate=$_POST['rate'];
		
		/*$query_weight = $this->db->query("select * from weight_range where weight_from='$weight_from' or weight_to='$weight_to'");
		if($query_weight->num_rows()<0)
		{*/
		
		
		$data = array('weight_from'=>$weight_from,'weight_to'=>$weight_to,'rate'=>$rate,'status'=>'1');
                print_r($data);
		$this->db->insert('weight_range',$data);
		return $this->db->insert_id();
		/*}
		else
		{
			  $this->session->set_flashdata('error', 'Weight from or to is already used.');
                $data['include'] = 'siteadmin/weight_range/add_weight_range';
                //$this->load->view('backend/container', $data);    
		}*/
	}
        
	/* public function upload_news_image($id,$filename)
	{
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
                {
                        $data = array('news_image'=>$filename);
                        $this->db->where('id',$id);
                        $this->db->update('news',$data);
                        $returnmsg=true;
                }
		return $returnmsg;
	} */
	
        public function edit_weight_range()
	{
		$weight_from = $_POST['weight_from'];
		$weight_to=$_POST['weight_to'];
		$rate=$_POST['rate'];
        $weight_id=$_POST['weight_id'];
		
		$data = array('weight_from'=>$weight_from,'weight_to'=>$weight_to,'rate'=>$rate);
		$this->db->where('id',$weight_id);
		$this->db->update('weight_range',$data);
		//$this->db->insert('category',$data);
		return TRUE;
		
	}
       /* public function upload_news_image_update($id,$filename)
	{
		$returnmsg=false;
		if($filename!="" && $filename!=NULL)
                {
                        $data = array('news_image'=>$filename);
                        $this->db->where('id',$id);
                        $this->db->update('news',$data);
                        $returnmsg=true;
                }
		return $returnmsg;
	} */
        
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('weight_range');
		return true;
	
	}
	
	
	public function getRecord($id)
	{
           
		$q= $this->db->get_where('weight_range',array('id'=>$id));
               
		return $q->row_array();
	}
       
	public function activate_weight_range($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('weight_range',$data);	
	}
        

	public function deactivate_weight_range($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('weight_range',$data);	
	}
        
       
	public function getAllWeightRange()
        {
            $result=$this->db->query("select * from weight_range where status='1'");
            return $result->result();
        }
        
}