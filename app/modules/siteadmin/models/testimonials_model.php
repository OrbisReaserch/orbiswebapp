<?php 
class Testimonials_model extends CI_Model {

	public function __construct()
	{
		
		parent::__construct();
		
	}	
 	
	
	 public function getall()
	{
		$query = $this->db->get('testimonials');
		
		return $query->result();
		
	}
	
	public function add_testimonials()
	{
		$name = $_POST['name'];
		$occupation= $_POST['occupation'];
		$testimonials_message=mysql_real_escape_string($_POST['testimonials_message']);
		$news_date=date('y-m-d');
		$meta_title=$_POST['meta_title'];
		$meta_desc=$_POST['meta_desc'];
		$meta_tag='';
		
		$data = array('testimonials_name'=>$name,'testimonials_occupation'=>$occupation,'testimonials_message'=>$testimonials_message,'testimonials_date'=>$news_date,'meta_title'=>$meta_title,'meta_desc'=>$meta_desc,'meta_tag'=>$meta_tag,'status'=>'1');
               // print_r($data);
		$this->db->insert('testimonials',$data);
		return $this->db->insert_id();
	}
        
	
	
        public function edit_testimonials($testimonials_id)
	{
		$name = $_POST['name'];
		$occupation= $_POST['occupation'];
		$testimonials_message=mysql_real_escape_string($_POST['testimonials_message']);
		
		$testimonials_id = $_POST['testimonials_id'];
		$meta_title=$_POST['meta_title'];
		$meta_desc=$_POST['meta_desc'];
		$meta_tag='';
		//$news_date=date('y-m-d');
		
		$data = array('testimonials_name'=>$name,'testimonials_message'=>$testimonials_message,'testimonials_occupation'=>$occupation,'meta_title'=>$meta_title,'meta_desc'=>$meta_desc,'meta_tag'=>$meta_tag);
		
		$this->db->where('id',$testimonials_id);
		$this->db->update('testimonials',$data);
		//$this->db->insert('category',$data);
		return TRUE;
		
	}
     
	public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('news');
		return true;
	
	}
	
	
	public function getRecord($id)
	{
           
		$q= $this->db->get_where('testimonials',array('id'=>$id));
               
		return $q->row_array();
	}
       
	public function activate_testimonials($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('testimonials',$data);	
	}
        

	public function deactivate_testimonials($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('testimonials',$data);	
	}
        
       
	public function getAllNews()
        {
            $result=$this->db->query("select * from news where status='1'");
            return $result->result();
        }
        
}