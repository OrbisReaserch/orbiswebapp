<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contact_us extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('contact_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
		 $this->load->library('email');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {

        $this->Checklogin();
        $data['result'] = $this->contact_model->getall();
        $data['include'] = 'siteadmin/contact_us/contact_management';
        $data['admin_section'] = 'contact_management';
        $this->load->view('backend/container', $data);
    }
  

    public function edit($id) 
    {
        $data['info'] = $this->contact_model->getRecord($id);
		//print_r($reort['info']); exit();
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'contact_us';
            //$edit = $this->contact_model->edit();
            
            
            $to = $_POST['to'];
			$subject=$_POST['subject'];
			$message=mysql_real_escape_string($_POST['message']);
			$contact_id=$_POST['contact'];
            
            
      		    $this->email->from('info@orbis.com');
                $this->email->to($to);
               // $this->email->cc('sandeep@velociters.com'); 
               // $this->email->bcc('them@their-example.com'); 
                $this->email->subject($subject);
                
                $this->email->message('Message From Orbis \n \n Name : '.$message);	

                $this->email->send();
            	
            		            	
                    $this->session->set_flashdata('success', 'Report has been update successfully.');
                    redirect(base_url().'siteadmin/contact_us');

            
          
        }
        else
        {
        	$this->session->set_flashdata('success', 'Unable to update Report.');
            $data['include'] = 'siteadmin/contact_us/edit';
            $this->load->view('backend/container', $data);
        }
    }
    
    public function delete($id) 
    {
        if ($this->advertisement_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url() . 'siteadmin/advertisement');
        }
    }

    public function activate($id) 
    {
        $this->country_model->activate($id);
        $this->session->set_flashdata('success', 'Country has been activated successfully');
        redirect('siteadmin/country');
    }
    
    public function deactivate($id) 
    {
        $this->country_model->deactivate($id);
        $this->session->set_flashdata('success', 'Country has been deactivated successfully');
        redirect('siteadmin/country');
    }
    
}
