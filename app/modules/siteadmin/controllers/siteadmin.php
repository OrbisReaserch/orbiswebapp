<?php
class Siteadmin extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
			$this->load->library('email');
		$this->load->model('siteadmin/siteadmin_model');
	}

	public function index()
	{
		
		if(isset($_POST))
		{
			
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			$this->form_validation->set_rules('username','Username','trim|required|xss_clean');
			$this->form_validation->set_rules('password','Password','trim|required|xss_clean');

			if($this->form_validation->run() == FALSE)
			{
				
			}
			else
			{
				
				
				if($this->siteadmin_model->checkLogin($this->input->post('username'),$this->input->post('password')))
				{
					redirect('siteadmin/dashboard');
				}
				else
				{
					$data['error_message'] = 'Invalid Credential. Please try again.';
				}
			}
		}
		else
		{
				if($this->session->userdata('admin_email')!='')
				{
					redirect('siteadmin/dashboard');
				}
		}

		$data['include'] = 'siteadmin/login';
		$this->load->view('siteadmin/login',$data);
	}

	
	public function get_mailid()
	{
		 $to = $_POST['email'];

	     $query = $this->db->get_where('admin',array('email'=>$to));
                if($query->num_rows()>0)
                {
                    $row=$query->result();
                    $email = $row[0]->email;
                    $password = $row[0]->pass;

                    $this->email->from('info@orbis.com', 'ORBIS');
                    $this->email->to($to); 
  
                    $this->email->subject('Recover Password');
                    $this->email->message('Your email is :'.$email.'Password is :'.$password); 
                    $this->email->send();

                    $this->email->print_debugger();
            
                  $data['error_message'] = 'Mail sent to your inbox.';
                }
                else
                {

                    $data['error_message'] = 'Entered Email is not vaild. Please try again.';
                }
                $data['include'] = 'siteadmin/login';
				$this->load->view('siteadmin/login',$data);
	}
	

	public function forgot_password()
	{
		
		$ret = $this->siteadmin_model->forgot_password();
		if($ret)
		$this->session->set_flashdata('error_message', 'Password has been sent to your mailbox.');
		else
		$this->session->set_flashdata('error_message', 'Error sending mail. Please try again later.');

		redirect('siteadmin/');

	}

	public function setting()
	{
		if(isset($_POST['SUBMIT']))
		{
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

			$this->form_validation->set_rules('user','Username','trim|required|xss_clean');
			$this->form_validation->set_rules('email','Email id','trim|required|xss_clean|valid_email');


			if($this->form_validation->run() != FALSE)
			{
				$user = $this->form_validation->xss_clean($this->input->post('user'));
				$email = $this->form_validation->xss_clean($this->input->post('email'));
				$this->siteadmin_model->update_setting($user,$email);

			}
		}

		$present_info = $this->siteadmin_model->admin_setting();
		$data['admin'] = $present_info;
		$data['include'] = 'siteadmin/dashboard/setting';
		$this->load->view('backend/container',$data);

	}


}