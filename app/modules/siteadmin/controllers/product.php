<?php
if(!defined('BASEPATH'))
exit('No direct script access allowed');

class Product extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('product_model');
		$this->load->model('category_model');
		$this->load->model('user_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->helper(array('form', 'url'));
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	//==================== all page session check =====================

	public function Checklogin()
	{
		if ($this->session->userdata('admin_email') == '')
		{
			redirect('siteadmin/');
		}
	}

	public function index()
	{

		$this->Checklogin();
		$data['result'] = $this->product_model->getall();

		$data['include'] = 'siteadmin/product/manage_product';
		$data['admin_section'] = 'manage_product';
		$this->load->view('backend/container', $data);
	}

	public function add()
	{
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'product';
			$data['category'] = $this->product_model->getcategory();
			$data['subcategory'] = $this->product_model->getsubcategory();
			$data['subsubcategory'] = $this->product_model->getsubsubcategory();
			$id = $this->product_model->add();
			if($id)
			{
				$this->session->set_flashdata('success', 'Product saved successfully.');
				redirect(base_url() . 'siteadmin/product');
			}
			else
			{
				$this->session->set_flashdata('error', 'Unable to save product.');
				$data['include'] = 'siteadmin/product/add_product';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/product/add_product';
			$data['category'] = $this->product_model->getcategory();
			$this->load->view('backend/container', $data);
		}

	}

	public function edit($id)
	{
		$data['info'] = $this->product_model->getRecord($id);
		$data['color'] = $this->product_model->getProductcolor($id);
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'product';
			$edit = $this->product_model->edit();
			if($edit)
			{
				$this->session->set_flashdata('success', 'Product has been updated successfully.');
				redirect(base_url() . 'siteadmin/product');
			}
			else
			{
				$this->session->set_flashdata('success', 'Unable to update product.');
				$data['include'] = 'siteadmin/product/edit_product';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/product/edit_product';
			$this->load->view('backend/container', $data);
		}
	}

	public function delete($id)
	{
		if ($this->category_model->delete($id))
		{
			$this->session->set_flashdata('success', 'Record has been deleted successfully.');
			redirect(base_url() . 'siteadmin/product');
		}
	}

	public function setbanner($id)
	{
		$this->product_model->setbanner($id);
		$this->session->set_flashdata('success', 'Product banner has been set successfully');
		redirect('siteadmin/product');
	}

	public function unsetbanner($id)
	{
		$this->product_model->unsetbanner($id);
		$this->session->set_flashdata('success', 'Product banner has been unset successfully');
		redirect('siteadmin/product');
	}
	public function setfeatured($id)
	{
		$this->product_model->setfeatured($id);
		$this->session->set_flashdata('success', 'Product has been set as featured successfully');
		redirect('siteadmin/product');
	}
	public function setunfeatured($id)
	{
		$this->product_model->setunfeatured($id);
		$this->session->set_flashdata('success', 'Product banner has been unset successfully');
		redirect('siteadmin/product');
	}
	public function activate($id)
	{
		$this->product_model->activate($id);
		$this->session->set_flashdata('success', 'Product has been activated successfully');
		redirect('siteadmin/product');
	}
	public function deactivate($id)
	{
		$this->product_model->deactivate($id);
		$this->session->set_flashdata('success', 'Product has been deactivated successfully');
		redirect('siteadmin/product');
	}

	public function deleteimage($id)
	{
		$this->product_model->deleteimage($id);
		$this->session->set_flashdata('success', 'Product has been deactivated successfully');
		redirect('siteadmin/product');
	}


	//Start Product Images Management

	public function manage_images($id)
	{
		$data['result'] = $this->product_model->getproduct_images($id);
		$data['product_id'] = $id;
		$data['include'] = 'siteadmin/product/manage_images';
		$data['admin_section'] = 'manage_images';
		$this->load->view('backend/container', $data);
	}  

	public function add_image($id)
	{
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'product';
			//$this->product_model->add_image();
			$product_id = $_POST['id'];
			$a = "";
			$config['upload_path'] = './uploads/product_images/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '2048';
			$config['max_width']  = '3000';
			$config['max_height']  = '3000';

			$this->load->library('image_lib', $config);
			$this->load->library('upload', $config);
			$i=0;
			if(isset($_FILES))
			{
				foreach($_FILES as $file=>$field)
				{
					if($this->upload->do_upload($file))
					{
						$data = $this->upload->data();
						$a = $data['file_name'];
					}
				}
			}
			$this->product_model->upload_product_image($product_id,$a);

			$this->session->set_flashdata('success', 'Product image has been added successfully.');
			redirect(base_url().'siteadmin/product/manage_images/'.$product_id);


			//            if($id)
			//            {
			//
			//                $this->session->set_flashdata('success', 'Brand added successfully.');
			//                redirect(base_url() . 'siteadmin/brand');
			//            }

		}
		else
		{
			$data['product_id'] = $id;
			$data['include'] = 'siteadmin/product/add_image';
			$this->load->view('backend/container', $data);
		}
	}

	public function activate_image($id,$product_id)
	{
		$this->product_model->activate_image($id);
		$this->session->set_flashdata('success', 'Product image has been activated successfully');
		redirect('siteadmin/product/manage_images/'.$product_id);
	}

	public function deactivate_image($id,$product_id)
	{
		$this->product_model->deactivate_image($id);
		$this->session->set_flashdata('success', 'Product image has been deactivated successfully');
		redirect('siteadmin/product/manage_images/'.$product_id);
	}
	public function manage_product_reviews()
	{
		$this->Checklogin();
		$data['result'] = $this->product_model->getProductReviews();
		$data['include'] = 'siteadmin/product/manage_product_reviews';
		$data['admin_section'] = 'manage_product_reviews';
		$this->load->view('backend/container', $data);
	}
	public function activate_review($id)
	{
		$this->product_model->activate_review($id);
		$this->session->set_flashdata('success', 'Product Review has been activated successfully');
		redirect('siteadmin/product/manage_product_reviews');
	}

	public function deactivate_review($id)
	{
		$this->product_model->deactivate_review($id);
		$this->session->set_flashdata('success', 'Product Review has been deactivated successfully');
		redirect('siteadmin/product/manage_product_reviews');
	}
}
