<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class request extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('request_model');
		$this->load->model('country_model');
		$this->load->model('publisher_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->helper(array('form', 'url'));
		$this->load->library('email');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	//==================== all page session check =====================
	public function Checklogin()
	{
		if ($this->session->userdata('username') == '')
		{
			redirect('siteadmin/');
		}
	}
	public function indexfilter()
	{
		$this->Checklogin(); 
		$data['result'] = $this->request_model->getall();
		$data['include'] = 'siteadmin/request_management/manage_request';
		$data['admin_section'] = 'manage_request';
		$this->load->view('backend/container', $data);
	}
	public function index($condtion = "")
	{
		$this->Checklogin(); 
		$data['condtion'] = $condtion;
		$data['countries'] = $this->request_model->GetCountries();
		$data['publishers'] = $this->publisher_model->getall();
		
		$last_date = date('Y-m-d', strtotime('-7 days'));
		$defaultCondition = "lead_date >= ~".$last_date."~ ";
		if("" == $condtion){
			 $data['result'] = $this->request_model->getall(); 
			/* $data['result'] = $this->request_model->getallByCond($defaultCondition); */
		}else{
			$data['result'] = $this->request_model->getallByCond($condtion);		
		}
		$data['include'] = 'siteadmin/request_management/manage_request';
		$data['admin_section'] = 'manage_request';
		$this->load->view('backend/container', $data);
	}
	public function index_new($page,$condition = "")
	{ 
		$this->Checklogin(); 
		$curr_page = $page;
		 if($page < 1){
			$page = 0;
			$data['curr_page'] = 1;
		}else{
			$data['curr_page'] = $curr_page;
			$page -= 1;
		}
	 	
	 	$per_page = 10;
	 	$start = $page * $per_page;
		$limit = "LIMIT $start, $per_page";
		$data['result'] = $this->request_model->getallByLimit($condition,$limit);		
		$data['countries'] = $this->request_model->GetCountries();
		$data['publishers'] = $this->publisher_model->getall();
		$data['condtion'] = $condition;
		$data['next_page'] = $curr_page + 1;
		if($curr_page == 1){
			$data['prev_page'] = 1;
		}else{
			$data['prev_page'] = $curr_page - 1;
		}
		
		$data['include'] = 'siteadmin/request_management/manage_request_new';
		$data['admin_section'] = 'manage_request';
		$this->load->view('backend/container', $data);
	}
public function getRequests($id)
	{	$this->Checklogin();
		$data['result'] = $this->request_model->getRequestsOfPublisher($id);
		$data['include'] = 'siteadmin/request_management/manage_request';
		$data['admin_section'] = 'manage_request';
		$this->load->view('backend/container', $data);
	}
	public function activate($id)
	{
		$this->request_model->activate($id);
		$this->session->set_flashdata('success', 'Request has been activated successfully');
		redirect('siteadmin/request');
	}
	public function DeleteRequest($id)
	{
		$this->request_model->delete($id);
		$this->session->set_flashdata('success', 'Request has been deleted successfully');
		redirect('siteadmin/request');
	}
	public function deactivate($id)
	{
		$this->request_model->deactivate($id);
		$this->session->set_flashdata('success', 'Request has been deactivated successfully');
		redirect('siteadmin/request');
	}

	public function send_mail($publisher_id,$request_id,$report_code)
	{
//		echo $publisher_id;
//		echo $request_id;
//		echo $report_code;
		
		$report = $this->db->query("select * from report where report_code = '".$report_code."'");
        $report_name = $report->result();	
		 $report_name[0]->report_name;
		 $report_name[0]->id;
		
		
		$request = $this->db->query("select * from request where id = '".$request_id."'");
        $request_name = $request->result();	
		 $request_name[0]->full_name;
		 $request_name[0]->email;
		 $request_name[0]->contact_no;
		
		
		
		
		$publisher_email = $this->getEmailId($publisher_id);
		$to = $publisher_email;
		
		
		$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>Untitled Document</title>
                        </head>

                        <body>
                        <table width="700"  align="center" height="458" border="0" style="border: solid 1px #EBEBEB;background: rgb(252, 252, 252);">
                          <tr bgcolor="#1e5b85">
                            <td colspan="2"  height="63" style="text-align:center; color:white; font-family:elephant; font-size:25px;
                            text-align:center; font-family:elephant;background-color: #1e5b85;">Orbis</td>

                          </tr>
                          <tr>
                            
                            <td colspan="2" width="864">
                            <table width="600" height="350" border="0" align="center" style="background:red; position: relative;background: rgb(245, 245, 245); border-radius:20px; margin-top:10px;> <h1 style="color:red;"></h1>
                       
                            <tr>
                                <td width="526" height="50" align="center">
                                    <h1 style="font-family:Georgia, Times New Roman, Times, serif; font-weight:bold;font-size:25px; color:#000; height:30px; width:95%; margin:0 auto; background:#f9f9f9;margin-top:0px; border:solid 1px ;#333;border: solid 1px #E4E4E4;padding-top:10px; margin-top: 5px; ">
                                        Request From Orbis
                                    </h1>
                                </td>
                              </tr>
                              <tr>
                                <td height="200"><p style="text-align:left; padding:10px 0px; font-family:arial, cursive; font-size:16px; color:black; padding:10px; margin-top:-60px; margin-left:10px;">
                                Report Name  : '.$report_name[0]->report_name.'<br><br>
                                 
                                  <h3>  &nbsp;&nbsp;&nbsp;&nbsp; Requested By :-</h3><br>
		                                  
                                  		&nbsp;&nbsp;&nbsp;&nbsp; Name : '.$request_name[0]->full_name.'<br>
		                            &nbsp;&nbsp;&nbsp;&nbsp;    Email Id : '.$request_name[0]->email.'<br>
		                         &nbsp;&nbsp;&nbsp;&nbsp; Contact Number : '. $request_name[0]->contact_no.'<br>
                                  
                               </p>
                                </td>
                              </tr>
                            </table></td>
                          </tr>

                        </table>
                    
					  <table width="700" height="60" border="0" style="top: 5px;
                        /*background: rgb(231, 231, 231);*/
						background-color: #1F1F1F; color:#FFF;
                        width: 700px;
                        margin: 0 auto;
                        bottom: 20px;

                        border-radius: 0px 0px 10px 10px;
                        border-bottom: 4px solid #DBDBDB;">

		
			
			           </table>
                        
						</body>
                        </html>';
			
			
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= "From:Orbis";
		$subject = "Request From Orbis";
		$from = "admin@orbis.com";

		$sent = mail($to,$subject,$message,$headers);
		if($sent)
		{
		
//		$request = $this->db->query("update request set mail_status='1' where id = '".$request_id."'");
//        $data=$request->result();	
        
			 $data = array('mail_status'=>'1');
			  $this->db->where('id',$request_id);
			  $this->db->update('request',$data);
			
		}
		$this->session->set_flashdata('success', 'Request has been send successfully.');
		redirect(base_url().'siteadmin/request');	
	}

	public function getEmailId($publisher_id)
	{
		$data = $this->request_model->getEmailId($publisher_id);
		return ($data[0]->email);
	}

     function clear_cache()
	{
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
	}

	// public function updateRegion()
	// {
	// 	 $data['result'] = $this->request_model->getall();
	// 	// echo "<pre>";
	// 	// print_r($data['result'][0]->country);
	// 	 foreach ($data['result']as $countryname) {
	// 	 	# code...
	// 	 $country=$countryname->country;
	// 	 $id=$countryname->id;
	// 	 $region=getEmailsRegion($country,'region');

	// 	 if($region !='ALL'){
		 	
	// 			$this->db->query("update request set region ='$region' where id='$id'");
	// 		}

	// 	 }
		 
	// }


}