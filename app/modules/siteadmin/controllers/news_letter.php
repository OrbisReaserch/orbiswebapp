<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News_letter extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('news_letter_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {

        $this->Checklogin();
        $data['result'] = $this->news_letter_model->getall();
       // print_r($data['result']);
		$data['include'] = 'siteadmin/news_letter/add_news';
        $data['admin_section'] = 'add_news';
        $this->load->view('backend/container', $data);
    }
    
    public function add_news_letter() 
    {
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'news_letter';
            //$id = $this->news_letter_model->add_news_letter();
             $news_title = $_POST['news_title'];
			 $news_description=$_POST['news_description'];
			 
			
                    $a = "";
                    $config['upload_path'] = './uploads/news_letter_images';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '2048';
                    $config['max_width']  = '3000';
                    $config['max_height']  = '3000';				

                    $this->load->library('image_lib', $config);
                    $this->load->library('upload', $config);
                    $i=0;
                         foreach($_FILES as $file=>$field)
                            {
                                    if($this->upload->do_upload($file))
                                    {
                                            $data = $this->upload->data();
                                            $a = $data['file_name'];
                                    }
                           
						    }
							
				$user['user_data'] = $this->news_letter_model->getall();
				//print_r($user['user_data']);
				for($i=0;$i<count($user['user_data']);$i++)
						{
							$to = $user['user_data'][$i]->email;
						 
						 
						$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>Untitled Document</title>
                        </head>

                        <body>
                        <table width="700"  align="center" height="458" border="0" style="border: solid 1px #EBEBEB;background: rgb(252, 252, 252);">
                          <tr bgcolor="#1e5b85">
                            <td colspan="2"  height="63" style="text-align:center; color:white; font-family:elephant; font-size:25px;
                            text-align:center; font-family:elephant;background-color: #1e5b85;">Orbis News Letter</td>

                          </tr>
                          <tr>
                            
                            <td colspan="2" width="864">
                            <table width="600" height="350" border="0" align="center" style="background:red; position: relative;background: rgb(245, 245, 245); border-radius:20px; margin-top:10px;> 
                            <h1 style="color:red;"></h1>
                       
                              <tr>
                            <td width="526" height="302" border="0" align="center" >
                            <img src="'.base_url().'uploads/news_letter_images/'.$a.'" height="200px" width="400"  style="margin-top:10px;"/>
                            </td>
                          </tr>

                            <tr>
                                <td width="526" height="50" align="center">
                                    <h1 style="font-family:Georgia, Times New Roman, Times, serif; font-weight:bold;font-size:25px; color:#000; height:30px; width:95%; margin:0 auto; background:#f9f9f9;margin-top:0px; border:solid 1px ;#333;border: solid 1px #E4E4E4;padding-top:10px; margin-top: 5px; ">
                                       '.$news_title.'
                                    </h1>
                                </td>
                              </tr>
                              <tr>
                                <td height="200"><p style="text-align:left; padding:10px 0px; font-family:arial, cursive; font-size:16px; color:black; padding:10px; margin-top:-60px; margin-left:10px;">
                                 '.$news_description.'
                                  
                               </p>
                                </td>
                              </tr>
                            </table></td>
                          </tr>

                        </table>
                    
					  <table width="700" height="60" border="0" style="top: 5px;
                        /*background: rgb(231, 231, 231);*/
						background-color: #1F1F1F; color:#FFF;
                        width: 700px;
                        margin: 0 auto;
                        bottom: 20px;

                        border-radius: 0px 0px 10px 10px;
                        border-bottom: 4px solid #DBDBDB;">

		
			
			           </table>
                        
						</body>
                        </html>';
							
					
							
						$headers  = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						$headers .= "From:Orbis";
						$subject = "News Letter From Orbis";
						$from = "admin@orbis.com";
						
						$sent = mail($to,$subject,$message,$headers);
						
						
						}
				
                    
                   // $id = $this->news_letter_model->upload_news_image($id,$a);					

                    $this->session->set_flashdata('success', 'News Letter has been send successfully.');
                    redirect(base_url().'siteadmin/news_letter');

       
           
        }
        else
        {
            $data['include'] = 'siteadmin/news_letter/add_news';
            $this->load->view('backend/container', $data);
        }
        
    }

   
    
  
    
}
