<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Country extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('country_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {

        $this->Checklogin();
        $data['result'] = $this->country_model->getall();
        $data['include'] = 'siteadmin/country/country_management';
        $data['admin_section'] = 'country_management';
        $this->load->view('backend/container', $data);
    }
    
    public function add() 
    {
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'country';
            $id = $this->country_model->add();
            if($id)
            { 
                    $this->session->set_flashdata('success', 'Country has been added successfully.');
                    redirect(base_url().'siteadmin/country');

            }
   
            else
            {
                $this->session->set_flashdata('error', 'Unable to save Advertisement.');
                $data['include'] = 'siteadmin/report_management/add_reports';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/country/add';
            $this->load->view('backend/container', $data);
        }
        
    }

    public function edit($id) 
    {
        $data['info'] = $this->country_model->getRecord($id);
		//print_r($reort['info']); exit();
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'country';
            $edit = $this->country_model->edit();
            
        if($edit)
            {
            	
            		            	
                    $this->session->set_flashdata('success', 'Report has been update successfully.');
                    redirect(base_url().'siteadmin/country');

            }
            
            else
            {
                $this->session->set_flashdata('success', 'Unable to update Report.');
                $data['include'] = 'siteadmin/country/edit';
                $this->load->view('backend/container', $data);
            }
        }
        else
        {
            $data['include'] = 'siteadmin/country/edit';
            $this->load->view('backend/container', $data);
        }
    }
    
    public function delete($id) 
    {
        if ($this->advertisement_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url() . 'siteadmin/advertisement');
        }
    }

    public function activate($id) 
    {
        $this->country_model->activate($id);
        $this->session->set_flashdata('success', 'Country has been activated successfully');
        redirect('siteadmin/country');
    }
    
    public function deactivate($id) 
    {
        $this->country_model->deactivate($id);
        $this->session->set_flashdata('success', 'Country has been deactivated successfully');
        redirect('siteadmin/country');
    }
    
}
