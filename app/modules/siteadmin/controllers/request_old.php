<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class request extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('request_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->helper(array('form', 'url'));
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	//==================== all page session check =====================

	public function index()
	{
		$data['result'] = $this->request_model->getall();
		$data['include'] = 'siteadmin/request_management/manage_request';
		$data['admin_section'] = 'manage_request';
		$this->load->view('backend/container', $data);
	}
	public function activate($id)
	{
		$this->request_model->activate($id);
		$this->session->set_flashdata('success', 'Request has been activated successfully');
		redirect('siteadmin/request');
	}
	public function deactivate($id)
	{
		$this->request_model->deactivate($id);
		$this->session->set_flashdata('success', 'Request has been deactivated successfully');
		redirect('siteadmin/request');
	}

	public function send_mail($publisher_id)
	{
		$publisher_email = $this->getEmailId($publisher_id);
		$to = $publisher_email;
		$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>Untitled Document</title>
                        </head>

                        <body>
                        <table width="864"  align="center"height="588" border="0" style="border: solid 1px #EBEBEB;background: rgb(252, 252, 252);">
                          <tr bgcolor="#041b32">
                            <td colspan="2"  height="63" style="text-align:center; color:white; font-family:elephant; font-size:25px;
                            text-align:center; font-family:elephant;background-color: #D30300;">Sanctus</td>

                          </tr>
                          <tr>
                            
                            <td colspan="2" width="864">
                            <table width="800" height="422" border="0" align="center" style="background:red; position: relative;background: rgb(245, 245, 245); border-radius:20px; margin-top:10px;> <h1 style="color:red;"></h1>

                            
                            
                          <tr>
                            <td width="526" height="302" border="0" align="center" ><img src="'.base_url().'uploads/news_letter_images'.$a.'" height="300px" width="554"  style="margin-top:10px;"/></td>
                          </tr>
                        
                            <tr>
                                <td width="526" height="70" align="center">
                                    <h1 style="font-family:Georgia, Times New Roman, Times, serif; font-weight:bold;font-size:25px; color:#000; height:40px; width:95%; margin:0 auto; background:#f9f9f9;margin-top:0px; border:solid 1px ;#333;border: solid 1px #E4E4E4;padding-top:10px; margin-top: 5px; border-radius:30px;">
                                        '.$news_title.'
                                    </h1>
                                </td>
                              </tr>
                              <tr>
                                <td height="258"><p style="text-align:left; padding:10px 0px; font-family:arial, cursive; font-size:16px; color:black; padding:10px;">
                                    '.$news_description.'
                                </td>
                              </tr>
                            </table></td>
                          </tr>

                        </table>
                    
					  <table width="831" height="145" border="0" style="top: 5px;
                        /*background: rgb(231, 231, 231);*/
						background-color: #1F1F1F; color:#FFF;
                        width: 866px;
                        margin: 0 auto;
                        bottom: 20px;

                        border-radius: 0px 0px 10px 10px;
                        border-bottom: 4px solid #DBDBDB;">

		
			
			           </table>
                        
						</body>
                        </html>';
			
			
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= "From:Litizen";
		$subject = "Request From Orbis";
		$from = "admin@orbis.com";

		$sent = mail($to,$subject,$message,$headers);
		$this->session->set_flashdata('success', 'Request has been send successfully.');
		redirect(base_url().'siteadmin/request');
	}

	public function getEmailId($publisher_id)
	{
		$data = $this->request_model->getEmailId($publisher_id);
		return ($data[0]->email);
	}





}
