<?php 

class Setting extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
		if(isset($_POST['SUBMIT']))
		{
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('user','Username','trim|required|xss_clean');
            $this->form_validation->set_rules('email','Email id','trim|required|xss_clean|valid_email');       
            $this->form_validation->set_rules('pass','Password','trim|required|xss_clean');


            if($this->form_validation->run() != FALSE)
            {	
				$user = $this->form_validation->xss_clean($this->input->post('user'));
				$email = $this->form_validation->xss_clean($this->input->post('email'));
				$pass = $this->form_validation->xss_clean($this->input->post('pass'));
				$this->load->model('siteadmin/siteadmin_model');
				$this->siteadmin_model->update_setting($user,$email,$pass);
				$this->session->set_flashdata('success', 'Settings has been Updated successfully.');
				redirect('siteadmin/');
			}
		}
		$this->load->model('siteadmin/siteadmin_model');
		$present_info = $this->siteadmin_model->admin_setting();
		$data['admin'] = $present_info;
		$data['include'] = 'siteadmin/dashboard/setting';
		$this->load->view('backend/container',$data);	

	}

}