<?php
class Csv extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('csv_model');
		$this->load->library('csvimport');
	}

	function index() {
		$data['addressbook'] = $this->csv_model->get_addressbook();
		//$this->load->view('csvindex', $data);
	}

	function importcsv() {
		//$data['addressbook'] = $this->csv_model->get_addressbook();
		$data['error'] = ''; //initialize image upload error array to empty
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'csv';
		$config['max_size'] = '1000';

		$this->load->library('upload', $config);
		// If upload failed, display error
		if (!$this->upload->do_upload()) {
			$data['error'] = $this->upload->display_errors();

//			$this->load->view('csvindex', $data);
		} else {
			$file_data = $this->upload->data();
			$file_path = './uploads/'.$file_data['file_name'];

			if ($this->csvimport->get_array($file_path)) {
				$csv_array = $this->csvimport->get_array($file_path);
				foreach ($csv_array as $row) {
					$insert_data = array('report_name'=>$row['report_name'],
					'report_description'=>$row['report_description'],
					'report_content'=>$row['report_content'],
					'list_of_tables'=>$row['list_of_tables'],
					'list_of_figures'=>$row['list_of_figures'],
					'report_price'=>$row['report_price']);
					$this->csv_model->insert_csv($insert_data);
				}
				$this->session->set_flashdata('successReport', 'Csv Data Imported Succesfully');
				redirect(base_url().'report');
				//echo "<pre>"; print_r($insert_data);
			} else
			$data['error'] = "Error occured";
			redirect(base_url().'report');
		}
	}

}
/*END OF FILE*/