<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_management extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('user_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {

        $this->Checklogin();
        $data['result'] = $this->user_model->getall();
        $data['include'] = 'siteadmin/user_management/manage_user';
        $data['admin_section'] = 'manage_user';
        $this->load->view('backend/container', $data);
    }
    
    public function add_user() 
    {
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'user';
            $id = $this->user_model->add_user();
            if($id)
            {
                   
                          $this->session->set_flashdata('success', 'User has been added successfully.');
                    redirect(base_url().'siteadmin/user_management');

            }
            else
            {
                $this->session->set_flashdata('error', 'Unable to save User.');
                $data['include'] = 'siteadmin/user_management/add_user';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/user_management/add_user';
            $this->load->view('backend/container', $data);
        }
        
    }

    public function edit_user($id) 
    {
        $data['info'] = $this->user_model->getRecord($id);
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'user';
            $edit = $this->user_model->edit_user();
            
        if($edit)
            {
                       $this->session->set_flashdata('success', 'User has been updated successfully.');
                    redirect(base_url().'siteadmin/user_management');

            }
            

            else
            {
                $this->session->set_flashdata('success', 'Unable to update User.');
                $data['include'] = 'siteadmin/user_mamagement/edit_user';
                $this->load->view('backend/container', $data);
            }
        }
        else
        {
            $data['include'] = 'siteadmin/user_management/edit_user';
            $this->load->view('backend/container', $data);
        }
    }
    
    public function delete($id) 
    {
        if ($this->advertisement_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url() . 'siteadmin/advertisement');
        }
    }

    public function activate_user($id) 
    {
        $this->user_model->activate_user($id);
        $this->session->set_flashdata('success', 'User has been activated successfully');
        redirect('siteadmin/user_management');
    }
    
    public function deactivate_user($id) 
    {
        $this->user_model->deactivate_user($id);
        $this->session->set_flashdata('success', 'User has been deactivated successfully');
        redirect('siteadmin/user_management');
    }
    
}
