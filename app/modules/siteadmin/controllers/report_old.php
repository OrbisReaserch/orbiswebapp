<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Report extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->model('report_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	//==================== all page session check =====================

	public function Checklogin()
	{
		if ($this->session->userdata('admin_email') == '')
		{
			redirect('siteadmin/');
		}
	}

	public function index()
	{

		$this->Checklogin();
		$data['result'] = $this->report_model->getall();
		$data['include'] = 'siteadmin/report_management/report_management';
		$data['admin_section'] = 'report_management';
		$this->load->view('backend/container', $data);
	}

	public function add_reports()
	{
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'report';
			$id = $this->report_model->add_reports();
			if($id)
			{
				$a = "";
				$config['upload_path'] = './uploads/report_images/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '2048';
				$config['max_width']  = '3000';
				$config['max_height']  = '3000';

				$this->load->library('image_lib', $config);
				$this->load->library('upload', $config);
				$i=0;
				if(isset($_FILES))
				{
					foreach($_FILES as $file=>$field)
					{
						if($this->upload->do_upload($file))
						{
							$data = $this->upload->data();
							$a = $data['file_name'];
						}
					}
				}
				$id = $this->report_model->upload_report_image($id,$a);

				$this->session->set_flashdata('success', 'Report has been added successfully.');
				redirect(base_url().'siteadmin/report');

			}

			else
			{
				$this->session->set_flashdata('error', 'Unable to save Advertisement.');
				$data['include'] = 'siteadmin/report_management/add_reports';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/report_management/add_reports';
			$this->load->view('backend/container', $data);
		}

	}

	public function edit_report($id)
	{
		$data['info'] = $this->report_model->getRecord($id);
		//print_r($reort['info']); exit();
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'report';
			$edit = $this->report_model->edit_report();

			if($edit)
			{
					
				$a = "";
				$config['upload_path'] = './uploads/report_images/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '2048';
				$config['max_width']  = '3000';
				$config['max_height']  = '3000';

				$this->load->library('image_lib', $config);
				$this->load->library('upload', $config);
				$i=0;
				if(isset($_FILES))
				{
					foreach($_FILES as $file=>$field)
					{
						if($this->upload->do_upload($file))
						{
							$data = $this->upload->data();
							$a = $data['file_name'];
						}
					}
				}
				$edit = $this->report_model->update_report_image($edit,$a);
					
				$this->session->set_flashdata('success', 'Report has been update successfully.');
				redirect(base_url().'siteadmin/report');

			}

			else
			{
				$this->session->set_flashdata('success', 'Unable to update Report.');
				$data['include'] = 'siteadmin/report/edit_report';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/report_management/edit_report';
			$this->load->view('backend/container', $data);
		}
	}

	public function delete($id)
	{
		if ($this->advertisement_model->delete($id))
		{
			$this->session->set_flashdata('success', 'Record has been deleted successfully.');
			redirect(base_url() . 'siteadmin/advertisement');
		}
	}

	public function activate_report($id)
	{
		$this->report_model->activate_report($id);
		$this->session->set_flashdata('success', 'Report has been activated successfully');
		redirect('siteadmin/report');
	}

	public function deactivate_report($id)
	{
		$this->report_model->deactivate_report($id);
		$this->session->set_flashdata('success', 'Report has been deactivated successfully');
		redirect('siteadmin/report');
	}


	public function read_excel()
	{
		$config['upload_path'] = './uploads/report_images/';
		$config['allowed_types'] = 'csv';
		$config['max_size']	= '2048';
		$config['max_width']  = '3000';
		$config['max_height']  = '3000';

		//$this->load->library('image_lib', $config);
		$this->load->library('upload', $config);
		$i=0;
		if(isset($_FILES))
		{
			foreach($_FILES as $file=>$field)
			{
				if($this->upload->do_upload($file))
				{
					$data = $this->upload->data();
					$a = $data['file_name'];
				}
			}
		}

		print_r($_FILES);die();
		$this->load->library('csvreader');
		$result = $this->csvreader->parse_file('Test.csv');

		$data['csvData'] =  $result;
		$this->load->view('view_csv', $data);
	}

}
