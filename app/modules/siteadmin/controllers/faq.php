<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Faq extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('faq_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('download');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {

        $this->Checklogin();
        $data['result'] = $this->faq_model->getall();
        $data['include'] = 'siteadmin/faq/manage_faq';
        $data['admin_section'] = 'manage_faq';
        $this->load->view('backend/container', $data);
    }

    public function add() 
    {
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'faq';
            $id = $this->faq_model->add();
            if($id)
            {
                $this->session->set_flashdata('success', 'FAQ Saved successfully.');
                redirect(base_url() . 'siteadmin/faq');        
            }
            else
            {
                $this->session->set_flashdata('error', 'Unable to save FAQ.');
                $data['include'] = 'siteadmin/faq/add_faq';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/faq/add_faq';
            $this->load->view('backend/container', $data);
        }
        
    }

    
    public function edit($id='') 
    {
        $data['info'] = $this->faq_model->getRecord($id);
	    if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'faq';
            $edit = $this->faq_model->edit();
            if($edit)
            {
                $this->session->set_flashdata('success', 'FAQ has been updated successfully.');
                redirect(base_url() . 'siteadmin/faq');
            }
            else
            {
                $this->session->set_flashdata('success', 'Unable to update FAQ.');
                $data['include'] = 'siteadmin/faq/edit_faq';
                $this->load->view('backend/container', $data);
            }
        }
        else
        {
            $data['include'] = 'siteadmin/faq/edit_faq';
            $this->load->view('backend/container', $data);
        }
    }

    
    public function activate($id) 
    {
        $this->faq_model->activate($id);
        $this->session->set_flashdata('success', 'FAQ has been activated successfully');
        redirect('siteadmin/faq');
    }

    public function deactivate($id) 
    {
        $this->faq_model->deactivate($id);
        $this->session->set_flashdata('success', 'FAQ has been deactivated successfully');
        redirect('siteadmin/faq');
    }
   
     
   }
