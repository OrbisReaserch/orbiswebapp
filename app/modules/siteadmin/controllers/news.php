<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('news_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {
		// die();
        $this->Checklogin();
        $data['result'] = $this->news_model->getall();
        $data['include'] = 'siteadmin/news/manage_news';
        $data['admin_section'] = 'manage_news';
        $this->load->view('backend/container', $data);
    }
    
    public function add_news() 
    {
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'news';
            $id = $this->news_model->add_news();
            if($id)
            {
                    $a = "";
                    $config['upload_path'] = './uploads/news_image';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '2048';
                    $config['max_width']  = '3000';
                    $config['max_height']  = '3000';				

                    $this->load->library('image_lib', $config);
                    $this->load->library('upload', $config);
                    $i=0;
                    if(isset($_FILES))
                    {
                            foreach($_FILES as $file=>$field)
                            {
                                    if($this->upload->do_upload($file))
                                    {
                                            $data = $this->upload->data();
                                            $a = $data['file_name'];
                                    }
                            }
                    }
                    $id = $this->news_model->upload_news_image($id,$a);					

                    $this->session->set_flashdata('success', 'News has been added successfully.');
                    redirect(base_url().'siteadmin/news');

            }
//            if($id)
//            {
//                $this->session->set_flashdata('success', 'News added successfully.');
//                redirect(base_url() . 'siteadmin/news');        
//            }
            else
            {
                $this->session->set_flashdata('error', 'Unable to save news.');
                $data['include'] = 'siteadmin/news/add_news';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/news/add_news';
            $this->load->view('backend/container', $data);
        }
        
    }

    public function edit_news($id) 
    {
        $data['info'] = $this->news_model->getRecord($id);
        if(isset($_POST['submit']))
        {
            $id=$_POST['news_id'];
            $this->Checklogin();
            $data['admin_section'] = 'news';
            $edit = $this->news_model->edit_news();
            if($edit)
            {
                    $b = "";
                    $config['upload_path'] = './uploads/news_image';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '2048';
                    $config['max_width']  = '3000';
                    $config['max_height']  = '3000';				

                    $this->load->library('image_lib', $config);
                    $this->load->library('upload', $config);
                    $j=0;
                    if(isset($_FILES))
                    {
                            foreach($_FILES as $file=>$field)
                            {
                                    if($this->upload->do_upload($file))
                                    {
                                            $data = $this->upload->data();
                                            $b = $data['file_name'];
                                    }
                            }
                    }
                    $edit = $this->news_model->upload_news_image_update($id,$b);					

                    $this->session->set_flashdata('success', 'News have been updated successfully.');
                    redirect(base_url().'siteadmin/news');

            }
//            if($edit)
//            {
//                $this->session->set_flashdata('success', 'News have been updated successfully.');
//                redirect(base_url() . 'siteadmin/news');
//            }
            else
            {
                $this->session->set_flashdata('success', 'Unable to update news.');
                $data['include'] = 'siteadmin/news/edit_news';
                $this->load->view('backend/container', $data);
            }
        }
        else
        {
            $data['include'] = 'siteadmin/news/edit_news';
            $this->load->view('backend/container', $data);
        }
    }
    
    public function delete($id) 
    {
        if ($this->news_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url() . 'siteadmin/news');
        }
    }

    public function activate_news($id) 
    {
        $this->news_model->activate_news($id);
        $this->session->set_flashdata('success', 'News has been activated successfully');
        redirect('siteadmin/news');
    }
    
    public function deactivate_news($id) 
    {
        $this->news_model->deactivate_news($id);
        $this->session->set_flashdata('success', 'News has been deactivated successfully');
        redirect('siteadmin/news');
    }
    
}
