<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Category extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->model('category_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');

		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	//==================== all page session check =====================

	public function Checklogin()
	{
		if ($this->session->userdata('admin_email') == '')
		{
			redirect('siteadmin/');
		}
	}

	public function index()
	{

		$this->Checklogin();
		$data['result'] = $this->category_model->getall();
		$data['include'] = 'siteadmin/category/manage_category';
		$data['admin_section'] = 'manage_category';
		$this->load->view('backend/container', $data);
	}
	public function manage_sub_category()
	{

		$this->Checklogin();
		$data['result'] = $this->category_model->getallSubCategory();
		$data['include'] = 'siteadmin/category/manage_sub_category';
		$data['admin_section'] = 'manage_sub_category';
		$this->load->view('backend/container', $data);
	}

	public function manage_sub_sub_category()
	{

		$this->Checklogin();
		$data['result'] = $this->category_model->getsub_subcategory();
		$data['include'] = 'siteadmin/category/manage_sub_sub_category';
		$data['admin_section'] = 'manage_sub_sub_category';
		$this->load->view('backend/container', $data);
	}

	public function add()
	{
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'category';
			$id = $this->category_model->add();
			if($id)
			{
				$a = "";
				$config['upload_path'] = './uploads/category_icons/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '2048';
				$config['max_width']  = '3000';
				$config['max_height']  = '3000';

				$this->load->library('image_lib', $config);
				$this->load->library('upload', $config);
				$i=0;
				if(isset($_FILES['category_image']))
				{
					//print_r($_FILES['category_image']);die();
					foreach($_FILES as $file=>$field)
					{
						if($this->upload->do_upload($file))
						{
							$data = $this->upload->data();
							$a = $data['file_name'];
						}
					}
				}
				$id = $this->category_model->upload_category_image($id,$a);
				$this->session->set_flashdata('success', 'Category saved successfully.');
				redirect(base_url() . 'siteadmin/category');
			}
			else
			{
				$this->session->set_flashdata('error', 'Unable to save category.');
				$data['include'] = 'siteadmin/category/add';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/category/add';
			$this->load->view('backend/container', $data);
		}

	}

	public function add_sub_category()
	{
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'category';
			$id = $this->category_model->add_sub_category();
			if($id)
			{
				$this->session->set_flashdata('success', 'Sub Category saved successfully.');
				redirect(base_url() . 'siteadmin/category/manage_sub_category');
			}
			else
			{
				$this->session->set_flashdata('error', 'Unable to save sub category.');
				$data['include'] = 'siteadmin/category/add_sub_category';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/category/add_sub_category';
			$this->load->view('backend/container', $data);
		}

	}

	function add_sub_sub_category()
	{
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'manage_sub_sub_category';
			$id = $this->category_model->add_sub_sub_category();
			if($id)
			{
				$this->session->set_flashdata('success', 'Sub To Sub Category saved successfully.');
				redirect(base_url() . 'siteadmin/category/manage_sub_sub_category');
			}
			else
			{
				$this->session->set_flashdata('error', 'Unable to save subtosub category.');
				$data['include'] = 'siteadmin/category/manage_sub_sub_category';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/category/add_sub_sub_category';
			$this->load->view('backend/container', $data);
		}

	}

	public function edit($id)
	{
		
		$data['info'] = $this->category_model->getRecord($id);
		if(isset($_POST['category_id']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'category';
			$edit = $this->category_model->edit();
			if($edit)
			{
				$a = "";
				$config['upload_path'] = './uploads/category_icons/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '2048';
				$config['max_width']  = '3000';
				$config['max_height']  = '3000';

				$this->load->library('image_lib', $config);
				$this->load->library('upload', $config);
				$i=0;
				if(isset($_FILES['category_image']))
				{
					//print_r($_FILES['category_image']);die();
					foreach($_FILES as $file=>$field)
					{
						if($this->upload->do_upload($file))
						{
							$data = $this->upload->data();
							$a = $data['file_name'];
						}
					}
				}
				$id = $this->category_model->upload_category_image($id,$a);
					
				$this->session->set_flashdata('success', 'Category has been updated successfully.');
				redirect(base_url() . 'siteadmin/category');
			}
			else
			{
				$this->session->set_flashdata('success', 'Unable to update category.');
				$data['include'] = 'siteadmin/category/edit';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/category/edit';
			$this->load->view('backend/container', $data);
		}
	}

	public function edit_sub_category($id)
	{
		$data['info'] = $this->category_model->getRecordSubCategory($id);
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'category';
			$edit = $this->category_model->edit_sub_category();
			if($edit)
			{
				$this->session->set_flashdata('success', 'Sub Category has been updated successfully.');
				redirect(base_url() . 'siteadmin/category/manage_sub_category');
			}
			else
			{
				$this->session->set_flashdata('success', 'Unable to update sub category.');
				$data['include'] = 'siteadmin/category/edit_sub_category';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/category/edit_sub_category';
			$this->load->view('backend/container', $data);
		}
	}

	public function edit_sub_sub_category($id)
	{
		$data['info'] = $this->category_model->getRecordSubtoSubCategory($id);
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'category';
			$edit = $this->category_model->edit_sub_sub_category();
			if($edit)
			{
				$this->session->set_flashdata('success', 'Sub Category has been updated successfully.');
				redirect(base_url() . 'siteadmin/category/manage_sub_sub_category');
			}
			else
			{
				$this->session->set_flashdata('success', 'Unable to update sub category.');
				$data['include'] = 'siteadmin/category/edit_sub_sub_category';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/category/edit_sub_sub_category';
			$this->load->view('backend/container', $data);
		}
	}

	public function delete($id)
	{
		if ($this->category_model->delete($id))
		{
			$this->session->set_flashdata('success', 'Record has been deleted successfully.');
			redirect(base_url() . 'siteadmin/category');
		}
	}

	public function activate($id)
	{
		$this->category_model->activate($id);
		$this->session->set_flashdata('success', 'Category has been activated successfully');
		redirect('siteadmin/category');
	}
	public function activate_sub_category($id)
	{
		$this->category_model->activate_sub_category($id);
		$this->session->set_flashdata('success', 'Sub Category has been activated successfully');
		redirect('siteadmin/category/manage_sub_category');
	}

	public function activate_sub_sub_category($id)
	{
		$this->category_model->activate_sub_sub_category($id);
		$this->session->set_flashdata('success', 'Sub Category has been activated successfully');
		redirect('siteadmin/category/manage_sub_sub_category');
	}

	public function deactivate($id)
	{
		$this->category_model->deactivate($id);
		$this->session->set_flashdata('success', 'Category has been deactivated successfully');
		redirect('siteadmin/category');
	}
	public function deactivate_sub_category($id)
	{
		$this->category_model->deactivate_sub_category($id);
		$this->session->set_flashdata('success', 'Sub Category has been deactivated successfully');
		redirect('siteadmin/category/manage_sub_category');
	}
	public function deactivate_sub_sub_category($id)
	{
		$this->category_model->deactivate_sub_sub_category($id);
		$this->session->set_flashdata('success', 'Sub Category has been deactivated successfully');
		redirect('siteadmin/category/manage_sub_sub_category');
	}

}
