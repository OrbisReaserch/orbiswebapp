<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Testimonials extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('testimonials_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {

        $this->Checklogin();
        $data['result'] = $this->testimonials_model->getall();
        $data['include'] = 'siteadmin/testimonials/manage_testimonials';
        $data['admin_section'] = 'manage_testimonials';
        $this->load->view('backend/container', $data);
    }
    
    public function add_testimonials() 
    {
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'testimonials';
            $id = $this->testimonials_model->add_testimonials();
            if($id)
            {
                   	

                    $this->session->set_flashdata('success', 'Testimonials has been added successfully.');
                    redirect(base_url().'siteadmin/testimonials');

            }

            else
            {
                $this->session->set_flashdata('error', 'Unable to save testimonials.');
                $data['include'] = 'siteadmin/testimonials/add_testimonials';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/testimonials/add_testimonials';
            $this->load->view('backend/container', $data);
        }
        
    }

    public function edit_testimonials($id) 
    {
        $data['info'] = $this->testimonials_model->getRecord($id);
        if(isset($_POST['submit']))
        {
            $testimonials_id=$_POST['testimonials_id'];
            $this->Checklogin();
            $data['admin_section'] = 'testimonials';
            $edit = $this->testimonials_model->edit_testimonials($testimonials_id);
            if($edit)
            {
                  			

                    $this->session->set_flashdata('success', 'Testimonials have been updated successfully.');
                    redirect(base_url().'siteadmin/testimonials');

            }
//            if($edit)
//            {
//                $this->session->set_flashdata('success', 'News have been updated successfully.');
//                redirect(base_url() . 'siteadmin/news');
//            }
            else
            {
                $this->session->set_flashdata('success', 'Unable to update testimonials.');
                $data['include'] = 'siteadmin/testimonials/edit_testimonials';
                $this->load->view('backend/container', $data);
            }
        }
        else
        {
            $data['include'] = 'siteadmin/testimonials/edit_testimonials';
            $this->load->view('backend/container', $data);
        }
    }
    
    public function delete($id) 
    {
        if ($this->news_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url() . 'siteadmin/news');
        }
    }

    public function activate_testimonials($id) 
    {
        $this->testimonials_model->activate_testimonials($id);
        $this->session->set_flashdata('success', 'Testimonials has been activated successfully');
        redirect('siteadmin/testimonials');
    }
    
    public function deactivate_testimonials($id) 
    {
        $this->testimonials_model->deactivate_testimonials($id);
        $this->session->set_flashdata('success', 'Testimonials has been deactivated successfully');
        redirect('siteadmin/testimonials');
    }
    
}
