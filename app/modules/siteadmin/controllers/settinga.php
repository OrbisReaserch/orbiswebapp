<?php 

class Setting extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// If admin is already logged in, redirect admin to dashboard.
		if($this->session->userdata('admin_email') == '')
		{                    
		   redirect('siteadmin/');
		}
		
		$data['include'] = 'siteadmin/dashboard/setting';
		$this->load->view('backend/container',$data);	
	}

}