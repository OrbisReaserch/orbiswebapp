<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Report extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('report_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		//$this->load->library('csvimport');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	//==================== all page session check =====================

	public function Checklogin()
	{
		if ($this->session->userdata('admin_email') == '')
		{
			redirect('siteadmin/');
		}
	}

	public function index()
	{
		$this->Checklogin();
		$data['result'] = $this->report_model->getall();
		$data['include'] = 'siteadmin/report_management/report_management';
		$data['admin_section'] = 'report_management';
		$this->load->view('backend/container', $data);
	}
public function getReports($id)
	{
		$this->Checklogin();
		$data['result'] = $this->report_model->getReportsOfPublisher($id);
		$data['include'] = 'siteadmin/report_management/report_management';
		$data['admin_section'] = 'report_management';
		$this->load->view('backend/container', $data);
	}

	public function add_reports()
	{
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'report';
			$id = $this->report_model->add_reports();
			if($id)
			{
				$a = "";
				$config['upload_path'] = './uploads/report_images/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '2048';
				$config['max_width']  = '3000';
				$config['max_height']  = '3000';

				$this->load->library('image_lib', $config);
				$this->load->library('upload', $config);
				$i=0;
				if(isset($_FILES))
				{
					foreach($_FILES as $file=>$field)
					{
						if($this->upload->do_upload($file))
						{
							$data = $this->upload->data();
							$a = $data['file_name'];
						}
					}
				}
				$id = $this->report_model->upload_report_image($id,$a);

				$this->session->set_flashdata('success', 'Report has been added successfully.');
				redirect(base_url().'siteadmin/report');

			}

			else
			{
				$this->session->set_flashdata('error', 'Unable to save Advertisement.');
				$data['include'] = 'siteadmin/report_management/add_reports';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/report_management/add_reports';
			$this->load->view('backend/container', $data);
		}

	}

	public function edit_report($id)
	{
		$data['info'] = $this->report_model->getRecord($id);
		//print_r($reort['info']); exit();
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'report';
			$edit = $this->report_model->edit_report();

			if($edit)
			{
					
				$a = "";
				$config['upload_path'] = './uploads/report_images/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '2048';
				$config['max_width']  = '3000';
				$config['max_height']  = '3000';

				$this->load->library('image_lib', $config);
				$this->load->library('upload', $config);
				$i=0;
				if(isset($_FILES))
				{
					foreach($_FILES as $file=>$field)
					{
						if($this->upload->do_upload($file))
						{
							$data = $this->upload->data();
							$a = $data['file_name'];
						}
					}
				}
				$edit = $this->report_model->update_report_image($edit,$a);
					
				$this->session->set_flashdata('success', 'Report has been update successfully.');
				redirect(base_url().'siteadmin/report');

			}

			else
			{
				$this->session->set_flashdata('success', 'Unable to update Report.');
				$data['include'] = 'siteadmin/report/edit_report';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/report_management/edit_report';
			$this->load->view('backend/container', $data);
		}
	}

	public function delete($id)
	{
		if ($this->advertisement_model->delete($id))
		{
			$this->session->set_flashdata('success', 'Record has been deleted successfully.');
			redirect(base_url() . 'siteadmin/advertisement');
		}
	}

	public function activate_report($id)
	{
		$this->report_model->activate_report($id);
		$this->session->set_flashdata('success', 'Report has been activated successfully');
		redirect('siteadmin/report');
	}

	public function delete_report($id)
	{
		$this->report_model->delete_report($id);
		$this->session->set_flashdata('success', 'Report has been deactivated successfully');
		redirect('siteadmin/report');
	}

	public function featured_report($id)
	{
		$this->report_model->featured_report($id);
		$this->session->set_flashdata('success', 'Report has been featured successfully');
		redirect('siteadmin/report');
	}

	public function notfeatured_report($id)
	{
		$this->report_model->notfeatured_report($id);
		$this->session->set_flashdata('success', 'Report has been deactivated successfully');
		redirect('siteadmin/report');
	}


	public function read_excel()
	{
            if(isset($_POST['submit']))
            {
               
                $file_name = time().$_FILES['bulk_upload']['name'];


                // $ext = pathinfo($file_name, PATHINFO_EXTENSION)

                //echo $ext;
                //exit;
				//echo "Total Sheets in this xls file: ".count($data->sheets)."<br /><br />";

				//$html="<table border='1'>";
                
                //$index=count($file_name);
                
               		//$a = "";
                    //$config['upload_path'] = 'uploads/report_file/';
                    //$config['allowed_types'] = '*';
  //  $config['allowed_types'] = 'text/plain|text/anytext|csv|text/x-comma-separated-values|text/comma-separated-values|application/octet-stream|application/vnd.ms-excel|application/x-csv|text/x-csv|text/csv|application/csv|application/excel|application/vnd.msexcel';
                    //$config['max_size']	= '2048';
                    

                    // $this->load->library('image_lib', $config);
                    //$this->load->library('upload', $config);
                   
                    if(isset($_FILES))
                   { 


 						$target = 'uploads/report_file/'.$file_name;
 						move_uploaded_file( $_FILES['bulk_upload']['tmp_name'], $target);	
 						chmod($target,0777);


                        /*foreach($_FILES as $file=>$field)
                            {
                            		if (!$this->upload->do_upload()):
								      $error = array('error' => $this->upload->display_errors());
								      var_dump($error);
								    else:
								      $data = array('upload_data' => $this->upload->data());
								    endif;

                                    if($this->upload->do_upload($file))
                                    {
                                    	
                                            $data = $this->upload->data();
                                            $a = $data['file_name'];
                                           
                                    }
                            }*/
                    }

                    
                   $id = $this->report_model->bulkUpload($target);
                   
                    if($id)
                    {
                            $this->session->set_flashdata('success_file', 'Excel Upload done successfully');
                            redirect(base_url() . 'siteadmin/report');
                    }
                    else
                    {
                            $this->session->set_flashdata('error_file', 'Unable to upload Excel file.');
                            $data['include'] = 'siteadmin/report';
                    }
           }				
	}
	
	public function getdata()
	{
		$data['include'] = 'report/sitemap';
		$this->load->view('report/sitemap',$data);
	}

}


/*for($i=0;$i<count($data->sheets);$i++) // Loop to get all sheets in a file.
				{	
					if(count($data->sheets[$i][cells])>0) // checking sheet not empty
					{
						//echo "Sheet $i:<br /><br />Total rows in sheet $i  ".count($data->sheets[$i][cells])."<br />";
						for($j=1;$j<=count($data->sheets[$i][cells]);$j++) // loop used to get each row of the sheet
						{ 
							
							$eid = $eid = mysqli_real_escape_string($connection,$data->sheets[$i][cells][$j][1]);
							$name = mysqli_real_escape_string($connection,$data->sheets[$i][cells][$j][2]);
							$email = mysqli_real_escape_string($connection,$data->sheets[$i][cells][$j][3]);
							$dob = mysqli_real_escape_string($connection,$data->sheets[$i][cells][$j][4]);
							$query = "insert into excel(eid,name,email,dob) values('".$eid."','".$name."','".$email."','".$dob."')";
							
							mysqli_query($connection,$query);
							$html.="</tr>";
						}
					}
					
				} 	*/

                /*$index = count($file_name);
                if($file_name[$index-1]=='csv')
                {
                    $a = "";
                    $config['upload_path'] = 'uploads/report_file/';
                    $config['allowed_types'] = '*';
  //  $config['allowed_types'] = 'text/plain|text/anytext|csv|text/x-comma-separated-values|text/comma-separated-values|application/octet-stream|application/vnd.ms-excel|application/x-csv|text/x-csv|text/csv|application/csv|application/excel|application/vnd.msexcel';
                    //$config['max_size']	= '2048';
                    

                    // $this->load->library('image_lib', $config);
                    $this->load->library('upload', $config);
                    $i=0;
                    if(isset($_FILES))
                    {
                        foreach($_FILES as $file=>$field)
                            {
                                
                                    if($this->upload->do_upload($file))
                                    {
                                            $data = $this->upload->data();
                                            $a = $data['file_name'];
                                           
                                    }
                            }
                    }
                    $id = $this->report_model->bulkUpload($a);
                    if($id)
                    {
                            $this->session->set_flashdata('success_file', 'CSV Upload done successfully');
                            redirect(base_url() . 'siteadmin/report');
                    }
                    else
                    {
                            $this->session->set_flashdata('error_file', 'Unable to upload CSV file.');
                            $data['include'] = 'siteadmin/report';
                    }
                   
                }
                else
                {
                    $this->session->set_flashdata('error_file', 'Please upload CSV file Only.');
                    redirect(base_url() . 'siteadmin/report');
                }
            }*/