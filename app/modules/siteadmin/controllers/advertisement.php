<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Advertisement extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('advertisement_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {

        $this->Checklogin();
        $data['result'] = $this->advertisement_model->getall();
        $data['include'] = 'siteadmin/advertisement/manage_advertisement';
        $data['admin_section'] = 'manage_advertisement';
        $this->load->view('backend/container', $data);
    }
    
    public function add_advertisement() 
    {
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'advertisement';
            $id = $this->advertisement_model->add_advertisement();
            if($id)
            {
                    $a = "";
                    $config['upload_path'] = './uploads/advertisement_images/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '2048';
                    $config['max_width']  = '3000';
                    $config['max_height']  = '3000';				

                    $this->load->library('image_lib', $config);
                    $this->load->library('upload', $config);
                    $i=0;
                    if(isset($_FILES))
                    {
                            foreach($_FILES as $file=>$field)
                            {
                                    if($this->upload->do_upload($file))
                                    {
                                            $data = $this->upload->data();
                                            $a = $data['file_name'];
                                    }
                            }
                    }
                    $id = $this->advertisement_model->upload_advertisement_image($id,$a);					

                    $this->session->set_flashdata('success', 'Advertisement has been added successfully.');
                    redirect(base_url().'siteadmin/advertisement');

            }
//            if($id)
//            {
//                
//                $this->session->set_flashdata('success', 'Brand added successfully.');
//                redirect(base_url() . 'siteadmin/brand');        
//            }
            else
            {
                $this->session->set_flashdata('error', 'Unable to save Advertisement.');
                $data['include'] = 'siteadmin/advertisement/add_advertisement';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/advertisement/add_advertisement';
            $this->load->view('backend/container', $data);
        }
        
    }

    public function edit_advertisement($id) 
    {
        $data['info'] = $this->advertisement_model->getRecord($id);
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'advertisement';
            $edit = $this->advertisement_model->edit_advertisement();
            
        if($edit)
            {
                    $a = "";
                    $config['upload_path'] = './uploads/advertisement_images/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '2048';
                    $config['max_width']  = '3000';
                    $config['max_height']  = '3000';				

                    $this->load->library('image_lib', $config);
                    $this->load->library('upload', $config);
                    $i=0;
                    if(isset($_FILES))
                    {
                            foreach($_FILES as $file=>$field)
                            {
                                    if($this->upload->do_upload($file))
                                    {
                                            $data = $this->upload->data();
                                            $a = $data['file_name'];
                                    }
                            }
                    }
                    $edit = $this->advertisement_model->update_advertisement_image($edit,$a);					

                    $this->session->set_flashdata('success', 'Advertisement has been update successfully.');
                    redirect(base_url().'siteadmin/advertisement');

            }
            
//            if($edit)
//            {
//                $this->session->set_flashdata('success', 'Advertisement have been updated successfully.');
//                redirect(base_url() . 'siteadmin/advertisement');
//            }
            else
            {
                $this->session->set_flashdata('success', 'Unable to update Advertisement.');
                $data['include'] = 'siteadmin/advertisement/edit_advertisement';
                $this->load->view('backend/container', $data);
            }
        }
        else
        {
            $data['include'] = 'siteadmin/advertisement/edit_advertisement';
            $this->load->view('backend/container', $data);
        }
    }
    
    public function delete($id) 
    {
        if ($this->advertisement_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url() . 'siteadmin/advertisement');
        }
    }

    public function activate_advertisement($id) 
    {
        $this->advertisement_model->activate_advertisement($id);
        $this->session->set_flashdata('success', 'Advertisement has been activated successfully');
        redirect('siteadmin/advertisement');
    }
    
    public function deactivate_advertisement($id) 
    {
        $this->advertisement_model->deactivate_advertisement($id);
        $this->session->set_flashdata('success', 'Advertisement has been deactivated successfully');
        redirect('siteadmin/advertisement');
    }
    
}
