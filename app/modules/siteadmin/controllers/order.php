<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Order extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('order_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		//$this->load->library('csvimport');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	//==================== all page session check =====================

	public function Checklogin()
	{
		if ($this->session->userdata('admin_email') == '')
		{
			redirect('siteadmin/');
		}
	}

	public function index()
	{
		
		$data['result'] = $this->order_model->getall();
		$data['include'] = 'siteadmin/order_management/manage_orders';
		$data['admin_section'] = 'order_management';
		$this->load->view('backend/container', $data);
	}
public function guestOrders()
	{
		
		$data['result'] = $this->order_model->getallGuestOrders();
		$data['include'] = 'siteadmin/order_management/manage_guest_orders';
		$data['admin_section'] = 'order_management';
		$this->load->view('backend/container', $data);
	}
	public function deleverReport($id)
	{
		$this->order_model->deleverReport($id);
		$this->session->set_flashdata('success', 'Report has been sent successfully');
		redirect('siteadmin/order');
	}
public function deleverGuestReport($id)
	{
		$this->order_model->deleverGuestReport($id);
		$this->session->set_flashdata('success', 'Report has been sent successfully');
		redirect('siteadmin/order');
	}

}
