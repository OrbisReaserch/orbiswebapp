<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class contact_info extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->model('contact_info_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->library('email');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	//==================== all page session check =====================

	public function Checklogin()
	{
		if ($this->session->userdata('admin_email') == '')
		{
			redirect('siteadmin/');
		}
	}

	public function index()
	{
		$this->Checklogin();
		$data['result'] = $this->contact_info_model->getall();
		$data['include'] = 'siteadmin/contact_info/contact_management';
		$data['admin_section'] = 'contact_management';
		$this->load->view('backend/container', $data);
	}
	public function add()
	{
		if(isset($_POST['submit']))
		{
			//			$this->Checklogin();
			$data['admin_section'] = 'contact_info';
			$id = $this->contact_info_model->add();
			if($id)
			{
				$this->session->set_flashdata('success', 'Contact information saved successfully.');
				redirect(base_url() . 'siteadmin/contact_info');
			}
			else
			{
				$this->session->set_flashdata('error', 'Unable to save contact information.');
				$data['include'] = 'siteadmin/contact_info/add_contact_info';
				$this->load->view('backend/container', $data);
			}
		}
		else
		{
			$data['include'] = 'siteadmin/contact_info/add_contact_info';
			$this->load->view('backend/container', $data);
		}

	}

	public function edit($id)
	{

		//print_r($reort['info']); exit();
		if(isset($_POST['submit']))
		{
			$data['admin_section'] = 'contact_info';
			$edit = $this->contact_info_model->edit();
			if($edit)
			{
				$this->session->set_flashdata('success', 'Contact has been update successfully.');
				redirect(base_url().'siteadmin/contact_info');
			}
			else
			{
				$this->session->set_flashdata('success', 'Contact has been update successfully.');
				redirect(base_url().'siteadmin/contact_info');
			}
		}
		else
		{
			$data['info'] = $this->contact_info_model->getRecord($id);
			$data['include'] = 'siteadmin/contact_info/edit_contact_info';
			$this->load->view('backend/container', $data);
		}
	}

	public function delete($id)
	{
		if ($this->advertisement_model->delete($id))
		{
			$this->session->set_flashdata('success', 'Record has been deleted successfully.');
			redirect(base_url() . 'siteadmin/advertisement');
		}
	}

	public function activate($id)
	{
		$this->contact_info_model->activate($id);
		$this->session->set_flashdata('success', 'Contact has been activated successfully');
		redirect('siteadmin/contact_info');
	}

	public function deactivate($id)
	{
		$this->contact_info_model->deactivate($id);
		$this->session->set_flashdata('success', 'Contact has been deactivated successfully');
		redirect('siteadmin/contact_info');
	}

}
