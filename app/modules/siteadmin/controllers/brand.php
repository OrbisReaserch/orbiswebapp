<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Brand extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('brand_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {

        $this->Checklogin();
        $data['result'] = $this->brand_model->getall();
        $data['include'] = 'siteadmin/brand/manage_brand';
        $data['admin_section'] = 'manage_brand';
        $this->load->view('backend/container', $data);
    }
    
    public function add_brand() 
    {
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'brand';
            $id = $this->brand_model->add_brand();
            if($id)
            {
                    $a = "";
                    $config['upload_path'] = './uploads/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '2048';
                    $config['max_width']  = '3000';
                    $config['max_height']  = '3000';				

                    $this->load->library('image_lib', $config);
                    $this->load->library('upload', $config);
                    $i=0;
                    if(isset($_FILES))
                    {
                            foreach($_FILES as $file=>$field)
                            {
                                    if($this->upload->do_upload($file))
                                    {
                                            $data = $this->upload->data();
                                            $a = $data['file_name'];
                                    }
                            }
                    }
                    $id = $this->brand_model->upload_brand_image($id,$a);					

                    $this->session->set_flashdata('success', 'Brand has been added successfully.');
                    redirect(base_url().'siteadmin/brand');

            }
//            if($id)
//            {
//                
//                $this->session->set_flashdata('success', 'Brand added successfully.');
//                redirect(base_url() . 'siteadmin/brand');        
//            }
            else
            {
                $this->session->set_flashdata('error', 'Unable to save brand.');
                $data['include'] = 'siteadmin/brand/add_brand';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/brand/add_brand';
            $this->load->view('backend/container', $data);
        }
        
    }

    public function edit_brand($id) 
    {
        
        $data['info'] = $this->brand_model->getRecord($id);
        if(isset($_POST['submit']))
        {
            $id=$_POST['brand_id'];
            $this->Checklogin();
            $data['admin_section'] = 'brand';
            $edit = $this->brand_model->edit_brand();
            if($edit)
            {
                    $b = "";
                    $config['upload_path'] = './uploads/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '2048';
                    $config['max_width']  = '3000';
                    $config['max_height']  = '3000';				

                    $this->load->library('image_lib', $config);
                    $this->load->library('upload', $config);
                    $j=0;
                    if(isset($_FILES))
                    {
                            foreach($_FILES as $file=>$field)
                            {
                                    if($this->upload->do_upload($file))
                                    {
                                            $data = $this->upload->data();
                                            $b = $data['file_name'];
                                    }
                            }
                    }
                    $edit = $this->brand_model->upload_edit_brand_image($id,$b);					

                    $this->session->set_flashdata('success', 'Brand have been updated successfully.');
                    redirect(base_url().'siteadmin/brand');

            }
//            if($edit)
//            {
//                $this->session->set_flashdata('success', 'Brand have been updated successfully.');
//                redirect(base_url() . 'siteadmin/brand');
//            }
            else
            {
                $this->session->set_flashdata('success', 'Unable to update brand.');
                $data['include'] = 'siteadmin/brand/edit_brand';
                $this->load->view('backend/container', $data);
            }
        }
        else
        {
            $data['include'] = 'siteadmin/brand/edit_brand';
            $this->load->view('backend/container', $data);
        }
    }
    
    public function delete($id) 
    {
        if ($this->brand_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url() . 'siteadmin/brand');
        }
    }

    public function activate_brand($id) 
    {
        $this->brand_model->activate_brand($id);
        $this->session->set_flashdata('success', 'Brand has been activated successfully');
        redirect('siteadmin/brand');
    }
    
    public function deactivate_brand($id) 
    {
        $this->brand_model->deactivate_brand($id);
        $this->session->set_flashdata('success', 'Brand has been deactivated successfully');
        redirect('siteadmin/brand');
    }
    
}
