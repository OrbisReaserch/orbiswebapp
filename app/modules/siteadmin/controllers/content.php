<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Content extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('content_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {
        $this->Checklogin();
        $data['result'] = $this->content_model->getall();
        $data['include'] = 'siteadmin/content/manage_content';
        $data['admin_section'] = 'manage_content';
        $this->load->view('backend/container', $data);
    }
    
    public function add_content() 
    {
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'content';
            $id = $this->content_model->add_content();
            if($id)
            {
                    $a = "";
                    $config['upload_path'] = './uploads/content_images/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '2048';
                    $config['max_width']  = '3000';
                    $config['max_height']  = '3000';				

                    $this->load->library('image_lib', $config);
                    $this->load->library('upload', $config);
                    $i=0;
                    if(isset($_FILES))
                    {
                            foreach($_FILES as $file=>$field)
                            {
                                    if($this->upload->do_upload($file))
                                    {
                                        $data = $this->upload->data();
                                        $a = $data['file_name'];
                                    }
                            }
                    }
                    $id = $this->content_model->upload_content_image($id,$a);					
                    $this->session->set_flashdata('success', 'Content has been added successfully.');
                    redirect(base_url().'siteadmin/content');
            }
//            if($id)
//            {
//                $this->session->set_flashdata('success', 'News added successfully.');
//                redirect(base_url() . 'siteadmin/news');        
//            }
            else
            {
                $this->session->set_flashdata('error', 'Unable to save content.');
                $data['include'] = 'siteadmin/content/add_content';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/content/add_content';
            $this->load->view('backend/container', $data);
        }
        
    }

    public function edit_content($id) 
    {
        $data['info'] = $this->content_model->getRecord($id);
        if(isset($_POST['submit']))
        {
            $id=$_POST['content_id'];
            $this->Checklogin();
            $data['admin_section'] = 'content';
            $edit = $this->content_model->edit_content();
            if($edit)
            {
                    $b = "";
                    $config['upload_path'] = './uploads/content_images/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size']	= '2048';
                    $config['max_width']  = '3000';
                    $config['max_height']  = '3000';				

                    $this->load->library('image_lib', $config);
                    $this->load->library('upload', $config);
                    $j=0;
                    if(isset($_FILES))
                    {
                            foreach($_FILES as $file=>$field)
                            {
                                    if($this->upload->do_upload($file))
                                    {
                                            $data = $this->upload->data();
                                            $b = $data['file_name'];
                                    }
                            }
                    }
                    $edit = $this->content_model->upload_content_image_update($id,$b);					

                    $this->session->set_flashdata('success', 'Content have been updated successfully.');
                    redirect(base_url().'siteadmin/content');

            }
//            if($edit)
//            {
//                $this->session->set_flashdata('success', 'News have been updated successfully.');
//                redirect(base_url() . 'siteadmin/news');
//            }
            else
            {
                $this->session->set_flashdata('success', 'Unable to update content.');
                $data['include'] = 'siteadmin/content/edit_content';
                $this->load->view('backend/container', $data);
            }
        }
        else
        {
            $data['include'] = 'siteadmin/content/edit_content';
            $this->load->view('backend/container', $data);
        }
    }
    
    public function delete($id) 
    {
        if ($this->content_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url() . 'siteadmin/content');
        }
    }

    public function activate_content($id) 
    {
        $this->content_model->activate_content($id);
        $this->session->set_flashdata('success', 'Content has been activated successfully');
        redirect('siteadmin/content');
    }
    
    public function deactivate_content($id) 
    {
        $this->content_model->deactivate_content($id);
        $this->session->set_flashdata('success', 'Content has been deactivated successfully');
        redirect('siteadmin/content');
    }
    
}
