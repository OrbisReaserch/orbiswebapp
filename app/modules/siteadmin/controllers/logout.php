<?php 

class Logout extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
	}

	public function index()
	{

		$array_items = array('id'=>'','admin_email'=>'','username'=>'','user_type'=>'');
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		$this->session->set_flashdata('error_message', 'You have logged out successfully.');
		redirect(base_url().'siteadmin/');
	}

}