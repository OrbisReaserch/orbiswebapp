<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Change_password extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();
		session_start();
        $this->load->database();
        $this->load->model('change_password_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {

        $this->Checklogin();
	
		$admin_id = $this->session->userdata('admin_id');
		
        $data['result'] = $this->change_password_model->getall($admin_id);
        $data['include'] = 'siteadmin/change_password/change_password';
        $data['admin_section'] = 'change_password';
        $this->load->view('backend/container', $data);
    }
    
    public function change() 
    {
		$admin_id = $this->session->userdata('admin_id');
		
        $data['result'] = $this->change_password_model->getall($admin_id);
		
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'admin';
			 $session_id = $this->session->userdata('admin_id');
            $id = $this->change_password_model->change_password($session_id);
            if($id)
            {
				
					
				$this->session->set_flashdata('success', 'Password has been Changed successfully.');
                 redirect(base_url().'siteadmin/change_password');
            }
   
            else
            {
                $this->session->set_flashdata('error', 'Unable to Change Password.');
                $data['include'] = 'siteadmin/change_password/change_password';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/change_password/change_password';
            $this->load->view('backend/container', $data);
        }
        
    }

  
  
    
}
