<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Weight_range extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('weight_range_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {

        $this->Checklogin();
        $data['result'] = $this->weight_range_model->getall();
        $data['include'] = 'siteadmin/weight_range/manage_weight_range';
        $data['admin_section'] = 'manage_weight_range';
        $this->load->view('backend/container', $data);
    }
    
    public function add_weight_range() 
    {
       if(isset($_POST['submit']))
        {
			$this->Checklogin();
			$data['admin_section'] = 'weight_range';
			
			$weight_from = $_POST['weight_from'];
			$weight_to=$_POST['weight_to'];
			
            
			$query_weightrate = $this->db->query("select * from weight_range where status='1' and weight_from='".$weight_from."' or weight_to='".$weight_to."'");
			if ($query_weightrate->num_rows() == 0)
			{
			
					
					$id = $this->weight_range_model->add_weight_range();
					if($id)
					{
						$this->session->set_flashdata('success', 'Weight range saved successfully.');
						redirect(base_url() . 'siteadmin/weight_range');        
					}
					else
					{
						$this->session->set_flashdata('error', 'Unable to save Weight range.');
						$data['include'] = 'siteadmin/weight_range/add_weight_range';
						$this->load->view('backend/container', $data);    
					}
			}
			else
			{
						$this->session->set_flashdata('success', 'Weight range is already assigned.');
						redirect(base_url() . 'siteadmin/weight_range');        	
			}
        }
        else
        {
            $data['include'] = 'siteadmin/weight_range/add_weight_range';
            $this->load->view('backend/container', $data);
        }
        
    }
    
        


    public function edit_weight_range($id) 
    {
    
        $data['info'] = $this->weight_range_model->getRecord($id);
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'weight_range';
			
			$weight_from = $_POST['weight_from'];
			$weight_to=$_POST['weight_to'];
			
            
			$query_weightrate = $this->db->query("select * from weight_range where status='1' and weight_from='".$weight_from."' or weight_to='".$weight_to."'");
			if ($query_weightrate->num_rows() == 0)
			{
			
					
					$edit = $this->weight_range_model->edit_weight_range();
					if($edit)
					{
						$this->session->set_flashdata('success', 'Weight range has been updated successfully.');
						redirect(base_url() . 'siteadmin/weight_range');
					}
					else
					{
						$this->session->set_flashdata('success', 'Unable to update Weight Range.');
						$data['include'] = 'siteadmin/weight_range/edit_weight_range';
						$this->load->view('backend/container', $data);
					}
			}
			else
			{
						$this->session->set_flashdata('success', 'Weight range is already assigned.');
						redirect(base_url() . 'siteadmin/weight_range');        	
			}
			
        }
        else
        {
            $data['include'] = 'siteadmin/weight_range/edit_weight_range';
            $this->load->view('backend/container', $data);
        }
    
    
    }
    
    public function delete($id) 
    {
        if ($this->weight_range_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url() . 'siteadmin/weight_range');
        }
    }

    public function activate_weight_range($id) 
    {
        $this->weight_range_model->activate_weight_range($id);
        $this->session->set_flashdata('success', 'Weight range has been activated successfully');
        redirect('siteadmin/weight_range');
    }
    
    public function deactivate_weight_range($id) 
    {
        $this->weight_range_model->deactivate_weight_range($id);
        $this->session->set_flashdata('success', 'Weight range has been deactivated successfully');
        redirect('siteadmin/weight_range');
    }
    
}
