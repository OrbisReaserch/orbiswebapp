<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Report extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('report_model');
		$this->load->model('country_model');
		$this->load->model('request_model');
		$this->load->model('publisher_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		 
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	//==================== all page session check =====================

	public function Checklogin()
	{
		if ($this->session->userdata('admin_email') == '')
		{
			redirect('siteadmin/');
		}
	}
function subcategory_by_id($id)
{
    $r = $this->report_model->subcategory_by_id($id);
	
    echo "<option value='#'>---Select Sub Category----</option>";
    if (count($r)) {
        for ($i = 0; $i < count($r); $i++) {
            
            		echo '<option value='.$r[$i]->id.'>'.$r[$i]->sub_category_name.'</option>';
           
        }
    }
}
	
	public function indexb4filter()
	{
		$this->Checklogin();
		$data['result'] = $this->report_model->getall();
		$data['include'] = 'siteadmin/report_management/report_management';
		$data['admin_section'] = 'report_management';
		$this->load->view('backend/container', $data);
	}
	public function index($condtion = "")
	{
		$this->Checklogin();
		/*$data['result'] = $this->report_model->getall();*/
		$data['condtion'] = $condtion;
		$data['countries'] = $this->country_model->getall();
		$data['publishers'] = $this->publisher_model->getall();
		$data['include'] = 'siteadmin/report_management/report_management';
		$data['admin_section'] = 'report_management';
		$this->load->view('backend/container', $data);
	}
		public function GetSearchReportList()
	{
		$keyword  = $_REQUEST['keyword'];
		$k = 1;
		$query_pag_data = "SELECT id,report_name,page_urls FROM report WHERE report_name LIKE '%$keyword%' LIMIT 0,10;";
		$result_pag_data = $this->db->query($query_pag_data);
	 	$reportlist=$result_pag_data->result();
	$msg ='';
			$msg .=' <p class="close-img"> 
        	<img src="'.theme_url().'images/close.png" id="close_auto_complete" width="20%" style="float:right; cursor:pointer"/>
        </p>';
								//print_r($reportlist); die();
	 		for($i=0;$i<count($reportlist);$i++)
	 		{
	 				
							$reportName = $reportlist[$i]->report_name;	
						 
	 		$msg .='<p style="color:#222"><a onclick = "displayName(this.id)" style="color: black;font-size: 16px;" href = "'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" id = "'.$reportlist[$i]->report_name.'">'.$reportlist[$i]->report_name.'</a></p>';
				$k++;
				}
							echo $msg;
	
	}
	public function GetSearchReports()
	{
		$user_type=$this->session->userdata('user_type');
		$keyword  = $_REQUEST['keyword'];
		$k = 1;
		$query_pag_data = "SELECT id,report_name,country_id,category_id,sub_category_id,publisher_id,report_date,featured_report,report_price,is_requested_for_delete,report_status FROM report WHERE report_name LIKE '%$keyword%' ORDER BY report_date DESC LIMIT 0,10;";
		$result_pag_data = $this->db->query($query_pag_data);
	 	$reportlist=$result_pag_data->result();
 
	$msg ='';
			$msg .='<table  class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th style="width:10px;">Sr No</th>
                                        <th>Report name</th>
                                        <th>Category</th>
                                        <th>Sub Category</th>
                                        <th>Publisher</th>
										<th>Country</th>
										<th>Price</th>
                                        <th>Date</th>
										<th>Status</th>
                                        <th style="width:195px;">Action</th>
                                        <th><input type="checkbox" id="selectall" title="select bulk report"/> Bulk Reports</th>
                                    </tr> 
                                </thead>
                                <tbody>';
								//print_r($reportlist); die();
	 		for($i=0;$i<count($reportlist);$i++)
	 		{
	 				 $cid =  $reportlist[$i]->category_id;
					 $subcat = $reportlist[$i]->sub_category_id;
					 $vendor = $reportlist[$i]->publisher_id;
                              $country = $this->report_model->GetCountryById($reportlist[$i]->country_id);	              
									$a =  $this->report_model->getcategory($cid);
									$su=$this->report_model->getsubcategory($subcat);
									$v = $this->report_model->getvendor($vendor);
									//print_r($su); die();
	 		$msg .='
                                    
                                     <tr class="gradeA" style ="font-weight: bold;'; 
									if($reportlist[$i]->is_requested_for_delete == '1'){
										$msg .= 'color: red;';
									}
									$msg .= '">
                                        <td>'.$k.'</td>
                                        <td>'.$reportlist[$i]->report_name.'</td>
                                        <td>
                                         
                                         
                                      '.
                                       $a['category_name'].'
                                        
                                        </td>
                                        <td>
                                            '. $su['sub_category_name'].'
                                            
                                        </td>
                                         <td> '. $v['display_name'].'
										 </td><td>'.$country.'</td><td>$'.$reportlist[$i]->report_price.'</td>
<td> '. $reportlist[$i]->report_date.'</td><td>';
                                          if($reportlist[$i]->report_status == 1)
                                            {
												$msg .= "Published";
											}else{
												$msg .= "Not Published";
											}
                                        $msg .= '</td>
                                          
                                          
                                        <td>
                     <a href="'.base_url().'siteadmin/report/edit_report/'.$reportlist[$i]->id.'" class="btn btn-primary btn-mini">Edit</a>
					 <a target = "_blank" href="'.base_url().'siteadmin/report/report_pdf/'.$reportlist[$i]->id.'" style="color: #ffffff;" class="btn btn-primary btn-mini">Generate PDF</a>';
                                           
                    if($user_type == 'Subsubsubadmin'){
							 if($reportlist[$i]->is_requested_for_delete == 1){
								 $msg .= '<a href=" '.base_url().'siteadmin/report/cancel_request_delete_report/'.$reportlist[$i]->id.'" class="btn btn-warning btn-mini">Cancel Delete Request </a>';
							 }else{
								 $msg .= '<a href=" '.base_url().'siteadmin/report/request_delete_report/'.$reportlist[$i]->id.'" class="btn btn-warning btn-mini">Request Delete</a>';
							 }
							 
						 }else{
							 $msg .= '<a href=" '.base_url().'siteadmin/report/delete_report/'.$reportlist[$i]->id.'" class="btn btn-warning btn-mini">Delete</a>';
						 }        
                                 
                                            if($reportlist[$i]->featured_report==0)
                                            {
                                            
                    $msg .=' <a href=" '.base_url().'siteadmin/report/featured_report/'.$reportlist[$i]->id.'" class="btn btn-success btn-mini">Featured</a>';
                                            
                                            }
                                            else
                                            {
                                            
                    $msg .='  <a href="'.base_url().'siteadmin/report/notfeatured_report/'.$reportlist[$i]->id.'" class="btn btn-warning btn-mini">Not Featured</a>';
                                            }
                                 $msg .='       </td>
                                   <td><input type="checkbox" class="bulkcheckbox" value="'.$reportlist[$i]->id.'" /></td>
                                    </tr>';
				$k++;
				}
 $msg .='      </tbody>
                            ';
							echo $msg;
	}
	public function getDataPage()
	{
	 if($_REQUEST)
	 {
		 	$user_type=$this->session->userdata('user_type');
			if(!empty($_REQUEST['page']))
			{
				$page=$_REQUEST['page'];
			}else
			{
				$page=1;
			}
			$sqlcondition='';
			$keyword=$_POST['keyword'];
			$country_id=$_POST['country_id'];
			$publisher_id=$_POST['publisher_id'];
		
			$sqlcondition .= (!empty($_POST['keyword'])) ? "report_name LIKE '%$keyword%' and " : "1 and ";
			$sqlcondition .= (!empty($_POST['country_id'])) ? "country_id='$country_id' and " : "1 and ";
			$sqlcondition .= (!empty($_POST['publisher_id'])) ? "publisher_id='$publisher_id' " : "1 ";
	
		 	$cur_page = $page;
		 	$page -= 1;
		 	$per_page = 10;
		 	$previous_btn = true;
		 	$next_btn = true;
		 	$first_btn = true;
		 	$last_btn = true;
		 	$start = $page * $per_page;
			$k = $cur_page * $per_page - 9; //Added new for display record per page 10
			$sql="select id,report_name,country_id,category_id,publisher_id,sub_category_id,report_date,featured_report,page_urls,report_price,report_status,is_requested_for_delete from report where ".$sqlcondition."ORDER BY report_date DESC";
			$query_pag_data = $sql." LIMIT $start, $per_page";
			$result_pag_data = $this->db->query($query_pag_data);
	 		$reportlist=$result_pag_data->result();
			
			$msg ='';
			$msg .='<table  class="display table table-bordered table-striped" id="dynamic-table">
                <thead>
                    <tr>
                        <th style="width:10px;">Sr No</th>
                        <th>Report name</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                        <th>Publisher</th>
						<th>Country</th>
						<th>Price</th>
                        <th>Date</th>
						<th>Status</th>
                        <th style="width:195px;">Action</th>
                        <th><input type="checkbox" id="selectall" title="select bulk report"/> Bulk Reports</th>
                    </tr>
                </thead>
                <tbody>';
								
	 		for($i=0;$i<count($reportlist);$i++)
	 		{
	 				 $cid =  $reportlist[$i]->category_id;
					 $subcat = $reportlist[$i]->sub_category_id;
					 $vendor = $reportlist[$i]->publisher_id;
                     $a =  $this->report_model->getcategory($cid);
					 $su=$this->report_model->getsubcategory($subcat);
					 $v = $this->report_model->getvendor($vendor);
					 $country = $this->report_model->GetCountryById($reportlist[$i]->country_id);				
					$msg .='<tr class="gradeA" style ="font-weight: bold;'; 
							if($reportlist[$i]->is_requested_for_delete == '1'){
								$msg .= 'color: red;';
							}
							$msg .= '">
                                <td>'.$k.'</td>
                                <td>'.$reportlist[$i]->report_name.'</td>
                                <td>'.$a['category_name'].'</td>
                                <td>'. $su['sub_category_name'].'</td>
                                <td> '. $v['display_name'].'</td>
                                <td>'.$country.'</td>
                                <td>$'.$reportlist[$i]->report_price.'</td>
								<td> '. $reportlist[$i]->report_date.'</td><td>';
                              if($reportlist[$i]->report_status == 1)
                                {
									$msg .= "Published";
								}else{
									$msg .= "Not Published";
								}
                            $msg .= '</td><td>
                     <a href="'.base_url().'siteadmin/report/edit_report/'.$reportlist[$i]->id.'" class="btn btn-primary btn-mini">Edit</a>
					 <a target = "_blank" href="'.base_url().'siteadmin/report/report_pdf/'.$reportlist[$i]->id.'" style="color: #ffffff;" class="btn btn-primary btn-mini">Generate PDF</a>';
                         if($user_type == 'Subsubsubadmin'){
							 if($reportlist[$i]->is_requested_for_delete == 1){
								 $msg .= '<a href=" '.base_url().'siteadmin/report/cancel_request_delete_report/'.$reportlist[$i]->id.'" class="btn btn-warning btn-mini">Cancel Delete Request </a>';
							 }else{
								 $msg .= '<a href=" '.base_url().'siteadmin/report/request_delete_report/'.$reportlist[$i]->id.'" class="btn btn-warning btn-mini">Request Delete</a>';
							 }
							 
						 }else{
							 $msg .= '<a href=" '.base_url().'siteadmin/report/delete_report/'.$reportlist[$i]->id.'" class="btn btn-warning btn-mini">Delete</a>';
						 }        
                    
                        if($reportlist[$i]->featured_report == 0)
                        {
                        
						$msg .=' <a href=" '.base_url().'siteadmin/report/featured_report/'.$reportlist[$i]->id.'" class="btn btn-success btn-mini">Featured</a>';
                        
                        }
                        else
                        {
                        
							$msg .='  <a href="'.base_url().'siteadmin/report/notfeatured_report/'.$reportlist[$i]->id.'" class="btn btn-warning btn-mini">Not Featured</a>';
                        }
     						$msg .='</td>
     						<td><input type="checkbox" class="bulkcheckbox" value="'.$reportlist[$i]->id.'" /></td>
       						 </tr>';
							$k++;
			}
 							$msg .=' </tbody>';
				
	 	/* --------------------------------------------- */
		
		$sqlcount="select count(id) as cnt from report where ".$sqlcondition." order by report_date desc";
		$q=$this->db->query($sqlcount);
		$count=$q->row()->cnt;
		 
	 	$no_of_paginations=ceil($count / $per_page);
			/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	 	if ($cur_page >= 10) {
	 		$start_loop = $cur_page - 3;
	 		if ($no_of_paginations > $cur_page + 3)
	 		{
	 			$end_loop = $cur_page + 3;
	 		}
	 		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6)
	 		{
	 			$start_loop = $no_of_paginations - 6;
	 			$end_loop = $no_of_paginations;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	else
	 	{
	 		$start_loop = 1;
	 		if ($no_of_paginations > 7)
	 		{
	 			$end_loop = 7;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	/* ----------------------------------------------------------------------------------------------------------- */
	 	$msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";

	 	// FOR ENABLING THE FIRST BUTTON
	 	if ($first_btn && $cur_page > 1)
	 	{
	 		$msg .= "<li p='1' class='active'>First</li>";
	 	}
	 	else if ($first_btn)
	 	{
	 		$msg .= "<li p='1' class='inactive'>First</li>";
	 	}

	 	// FOR ENABLING THE PREVIOUS BUTTON
	 	if ($previous_btn && $cur_page > 1)
	 	{
	 		$pre = $cur_page - 1;
	 		$msg .= "<li p='$pre' class='active'>Previous</li>";
	 	}
	 	else if ($previous_btn)
	 	{
	 		$msg .= "<li class='inactive'>Previous</li>";
	 	}
	 	for ($i = $start_loop; $i <= $end_loop; $i++)
	 	{

	 		if ($cur_page == $i)
	 		{
	 			$msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
	 		}
	 		else
	 		{
	 			$msg .= "<li p='$i' class='active'>{$i}</li>";
	 		}
	 	}

	 	// TO ENABLE THE NEXT BUTTON
	 	if ($next_btn && $cur_page < $no_of_paginations)
	 	{
	 		$nex = $cur_page + 1;
	 		$msg .= "<li p='$nex' class='active'>Next</li>";
	 	}
	 	else if ($next_btn)
	 	{
	 		$msg .= "<li class='inactive'>Next</li>";
	 	}

	 	// TO ENABLE THE END BUTTON
	 	if ($last_btn && $cur_page < $no_of_paginations)
	 	{
	 		$msg .= "<li p='$no_of_paginations' class='active'>Last</li>";
	 	}
	 	else if ($last_btn)
	 	{
	 		$msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
	 	}
	 	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
	 	$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
	 	$msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
	 	
		echo $msg;
	 }
		
	}

public function getReports($id)
	{
		$this->Checklogin();
		$data['result'] = $this->report_model->getReportsOfPublisher($id);
		$data['countries'] = $this->country_model->getall();
		$data['publishers'] = $this->publisher_model->getall();
		$data['include'] = 'siteadmin/report_management/report_management';
		$data['admin_section'] = 'report_management';
		$this->load->view('backend/container', $data);
	}

	public function add_reports()
	{
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'report';
			$report_name=$_POST['report_title'];
			$checkexist=$this->report_model->checkReportNameExist($report_name);
			if($checkexist==0)
			{
				$rep_id = $this->report_model->add_reports();		
				if($rep_id)
				{
					$a = "";
					$config['upload_path'] = './uploads/report_images/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '2048';
					$config['max_width']  = '3000';
					$config['max_height']  = '3000';

					$this->load->library('image_lib', $config);
					$this->load->library('upload', $config);
					$i=0;
					if(isset($_FILES))
					{
						foreach($_FILES as $file=>$field)
						{
							if($this->upload->do_upload($file))
							{
								$data = $this->upload->data();
								$a = $data['file_name'];
							}
						}
					}
				$id = $this->report_model->upload_report_image($rep_id,$a);

				$this->session->set_flashdata('success', 'Report has been added successfully');
				redirect(base_url().'siteadmin/report/view_report/'.$rep_id);

				}
				else
				{
					$this->session->set_flashdata('error', 'Unable to save Advertisement');
					$data['include'] = 'siteadmin/report_management/add_reports';
					$this->load->view('backend/container', $data);
				}
			}
			else
			{
				$this->session->set_flashdata('error', 'Report title name already exist');
				$data['include'] = 'siteadmin/report_management/add_reports';
				$this->load->view('backend/container', $data);
				
			}
		}
		else
		{
			$data['include'] = 'siteadmin/report_management/add_reports';
			$this->load->view('backend/container', $data);
		}

	}
	public function view_report($report_id){
		$this->Checklogin();
		$data['info'] = $this->report_model->getRecord($report_id);
		$data['include'] = 'siteadmin/report_management/report_view';
		$this->load->view('backend/container', $data);
	}
	public function report_pdf($report_id){
		$this->Checklogin();
		$data = $this->report_model->generate_pdf($report_id);
		
	}
	public function edit_report($id)
	{
		$data['info'] = $this->report_model->getRecord($id);
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'report';
			$report_name=$_POST['report_title'];
			$checkexist=$this->report_model->checkEditReportNameExist($report_name,$id);
			if($checkexist==0)
			{
				$edit = $this->report_model->edit_report();

				if($edit)
				{
					$a = "";
					$config['upload_path'] = './uploads/report_images/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '2048';
					$config['max_width']  = '3000';
					$config['max_height']  = '3000';

					$this->load->library('image_lib', $config);
					$this->load->library('upload', $config);
					$i=0;
					if(isset($_FILES))
					{
						foreach($_FILES as $file=>$field)
						{
							if($this->upload->do_upload($file))
							{
								$data = $this->upload->data();
								$a = $data['file_name'];
							}
						}
					}
					$edit = $this->report_model->update_report_image($edit,$a);
					
					$this->session->set_flashdata('success', 'Report has been update successfully.');
					redirect(base_url().'siteadmin/report');

				}
				else
				{
					$this->session->set_flashdata('success', 'Unable to update Report.');
					$data['include'] = 'siteadmin/report/edit_report';
					$this->load->view('backend/container', $data);
				}
			}
				else
				{
					$this->session->set_flashdata('error', 'Report title name already exist');
					redirect(base_url().'siteadmin/report/edit_report/'.$id);
				}	
		}
		else
		{
			$data['include'] = 'siteadmin/report_management/edit_report';
			$this->load->view('backend/container', $data);
		}
	}

	public function delete($id)
	{
		if ($this->advertisement_model->delete($id))
		{
			$this->session->set_flashdata('success', 'Record has been deleted successfully.');
			redirect(base_url() . 'siteadmin/advertisement');
		}
	}

	public function activate_report($id)
	{
		$this->report_model->activate_report($id);
		$this->session->set_flashdata('success', 'Report has been activated successfully');
		redirect('siteadmin/report');
	}
	public function request_delete_report($id){
		$this->report_model->request_delete_report($id);
		$this->session->set_flashdata('success', 'Request has been sent successfully');
		redirect('siteadmin/report');
	}
	public function cancel_request_delete_report($id){
		$this->report_model->cancel_request_delete_report($id);
		$this->session->set_flashdata('success', 'Request has been cancelled successfully');
		redirect('siteadmin/report');
	}
	
	public function delete_report($id)
	{
		$this->report_model->delete_report($id);
		$this->session->set_flashdata('success', 'Report has been deactivated successfully');
		redirect('siteadmin/report');
	}

	public function featured_report($id)
	{
		$abc =  $this->report_model->featured_report($id);
		$this->session->set_flashdata('success', 'Report has been featured successfully');
		redirect('siteadmin/report');
	}

	public function notfeatured_report($id)
	{
		$abc = $this->report_model->notfeatured_report($id);
		$this->session->set_flashdata('success', 'Report has been deactivated successfully');
		redirect('siteadmin/report');
	}


	public function read_excel()
	{
            if(isset($_POST['submit']))
            {
               
                $file_name = time().$_FILES['bulk_upload']['name'];

                    if(isset($_FILES))
                   { 


 						$target = 'uploads/report_file/'.$file_name;
 						move_uploaded_file( $_FILES['bulk_upload']['tmp_name'], $target);	
 						chmod($target,0777);


                    }

                    
                   $check_exist_report_title = $this->report_model->bulkUpload($target);
                   
                    // if($check_exist_report_title['success'])
                    // {	
                    // 	$this->session->set_flashdata('success_file', 'Excel Upload done successfully');
                    // 	$this->session->set_flashdata('check_exist_data',$check_exist_report_title);
                    //      redirect('siteadmin/report');  
                    // }
                    // else
                    // {
                    //         $this->session->set_flashdata('error_file', 'Unable to upload Excel file.');
                    //         $data['include'] = 'siteadmin/report';
                    // }
                   if($check_exist_report_title['success'])
                    {	
                    	
                    $data['uploadReportResult']=$check_exist_report_title;
                                       
                    }
                    else
                    {
                    	$data['uploadReportResult']=false;
                    }
                    $data['include'] = 'siteadmin/report_management/report_management';	
                    
                    $this->load->view('backend/container', $data);
           }				
	}
	public function getdata()
	{
		$data['include'] = 'report/sitemap';
		$this->load->view('report/sitemap',$data);
	}
	public function getrequestexcel()
	{ 
		$condtion = $_POST['excelCond'];
		if("" == $condtion){
			$result = $this->request_model->getall();
			//this code added by pravin
			//$result = $this->request_model->getallByDate();
		}else{
			$result = $this->request_model->getallByCond($condtion);		
		}
		/*echo "<pre>";
		 print_r($result);
		 die(); */
		 $this->report_model->bulkDownload($result);
	}

	/**
	 * This method helps downloading the excel file of all reports 
	 * by selected publisher id
	 * 
	 * @param int $publisherId
	 */
	public function downloadReportOfPublisher($publisherId)
	{
		if(!is_null($publisherId)) {
			$reports = $this->report_model->getAllReportsOfPublisher($publisherId);
			$this->report_model->downloadPublisherReports($reports);
		}
	}

	public function checkExistReportTitle() 
	{
		$reportname=$_POST['title_name'];
		echo $this->report_model->checkReportNameExist($reportname);	
	}

	public function checkEditExistReportTitle()
	{
		$reportname=$_POST['title_name'];
		$id=$_POST['report_id'];
		echo $this->report_model->checkEditReportNameExist($reportname,$id);
	}

	public function deleteBulkReport()
	{
		$selected_id=$_POST['selected_id'];
		if($selected_id!=='') {
			$spiltid=implode(',', $selected_id);
			$this->report_model->deletBulkReportData($spiltid);
		}
	}
	
	public function downloadReportIdandUrls($limit){

		if($limit < 5000){
		$this->report_model->downloadReportIdandUrls($limit);
	}
	else{
		echo "Your count is more than 3000, Please ask your Manager or IT Team";
		}
	}
public function downloadReportDetailsFromId($from,$to){

		$this->report_model->downloadReportDetailsFromId($from,$to);
	}
}
?>