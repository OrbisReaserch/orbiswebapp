<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Publisher extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        $this->load->database();
        //$this->load->model('user_model');
         $this->load->model('publisher_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('pagination');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    //==================== all page session check ===================== 

    public function Checklogin() 
    {
        if ($this->session->userdata('admin_email') == '') 
        {
            redirect('siteadmin/');
        }
    }

    public function index() 
    {
        $this->Checklogin();
        $data['result'] = $this->publisher_model->getall();
        $data['include'] = 'siteadmin/publisher_management/manage_publisher';
        $data['admin_section'] = 'manage_publisher';
        $this->load->view('backend/container', $data);
    }
    
   public function activate_user($id) 
    {
    	$query=$this->db->query("select * from users where id='".$id."'");

		$result= $query->result();
		 $to= $result[0]->email;
    			// $to='arunjith@velociters.com';
				$subject='Orbis - Publisher Activated  Information';
    			$message =' Your request has been Activated by Orbis Team';
//				echo $message;
//				die();
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				
				$headers .= "From:Orbis";
				//$subject = "News Letter From Orbis";
				//$from = "admin@orbis.com";
				$from='customerservice@orbisresearch.com';
				$sent = mail($to,$subject,$message,$headers);
    	
        $this->publisher_model->activate_user($id);
        $this->session->set_flashdata('success', 'Publisher has been Activated successfully');
        redirect('siteadmin/publisher');
    }
    public function deactivate_user($id,$email) 
    {
    	
    	
        $this->publisher_model->deactivate_user($id);
        $this->session->set_flashdata('success', 'Publisher has been Deactivated successfully');
        redirect('siteadmin/publisher');
    }
    
    
    public function manage_sub_category() 
    {

        $this->Checklogin();
        $data['result'] = $this->user_model->getallSubCategory();
        $data['include'] = 'siteadmin/category/manage_sub_category';
        $data['admin_section'] = 'manage_sub_category';
        $this->load->view('backend/container', $data);
    }
    
    public function manage_sub_sub_category() 
    {

        $this->Checklogin();
        $data['result'] = $this->user_model->getsub_subcategory();
        $data['include'] = 'siteadmin/category/manage_sub_sub_category';
        $data['admin_section'] = 'manage_sub_sub_category';
        $this->load->view('backend/container', $data);
    }

    public function add_user() 
    {
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'category';
            $id = $this->user_model->add_user();
            if($id)
            {
                $this->session->set_flashdata('success', 'Category saved successfully.');
                redirect(base_url() . 'siteadmin/category');        
            }
            else
            {
                $this->session->set_flashdata('error', 'Unable to save category.');
                $data['include'] = 'siteadmin/category/add';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/category/add';
            $this->load->view('backend/container', $data);
        }
        
    }
    
    public function add_sub_category() 
    {
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'category';
            $id = $this->user_model->add_sub_category();
            if($id)
            {
                $this->session->set_flashdata('success', 'Sub Category saved successfully.');
                redirect(base_url() . 'siteadmin/category/manage_sub_category');        
            }
            else
            {
                $this->session->set_flashdata('error', 'Unable to save sub category.');
                $data['include'] = 'siteadmin/category/add_sub_category';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/category/add_sub_category';
            $this->load->view('backend/container', $data);
        }
        
    }
    
    function add_sub_sub_category()
    {
         if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'manage_sub_sub_category';
            $id = $this->user_model->add_sub_sub_category();
            if($id)
            {
                $this->session->set_flashdata('success', 'Sub To Sub Category saved successfully.');
                redirect(base_url() . 'siteadmin/category/manage_sub_sub_category');        
            }
            else
            {
                $this->session->set_flashdata('error', 'Unable to save subtosub category.');
                $data['include'] = 'siteadmin/category/manage_sub_sub_category';
                $this->load->view('backend/container', $data);    
            }
        }
        else
        {
            $data['include'] = 'siteadmin/category/add_sub_sub_category';
            $this->load->view('backend/container', $data);
        }
        
    }

    public function edit($id) 
    {
        $data['info'] = $this->user_model->getRecord($id);
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'category';
            $edit = $this->user_model->edit();
            if($edit)
            {
                $this->session->set_flashdata('success', 'Category has been updated successfully.');
                redirect(base_url() . 'siteadmin/category');
            }
            else
            {
                $this->session->set_flashdata('success', 'Unable to update category.');
                $data['include'] = 'siteadmin/category/edit';
                $this->load->view('backend/container', $data);
            }
        }
        else
        {
            $data['include'] = 'siteadmin/category/edit';
            $this->load->view('backend/container', $data);
        }
    }

     public function edit_sub_category($id) 
    {
        $data['info'] = $this->user_model->getRecordSubCategory($id);
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'category';
            $edit = $this->user_model->edit_sub_category();
            if($edit)
            {
                $this->session->set_flashdata('success', 'Sub Category has been updated successfully.');
                redirect(base_url() . 'siteadmin/category/manage_sub_category');
            }
            else
            {
                $this->session->set_flashdata('success', 'Unable to update sub category.');
                $data['include'] = 'siteadmin/category/edit_sub_category';
                $this->load->view('backend/container', $data);
            }
        }
        else
        {
            $data['include'] = 'siteadmin/category/edit_sub_category';
            $this->load->view('backend/container', $data);
        }
    }
    
    public function edit_sub_sub_category($id) 
    {
        $data['info'] = $this->user_model->getRecordSubtoSubCategory($id);
        if(isset($_POST['submit']))
        {
            $this->Checklogin();
            $data['admin_section'] = 'category';
            $edit = $this->user_model->edit_sub_sub_category();
            if($edit)
            {
                $this->session->set_flashdata('success', 'Sub Category has been updated successfully.');
                redirect(base_url() . 'siteadmin/category/manage_sub_sub_category');
            }
            else
            {
                $this->session->set_flashdata('success', 'Unable to update sub category.');
                $data['include'] = 'siteadmin/category/edit_sub_sub_category';
                $this->load->view('backend/container', $data);
            }
        }
        else
        {
            $data['include'] = 'siteadmin/category/edit_sub_sub_category';
            $this->load->view('backend/container', $data);
        }
    }
    
    public function delete($id) 
    {
        if ($this->user_model->delete($id)) 
        {
            $this->session->set_flashdata('success', 'Record has been deleted successfully.');
            redirect(base_url() . 'siteadmin/category');
        }
    }
 
    public function activate_sub_sub_category($id) 
    {
        $this->user_model->activate_sub_sub_category($id);
        $this->session->set_flashdata('success', 'Sub Category has been activated successfully');
        redirect('siteadmin/category/manage_sub_sub_category');
    }
    public function deactivate($id) 
    {
        $this->user_model->deactivate($id);
        $this->session->set_flashdata('success', 'Category has been deactivated successfully');
        redirect('siteadmin/category');
    }
     public function deactivate_sub_category($id) 
    {
        $this->user_model->deactivate_sub_category($id);
        $this->session->set_flashdata('success', 'Sub Category has been deactivated successfully');
        redirect('siteadmin/category/manage_sub_category');
    }
    public function deactivate_sub_sub_category($id) 
    {
        $this->user_model->deactivate_sub_sub_category($id);
        $this->session->set_flashdata('success', 'Sub Category has been deactivated successfully');
        redirect('siteadmin/category/manage_sub_sub_category');
    }

}
