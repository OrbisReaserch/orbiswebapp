<style>
	
	input[type="date"]::-webkit-calendar-picker-indicator {
    color: rgba(0, 0, 0, 0);
    opacity: 1;
    display: block;
    background: url(https://mywildalberta.ca/images/GFX-MWA-Parks-Reservations.png) no-repeat;
    width: 20px;
    height: 20px;
    border-width: thin;
}
</style>

<?php $this->load->view('backend/leftsidebar'); ?>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
<section class="wrapper">
<!-- page start-->
<div class="row">
<div class="col-sm-12"><section class="panel"> <header
	class="panel-heading"> Manage Requests <span style="margin-left: 30px;">
<?php
if($this->session->flashdata('success'))
{
	echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
}
else if($this->session->flashdata('error'))
{
	echo "<font style='color:red;'>".$this->session->flashdata('errror')."</font>";
}
?> </span> <span class="tools pull-right"> 
<form action  = "<?php echo base_url();?>siteadmin/report/getrequestexcel" method = "post">
<input type = "hidden"   value = "<?php echo $this->uri->segment(4);?>" id = "excelCond" name = "excelCond"/>
 
<input type = "submit"  style="color: #ffffff;" class="btn btn-primary btn-mini" value = "Download Excel"/>

</form>
<a href="javascript:;" class="fa fa-chevron-down"></a> </span> </header>
<div class="panel-body">
<div class="row">
						 <div class="col-sm-3">
						 <select name="req_type" class=" form-control" id = "req_type" onchange = "SearchLeadConditioned(this.value);">
							<option value = "">Select Request Type</option>
						 
							<option <?php if(urldecode($condtion) == "request_type = ~Enquiry~" ){echo "selected";}?> value = "<?php echo "request_type = ~Enquiry~"; ?>">Enquiry</option>
							<option <?php if(urldecode($condtion) == "request_type = ~Request For Discount~" ){echo "selected";}?> value = "<?php echo "request_type = ~Request For Discount~"; ?>">Request For Discount</option>
							<option <?php if(urldecode($condtion) == "request_type = ~Request For Sample~" ){echo "selected";}?> value = "<?php echo "request_type = ~Request For Sample~"; ?>">Request For Sample</option>
									 
						</select>
						</div>
						<div class="col-sm-3">
						 <select name="region" class=" form-control" id = "region" onchange = "SearchLeadConditioned(this.value);">
							<option value = "">Select Region</option>
						 
							<option <?php if(urldecode($condtion) == "region = ~EUROPE~" ){echo "selected";}?> value = "<?php echo "region = ~EUROPE~"; ?>">EUROPE</option>
							<option <?php if(urldecode($condtion) == "region = ~USA~" ){echo "selected";}?> value = "<?php echo "region = ~USA~"; ?>">USA</option>
							<option <?php if(urldecode($condtion) == "region = ~APAC~" ){echo "selected";}?> value = "<?php echo "region = ~APAC~"; ?>">APAC</option>
									 
						</select>
						</div>
						 <div class="col-sm-3">
						 <select name="coyntry" class=" form-control" id = "coyntry" onchange = "SearchLeadConditioned(this.value);">
							<option value = "">Select Country</option>
							<?php 
							for($i=0;$i<count($countries);$i++)
                                    {
							?>
							<option <?php if(urldecode($condtion) == "country = ~".$countries[$i]->country_name."~" ){echo "selected";}?> value = "<?php echo "country = ~".$countries[$i]->country_name."~"; ?>"><?php echo $countries[$i]->country_name; ?></option>
									<?php } ?>
						</select>
						</div>
						<div class="col-sm-3">
						 <select name="publisher" class=" form-control" id = "publisher" onchange = "SearchLeadConditioned(this.value);">
							<option value = "">Select Publisher</option>
							<?php 
							for($i=0;$i<count($publishers);$i++)
                                    {
							?>
							<option <?php if(urldecode($condtion) == "publisher_id = ~".$publishers[$i]->id."~" ){echo "selected";}?> value = "<?php echo "publisher_id = ~".$publishers[$i]->id."~"; ?>"><?php echo $publishers[$i]->display_name; ?></option>
									<?php } ?>
						</select>
						</div>
						<div class="col-sm-1"> </br>
	 <input type = "button" style="color: #ffffff;" class="btn btn-primary btn-mini" id = "clear" value="Clear" onclick="window.location.href='<?php echo base_url()."siteadmin/request"?>'" />
						</div>
<!-- <div class="col-sm-3"> 
</br>
	 <input type = "date" placeholder="yyyy-mm-dd"  class="form-control input-medium " id = "date"  onblur="SearchLeadConditionedDate(this.value);"/>
						</div>
					</div> -->
					<br/>
<div class="adv-table">
<table class="display table table-bordered table-striped"
	id="dynamic-table">
	<thead>
		<tr>
			<th>Sr No</th>
			<th>User name</th>
			<th>Email Id</th>
			<th>Contact No</th>
			<th>Country</th>
			<th>Region</th>
			<th>Report</th>
			<th>Date</th>
			<th>Request Type</th>
			<th>Publisher</th>
			<th>Message</th>
			<th>Category</th>
			<th>Company</th>
			<th>Designation</th>
			
			<th width="270">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php
	for($i=0;$i<count($result);$i++)
	{
		$report = $this->db->query("select * from report where id = '".$result[$i]->report_id."'");
		$report_name = $report->result();
		$publisher= $this->db->query("select * from users where id = '".$result[$i]->publisher_id."'");
		$publisher_name = $publisher->result();
		
		$publisher= $this->db->query("select category_name from category where id = '".$report_name[0]->category_id."'");
		$category_name = $publisher->result();
		
		?>
		<tr class="gradeA">
			<td><?php echo $i+1;?></td>
			<td><?php echo $result[$i]->username;?></td>
			<td><?php echo $result[$i]->email;?></td>
			<td><?php echo $result[$i]->contact_no;?></td>
			<td><?php echo $result[$i]->country;?></td>
			<td><?php echo $result[$i]->region;?></td>
			<td><?php echo $report_name[0]->report_name;?></td>
			<td><?php echo $result[$i]->lead_date;?></td>
			<td><?php echo $result[$i]->request_type;?></td>
			<td><?php echo $publisher_name[0]->display_name;?></td>
			<td><?php echo $result[$i]->messages;?></td>
			<td><?php echo $category_name[0]->category_name;?></td>
			<td><?php echo $result[$i]->company_name;?></td>
			<td><?php echo $result[$i]->designation;?></td>
			<td><?php if($result[$i]->mail_status == '0'){?> <a
				href="<?php echo base_url();?>siteadmin/request/send_mail/<?php echo $result[$i]->publisher_id;?>/<?php echo $result[$i]->id;?>/<?php echo $result[$i]->report_code;?>"
				style="color: #ffffff;" class="btn btn-primary btn-mini">Send To
			Publisher</a> <?php } else {?> <a
				href="<?php echo base_url();?>siteadmin/request/send_mail/<?php echo $result[$i]->publisher_id;?>/<?php echo $result[$i]->id;?>/<?php echo $result[$i]->report_code;?>"
				style="color: #ffffff;" class="btn btn-primary btn-mini">Resend</a>
				<?php
			}
			if($result[$i]->status==0)
			{
				?> <a
				href="<?php echo base_url();?>siteadmin/request/activate/<?php echo $result[$i]->id;?>"
				class="btn btn-success btn-mini">Activate</a> <?php
			}
			else
			{
				?> <a
				href="<?php echo base_url();?>siteadmin/request/deactivate/<?php echo $result[$i]->id;?>"
				class="btn btn-warning btn-mini">Deactivate</a> <?php
			}
			?>
			 <a style = "display:none;"
				href="<?php echo base_url();?>siteadmin/request/DeleteRequest/<?php echo $result[$i]->id;?>"
				class="btn btn-success btn-mini">Delete</a> 
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>
</div>
</div>
</section></div>
</div>
<!-- page end-->
</section>
<div class="right-sidebar">
<div class="search-row"><input type="text" placeholder="Search"
	class="form-control"></div>
<ul class="right-side-accordion">
	<li class="widget-collapsible"><a href="#"
		class="head widget-head red-bg active clearfix"> <span
		class="pull-left">work progress (5)</span> <span
		class="pull-right widget-collapse"><i class="ico-minus"></i></span> </a>
	<ul class="widget-container">
		<li>
		<div class="prog-row side-mini-stat clearfix">
		<div class="side-graph-info">
		<h4>Target sell</h4>
		<p>25%, Deadline 12 june 13</p>
		</div>
		<div class="side-mini-graph">
		<div class="target-sell"></div>
		</div>
		</div>
		<div class="prog-row side-mini-stat">
		<div class="side-graph-info">
		<h4>product delivery</h4>
		<p>55%, Deadline 12 june 13</p>
		</div>
		<div class="side-mini-graph">
		<div class="p-delivery">
		<div class="sparkline" data-type="bar" data-resize="true"
			data-height="30" data-width="90%" data-bar-color="#39b7ab"
			data-bar-width="5"
			data-data="[200,135,667,333,526,996,564,123,890,564,455]"></div>
		</div>
		</div>
		</div>
		<div class="prog-row side-mini-stat">
		<div class="side-graph-info payment-info">
		<h4>payment collection</h4>
		<p>25%, Deadline 12 june 13</p>
		</div>
		<div class="side-mini-graph">
		<div class="p-collection"><span class="pc-epie-chart"
			data-percent="45"> <span class="percent"></span> </span></div>
		</div>
		</div>
		<div class="prog-row side-mini-stat">
		<div class="side-graph-info">
		<h4>delivery pending</h4>
		<p>44%, Deadline 12 june 13</p>
		</div>
		<div class="side-mini-graph">
		<div class="d-pending"></div>
		</div>
		</div>
		<div class="prog-row side-mini-stat">
		<div class="col-md-12">
		<h4>total progress</h4>
		<p>50%, Deadline 12 june 13</p>
		<div class="progress progress-xs mtop10">
		<div style="width: 50%" aria-valuemax="100" aria-valuemin="0"
			aria-valuenow="20" role="progressbar"
			class="progress-bar progress-bar-info"><span class="sr-only">50%
		Complete</span></div>
		</div>
		</div>
		</div>
		</li>
	</ul>
	</li>
	<li class="widget-collapsible"><a href="#"
		class="head widget-head terques-bg active clearfix"> <span
		class="pull-left">contact online (5)</span> <span
		class="pull-right widget-collapse"><i class="ico-minus"></i></span> </a>
	<ul class="widget-container">
		<li>
		<div class="prog-row">
		<div class="user-thumb"><a href="#"><img
			src="images/avatar1_small.jpg" alt=""></a></div>
		<div class="user-details">
		<h4><a href="#">Jonathan Smith</a></h4>
		<p>Work for fun</p>
		</div>
		<div class="user-status text-danger"><i class="fa fa-comments-o"></i>
		</div>
		</div>
		<div class="prog-row">
		<div class="user-thumb"><a href="#"><img src="images/avatar1.jpg"
			alt=""></a></div>
		<div class="user-details">
		<h4><a href="#">Anjelina Joe</a></h4>
		<p>Available</p>
		</div>
		<div class="user-status text-success"><i class="fa fa-comments-o"></i>
		</div>
		</div>
		<div class="prog-row">
		<div class="user-thumb"><a href="#"><img src="images/chat-avatar2.jpg"
			alt=""></a></div>
		<div class="user-details">
		<h4><a href="#">Jhone Doe</a></h4>
		<p>Away from Desk</p>
		</div>
		<div class="user-status text-warning"><i class="fa fa-comments-o"></i>
		</div>
		</div>
		<div class="prog-row">
		<div class="user-thumb"><a href="#"><img
			src="images/avatar1_small.jpg" alt=""></a></div>
		<div class="user-details">
		<h4><a href="#">Mark Henry</a></h4>
		<p>working</p>
		</div>
		<div class="user-status text-info"><i class="fa fa-comments-o"></i></div>
		</div>
		<div class="prog-row">
		<div class="user-thumb"><a href="#"><img src="images/avatar1.jpg"
			alt=""></a></div>
		<div class="user-details">
		<h4><a href="#">Shila Jones</a></h4>
		<p>Work for fun</p>
		</div>
		<div class="user-status text-danger"><i class="fa fa-comments-o"></i>
		</div>
		</div>
		<p class="text-center"><a href="#" class="view-btn">View all Contacts</a>
		</p>
		</li>
	</ul>
	</li>
	<li class="widget-collapsible"><a href="#"
		class="head widget-head purple-bg active"> <span class="pull-left">
	recent activity (3)</span> <span class="pull-right widget-collapse"><i
		class="ico-minus"></i></span> </a>
	<ul class="widget-container">
		<li>
		<div class="prog-row">
		<div class="user-thumb rsn-activity"><i class="fa fa-clock-o"></i></div>
		<div class="rsn-details ">
		<p class="text-muted">just now</p>
		<p><a href="#">John Sinna </a>Purchased new equipments for zonal
		office setup</p>
		</div>
		</div>
		<div class="prog-row">
		<div class="user-thumb rsn-activity"><i class="fa fa-clock-o"></i></div>
		<div class="rsn-details ">
		<p class="text-muted">2 min ago</p>
		<p><a href="#">Sumon </a>Purchased new equipments for zonal office
		setup</p>
		</div>
		</div>
		<div class="prog-row">
		<div class="user-thumb rsn-activity"><i class="fa fa-clock-o"></i></div>
		<div class="rsn-details ">
		<p class="text-muted">1 day ago</p>
		<p><a href="#">Mosaddek </a>Purchased new equipments for zonal office
		setup</p>
		</div>
		</div>
		</li>
	</ul>
	</li>
	<li class="widget-collapsible"><a href="#"
		class="head widget-head yellow-bg active"> <span class="pull-left">
	shipment status</span> <span class="pull-right widget-collapse"><i
		class="ico-minus"></i></span> </a>
	<ul class="widget-container">
		<li>
		<div class="col-md-12">
		<div class="prog-row">
		<p>Full sleeve baby wear (SL: 17665)</p>
		<div class="progress progress-xs mtop10">
		<div class="progress-bar progress-bar-success" role="progressbar"
			aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
			style="width: 40%"><span class="sr-only">40% Complete</span></div>
		</div>
		</div>
		<div class="prog-row">
		<p>Full sleeve baby wear (SL: 17665)</p>
		<div class="progress progress-xs mtop10">
		<div class="progress-bar progress-bar-info" role="progressbar"
			aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
			style="width: 70%"><span class="sr-only">70% Completed</span></div>
		</div>
		</div>
		</div>
		</li>
	</ul>
	</li>
</ul>
</div>
</section>
<!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

</section>

<script>
function SearchLeadConditioned(val){
	
	if('' === val){
		alert("Select search value first");
	}else{
		
		var url =  "<?php echo base_url()."siteadmin/request/index/"; ?>"+val;
		$( location ).attr("href",url);
	}
   
  
   
  }

function SearchLeadConditionedDate(val){

if('' === val){
		alert("Select search value first");
	}else{
		var newdate = val.split("-");
		var validDate = newdate['0']+"-"+newdate['1']+"-"+newdate['2'];
		 
		 var cond = "lead_date LIKE ~$"+validDate+"$~";
		 
		var url =  "<?php echo base_url()."siteadmin/request/index/"; ?>"+cond;
		$( location ).attr("href",url);
	} 
   

}
</script>