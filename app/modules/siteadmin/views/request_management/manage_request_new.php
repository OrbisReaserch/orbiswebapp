<?php $this->load->view('backend/leftsidebar'); ?>
<!--sidebar end-->
<style type="text/css">

#container .pagination ul li.inactive,
#container .pagination ul li.inactive:hover{
    background-color:#ededed;
    color:#bababa;
    border:1px solid #bababa;
    cursor: default;
}
#container .data ul li{
    list-style: none;
    font-family: verdana;
    margin: 5px 0 5px 0;
    color: #000;
    font-size: 12px;
}

#container .pagination{

    height: 25px;
    margin-top:20px;
    float: left;
    padding: 0;
	margin: 0;
	width: 100%;
	clear: both;
                    //margin-left:0px !important;
}
#container .pagination input{

    height: 30px;
    margin-right: 8px;
                    //margin-left:0px !important;
}
#container .pagination ul{
    list-style: none;
    float: left;
    padding: 0;
margin: 0;
}
#container .pagination ul li{
    list-style: none;
    float: left;
    border: 1px solid #1cc3c9;
    padding: 2px 6px 2px 6px;
    margin: 0 3px 0 3px;
    font-family: arial;
    font-size: 12px;
    color: #999;
    font-weight: bold;
    background-color: #ffffff;
}
#container .pagination ul li:hover{
    color: #fff;
    background-color: #1cc3c9;
    cursor: pointer;
}
            .go_button
            {
            background-color:#ffffff;border:1px solid #fb0455;color:#cc0000;padding:2px 6px 2px 6px;cursor:pointer;position:absolute;margin-top:-1px;
            }
            .total
            {
             float:right;font-family:arial;color:#bababa; font-size: 12px;
             margin:5px 25px;
            }

</style>
<!--main content start-->
<section id="main-content">
<section class="wrapper">
<!-- page start-->
<div class="row">
<div class="col-sm-12"><section class="panel"> <header
	class="panel-heading"> Manage Requests <span style="margin-left: 30px;">
<?php
if($this->session->flashdata('success'))
{
	echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
}
else if($this->session->flashdata('error'))
{
	echo "<font style='color:red;'>".$this->session->flashdata('errror')."</font>";
}
?> </span> <span class="tools pull-right"> 
<form action  = "<?php echo base_url();?>siteadmin/report/getrequestexcel" method = "post">
<input type = "hidden"   value = "<?php echo $this->uri->segment(4);?>" id = "excelCond" name = "excelCond"/>
 
<input type = "submit"  style="color: #ffffff;" class="btn btn-primary btn-mini" value = "Download Excel"/>

</form>
<a href="javascript:;" class="fa fa-chevron-down"></a> </span> </header>
<div class="panel-body">
<div class="row">
						 <div class="col-sm-3">
						 <select name="req_type" class=" form-control" id = "req_type" onchange = "SearchLeadConditioned(this.value);">
							<option value = "">Select Request Type</option>
						 
							<option <?php if(urldecode($condtion) == "request_type = ~Enquiry~" ){echo "selected";}?> value = "<?php echo "request_type = ~Enquiry~"; ?>">Enquiry</option>
							<option <?php if(urldecode($condtion) == "request_type = ~Request For Discount~" ){echo "selected";}?> value = "<?php echo "request_type = ~Request For Discount~"; ?>">Request For Discount</option>
							<option <?php if(urldecode($condtion) == "request_type = ~Request For Sample~" ){echo "selected";}?> value = "<?php echo "request_type = ~Request For Sample~"; ?>">Request For Sample</option>
									 
						</select>
						</div>
						 <div class="col-sm-3">
						 <select name="coyntry" class=" form-control" id = "coyntry" onchange = "SearchLeadConditioned(this.value);">
							<option value = "">Select Country</option>
							<?php 
							for($i=0;$i<count($countries);$i++)
                                    {
							?>
							<option <?php if(urldecode($condtion) == "country = ~".$countries[$i]->country_name."~" ){echo "selected";}?> value = "<?php echo "country = ~".$countries[$i]->country_name."~"; ?>"><?php echo $countries[$i]->country_name; ?></option>
									<?php } ?>
						</select>
						</div>
						<div class="col-sm-3">
						 <select name="publisher" class=" form-control" id = "publisher" onchange = "SearchLeadConditioned(this.value);">
							<option value = "">Select Publisher</option>
							<?php 
							for($i=0;$i<count($publishers);$i++)
                                    {
							?>
							<option <?php if(urldecode($condtion) == "publisher_id = ~".$publishers[$i]->id."~" ){echo "selected";}?> value = "<?php echo "publisher_id = ~".$publishers[$i]->id."~"; ?>"><?php echo $publishers[$i]->display_name; ?></option>
									<?php } ?>
						</select>
						</div>
<div class="col-sm-3"> 
	 <input type = "text" class="form-control-inline input-medium default-date-picker" id = "date" onblur="SearchLeadConditionedDate(this.value);"/>
						</div>
					</div>
					<br/>
<div class="adv-table">
<div class="pagination" style="margin-bottom: 2%;margin-top: 1%;">
	<ul>
	<li><a href="<?php echo base_url();?>siteadmin/request/index_new/<?php print_r($prev_page);?>/<?php echo $condtion;?>"
				>Previous</a></li>
	<li><a href="<?php echo base_url();?>siteadmin/request/index_new/<?php print_r($next_page);?>/<?php echo $condtion;?>"
				>Next</a></li>
	</ul> 
	<!--<span class="total"><input type = "number" min = "1" id = "page_no_1"/><a class="btn btn-warning btn-mini target_1" >Go</a></span>-->
</div>
<table class="display table table-bordered table-striped"
	id="dynamic-table">
	<thead>
		<tr>
			<th>Sr No</th>
			<th>User name</th>
			<th>Email Id</th>
			<th>Contact No</th>
			<th>Country</th>
			<th>Report</th>
			<th>Date</th>
			<th>Request Type</th>
			<th>Publisher</th>
			<th>Message</th>
			<th>Category</th>
			<th>Company</th>
			
			<th width="270">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php
	for($i=0;$i<count($result);$i++)
	{
		$report = $this->db->query("select * from report where id = '".$result[$i]->report_id."'");
		$report_name = $report->result();
		$publisher= $this->db->query("select * from users where id = '".$result[$i]->publisher_id."'");
		$publisher_name = $publisher->result();
		
		$publisher= $this->db->query("select category_name from category where id = '".$report_name[0]->category_id."'");
		$category_name = $publisher->result();
		
		?>
		<tr class="gradeA">
			<td><?php echo $result[$i]->id;?></td>
			<td><?php echo $result[$i]->username;?></td>
			<td><?php echo $result[$i]->email;?></td>
			<td><?php echo $result[$i]->contact_no;?></td>
			<td><?php echo $result[$i]->country;?></td>
			<td><?php echo $report_name[0]->report_name;?></td>
			<td><?php echo $result[$i]->lead_date;?></td>
			<td><?php echo $result[$i]->request_type;?></td>
			<td><?php echo $publisher_name[0]->display_name;?></td>
			<td><?php echo $result[$i]->messages;?></td>
			<td><?php echo $category_name[0]->category_name;?></td>
			<td><?php echo $result[$i]->company_name;?></td>
			<td><?php if($result[$i]->mail_status == '0'){?> <a
				href="<?php echo base_url();?>siteadmin/request/send_mail/<?php echo $result[$i]->publisher_id;?>/<?php echo $result[$i]->id;?>/<?php echo $result[$i]->report_code;?>"
				style="color: #ffffff;" class="btn btn-primary btn-mini">Send To
			Publisher</a> <?php } else {?> <a
				href="<?php echo base_url();?>siteadmin/request/send_mail/<?php echo $result[$i]->publisher_id;?>/<?php echo $result[$i]->id;?>/<?php echo $result[$i]->report_code;?>"
				style="color: #ffffff;" class="btn btn-primary btn-mini">Resend</a>
				<?php
			}
			if($result[$i]->status==0)
			{
				?> <a
				href="<?php echo base_url();?>siteadmin/request/activate/<?php echo $result[$i]->id;?>"
				class="btn btn-success btn-mini">Activate</a> <?php
			}
			else
			{
				?> <a
				href="<?php echo base_url();?>siteadmin/request/deactivate/<?php echo $result[$i]->id;?>"
				class="btn btn-warning btn-mini">Deactivate</a> <?php
			}
			?>
			 <a style = "display:none;"
				href="<?php echo base_url();?>siteadmin/request/DeleteRequest/<?php echo $result[$i]->id;?>"
				class="btn btn-success btn-mini">Delete</a> 
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>
<div id="container" style=" margin-top: 2%; ">
	
	<div class="col-lg-12 col-md-12 col-sm-12">
	<div class="pagination">
		<ul>
		<li><a href="<?php echo base_url();?>siteadmin/request/index_new/<?php print_r($prev_page);?>/<?php echo $condtion;?>"
					>Previous</a></li>
		<li><a href="<?php echo base_url();?>siteadmin/request/index_new/<?php print_r($next_page);?>/<?php echo $condtion;?>"
					>Next</a></li>
		</ul>  
		<!--<span class="total"><input type = "number" min = "1" id = "page_no_1"/><a class="btn btn-warning btn-mini target_1" >Go</a></span>-->
	</div>
	
	</div>
</div>
</div>

</div>

</section></div>
</div>
<!-- page end-->
</section>
<div class="right-sidebar">
<div class="search-row"><input type="text" placeholder="Search"
	class="form-control"></div>
<ul class="right-side-accordion">
	<li class="widget-collapsible"><a href="#"
		class="head widget-head red-bg active clearfix"> <span
		class="pull-left">work progress (5)</span> <span
		class="pull-right widget-collapse"><i class="ico-minus"></i></span> </a>
	<ul class="widget-container">
		<li>
		<div class="prog-row side-mini-stat clearfix">
		<div class="side-graph-info">
		<h4>Target sell</h4>
		<p>25%, Deadline 12 june 13</p>
		</div>
		<div class="side-mini-graph">
		<div class="target-sell"></div>
		</div>
		</div>
		<div class="prog-row side-mini-stat">
		<div class="side-graph-info">
		<h4>product delivery</h4>
		<p>55%, Deadline 12 june 13</p>
		</div>
		<div class="side-mini-graph">
		<div class="p-delivery">
		<div class="sparkline" data-type="bar" data-resize="true"
			data-height="30" data-width="90%" data-bar-color="#39b7ab"
			data-bar-width="5"
			data-data="[200,135,667,333,526,996,564,123,890,564,455]"></div>
		</div>
		</div>
		</div>
		<div class="prog-row side-mini-stat">
		<div class="side-graph-info payment-info">
		<h4>payment collection</h4>
		<p>25%, Deadline 12 june 13</p>
		</div>
		<div class="side-mini-graph">
		<div class="p-collection"><span class="pc-epie-chart"
			data-percent="45"> <span class="percent"></span> </span></div>
		</div>
		</div>
		<div class="prog-row side-mini-stat">
		<div class="side-graph-info">
		<h4>delivery pending</h4>
		<p>44%, Deadline 12 june 13</p>
		</div>
		<div class="side-mini-graph">
		<div class="d-pending"></div>
		</div>
		</div>
		<div class="prog-row side-mini-stat">
		<div class="col-md-12">
		<h4>total progress</h4>
		<p>50%, Deadline 12 june 13</p>
		<div class="progress progress-xs mtop10">
		<div style="width: 50%" aria-valuemax="100" aria-valuemin="0"
			aria-valuenow="20" role="progressbar"
			class="progress-bar progress-bar-info"><span class="sr-only">50%
		Complete</span></div>
		</div>
		</div>
		</div>
		</li>
	</ul>
	</li>
	<li class="widget-collapsible"><a href="#"
		class="head widget-head terques-bg active clearfix"> <span
		class="pull-left">contact online (5)</span> <span
		class="pull-right widget-collapse"><i class="ico-minus"></i></span> </a>
	<ul class="widget-container">
		<li>
		<div class="prog-row">
		<div class="user-thumb"><a href="#"><img
			src="images/avatar1_small.jpg" alt=""></a></div>
		<div class="user-details">
		<h4><a href="#">Jonathan Smith</a></h4>
		<p>Work for fun</p>
		</div>
		<div class="user-status text-danger"><i class="fa fa-comments-o"></i>
		</div>
		</div>
		<div class="prog-row">
		<div class="user-thumb"><a href="#"><img src="images/avatar1.jpg"
			alt=""></a></div>
		<div class="user-details">
		<h4><a href="#">Anjelina Joe</a></h4>
		<p>Available</p>
		</div>
		<div class="user-status text-success"><i class="fa fa-comments-o"></i>
		</div>
		</div>
		<div class="prog-row">
		<div class="user-thumb"><a href="#"><img src="images/chat-avatar2.jpg"
			alt=""></a></div>
		<div class="user-details">
		<h4><a href="#">Jhone Doe</a></h4>
		<p>Away from Desk</p>
		</div>
		<div class="user-status text-warning"><i class="fa fa-comments-o"></i>
		</div>
		</div>
		<div class="prog-row">
		<div class="user-thumb"><a href="#"><img
			src="images/avatar1_small.jpg" alt=""></a></div>
		<div class="user-details">
		<h4><a href="#">Mark Henry</a></h4>
		<p>working</p>
		</div>
		<div class="user-status text-info"><i class="fa fa-comments-o"></i></div>
		</div>
		<div class="prog-row">
		<div class="user-thumb"><a href="#"><img src="images/avatar1.jpg"
			alt=""></a></div>
		<div class="user-details">
		<h4><a href="#">Shila Jones</a></h4>
		<p>Work for fun</p>
		</div>
		<div class="user-status text-danger"><i class="fa fa-comments-o"></i>
		</div>
		</div>
		<p class="text-center"><a href="#" class="view-btn">View all Contacts</a>
		</p>
		</li>
	</ul>
	</li>
	<li class="widget-collapsible"><a href="#"
		class="head widget-head purple-bg active"> <span class="pull-left">
	recent activity (3)</span> <span class="pull-right widget-collapse"><i
		class="ico-minus"></i></span> </a>
	<ul class="widget-container">
		<li>
		<div class="prog-row">
		<div class="user-thumb rsn-activity"><i class="fa fa-clock-o"></i></div>
		<div class="rsn-details ">
		<p class="text-muted">just now</p>
		<p><a href="#">John Sinna </a>Purchased new equipments for zonal
		office setup</p>
		</div>
		</div>
		<div class="prog-row">
		<div class="user-thumb rsn-activity"><i class="fa fa-clock-o"></i></div>
		<div class="rsn-details ">
		<p class="text-muted">2 min ago</p>
		<p><a href="#">Sumon </a>Purchased new equipments for zonal office
		setup</p>
		</div>
		</div>
		<div class="prog-row">
		<div class="user-thumb rsn-activity"><i class="fa fa-clock-o"></i></div>
		<div class="rsn-details ">
		<p class="text-muted">1 day ago</p>
		<p><a href="#">Mosaddek </a>Purchased new equipments for zonal office
		setup</p>
		</div>
		</div>
		</li>
	</ul>
	</li>
	<li class="widget-collapsible"><a href="#"
		class="head widget-head yellow-bg active"> <span class="pull-left">
	shipment status</span> <span class="pull-right widget-collapse"><i
		class="ico-minus"></i></span> </a>
	<ul class="widget-container">
		<li>
		<div class="col-md-12">
		<div class="prog-row">
		<p>Full sleeve baby wear (SL: 17665)</p>
		<div class="progress progress-xs mtop10">
		<div class="progress-bar progress-bar-success" role="progressbar"
			aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
			style="width: 40%"><span class="sr-only">40% Complete</span></div>
		</div>
		</div>
		<div class="prog-row">
		<p>Full sleeve baby wear (SL: 17665)</p>
		<div class="progress progress-xs mtop10">
		<div class="progress-bar progress-bar-info" role="progressbar"
			aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
			style="width: 70%"><span class="sr-only">70% Completed</span></div>
		</div>
		</div>
		</div>
		</li>
	</ul>
	</li>
</ul>
</div>
</section>
<!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
function SearchLeadConditioned(val){
	
	if('' === val){
		alert("Select search value first");
	}else{
		
		var url =  "<?php echo base_url()."siteadmin/request/index_new/1/"; ?>"+val;
		$( location ).attr("href",url);
	}
   
  
   
  }

function SearchLeadConditionedDate(val){

if('' === val){
		alert("Select search value first");
	}else{
		var newdate = val.split("-");
		var validDate = newdate['2']+"-"+newdate['0']+"-"+newdate['1'];
		 
		 var cond = "lead_date LIKE ~$"+validDate+"$~";
		 
		var url =  "<?php echo base_url()."siteadmin/request/index_new/1/"; ?>"+cond;
		$( location ).attr("href",url);
		
	} 
   

}
$( document ).ready(function() {
  $(".dataTables_paginate").css("display","none");
  $(".dataTables_info").css("display","none");
  $(".dataTables_length").css("display","none");
  $(".dataTables_filter").css("display","none");
});


</script>