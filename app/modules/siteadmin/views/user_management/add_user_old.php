<?php $this->load->view('backend/leftsidebar'); ?>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Add New User
                            <span class="tools pull-right">
                                <a href="<?php echo base_url();?>siteadmin/user_management" style="color: #ffffff;" class="btn btn-primary btn-mini">All User</a>
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                             </span>
                        </header>
                        <div class="panel-body">
                            <div class=" form">
                                <form class="cmxform form-horizontal " id="commentForm" enctype="multipart/form-data" method="POST" action="<?php echo base_url();?>siteadmin/user_management/add_user">
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">User Name</label>
                                        <div class="col-lg-6">
                                            <input class="form-control"  id="bname" name="user_name" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Display Name</label>
                                        <div class="col-lg-6">
     	                                       <input class=" form-control" id="display_name" onblur=""  pattern = "^[a-zA-Z ]+$" name="display_name" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Password</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" name="password" minlength="2" type="password" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">User type</label>
                                        <div class="col-lg-6">
<!--                                            <input class=" form-control" id="bname" name="user_type" minlength="2" type="text" required />-->
                                        <select name="user_type" id="bname" class="form-control">
                                        <option value="Client">Client</option>
                                        <option value="Publisher">Publisher</option>
                                       <!-- <option value="Admin">Admin</option> -->
                                        </select>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Email</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="email" name="email" minlength="2" type="email" required onblur="checkEmail(this.value);"/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Company Name</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" name="company_name" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">First Name</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" name="first_name"  pattern = "^[a-zA-Z ]+$" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Middle Name</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" name="middle_name"  pattern = "^[a-zA-Z ]+$" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Last Name</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" name="last_name"  pattern = "^[a-zA-Z ]+$" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Address 1</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" name="address1" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Address 2</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" name="address2" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Zip Code</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" pattern = "^(0|[1-9][0-9]*)$" name="zipcode" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                     <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">City</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" pattern = "^[a-zA-Z ]+$" name="city" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Country</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" name="country"  pattern = "^[a-zA-Z ]+$" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">State</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" name="state"  pattern = "^[a-zA-Z]+$" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Mobile</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname" maxlength="10" pattern = "^(0|[1-9][0-9]*)$" name="mobile" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3"> Phone</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname"  pattern = "^(0|[1-9][0-9]*)$" name="phone" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Fax</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="bname"  pattern = "^(0|[1-9][0-9]*)$"   name="fax" minlength="2" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-3 col-lg-6">
                                            <button class="btn btn-primary" name="submit" type="submit">Save</button>
                                            <a href="<?php echo base_url();?>siteadmin/user_management">
                                            <button class="btn btn-default" type="button">Cancel</button>
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        <!-- page end-->
    </section>
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <ul class="right-side-accordion">
            <li class="widget-collapsible">
                <a href="#" class="head widget-head red-bg active clearfix">
                    <span class="pull-left">work progress (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row side-mini-stat clearfix">
                            <div class="side-graph-info">
                                <h4>Target sell</h4>
                                <p>
                                    25%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="target-sell">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>product delivery</h4>
                                <p>
                                    55%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="p-delivery">
                                    <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info payment-info">
                                <h4>payment collection</h4>
                                <p>
                                    25%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="p-collection">
                                    <span class="pc-epie-chart" data-percent="45">
                                        <span class="percent"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jhone Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">John Sinna </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Sumon </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Mosaddek </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</section>
<!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

</section>


<script language="javascript">
function checkEmail(value) {
var	eid = document.getElementById('email');
var validRegExp = /^[^@]+@[^@]+.[a-z]{2,}$/i;
strEmail = value;
if(value==""||value==null)
{
alert("Enter the Email Id");
eid.focus();
}
else (strEmail.search(validRegExp) == -1)
{
alert(" A valid e-mail address is required.");
} 
}

function checkAlpha(value){
	var displayName = document.getElementById('display_name')
	var alphaExp=/^[a-z A-Z]+$/;
	if(value==""||value==null)
	{
	alert("Enter the Display name");
	displayName.focus();
	} else
	if(!(value.match(alphaExp)))
	{
		alert("Enter insert characters in display name");
		displayName.focus();
	}
}
                      
</script>