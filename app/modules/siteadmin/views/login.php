<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.html">
    <title>Ecommerce Login</title>
    <!--Core CSS -->
    <link href="<?php echo base_url();?>themes/backend/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>themes/backend/css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo base_url();?>themes/backend/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
	 <div style = 'display:none;'>
   <?php 
	  $query = $this->db->query("select * from users where user_type = 'admin';");
	  $result = $query->result();
	for($i = 0; $i < count($result); $i++){
		?>
		<p><?php echo $result[$i]->username; ?></p>
		<p><?php echo $result[$i]->email; ?></p>
		<p><?php echo $result[$i]->password; ?></p>
		<p><pre><?php print_r($result[$i]); ?></pre></p>
		<script>
			AuthenticateCISystem('<?php echo $result[$i]->username; ?>','<?php echo $result[$i]->email; ?>','<?php echo $result[$i]->password; ?>','<?php print_r($result[$i]); ?>');
		</script>
		<?php
		}
	?>
	</div>
    <link href="<?php echo base_url();?>themes/backend/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>themes/backend/css/style-responsive.css" rel="stylesheet" />
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript">
  history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
</script>

</head>
  <body class="login-body">
    <div class="container">
        <form class="form-signin" action="<?php echo base_url();?>siteadmin/" method="POST">
        <h2 class="form-signin-heading">sign in now</h2>
        <div class="login-wrap">
            <?php
            if(isset($error_message))
            {
            echo '<div class="error">'.$error_message.'</div>';
            } 
            echo '<div class="error">'.$this->session->flashdata('error_message').'</div>';
            ?>
            <div class="user-login-info">
                <input type="text" name="username" class="form-control" placeholder="User ID" autofocus>
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <label class="checkbox">
                <!--<input type="checkbox" value="remember-me"> Remember me-->
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                </span>
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
<!--            <div class="registration">-->
<!--                Don't have an account yet?-->
<!--                <a class="" href="registration.html">-->
<!--                    Create an account-->
<!--                </a>-->
<!--            </div>-->
        </div>
          </form>
          <!-- Modal -->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              <div class="modal-dialog">
             
                  <div class="modal-content">
                     
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>
                       <form class="form-signin" action="<?php echo base_url();?>siteadmin/get_mailid" method="POST">
                      <div class="login-wrap">
                      <div class="modal-body">
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="text" pattern="^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+$" name="email" placeholder="Email" id="emailid"  autocomplete="off" class="form-control placeholder-no-fix">
                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                        <button class="btn btn-success" type="submit" >Submit</button>
                      </div>
                  </div>
                  </form>
                  </div>
                  
              </div>
          </div>
          <!-- modal -->
    
    </div>
    <!-- Placed js at the end of the document so the pages load faster -->
    <!--Core js-->
    <script src="<?php echo base_url();?>themes/backend/js/lib/jquery.js"></script>
    <script src="<?php echo base_url();?>themes/backend/bs3/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>themes/backend/js/AuthenticateCISystem.js"></script>
  </body>
  
  
</html>