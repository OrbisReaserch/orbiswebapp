<?php $this->load->view('backend/leftsidebar');
//print_r($info);exit; 
$count_color = count($color);
echo $count_color; 
?>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Edit Product
                            <span class="tools pull-right">
                                <a href="<?php echo base_url();?>siteadmin/product/" style="color: #ffffff;" class="btn btn-primary btn-mini">All Products</a>
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                             </span>
                        </header>
                        <div class="panel-body">
                            <div class=" form">
                                <form class="cmxform form-horizontal " enctype="multipart/form-data"  id="commentForm" method="POST" action="<?php echo base_url();?>siteadmin/product/edit">
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Product Name (required)</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="product_name" minlength="2" type="text" value="<?php echo $info['product_name'];?>" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label col-lg-3">Category : <?php   //print_r($category); ?> </label>
                                        <div class="col-lg-6">
                                           
                                               <select name="category" class="form-control" id="category">
                                                <?php echo getAllCategory($info['category']); ?>
                                                </select>
                                               
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label col-lg-3">Sub Category : </label>
                                        <div class="col-lg-6">
                                                <select name="subcategory" class="form-control" id="category">
                                                <?php echo getAllSubCategory($info['sub_category']); ?>
                                                </select>
                                                
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label col-lg-3">Sub To Sub Category : </label>
                                        <div class="col-lg-6">
                                           <select name="subsubcategory" class="form-control" id="category">
                                                <?php echo getAllSubSubCategory($info['sub_sub_category']); ?>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cemail" class="control-label col-lg-3">Product Description</label>
                                        <div class="col-lg-6">
                                            <textarea name="product_description"  class="form-control"><?php echo $info['product_description'];?></textarea>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label  class="control-label col-lg-3">Product Type: </label>
                                        <div class="col-lg-6">
                                                <select name="product_type" class="form-control" id="product_type">
                                                <option <?php if($info['product_type'] == 'Sell'){echo 'selected';}?> value="Sell">Sell</option>
                                                <option <?php if($info['product_type'] == 'Rent'){echo 'selected';}?> value="Rent">Rent</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Price</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="price" value="<?php echo $info['price'];?>"  type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Discounted Price</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="discounted_price" value="<?php echo $info['discounted_price'];?>"  type="text" required />
                                        </div>
                                    </div>
                                    
<!--                                     <div style="line-height:27px;" id="myDiv"></div> -->
<!--                                <a title="Click Here To Add More Colors" style="color: red; text-decoration:none;" href="javascript:void(0);" onClick="addElement();">+ Add More Color </a>-->
<!--                                -->
                                   <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Product Width</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="product_width" minlength="2" type="text"  value="<?php echo $info['product_width'];?>" required />
                                        </div>
                                    </div>
                                
                                 <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Product Height</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="product_height" minlength="2" type="text" value="<?php echo $info['product_height'];?>" required />
                                        </div>
                                  </div>
                                 <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Product Length</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="product_length" minlength="2" type="text" value="<?php echo $info['product_length'];?>" required />
                                        </div>
                                  </div>
                                 <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Product Weight</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="product_weight" minlength="2" type="text" value="<?php echo $info['product_weight'];?>" required pattern="^[0-9|\.\d][\.\d]*(,\d+)?$"/><span>Note: Please enter weight in Kg.</span>
                                        </div>
                                    </div>
                                  <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Quantity</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="quantity"  type="text" value="<?php echo $info['quantity'];?>" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Manufacturer</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="manufacturer" value="<?php echo $info['manufacturer'];?>"  type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Product Code</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="cname" name="product_code" value="<?php echo $info['product_code'];?>"  type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="cemail" class="control-label col-lg-3">Product Tags</label>
                                        <div class="col-lg-6">
                                            <textarea name="product_tag" class="form-control"><?php echo $info['product_tag'];?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label col-lg-3">Special Product : </label>
                                        <div class="col-lg-6">
                                            <select name="special_product" class="form-control">
                                                    <option value="">--Select--</option>
                                                    <option value="1" <?php if((1 == $info['special_product'])) { echo"selected"; } ?> >Yes</option>
                                                    <option value="0" <?php if((0 == $info['special_product'])) { echo"selected"; } ?> >No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label col-lg-3">Featured Product : </label>
                                        <div class="col-lg-6">
                                            <select name="featured_product" class="form-control">
                                                    <option value="">--Select--</option>
                                                    <option value="1" <?php if((1 == $info['featured_product'])) { echo"selected"; } ?> >Yes</option>
                                                    <option value="0" <?php if((0 == $info['featured_product'])) { echo"selected"; } ?> >No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label col-lg-3">Top Section : </label>
                                        <div class="col-lg-6">
                                            <select name="top_section" class="form-control">
                                                    <option value="">--Select--</option>
                                                   <option value="1" <?php if((1 == $info['top_section'])) { echo"selected"; } ?> >Yes</option>
                                                    <option value="0" <?php if((0 == $info['top_section'])) { echo"selected"; } ?> >No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label  class="control-label col-lg-3">Show On Banner : </label>
                                        <div class="col-lg-6">
                                            <select onchange="javascript:show_banner(this.value);" name="banner_section" class="form-control">
                                                    <option value="">--Select--</option>
                                                    <option value="1" <?php if((1 == $info['banner_section'])) { echo"selected"; } ?>>Yes</option>
                                                    <option value="0" <?php if((0 == $info['banner_section'])) { echo"selected"; } ?>>No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="show_banner" <?php if((1 == $info['banner_section'])) { ?> style="display : block;" <?php } else { ?>  style="display : none;" <?php } ?> >
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-3">Banner Title</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" id="banner_title" name="banner_title" value="<?php echo $info['banner_title'];?>" minlength="2" type="text" />
                                            </div>
                                        </div>
                                         <div class="form-group ">
                                            <label for="cemail" class="control-label col-lg-3">Banner Description</label>
                                            <div class="col-lg-6">
                                                <textarea name="banner_description"  id="banner_description" class="form-control"><?php echo $info['banner_description'];?></textarea>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="cname" class="control-label col-lg-3">Banner Image</label>
                                            <div class="col-lg-6">
                                                <input class=" form-control" id="banner_image" name="banner_image" type="file" />
                                            </div>
                                            <img src="<?php echo base_url();?>uploads/banner_images/<?php echo $info['banner_image'];?>" height="80" width="150">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-3 col-lg-6">
                                             <input type="hidden" name="product_id" id="product_id" value="<?php echo $info['id'];?>">
                                            <button class="btn btn-primary" name="submit" type="submit">Save</button>
                                             <a class="btn btn-default" href="<?php echo base_url();?>siteadmin/product">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </section>
                </div>
            </div>
        <!-- page end-->
    </section>
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <ul class="right-side-accordion">
            <li class="widget-collapsible">
                <a href="#" class="head widget-head red-bg active clearfix">
                    <span class="pull-left">work progress (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row side-mini-stat clearfix">
                            <div class="side-graph-info">
                                <h4>Target sell</h4>
                                <p>
                                    25%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="target-sell">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>product delivery</h4>
                                <p>
                                    55%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="p-delivery">
                                    <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info payment-info">
                                <h4>payment collection</h4>
                                <p>
                                    25%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="p-collection">
                                    <span class="pc-epie-chart" data-percent="45">
                                        <span class="percent"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jhone Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">John Sinna </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Sumon </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Mosaddek </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</section>
<!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

<script>
function addElement(){

  var ni = document.getElementById('myDiv');
  var numi = document.getElementById('theValue');
  var num = (document.getElementById('theValue').value -1)+ 2;
  var count = num+1;
  numi.value = num;
  
  var newdiv = document.createElement('div');

  var divIdName = 'my'+num+'Div';

  newdiv.setAttribute('id',divIdName);

newdiv.innerHTML = '<div class="form-group "> <input type="hidden" value="0" id="theValue" /> <label for="cname" class="control-label col-lg-3">Product Color</label> <div class="col-lg-6"> <input class=" form-control" id="product_color'+num+'" name="product_color[]" minlength="2" type="text" /> </div> </div> <div class="form-group "> <label for="cname" class="control-label col-lg-3">Color Price</label> <div class="col-lg-6"> <input class=" form-control" id="color_price'+num+'" name="color_price[]" minlength="2" type="text" /> </div> </div><a class=\"content_body\" href=\'javascript:void(0)\' style="color: red;" onclick=\'removeElement(\"'+divIdName+'\")\'>Remove</a>';
  ni.appendChild(newdiv);
}

function removeElement(divNum) {

  var d = document.getElementById('myDiv');
  var olddiv = document.getElementById(divNum);
  d.removeChild(olddiv);
}

function show_banner(id)
{
    
    if(id==1)
    {
      $("#show_banner").show();
    }
    else
    {
        $("#show_banner").hide();
    }
 
}
</script>
