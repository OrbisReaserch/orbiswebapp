<div class="container mt20">
  <div class="leftNav">
 <?php  $this->load->view('backend/leftsidebar');?> 
  </div>
  <div class="span9">
    <div class="row-fluid">
      <div class="span12">
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>">Home</a> <span class="divider">/</span></li>
          <li><a href="<?php echo base_url();?>admin/page_content/">Edit Membership Plan</a> <span class="divider">/</span></li>
          <li class="active">Edit Membership Plan</li>
        </ul>
      </div>
    </div>
    <div class="row-fluid">
			<div class="span12">
				<div class="widget-block">
					<div class="widget-head">
						<h5>Edit Page Content</h5>
					</div>
					<div class="widget-content">
				    <div class="widget-box">
				    <div id="DataTables_Table_2_wrapper" class="dataTables_wrapper" role="grid">
				      <div class="table_content">
 
						<?php echo form_open(base_url().'siteadmin/membership_plan/edit_membership_plan/'.$info[0]->id); ?>
                      <table class="data-tbl-tools table dataTable" id="DataTables_Table_2">
							<tbody role="alert" aria-live="polite" aria-relevant="all">
							  <tr class="odd">
							    <td colspan="3" class=" sorting_1"><span class=" sorting_1" style="width:300px;"><strong>    Information </strong>  </span></td>
                                <input type="hidden" name="id" value="<?php echo $info[0]->id;  ?>">
						      </tr>
                             
							  <tr class="odd">
								<th class=" " style="width:180px;">  Name</th>
								<td class=" " style="width:180px;">
								 <input type="text"  name="name" value=" <?php echo $info[0]->name;  ?>"  >
								</td>
							  </tr>
                              
                              <tr class="even">
									<th class=" ">Description</th>
								<td class=" ">
								<textarea name="description"   rows="10">  <?php echo $info[0]->description;  ?></textarea>
								</td>
                                </tr>
                                
                                <tr class="odd">
								<th class=" ">Price</th>
								<td class=" ">
								 <input type="text" name="price" value="<?php echo $info[0]->price;  ?>" />
								</td>
								</tr>
                                
                                
                                <tr class="even">
									<th class=" ">validity</th>
								<td class=" ">
							 <input type="text" name="validity" value="<?php echo $info[0]->validity;  ?>" />
								</td>
                                </tr>
                                
                                
                                 <tr class="odd">
								<th class=" ">Unit</th>
								<td class=" ">
								 <input type="text" name="unit" value="<?php echo $info[0]->validity_unit;  ?>" />
								</td>
								</tr>
                                
								 <tr class="odd">
								<th class=" " ></th>
								<td class=" ">
								 <input  class="btn btn-inverse" type="submit" name="UPDATE" value="UPDATE">
								</td>
								</tr>	 
						   
                              
                                 </tbody></table>
                                 </form>
                            </div><div class="widget-bottom">
					     <div class="clear"></div></div></div>
						</div>
					</div>
				</div>
			</div>
		</div>

  </div>