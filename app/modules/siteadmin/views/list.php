<div class="container mt20">
<div class="leftNav"><?php  $this->load->view('backend/leftsidebar');?>
</div>
<div class="span9 minh">
<div class="row-fluid">
<div class="span12">
<ul class="breadcrumb">
	<li><a href="<?php echo base_url();?>">Home</a> <span class="divider">/</span></li>
	<li><a
		href="<?php echo base_url();?>siteadmin/<?php echo $admin_section;?>/"><?php echo ucfirst($admin_section);?></a>
	<span class="divider">/</span></li>
	<li class="active"></li>
</ul>
</div>
</div>
<?php  if($this->session->flashdata('success')) {?>
<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?>
</div>
<?php }?>
<div class="row-fluid">
<div class="span12">
<div class="widget-block">
<div class="widget-head">
<h5><?php echo ucfirst($admin_section);?></h5>
</div>
<div class="widget-content">
<div class="widget-box">
<div id="DataTables_Table_2_wrapper" class="dataTables_wrapper"
	role="grid">
<div class="tbl_tools">
<div class="DTTT_container"><a
	href="<?php echo base_url();?>siteadmin/<?php echo str_replace(' ','_',$admin_section); ?>/add/">
<input class="btn btn-inverse" type="submit" name="CREATE NEW META FAQ"
	value="Add New <?php echo ucfirst($admin_section);?>"></a></div>
<div class="clear"></div>
</div>
<div class="table_content">
<table class="data-tbl-tools table dataTable" id="DataTables_Table_2">
	<thead>

		<tr role="row">
			<th style="width: 80px;">Sr.No</th>

			<th style="width: 129px;">Couple Name</th>

			<th style="width: 129px;">Image</th>

			<th style="width: 150px;">Action</th>
		</tr>
	</thead>

	<tbody role="alert" aria-live="polite" aria-relevant="all">
	<?php if(count($result)>0){
		$i=0;
		foreach($result as $info){ $i++ ;?>
		<tr class="odd">
			<td><?php echo $i ;?></td>
			<td class=" "><?php echo $info->bride_name.' and '.$info->groom_first_name.' '.$info->groom_surname;  ?>
			</td>
			<td class=" "><?php if($info->banner)
			echo '<img src="'.base_url().'uploads/'.$info->banner.'" style="max-width:100px; max-height:50px;">';
			?></td>

			<td class=" "><a
				href="<?php echo base_url();?>siteadmin/<?php echo str_replace(' ','_',$admin_section); ?>/edit/<?php echo $info->id;?>"
				class="icol-desktop" title="Edit"><span class="label label-success">Edit</span></a>
				<?php
				if($info->status == 'Y')
				{
					?> <a
				href="<?php echo base_url();?>siteadmin/<?php echo str_replace(' ','_',$admin_section); ?>/deactivate/<?php echo $info->id;?>"
				title="Edit"><span class="label label-success">Deactivate</span></a>
				<?php
				}
				else {
					?> <a
				href="<?php echo base_url();?>siteadmin/<?php echo str_replace(' ','_',$admin_section); ?>/activate/<?php echo $info->id;?>"
				title="Edit"><span class="label label-info">Activate</span></a> <?php }    ?>

			<a
				href="<?php echo base_url();?>siteadmin/<?php echo str_replace(' ','_',$admin_section); ?>/delete/<?php echo $info->id;?>"
				class="icol-cancel" title="Delete"
				onclick="return confirm('Are you sure ? You want to  Delete it');"><span
				class="label label-important">Delete</span></a></td>

		</tr>
		<?php } }else{?>
		<tr>
			<td colspan="7" align="center" bordercolor="#FF0000"><font
				color="#FF0000"><strong>No real weddings are present. Start adding
			them by using Add link.</strong></font></td>
		</tr>
		<?php }?>
	</tbody>
</table>
</div>

<div class="widget-bottom">




<div class="dataTables_paginate paging_full_numbers"
	id="DataTables_Table_2_paginate"><!--
                            
                            <a tabindex="0" class="first paginate_button paginate_button_disabled" id="DataTables_Table_2_first">First</a>
                            <a tabindex="0" class="previous paginate_button paginate_button_disabled" id="DataTables_Table_2_previous">Previous</a>
                            <span>
                            <a tabindex="0" class="paginate_active">1</a>
                            <a tabindex="0" class="paginate_button">2</a>
                            <a tabindex="0" class="paginate_button">3</a>
                            <a tabindex="0" class="paginate_button">4</a>
                            <a tabindex="0" class="paginate_button">5</a>
                            </span>
                            <a tabindex="0" class="next paginate_button" id="DataTables_Table_2_next">Next</a>
                            <a tabindex="0" class="last paginate_button" id="DataTables_Table_2_last">Last</a>-->
</div>

<div class="clear"></div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>