<?php
class Brand_model extends CI_Model {
                public function add()
		{
			$this->Checklogin();
			$data['admin_section'] = 'category';

			$this->form_validation->set_rules('name', 'Category Name', 'required|is_unique[category.name]');
			$this->form_validation->set_rules('cat_order', 'Category Order', 'required');


			if ($this->form_validation->run() === FALSE)
			{
				$data['include'] = 'siteadmin/category/add';
				$this->load->view('backend/container',$data);	

			}
			else 
			{
				$id = $this->category_model->add();
				if($id)
				{
					$a = "";
					$config['upload_path'] = './uploads/';
					$config['allowed_types'] = 'gif|jpg|png';
					$config['max_size']	= '2048';
					$config['max_width']  = '3000';
					$config['max_height']  = '3000';				

					$this->load->library('image_lib', $config);
					$this->load->library('upload', $config);
					$i=0;
					if(isset($_FILES))
					{
						foreach($_FILES as $file=>$field)
						{				
							$ind = explode('_',$file);
							//$index = $ind[1]-1;
							if($this->upload->do_upload($file))
							{
								$data = $this->upload->data();
								$data['file_name'];
								$b=count($data['file_name']);
								$a[0] = $data['file_name'];
							}
						}
					}
					$id = $this->category_model->upload_cat_image($id,$a);					
					
					$this->session->set_flashdata('success', 'Record has been added successfully.');
					redirect(base_url().'siteadmin/category');

				}
			}

		}
}
                ?>