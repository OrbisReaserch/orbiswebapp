<?php $this->load->view('backend/leftsidebar'); ?>
<!--sidebar end-->
<!--main content start-->

<script type="text/javascript" src="<?php echo theme_url();?>tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
  <script type="text/javascript" src="<?php echo theme_url();?>tinymce/jscripts/tiny_mce/plugins/media/js/embed.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		// General options
		selector: "textarea",
		theme : "advanced",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",
	    //plugins : "media",
		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,cut,copy,paste,pastetext,pasteword,|,undo,redo,|,link,unlink,anchor",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "center",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example content CSS (should be your site CSS)
		content_css : "<?php echo theme_url();?>tinymce/examples/css/content.css",
		image_advtab: true,
		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "<?php echo theme_url();?>tinymce/examples/lists/template_list.js",
		external_link_list_url : "<?php echo theme_url();?>tinymce/examples/lists/link_list.js",
		external_image_list_url : "<?php echo theme_url();?>tinymce/examples/lists/image_list.js",
		external_media_list_url : "<?php echo theme_url();?>tinymce/examples/lists/media_list.js",

		// Style formats
		style_formats : [
			{title : 'Bold text', inline : 'b'},
			{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
			{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
			{title : 'Example 1', inline : 'span', classes : 'example1'},
			{title : 'Example 2', inline : 'span', classes : 'example2'},
			{title : 'Table styles'},
			{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
		],
		
		
	});
	
	
	
</script>
<script>
	
function get_subcategory(id)
{
 // alert(id);
$.ajax({                                      
      url: "<?php echo base_url().'siteadmin/report/subcategory_by_id/'?>"+id,                  
      data: "id="+id,      //id=5&parent=6
      dataType: 'text',
      success: function(data)       
      { 
	  // alert(data);
		$('#subcategory').html(data); 
      } 
    });

}
 function checkExist(){
    $(".haserror").hide();
    $.ajax({  
            type: "POST",                                    
              url: "<?php echo base_url().'siteadmin/report/checkExistReportTitle'?>",                  
              data: {
                title_name:$("#report_title").val(),
              },     
              success: function(data){ 
                if(data!=='0' )
                {
                    $(".haserror").show();
                    $(".haserror").html("Report title name already exist.Please enter another title name");   
                }
               
            } 
        });
 }
</script>
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Add New Report
                             <span>
                            <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
                            }
                            ?>
                        </span>
                            <span class="tools pull-right">
                                <a href="<?php echo base_url();?>siteadmin/report" style="color: #ffffff;" class="btn btn-primary btn-mini">All Reports</a>
                                <a class="fa fa-chevron-down" href="javascript:;"></a>
                             </span>
                        </header>
                        <div class="panel-body">
                            <div class=" form">
                                <form class="cmxform form-horizontal" name="reg_form" id="commentForm" enctype="multipart/form-data" method="POST" action="<?php echo base_url();?>siteadmin/report/add_reports">
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Report Title</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="report_title" name="report_title" minlength="2" type="text"   onchange="checkExist()" required />
                                            <div class="haserror" style="color:red;"></div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Select Category </label>
                                        <div class="col-lg-6">
										<!--  <input class=" form-control" id="bname" name="user_type" minlength="2" type="text" required />-->
                                        <select name="category" id="category" class="form-control" required="required" onchange = "get_subcategory(this.value);">
                                         <?php echo allcategory($info['category']); ?>
                                       
                                       
                                        
                                        </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Select Sub Category </label>
                                        <div class="col-lg-6">
										<!--  <input class=" form-control" id="bname" name="user_type" minlength="2" type="text" required />-->
                                        <select name="subcategory" id="subcategory" class="form-control" required="required">
                                         <?php //echo allsubcategory($info['subcategory']); ?>
                                      </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Select Publisher </label>
                                        <div class="col-lg-6">
										<!-- <input class=" form-control" id="bname" name="user_type" minlength="2" type="text" required />-->
                                        <select name="vendor" id="vendor" class="form-control" required="required">
                                          <?php echo allvendor($info['user']); ?>
                                        </select>
                                        </div>
                                    </div>
                                    
                                      <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3">Select Country </label>
                                        <div class="col-lg-6">
										<!--  <input class=" form-control" id="bname" name="user_type" minlength="2" type="text" required />-->
                                        <select name="country_id"   class="form-control">
                                         <?php echo allcountry($info['country'],"0"); ?>
                                       
                                       
                                        
                                        </select>
                                        </div>
                                    </div>									  <div class="form-group ">                                         <div class="col-lg-6" style="margin-left: 25%;">											OR                                         </div></div>								
									<div class="form-group ">                                        <label for="cname" class="control-label col-lg-3">Select Continent </label>                                        <div class="col-lg-6">										   <select name="continent_id"  class="form-control">                                         <?php echo allcountry($info['country'],"1"); ?>                                         </select>                                        </div>                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="cname" class="control-label col-lg-3"> Image</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="blogo" name="report_image" type="file" />
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">Report Description</label>
                                        <div class="col-lg-6">
                                         <textarea class=" form-control" id= "elm1" name="report_dec"></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">Table of Content</label>
                                        <div class="col-lg-6">
                                          
                                            <textarea class=" form-control" id= "elm2" name="report_content"></textarea>
                                        </div>
                                    </div>
                                    
                                      <div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">List Of Tables</label>
                                        <div class="col-lg-6">
                                          
                                            <textarea class=" form-control" name="list_of_tables" id= "elm3"></textarea>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3"> Single Price</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="price" name="single_price"  type="text" pattern = "^[0-9]+$"  required />
                                        </div>
                                    </div>
									 <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3"> Multiple  Price</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="price" name="multiple_price"  type="text" pattern = "^[0-9]+$"  required />
                                        </div>
                                    </div>
									 <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3"> Global Price</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="price" name="global_price"  type="text" pattern = "^[0-9]+$"  required />
                                        </div>
                                    </div>
									 <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3"> Corporate Price</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="price" name="corporate_price"  type="text" pattern = "^[0-9]+$"  required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="cname" class="control-label col-lg-3"> Published Date</label>
                                         <div class="col-lg-6">

                                             <select name="year" >
                                                <!-- <option value="0">Year</option> -->
                                                <option value="<?php echo date('Y');?>" selected><?php echo date('Y');?></option>
                                                <?php 
                                                $year=date("Y");    
                                                for($y=1970;$y<=$year;$y++)
                                                {
                                                ?>
                                                <option value="<?php echo $y;?>"><?php echo $y;?></option>
                                                <?php } ?>
                                            </select>                                        
                                            <select name="month">
                                                <!-- <option value="0">Month</option> -->
                                                <option value="<?php echo date('m');?>" selected><?php echo date('m');?></option>
                                                <?php 
                                                for($j=1;$j<13;$j++)
                                                {
                                                ?>
                                                <option value="<?php echo $j;?>"><?php echo $j;?></option>
                                                <?php } ?>
                                            </select>
                                            <select name="day" >
                                                <!-- <option value="0">Day</option> -->
                                                <option value="<?php echo date('d');?>"selected><?php echo date('d');?></option>
                                                <?php 
                                                for($i=1;$i<32;$i++)
                                                {
                                                ?>
                                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                <?php 
                                                } ?>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3"> Featured Report</label>
                                        <div class="col-lg-6">
                                            <input type="radio"  name="featured" checked>  
                                        </div>
                                    </div>
									<div class="form-group ">
                                    <label for="cname" class="control-label col-lg-3">Page Title</label>
                                    <div class="col-lg-6">
                                        <input class=" form-control" placeholder="Page Title" id="page_title" name="page_title" type="text" />
                                    </div>
                                </div> 
									<div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">Meta Title</label>
                                        <div class="col-lg-6">
                                           <input class=" form-control" id="meta_title" name="meta_title" minlength="2" type="text" placeholder="Meta Title" />
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">Meta Keywords</label>
                                        <div class="col-lg-6">
                                           <input class=" form-control" id="metatitle" name="metatitle" minlength="2" type="text" placeholder="Put comma [,] to separate values" />
                                        </div>
                                    </div>
									 <div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">Meta Description</label>
                                        <div class="col-lg-6">
                                         <textarea class=" form-control" name="report_summ"></textarea>
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">QY Report Name</label>
                                        <div class="col-lg-6">
                                           <input class=" form-control" id="qy_report_name" name="qy_report_name" minlength="2" type="text" placeholder="QY Report Name" />
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">QY Meta Title</label>
                                        <div class="col-lg-6">
                                           <input class=" form-control" id="qy_meta_title" name="qy_meta_title" minlength="2" type="text" placeholder="QY Meta Title" />
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">QY Meta Keywords</label>
                                        <div class="col-lg-6">
                                           <input class=" form-control" id="qy_meta_keyword" name="qy_meta_keyword" minlength="2" type="text" placeholder="Put comma [,] to separate values" />
                                        </div>
                                    </div>
									 <div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">QY Meta Description</label>
                                        <div class="col-lg-6">
                                         <textarea class=" form-control" name="qy_meta_desc"></textarea>
                                        </div>
                                    </div>
									<div class="form-group ">
                                        <label for="cname" class="control-label col-lg-3"> Number of Pages</label>
                                        <div class="col-lg-6">
                                            <input class=" form-control" id="no_pages" name="no_pages" type="text" pattern = "^[0-9 ]+$"  required />
                                        </div>
                                    </div>
									
									<div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">Status</label>
                                        <div class="col-lg-6">
                                           <select class=" form-control" name="report_status" required="required">
											   <option value=""> Select Report Status </option>
											   <option value="1">Published </option>
											   <option value="0"> Upcoming  </option>
										   </select>
                                        </div>
                                    </div>
                                   <div class="form-group">
                                        <label for="cname" class="control-label col-lg-3">Search Keywords</label>
                                        <div class="col-lg-6">
                                           <input class=" form-control" id="search_keywords" name="search_keywords" minlength="2" type="text" placeholder="Put comma [,] to separate values" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-3 col-lg-6">
                                            <button class="btn btn-primary" name="submit" type="submit"  onclick="return validation()" >Save</button>
                                            <a href="<?php echo base_url();?>siteadmin/report">
                                            <button class="btn btn-default" type="button">Cancel</button>
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </section>
                </div>
            </div>
        <!-- page end-->
    </section>
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <ul class="right-side-accordion">
            <li class="widget-collapsible">
                <a href="#" class="head widget-head red-bg active clearfix">
                    <span class="pull-left">work progress (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row side-mini-stat clearfix">
                            <div class="side-graph-info">
                                <h4>Target sell</h4>
                                <p>
                                    25%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="target-sell">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>product delivery</h4>
                                <p>
                                    55%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="p-delivery">
                                    <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info payment-info">
                                <h4>payment collection</h4>
                                <p>
                                    25%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="p-collection">
                                    <span class="pc-epie-chart" data-percent="45">
                                        <span class="percent"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jhone Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">John Sinna </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Sumon </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Mosaddek </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</section>
<!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

</section>