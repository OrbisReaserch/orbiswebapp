<?php $this->load->view('backend/leftsidebar'); ?>
<!--sidebar end-->
<!--main content start-->
<style type="text/css">

#container .pagination ul li.inactive,
#container .pagination ul li.inactive:hover{
    background-color:#ededed;
    color:#bababa;
    border:1px solid #bababa;
    cursor: default;
}
#container .data ul li{
    list-style: none;
    font-family: verdana;
    margin: 5px 0 5px 0;
    color: #000;
    font-size: 12px;
}

#container .pagination{

    height: 25px;
    margin-top:20px;
    float: left;
    padding: 0;
    margin: 0;
    width: 100%;
    clear: both;
                    //margin-left:0px !important;
}
#container .pagination input{

    height: 30px;
    margin-right: 8px;
                    //margin-left:0px !important;
}
#container .pagination ul{
    list-style: none;
    float: left;
    padding: 0;
margin: 0;
}
#container .pagination ul li{
    list-style: none;
    float: left;
    border: 1px solid #1cc3c9;
    padding: 2px 6px 2px 6px;
    margin: 0 3px 0 3px;
    font-family: arial;
    font-size: 12px;
    color: #999;
    font-weight: bold;
    background-color: #ffffff;
}
#container .pagination ul li:hover{
    color: #fff;
    background-color: #1cc3c9;
    cursor: pointer;
}
.go_button
{
background-color:#ffffff;border:1px solid #fb0455;color:#cc0000;padding:2px 6px 2px 6px;cursor:pointer;position:absolute;margin-top:-1px;
}
.total
{
 float:right;font-family:arial;color:#bababa; font-size: 12px;
 margin:5px 25px;
}
.colexcel{
 
    color:#1fb5ad; 
}

</style>
<section id="main-content">
<section class="wrapper">
<!-- page start-->
 <?php //if($this->session->userdata('user_type') == 'Admin'){?>
           <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                       Upload Report Data
                         <span style="margin-left: 30px;">
                            <?php
          
                            if($uploadReportResult)
                            {
                            //print_r($uploadReportResult);exit;
                                echo "<font style='color:green;'>'Excel Upload done successfully.'</font>";

                                ?>
                                <div class="alert alert-danger alert-dismissible col-md-12" style="overflow-y: scroll;overflow-x: scroll;height: 150px;width: 100%; ">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span  class="colexcel"><strong style="color:green">Total Count :  <?php echo $uploadReportResult['totalcount'] ?><br>  Inserted Record :  <?php echo $uploadReportResult['validcount'];?><br>   Already Exist :  <?php echo $uploadReportResult['invalidcount'];
                            if($uploadReportResult['validcount'] > 0) { 
                                ?>
                                   <br> <a style="color: #ffffff;" class="btn btn-primary btn-mini"  href = "<?php echo base_url();?>siteadmin/report/downloadReportIdandUrls/<?php echo $uploadReportResult['validcount'];?>">Download Report Id+Urls</a>
                           <?php } ?></strong> </span><br><br>
                          
                             <ol>
                              <?php
                                 $i=1;
                                 unset($uploadReportResult['success']);
                                 unset($uploadReportResult['totalcount']);
                                 unset($uploadReportResult['validcount']);
                                 unset($uploadReportResult['invalidcount']);
                                 foreach($uploadReportResult as $existname)
                                {
                                 echo "<li style='font-size: 12px;'> $existname</li>";
                                    
                                }
                              ?>
                             </ol>
                             </div>
                           <?php }
                            else if(isset($uploadReportResult))
                            {
                                echo "<font style='color:red;'>'Unable to upload Excel file.'</font>";
                            }
                            ?>
                        </span>
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                        </span>
                    </header>
                                                     
                    <form action="<?php echo base_url();?>siteadmin/report/read_excel" method="post" enctype="multipart/form-data">
                    <div class="panel-body">
                        <div class="adv-table">
                       <div class="form-group">
                          <label for="cname" class="control-label col-lg-3">Upload File</label>
                        <div class="col-lg-4">
                            <input class=" form-control" id="bulk_upload" name="bulk_upload" type="file"/>
                            
                        </div>
                     </div>
                                    
                    <input style="color: #ffffff;" class="btn btn-primary btn-mini" id="bname" name="submit" value="Upload" type="submit" onclick="Refresh_page()" />
                     <a style="color: #ffffff;" class="btn btn-primary btn-mini" target = "_blank" href = "<?php echo base_url();?>uploads/orbis.xlsx">Download Sample</a>
                   <!--  <?php if($this->session->flashdata('limits_report'))
                            {
                            $limit=$this->session->flashdata('limits_report');
                                ?>
                     <a style="color: #ffffff;" class="btn btn-primary btn-mini"  href = "<?php echo base_url();?>siteadmin/report/downloadReportIdandUrls/<?php echo $limit;?>">Download Report Id+Urls</a>
                <?php  } ?> -->
                    <!--  <a style="color: #ffffff;" class="btn btn-primary btn-mini"  href = "<?php echo base_url();?>siteadmin/report/downloadReportIdandUrls">Download Report Id+Urls</a> -->
                                    
                        </div>
                    </div>
                    </form>
                   
                </section>
            </div>
        </div> 
         <?php // }?>

        <div class="row">
            <div class="col-sm-12">
                
                <section class="panel">
                    <header class="panel-heading">
                        Manage Report Management
                        <span style="margin-left: 30px;">
                            <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
                            }
                            ?>
                        </span>
                        <span class="tools pull-right">
                             
                            <a href="<?php echo base_url();?>siteadmin/report/add_reports" style="color: #ffffff;" class="btn btn-primary btn-mini">Add new Report</a>
                            <a href="javascript:;" class="fa fa-chevron-down"></a>

                        </span>
                    </header>
                    <div class="panel-body">
                    <div class="row">
                         <div class="col-sm-4"><input type="text" name="search_keyword" class=" form-control" id = "search_keyword" placeholder = "Search Report Name" onkeyup = "SearchReport(this.value);" autocomplete="off" /></div>
                         <div class="col-sm-4">
                         <select name="coyntry" class=" form-control" id = "coyntry" onchange = "SearchReport(this.value);">
                            <option value = "">Select Country</option>
                            <?php 
                            for($i=0;$i<count($countries);$i++)
                             {
                            ?>
                             <option value = <?php echo $countries[$i]->id ?>><?php  echo $countries[$i]->country_name; ?>
                                
                            </option>
                            
                            <?php } ?>
                        </select>
                        </div>
                        <div class="col-sm-4">
                         <select name="publisher" class=" form-control" id = "publisher" onchange = "SearchReport(this.value);">
                            <option value = "">Select Publisher</option>
                            <?php 
                            $publisherId = "";
                            for($i=0;$i<count($publishers);$i++)
                              {
                            ?>
                            <option  value = <?php echo $publishers[$i]->id ?>><?php echo $publishers[$i]->display_name; ?>
                                
                            </option>

                          <?php } ?>
                        </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="" class="btn btn-default btn-sm bulk-download-excel" publisher="">
                                <b>Download Excel &nbsp;</b> <i class="fa fa-download" aria-hidden="true"></i>
                            </a>
                            <button type="button"  value="Delete Bulk Report" name="bulkreport" class="btn btn-default btn-sm" id="bulkreport"><b>Delete Bulk Report</b></button>
                         </div>
                    </div>
                    <br/>
                        <div class="adv-table">
                             <table class="display table table-bordered table-striped" id="dynamic-table">
                                
                            </table><br>
                            <div id="container">
                                <div  id = "pagination"></div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
    <div class="right-sidebar">
        <div class="search-row">
            <input type="text" placeholder="Search" class="form-control">
        </div>
        <ul class="right-side-accordion">
            <li class="widget-collapsible">
                <a href="#" class="head widget-head red-bg active clearfix">
                    <span class="pull-left">work progress (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row side-mini-stat clearfix">
                            <div class="side-graph-info">
                                <h4>Target sell</h4>
                                <p>
                                    25%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="target-sell">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>product delivery</h4>
                                <p>
                                    55%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="p-delivery">
                                    <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info payment-info">
                                <h4>payment collection</h4>
                                <p>
                                    25%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="p-collection">
                                    <span class="pc-epie-chart" data-percent="45">
                                        <span class="percent"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="side-graph-info">
                                <h4>delivery pending</h4>
                                <p>
                                    44%, Deadline 12 june 13
                                </p>
                            </div>
                            <div class="side-mini-graph">
                                <div class="d-pending">
                                </div>
                            </div>
                        </div>
                        <div class="prog-row side-mini-stat">
                            <div class="col-md-12">
                                <h4>total progress</h4>
                                <p>
                                    50%, Deadline 12 june 13
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head terques-bg active clearfix">
                    <span class="pull-left">contact online (5)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jonathan Smith</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Anjelina Joe</a></h4>
                                <p>
                                    Available
                                </p>
                            </div>
                            <div class="user-status text-success">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/chat-avatar2.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Jhone Doe</a></h4>
                                <p>
                                    Away from Desk
                                </p>
                            </div>
                            <div class="user-status text-warning">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1_small.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Mark Henry</a></h4>
                                <p>
                                    working
                                </p>
                            </div>
                            <div class="user-status text-info">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb">
                                <a href="#"><img src="images/avatar1.jpg" alt=""></a>
                            </div>
                            <div class="user-details">
                                <h4><a href="#">Shila Jones</a></h4>
                                <p>
                                    Work for fun
                                </p>
                            </div>
                            <div class="user-status text-danger">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                        <p class="text-center">
                            <a href="#" class="view-btn">View all Contacts</a>
                        </p>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head purple-bg active">
                    <span class="pull-left"> recent activity (3)</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    just now
                                </p>
                                <p>
                                    <a href="#">John Sinna </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    2 min ago
                                </p>
                                <p>
                                    <a href="#">Sumon </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                        <div class="prog-row">
                            <div class="user-thumb rsn-activity">
                                <i class="fa fa-clock-o"></i>
                            </div>
                            <div class="rsn-details ">
                                <p class="text-muted">
                                    1 day ago
                                </p>
                                <p>
                                    <a href="#">Mosaddek </a>Purchased new equipments for zonal office setup
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="widget-collapsible">
                <a href="#" class="head widget-head yellow-bg active">
                    <span class="pull-left"> shipment status</span>
                    <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
                </a>
                <ul class="widget-container">
                    <li>
                        <div class="col-md-12">
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div class="prog-row">
                                <p>
                                    Full sleeve baby wear (SL: 17665)
                                </p>
                                <div class="progress progress-xs mtop10">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                        <span class="sr-only">70% Completed</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

</section>
<div class="right-sidebar">
<div class="search-row"><input type="text" placeholder="Search"
    class="form-control"></div>
<ul class="right-side-accordion">
    <li class="widget-collapsible"><a href="#"
        class="head widget-head red-bg active clearfix"> <span
        class="pull-left">work progress (5)</span> <span
        class="pull-right widget-collapse"><i class="ico-minus"></i></span> </a>
    <ul class="widget-container">
        <li>
        <div class="prog-row side-mini-stat clearfix">
        <div class="side-graph-info">
        <h4>Target sell</h4>
        <p>25%, Deadline 12 june 13</p>
        </div>
        <div class="side-mini-graph">
        <div class="target-sell"></div>
        </div>
        </div>
        <div class="prog-row side-mini-stat">
        <div class="side-graph-info">
        <h4>product delivery</h4>
        <p>55%, Deadline 12 june 13</p>
        </div>
        <div class="side-mini-graph">
        <div class="p-delivery">
        <div class="sparkline" data-type="bar" data-resize="true"
            data-height="30" data-width="90%" data-bar-color="#39b7ab"
            data-bar-width="5"
            data-data="[200,135,667,333,526,996,564,123,890,564,455]"></div>
        </div>
        </div>
        </div>
        <div class="prog-row side-mini-stat">
        <div class="side-graph-info payment-info">
        <h4>payment collection</h4>
        <p>25%, Deadline 12 june 13</p>
        </div>
        <div class="side-mini-graph">
        <div class="p-collection"><span class="pc-epie-chart"
            data-percent="45"> <span class="percent"></span> </span></div>
        </div>
        </div>
        <div class="prog-row side-mini-stat">
        <div class="side-graph-info">
        <h4>delivery pending</h4>
        <p>44%, Deadline 12 june 13</p>
        </div>
        <div class="side-mini-graph">
        <div class="d-pending"></div>
        </div>
        </div>
        <div class="prog-row side-mini-stat">
        <div class="col-md-12">
        <h4>total progress</h4>
        <p>50%, Deadline 12 june 13</p>
        <div class="progress progress-xs mtop10">
        <div style="width: 50%" aria-valuemax="100" aria-valuemin="0"
            aria-valuenow="20" role="progressbar"
            class="progress-bar progress-bar-info"><span class="sr-only">50%
        Complete</span></div>
        </div>
        </div>
        </div>
        </li>
    </ul>
    </li>
    <li class="widget-collapsible"><a href="#"
        class="head widget-head terques-bg active clearfix"> <span
        class="pull-left">contact online (5)</span> <span
        class="pull-right widget-collapse"><i class="ico-minus"></i></span> </a>
    <ul class="widget-container">
        <li>
        <div class="prog-row">
        <div class="user-thumb"><a href="#"><img
            src="images/avatar1_small.jpg" alt=""></a></div>
        <div class="user-details">
        <h4><a href="#">Jonathan Smith</a></h4>
        <p>Work for fun</p>
        </div>
        <div class="user-status text-danger"><i class="fa fa-comments-o"></i>
        </div>
        </div>
        <div class="prog-row">
        <div class="user-thumb"><a href="#"><img src="images/avatar1.jpg"
            alt=""></a></div>
        <div class="user-details">
        <h4><a href="#">Anjelina Joe</a></h4>
        <p>Available</p>
        </div>
        <div class="user-status text-success"><i class="fa fa-comments-o"></i>
        </div>
        </div>
        <div class="prog-row">
        <div class="user-thumb"><a href="#"><img src="images/chat-avatar2.jpg"
            alt=""></a></div>
        <div class="user-details">
        <h4><a href="#">Jhone Doe</a></h4>
        <p>Away from Desk</p>
        </div>
        <div class="user-status text-warning"><i class="fa fa-comments-o"></i>
        </div>
        </div>
        <div class="prog-row">
        <div class="user-thumb"><a href="#"><img
            src="images/avatar1_small.jpg" alt=""></a></div>
        <div class="user-details">
        <h4><a href="#">Mark Henry</a></h4>
        <p>working</p>
        </div>
        <div class="user-status text-info"><i class="fa fa-comments-o"></i></div>
        </div>
        <div class="prog-row">
        <div class="user-thumb"><a href="#"><img src="images/avatar1.jpg"
            alt=""></a></div>
        <div class="user-details">
        <h4><a href="#">Shila Jones</a></h4>
        <p>Work for fun</p>
        </div>
        <div class="user-status text-danger"><i class="fa fa-comments-o"></i>
        </div>
        </div>
        <p class="text-center"><a href="#" class="view-btn">View all Contacts</a>
        </p>
        </li>
    </ul>
    </li>
    <li class="widget-collapsible"><a href="#"
        class="head widget-head purple-bg active"> <span class="pull-left">
    recent activity (3)</span> <span class="pull-right widget-collapse"><i
        class="ico-minus"></i></span> </a>
    <ul class="widget-container">
        <li>
        <div class="prog-row">
        <div class="user-thumb rsn-activity"><i class="fa fa-clock-o"></i></div>
        <div class="rsn-details ">
        <p class="text-muted">just now</p>
        <p><a href="#">John Sinna </a>Purchased new equipments for zonal
        office setup</p>
        </div>
        </div>
        <div class="prog-row">
        <div class="user-thumb rsn-activity"><i class="fa fa-clock-o"></i></div>
        <div class="rsn-details ">
        <p class="text-muted">2 min ago</p>
        <p><a href="#">Sumon </a>Purchased new equipments for zonal office
        setup</p>
        </div>
        </div>
        <div class="prog-row">
        <div class="user-thumb rsn-activity"><i class="fa fa-clock-o"></i></div>
        <div class="rsn-details ">
        <p class="text-muted">1 day ago</p>
        <p><a href="#">Mosaddek </a>Purchased new equipments for zonal office
        setup</p>
        </div>
        </div>
        </li>
    </ul>
    </li>
    <li class="widget-collapsible"><a href="#"
        class="head widget-head yellow-bg active"> <span class="pull-left">
    shipment status</span> <span class="pull-right widget-collapse"><i
        class="ico-minus"></i></span> </a>
    <ul class="widget-container">
        <li>
        <div class="col-md-12">
        <div class="prog-row">
        <p>Full sleeve baby wear (SL: 17665)</p>
        <div class="progress progress-xs mtop10">
        <div class="progress-bar progress-bar-success" role="progressbar"
            aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
            style="width: 40%"><span class="sr-only">40% Complete</span></div>
        </div>
        </div>
        <div class="prog-row">
        <p>Full sleeve baby wear (SL: 17665)</p>
        <div class="progress progress-xs mtop10">
        <div class="progress-bar progress-bar-info" role="progressbar"
            aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
            style="width: 70%"><span class="sr-only">70% Completed</span></div>
        </div>
        </div>
        </div>
        </li>
    </ul>
    </li>
</ul>
</div>
</section>
<!--main content end-->
<!--right sidebar start-->

<!--right sidebar end-->

</section>
    
<script src="<?php echo theme_url();?>js/ajax.googleapis.com.ajax.libs.jquery.1.4.2.jquery.min.js"></script>

<script type="text/javascript">
  
    function SearchReport(val){
           var val=$("#search_keyword").val().trim();
           var country_id= $("#coyntry option:selected" ).val();
           var publisher_id= $("#publisher option:selected" ).val();
            $.ajax({      
                    type: "POST",                                    
                      url: "<?php echo base_url().'siteadmin/report/getDataPage'?>",                  
                      data: {
                          keyword:val,
                          country_id:country_id,
                          publisher_id:publisher_id,
                                                                            
                      },     
                     success: function(data){ 
                      var split = data.split("<div class='clearfix'></div>");  
                      $("#dynamic-table").html(split[0]);
                      $("#pagination").html(split[1]); 
                    } 
            }); 

            /*Set download bulk url using publisher id*/
           url= "<?php echo base_url().'siteadmin/report/downloadReportOfPublisher/'?>"+publisher_id;
           $(".bulk-download-excel").attr('href',url);
         
    }
    
    $(document).ready(function(){
        
        function loadData(page) {
           
        $.ajax({      
                type: "POST",                                    
                  url: "<?php echo base_url().'siteadmin/report/getDataPage'?>",                  
                  data: {
                      page:page,
                                       
                  },     
                 success: function(data){ 
                  var split = data.split("<div class='clearfix'></div>");  
                  $("#dynamic-table").html(split[0]);
                  $("#pagination").html(split[1]); 
                } 
             });
         
        }
    
        loadData(1);  
        
        $('#container .pagination li.active').live('click',function() {
            var page = $(this).attr('p');
            var val=$("#search_keyword").val();
            var country_id= $("#coyntry option:selected").val();
            var publisher_id= $("#publisher option:selected" ).val();
            if(val !=="" || country_id !=="" || publisher_id !=="")
            {
                $.ajax({      
                     type: "POST",                                    
                     url: "<?php echo base_url().'siteadmin/report/getDataPage'?>",                  
                    data: {
                     keyword:val,
                     country_id:country_id,
                     publisher_id:publisher_id, 
                     page:page
                  },     
                 success: function(data){ 
                  var split = data.split("<div class='clearfix'></div>");  
                  $("#dynamic-table").html(split[0]);
                  $("#pagination").html(split[1]); 
                } 
             });

            } else {
                   loadData(page);
            }
         });

        $('#go_btn').live('click',function(){
            var page = parseInt($('.goto').val());
            var no_of_pages = parseInt($('.total').attr('a'));
            
            if(page != 0 && page <= no_of_pages) {
                loadData(page);
            } else {
                alert('Enter a PAGE between 1 and '+no_of_pages);
                $('.goto').val("").focus();
                return false;
            }
        });

        // Bulk download reports by publisher 
        bulkDownloadReportsByPublisher();

        /* add multiple select / deselect functionality*/
        $("#bulkreport").hide();  
        $('#selectall').live('click',function(){   
            $("#bulkreport").show();      
            $('.bulkcheckbox').attr('checked', this.checked);
            $("input.bulkcheckbox:not(:checked)").each (function () {
                $("#bulkreport").hide(); 
            });

        });

        /* if all checkbox are selected, check the selectall checkbox */
       
        $('.bulkcheckbox').live('click',function() {       
            if($(".bulkcheckbox").length == $(".bulkcheckbox:checked").length) {
                $("#selectall").attr("checked", "checked");
            } else {
                $("#selectall").removeAttr("checked");
            }

        });

        $(".bulkcheckbox").live('click',function() {
            $("input.bulkcheckbox:not(:checked)").each (function () {
                $("#bulkreport").hide(); 
            });
            $("input.bulkcheckbox:checked").each (function () {
                $("#bulkreport").show(); 
            }); 
        });

    /*Delete Bulk report  ajax call */ 
        $("#bulkreport").live('click',function() {
            var selected_value = [];
            $(".bulkcheckbox:checked").each(function() {
                selected_value.push($(this).val());
            });

            if (confirm("Do you want to delete?")) {
                $.ajax({  
                    type: "POST",                                    
                    url: "<?php echo base_url().'siteadmin/report/deleteBulkReport'?>",                  
                    data: {
                        selected_id:selected_value,
                    },        
                    success: function(data){ 
                        alert("Bulk report has been deleted successfully");
                        location.reload();
                    } 
                });   
            }
            return false;
        });
       
       $("#bname").live('click',function(e){
            var bulkfile=$("#bulk_upload").val();
            if(bulkfile=='') {
                 e.preventDefault();
                alert('Please select upload excel file');
            }
        }); 

    });

    function bulkDownloadReportsByPublisher() {
        $('.bulk-download-excel').click(function(e) {
            var publisher = $('#publisher').val();
            if(publisher == '' || publisher == null) {
                e.preventDefault();
                alert('Please select publisher');
            }
        });
    }
 
</script>
<!-- <script>

function Refresh_page() {

  setTimeout(function(){
   window.location.reload(1);
}, 10000);

}
</script> -->