<?php
class Report_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function getUsers()
	{
		$result=$this->db->query("select * from users where status='1'");
		return $result->result();
	}	
	public function getCountryData($id){		
		$result=$this->db->query("SELECT * FROM country where id = '".$id."'");		
		return $result->row_array();	
	}
	public function getcatgoryreports($category_id){
		$result = $this->db->query("select * from report where category_id = '$category_id' AND status = '1' LIMIT 0,10");
		return $result->result();
	}
	public function getSearchKeywordWiseReports($search_keyword){
		$result = $this->db->query("select id,page_urls,report_name,category_id from report where search_keywords = '$search_keyword' AND status = '1' order by report_date desc LIMIT 0,10");
		return $result->result();
	}	
	public function getReports($page_urls)
	{
		$result=$this->db->query("select r.*,c.category_name as category_name from report r INNER JOIN category c on(r.category_id = c.id)  where page_urls='$page_urls' AND r.status='1' order by r.report_date desc LIMIT 0,100");
		return $result->result();
	}

	public function getPublisherName($id)
	{
		$this->db->select('u.display_name as publisher_name');
		$this->db->from('users u');
		$this->db->join('report r','u.id=r.publisher_id');
		$this->db->where('u.id',$id);
		$query = $this->db->get();
		return $query->result();

	}
		public function getReports1($report_id)
	{
	 
		$result=$this->db->query("select r.*,u.display_name as publisher_name from report r INNER JOIN users u on(r.publisher_id = u.id) where r.id='$report_id' AND r.status='1' LIMIT 0,100");
		return $result->result();
	}
	public function getCountryWiseReportList($country_id)
	{
		$result=$this->db->query("select * from report where country_id='$country_id' AND status='1' order by id desc LIMIT 0,100");
		return $result->result();
	}
	public function getlatestReportList()
	{
		$result=$this->db->query("select * from report where status='1' order by id desc LIMIT 0,100");
		return $result->result();
	}
	public function getCountries()
	{
		$result=$this->db->query("select * from country ORDER BY 'country_name';");
		return $result->result();
	}
	
	public function getCategoryById($category_id)
	{
		$result=$this->db->query("select category_name from category where id = '".$category_id."'");
		return $result->row_array();
	}
	
	public function getCategory()
	{
		$result=$this->db->query("select * from category where status='1'");
		return $result->result();
	}
	public function getSubCategory()
	{
		$result=$this->db->query("select * from sub_category where status='1'");
		return $result->result();
	}
	public function getFeaturedReports()
	{
		$result=$this->db->query("select * from report where status='1' and featured_report='1'  limit 0,3");
		return $result->result();
	}
	public function getAllFeaturedReports()
	{
		$result=$this->db->query("select page_urls,report_name,category_id from report WHERE featured_report = '1' LIMIT 0,6;");
		return $result->result();
	}
	public function getProductImage()
	{
		$result=$this->db->query("select * from product_images where status='1' limit 1");
		return $result->result();	
	}
	public function getPublisher()
	{
		$result=$this->db->query("select * from users where status='1'");
		return $result->result();	
	}
	public function getReportList($report_id)
	{
		$result=$this->db->query("select * from report where sub_category_id = '$report_id' and status='1' order by report_date DESC");
		return $result->result();
	}
	public function getCategoryReportList($category_id)
	{
	$result=$this->db->query("select * from report where category_id = '$category_id' and status='1' order by report_date DESC");
	return $result->result();	
	
	}
	
	public function getTestimonials()
	{
		$result=$this->db->query("select * from testimonials where status='1' order by id DESC limit 1");
		return $result->result();
	}
	
	public function getReportData($id)
	{
		$result=$this->db->query("select * from report where id ='".$id."'");
		return $result->result();
	}

	public function getTemplateReportInfo($id)
	{
		$result=$this->db->query("select id,report_name,page_urls,report_description,report_content from report where id ='".$id."'");
		return $result->result();
	}

	
	public function send_enquiry() 
    {
    	if("valid" == $this->CheckEmailBlockStatus($_POST['email'])){
			
			$username= $_POST['cf_864'];
			$email=$_POST['email'];
			// $phone=$_POST['cf_868'];
			$country_code=trim($_POST['country_code'],'+');
			$phone=$country_code.' '.$_POST['cf_868'];
			$company_name = $_POST['company'];
			$message=mysql_real_escape_string($_POST['cf_876']);// query on report
			$report_id=$_POST['id'];
			$country=$_POST['cf_858'];
			$reportname=$_POST['cf_860'];
			$designation=$_POST['cf_870'];//job title
			$publisher_id=$_POST['publisher_id'];
			$report_code=$_POST['report_code'];
			$date=date('Y-m-d H:i:s');
			$region='';
			$region=getEmailsRegion($country,'region');

		$data = array('username'=>$username,'report_id'=>$report_id,'company_name'=>$company_name,'country'=>$country,'email'=>$email,'contact_no'=>$phone,'designation'=>$designation,'request_type'=>"Enquiry",'messages'=>$message,'publisher_id'=>$publisher_id,'report_code'=>$report_code,'status'=>'1','lead_date'=>$date,'region'=>$region);
         
		$this->db->insert('request',$data);
		return $this->db->insert_id();
		}else{
			return false;
		}
    }
		
public function send_request() 
    {
		if("valid" == $this->CheckEmailBlockStatus($_POST['email'])){

			$username= $_POST['cf_864'];
			$email=$_POST['email'];
			// $phone=$_POST['cf_868'];
			$country_code=trim($_POST['country_code'],'+');
			$phone=$country_code.' '.$_POST['cf_868'];
			$company_name = $_POST['company'];
			$message=mysql_real_escape_string($_POST['cf_876']);// query on report
			$report_id=$_POST['id'];
			$country=$_POST['cf_858'];
			$reportname=$_POST['cf_860'];
			$designation=$_POST['cf_870'];//job title
			$publisher_id=$_POST['publisher_id'];
			$report_code=$_POST['report_code'];
			$date=date('Y-m-d H:i:s');
			$region='';
			$region=getEmailsRegion($country,'region');

		$data = array('username'=>$username,'report_id'=>$report_id,'company_name'=>$company_name,'country'=>$country,'email'=>$email,'contact_no'=>$phone,'designation'=>$designation,'request_type'=>"Request For Sample",'messages'=>$message,'publisher_id'=>$publisher_id,'report_code'=>$report_code,'status'=>'1','lead_date'=>$date,'region'=>$region);
     
		$this->db->insert('request',$data);
		return $this->db->insert_id();
		}else{
			return false;
		}
    }
public function check_discount() 
	{
		if("valid" == $this->CheckEmailBlockStatus($_POST['email'])){
		
			$username= $_POST['cf_864'];
			$email=$_POST['email'];
			// $phone=$_POST['cf_868'];
			$country_code=trim($_POST['country_code'],'+');
			$phone=$country_code.' '.$_POST['cf_868'];
			$company_name = $_POST['company'];
			$message=mysql_real_escape_string($_POST['cf_876']);// query on report
			$report_id=$_POST['id'];
			$country=$_POST['cf_858'];
			$reportname=$_POST['cf_860'];
			$designation=$_POST['cf_870'];//job title
			$publisher_id=$_POST['publisher_id'];
			$report_code=$_POST['report_code'];
			$date=date('Y-m-d H:i:s');
			$region='';
			$region=getEmailsRegion($country,'region');

		$data = array('username'=>$username,'report_id'=>$report_id,'company_name'=>$company_name,'country'=>$country,'email'=>$email,'contact_no'=>$phone,'designation'=>$designation,'request_type'=>"Request For Discount",'messages'=>$message,'publisher_id'=>$publisher_id,'report_code'=>$report_code,'status'=>'1','lead_date'=>$date,'region'=>$region);
		 
		$this->db->insert('request',$data);
		return $this->db->insert_id();
		}else{
			return false;
		}
	} 
	
	
	
	//-------------- Repourt Sorting Start------------
	
	
	public function getReportListPriceDesc($subcategoryid)
	{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' AND status='1' order by report_price DESC");
		return $result->result();	
	}
	public function getReportListPriceAsc($subcategoryid)
	{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' AND status='1' order by report_price ASC");
		return $result->result();	
	}
	
	public function getReportListDateDesc($subcategoryid)
	{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' AND status='1' order by report_date DESC");
		return $result->result();	
	}
	public function getReportListDateAsc($subcategoryid)
	{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' AND status='1' order by report_date ASC");
		return $result->result();	
	}
	
	public function getReportListTitleDesc($subcategoryid)
	{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' AND status='1' order by report_name DESC");
		return $result->result();	
	}
	public function getReportListTitleAsc($subcategoryid)
	{
		
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' AND status='1' order by report_name ASC");
		return $result->result();	
	}
	
	public function getFooterNews()
	{
		$result=$this->db->query("select * from news where status='1' order by id desc limit 2");
		return $result->result();
	}
	public function getFooterReport()
	{
		$result=$this->db->query("select * from report where status='1' order by report_date DESC limit 5");
		return $result->result();
	
	}
	public function CheckEmailBlockStatus($email)
	{
		$result = $this->db->query("select email from blocked_email_ids where status='1' and email = '".$email."';");
		 $data = $result->row_array();
		 if(empty($data)){
			 return "valid";
		 }else{
			 return "invalid";
		 }
	
	}
	
	//-------------- Repourt Sorting End------------
	
	public function getReportsByKeyword($searchKey)
	{
		$result=$this->db->query("SELECT report_name,page_urls FROM report where report_name LIKE '%".$searchKey."%' AND status = 1 ORDER BY report_name limit 50");
		return $result->result();
	}
	public function getCategorypageinfo($id){
	$resultp=$this->db->query("select * from category where id=$id");
	return $resultp->result();	
	}
	public function getsubCategorypageinfo($id){
	$resultp=$this->db->query("select * from sub_category where id=$id");
	return $resultp->result();	
	}

	public function update_report_price($id,$price,$price_type){
		
		//$data=array('report_price'=>$price);
		if($price_type=='single'){
			$column_name='report_price';
		}
		elseif($price_type=='multiple'){
			$column_name='multiple_price';
		}elseif($price_type=='global'){
			$column_name='global_price';
		}elseif($price_type=='corporate'){
			$column_name='corporate_price';
		}

		$this->db->set($column_name,$price);
		$this->db->where('id',$id);
		$this->db->update('report');
		//return true;
		// if($this->db->affected_rows() > 0) {
		// 	return true;
		// }
		// else{
		// 	return false;
		// }
		return $this->db->affected_rows();
		
	}	
// 	public function generate_pdf($report_id){
// 		error_reporting(E_ALL);
		
// 		require_once('PDF_CON_Astha/config/lang/eng.php');
//         require_once('PDF_CON_Astha/tcpdf.php');

//         ini_set('max_execution_time', 500);

        
// 		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


//         $pdf->SetCreator(PDF_CREATOR);
//         $pdf->SetAuthor('ORBIS');
//         $pdf->SetTitle('ORBIS');
//         $pdf->SetSubject('ORBIS');
//         $pdf->SetKeywords('TCPDF, PDF, ORBIS, ORBIS, ORBIS');
//         $pdf->setPrintHeader(TRUE);
//         $pdf->setPrintFooter(TRUE,'https://www.orbisresearch.com Copyright © 2019 Orbis Research Contact: enquiry@orbisresearch.com');
//         $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
//         $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
//         $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
//         $pdf->SetFont('helvetica', '', 10);
//         // set default header data
// 		$pdf->SetHeaderData(PDF_HEADER_LOGO, 50);
// 		// $pdf->setFooterData(PDF_FOOTER_LOGO, 50);

// 		//  function Footer(){
// 		// 	$this->SetY(-8);

// 		// 	$this->SetFont('helvetica','I',8);

// 		// 	$this->Cell(0,10,'https://www.orbisresearch.com Copyright © 2019 Orbis Research Contact: enquiry@orbisresearch.com');
// 		// }
		
// 		// set header and footer fonts
// 		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// 		$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// 		// set margins
// 		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// 		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// 		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// 		// function Footer()
// 		// {
// 		//      $this->writeHTML('https://www.orbisresearch.com Copyright © 2019 Orbis Research Contact: enquiry@orbisresearch.com', false, true, false, true);
// 		//     $this->SetY(8);
		   
// 		//     $this->SetFont('helvetica', 'I', 8);
		    
// 		// }
// 		// $pdf->writeHTML('https://www.orbisresearch.com Copyright © 2019 Orbis Research Contact: enquiry@orbisresearch.com', false, true, false, true);
//         $pdf->AddPage();
// 		$data = $this->getReportData($report_id);
// 		function clean($string){

//    		return preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
// 	 }
// 	 $find=['Table of Content','\n'];
// 	 $replace=['',''];
// 	 $report_containt=str_replace($find,$replace,stripcslashes($data[0]->report_content));

// 	 $find=['<h1>','</h1>','\n'];
// 	 $replace=['<h2>','</h2>',''];
// 	 $list_of_tables=str_replace($find,$replace,stripcslashes($data[0]->list_of_tables));


// 	 $request=base_url()."contacts/request-sample/".$data[0]->id;
// 	 $browse = base_url()."reports/index/".$data[0]->page_urls;

// $aramexhtml = '<div style="">
// <p style = "text-align:center;font-size: x-large;"><span style="font-weight: 400;"><b>'.$data[0]->report_name.'</b></span></p><hr>
// <p></p>

// <p><strong>No of Report Pages &ndash; </strong><span style="font-weight: 400;">'.$data[0]->no_pages.'</span>
// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
// <span style="float:right;"><strong>Publishing Date &ndash; </strong><span style="font-weight: 400;">'.date("M d Y", strtotime($data[0]->report_date)).'</span></span>
// </p>
// <p><strong>Request a PDF sample @ </strong><span style = "color: blue;"><a href="'.$request.'">'.$request.'</a></span>

// <p><strong>Report Description</strong></p>
// <p><span style="font-weight: 400;">'.str_replace('\n', '', stripcslashes($data[0]->report_description)).'</span></p>
// <p><strong>Browse the full report @ </strong><span style = "color: blue;">'.base_url().'reports/index/'.$data[0]->page_urls.'</span></p>

// <p><strong>Table of Contents:</strong></p>
// <p><span style="font-weight: 400;">'.$report_containt.'</span></p> 
// <br>
// <strong>List Of Tables:</strong>
//                     <p>';
                    
					
//                     $aramexhtml .= $list_of_tables;
					
// 					$aramexhtml .= '</p>
// <p><strong>For Enquiry and Customization of Report, Contact @ </strong><span style = "color: blue;">'.base_url().'contacts/enquiry-before-buying/'.$data[0]->id.'</span></p>
// <p><strong>Get Discount @ </strong><span style = "color: blue;">'.base_url().'contacts/discount/'.$data[0]->id.'</span></p>
// <p>&nbsp;</p>
// <p><strong>Report Details &ndash;</strong></p>
// <p><strong>Single User PDF License &ndash; </strong><strong>$'.$data[0]->report_price.'</strong></p>
// <p><strong>Corporate License &ndash; </strong><strong>$'.$data[0]->corporate_price.'</strong></p>
// <p><strong>Buy this Report Now  @ </strong><span style = "color: blue;">'.base_url().'contact/purchase-single-user/'.$data[0]->id.'</span></p>

// <p><strong>About Us:</strong></p>
// <p><strong>Orbis Research</strong><span style="font-weight: 400;"> is a single point aid for all your Market research requirements. We have vast database of reports from the leading publishers and authors across the globe. We specialize in delivering customised reports as per the requirements of our clients. We have complete information about our publishers and hence are sure about the accuracy of the industries and verticals of their specialisation. This helps our clients to map their needs and we produce the perfect required Market research study for our clients.</span></p>
// <p><strong>Contact Us:</strong><strong><br /></strong><span style="font-weight: 400;">Hector Costello </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Senior Manager &ndash; Client Engagements</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">4144N Central Expressway, </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Suite 600, Dallas, </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Texas - 75204, U.S.A. </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><strong>Phone No.</strong>: +1 (214) 884-6817; +91 895 659 5155</span></p>
// <p><strong>Email ID: </strong><a href="mailto:sales@orbisresearch.com"><strong>sales@orbisresearch.com</strong></p>
// </div>';
      
// 	  $pdf->writeHTML($aramexhtml, true, false, true, false, '');
// 	  //$pdf->writeHTMLCell(0, 0, '', '', 'https://www.orbisresearch.com Copyright © 2019 Orbis Research Contact: enquiry@orbisresearch.com', 0, 0, false,true, "L", true);
// 	  $name=clean($data[0]->report_name);

//         $report_pdf = substr($name,0,200)."-" . time() . ".pdf";
//         $pdf->Output('./uploads/report_pdf/' .$report_pdf, 'F');
// 		$label = base_url().'uploads/report_pdf/'.$report_pdf;
//         header('location:'.$label);
		 
// 	}	
}
?>