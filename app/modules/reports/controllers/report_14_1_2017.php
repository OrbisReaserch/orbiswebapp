<?php
class Report extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('reports/report_model');
		$this->load->model('profile/profile_model');
		$this->load->model('contact/contact_model');
		$this->load->library('user_agent');
		$this->load->helper('url');
	}
	
	public function index($report_id)
	{
 		$data['publisher'] = $this->report_model->getPublisher();
		$data['reports'] = $this->report_model->getReports($report_id);
		$data['category'] = $this->report_model->getCategory();
		$data['subcategory'] = $this->report_model->getSubCategory();
		$data['featured'] = $this->report_model->getFeaturedReports();
		$data['testimonials'] = $this->report_model->getTestimonials();
		$data['footernews'] = $this->report_model->getFooterNews();
		$data['footerreport'] = $this->report_model->getFooterReport();
		$data['countries'] = $this->contact_model->getCountryList();
		$data['include'] = 'reports/report';
		$this->load->view('frontend/container',$data);
	}
	public function getSubcategoryInfo($id)
	{
		$data['subcat_id'] =$id;
		 
		$data['category'] = $this->report_model->getCategory();
		$data['subcategory'] = $this->report_model->getSubCategory();
		 
		$data['include'] = 'reports/subcategory_reportlist';
		$this->load->view('frontend/container',$data);
	}

	public function getLatestReportList()
	{
		 
		$data['category'] = $this->report_model->getCategory();
		$data['subcategory'] = $this->report_model->getSubCategory();
		 
		$data['include'] = 'reports/latest_report';
		$this->load->view('frontend/container',$data);
	}

	public function getCategoryReport($catid)
	{
		$data['category_id'] =$catid;
		 $data['category'] = $this->report_model->getCategory();
		 $data['include'] = 'reports/reportlist';
		$this->load->view('frontend/container',$data);
	}

	public function getCountryInfo($id)
	{
		$data['contry_id'] =$id;
		$data['reportlist'] = $this->report_model->getCountryWiseReportList($id);
		$data['category'] = $this->report_model->getCategory();
		$data['subcategory'] = $this->report_model->getSubCategory();
		 $data['country_data'] = $this->report_model->getCountryData($id);
	 
		$data['include'] = 'reports/country_reportlist';
		$this->load->view('frontend/container',$data);
	}
	
  public function getPublisherList($pid)
	{
		$data['publisher_id'] =$pid;
		 
		$data['category'] = $this->report_model->getCategory();
		$data['subcategory'] = $this->report_model->getSubCategory();
		 
		$data['include'] = 'reports/publisher_reportlist';
		$this->load->view('frontend/container',$data);
	}
	
	public function send_enquiry1()
	{
		$id_report=$_POST['id'];
		$page_urls=$_POST['page_urls'];

		$id = $this->report_model->send_enquiry();
		if($id)
		{
			$this->session->set_flashdata('success', 'Enquiry has been send successfully.');
			redirect(base_url().'reports/report/reportEnquiry/'.$page_urls);
		}
		else
		{
			$this->session->set_flashdata('error', 'Unable to send Enquiry. Try again');
			redirect(base_url().'reports/report/reportEnquiry/'.$page_urls);
		}

	}
	
	public function send_enquiry()
	{
		$id_report=$_POST['id'];
		$page_urls=$_POST['page_urls'];

		$id = $this->report_model->send_enquiry();
		if($id)
		{
			$this->session->set_flashdata('success', 'Enquiry has been send successfully.');
			redirect(base_url().'reports/report/index/'.$page_urls.'#enquiry');

		}
		else
		{
			$this->session->set_flashdata('error', 'Unable to send Enquiry. Try again');
			redirect(base_url().'reports/report/index/'.$page_urls.'#enquiry');
		}

	}

	public function reportEnquiry($report_id)
	{
		 
		$data['publisher'] = $this->report_model->getPublisher();
		$data['reports'] = $this->report_model->getReports($report_id);
		$data['category'] = $this->report_model->getCategory();
		$data['subcategory'] = $this->report_model->getSubCategory();
		 
		$data['include'] = 'reports/reportEnquiry';
		$this->load->view('frontend/container',$data);
		
	}
	
	
	
	
	
	public function send_request()
	{
			
			
		$id_report=$_POST['id'];
		$page_urls=$_POST['page_urls'];
		$id = $this->report_model->send_request();
		if($id)
		{
			$username= $_POST['username'];  
			$email=$_POST['email'];
			$phone=$_POST['phone'];
			$message=mysql_real_escape_string($_POST['message']);
			$report_id=$_POST['id'];
			$country=$_POST['country'];
			
			$report_name = $this->report_model->getReportData($report_id);
			//$msg = $username."(".$email."/".$phone.")"." has requested a sample report for <strong>".$report_name[0]->report_name."</strong><br> Message: ".$message;
			$msg = $username.' ('.$email.'/'.$phone.') has requested a sample for report  <strong>'.$report_name[0]->report_name.' </strong> <br>
			<br>
						Name: <strong>'.$username.'</strong><br>
						Email: <strong>'.$email.'</strong><br>
						Country: <strong>'.$country.'</strong><br>
						Number: <strong>'.$phone.'</strong><br>
						Request type: <strong>Sample Report Request</strong><br>
						Report: <strong>'.$report_name[0]->report_name.'</strong><br>
						Message: '.$message.'<br>
						URL: http://www.orbisresearch.com/reports/index/'.$report_name[0]->page_urls.'<br>';
			$this->send_mail('rahul@orbisresearch.com',$msg);
			$this->send_mail('ranjeet@orbisresearch.com',$msg);
			$this->send_mail('abhinay@orbisresearch.com',$msg);
			$this->send_mail('khushru@orbisresearch.com',$msg);
						
			
			
			$this->session->set_flashdata('success', 'Request has been send successfully.');
			redirect(base_url().'reports/reports/thanks?url=RequestSample&repid='.$report_id);

		}
		else
		{
			$this->session->set_flashdata('error', 'Unable to send Request. Try again');
			redirect(base_url().'reports/report/index/'.$page_urls.'#requestsample');
		}

	}
	
	public function check_discount()
	{
			
			
		$id_report=$_POST['id'];
		$page_urls=$_POST['page_urls'];
		$id = $this->report_model->check_discount();
		if($id)
		{
			$username= $_POST['username'];  
			$email=$_POST['email'];
			$phone=$_POST['phone'];
			$message=mysql_real_escape_string($_POST['message']);
			$report_id=$_POST['id'];
			$country=$_POST['country'];
			
			$report_name = $this->report_model->getReportData($report_id);
			//$msg = $username."(".$email."/".$phone.")"." has checked for a discount of report <strong>".$report_name[0]->report_name."</strong><br> Message: ".$message;
			$msg = $username.' ('.$email.'/'.$phone.') has requested for discount on report  <strong>'.$report_name[0]->report_name.' </strong> <br>
			<br>
						Name: <strong>'.$username.'</strong><br>
						Email: <strong>'.$email.'</strong><br>
						Country: <strong>'.$country.'</strong><br>
						Number: <strong>'.$phone.'</strong><br>
						Request type: <strong>Discount Request</strong><br>
						Report: <strong>'.$report_name[0]->report_name.'</strong><br>
						Message: '.$message.'<br>
						URL: http://www.orbisresearch.com/reports/index/'.$report_name[0]->page_urls.'<br>';
			$this->send_mail('rahul@orbisresearch.com',$msg);
			$this->send_mail('ranjeet@orbisresearch.com',$msg);
			$this->send_mail('abhinay@orbisresearch.com',$msg);
			$this->send_mail('khushru@orbisresearch.com',$msg);
		
			$this->session->set_flashdata('success', 'Checking for Discount has been send successfully.');
			redirect(base_url().'reports/reports/thanks?url=Discount&repid='.$report_id);

		}
		else
		{
			$this->session->set_flashdata('error', 'Unable to send Checking for Discount. Try again');
			redirect(base_url().'reports/report/index/'.$page_urls);
		}

	}
public function send_mail($to,$desc)
	{
			$subject = "Orbis Research";
                    $message = '<table border="1" cellspacing="0" width="100%">
                                <tr style="">
                                    <td style=" padding: 10px;">
                                        <strong>Orbis Research</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:10px; background-color: #ffffff;">
                                       	<p>Dear Admin,
                                        <p>'.$desc.' </p>
									</td>
                                </tr>
                            </table>';
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: <enquiry@orbisresearch.com>' . "\r\n";
                    mail($to,$subject,$message,$headers);
	}




	public function confirm_order($amount,$name)
	{
		$user_id = $this->session->userdata('user_id');
	
		if(''!= $this->session->userdata('guestArray')){
			$data['user_info'] = $this->profile_model->getRecord($user_id);
			$username = $data['user_info']['username'];
			$full_name = $data['user_info']['last_name']." ".$data['user_info']['first_name'];
			$email = $data['user_info']['email'];
			$contact_no = $data['user_info']['mobile'];
			
		}else{
			$guestArray =  $this->session->userdata('guestArray');
			$email = $guestArray['gemail'];
			$username= $guestArray['gusername'];
			$full_name =$guestArray['gfull_name'];
			$email = $guestArray['gemail'];
			$contact_no = $guestArray['gphone'];
			$designation = $guestArray['gdesignation'];
			$greport_id = $guestArray['greport_id'];
			
		}
		
		$order_code = 'OR'.time();
		$order_date = date('Y-m-d');
		$this->session->set_userdata('order_code',$order_code);
		
						$guestArrays =  $this->session->userdata('guestArray');			
						$order_id =  $this->session->userdata('order_code');		
						$guestArrayData = array(			
						'order_no'=> $order_id,			
						'full_name'=>$guestArrays['gfull_name'],	
						'email'=>$guestArrays['gemail'],		
						'phone'=>$guestArrays['gphone'],		
						'address'=>$guestArrays['gaddress'],			
						'city'=>$guestArrays['gcity'],			
						'state'=>$guestArrays['gstate'],
						'country'=>$guestArrays['gcountry'],						
						'zip_code'=>$guestArrays['gzip'],		
						'username'=>$guestArrays['gusername'],		
						'designation'=>$guestArrays['gdesignation'],	
						'status'=>'1'			
						);		
						$this->db->insert('guest',$guestArrayData);		
						$guest_id  =  $this->db->insert_id();	
						$greport_id = $guestArrays['greport_id'];	
						$guestArrayOD = array(	
						'order_no'=> $order_id,	
						'guest_id'=>$guest_id,	
						'report_id'=>$greport_id,	
						'price'=>$amount,			
						'payment_status' =>'pending',
						'status'=>'1'				
						);		$this->db->insert('guest_order',$guestArrayOD);		 		
						$report_name = $this->report_model->getReportData($greport_id);		
						//$msg = $guestArrays['gfull_name']." tried to purchase report <strong>".$report_name[0]->report_name."</strong> <br><br>";	
						$msg = $guestArrays['gfull_name'].' ('.$guestArrays['gemail'].'/'.$guestArrays['gphone'].') has attempted to buy a report  <strong>'.$report_name[0]->report_name.' </strong> <br><br>
						 
						 
						 
						Name: <strong>'.$guestArrays['gfull_name'].'</strong><br>
						Email: <strong>'.$guestArrays['gemail'].'</strong><br>
						Number: <strong>'.$guestArrays['gphone'].'</strong><br>
						Country: <strong>'.$guestArrays['gcountry'].'</strong><br>
						Request type: <strong>Payment Attempt</strong><br>
						Report: <strong>'.$report_name[0]->report_name.'</strong><br>
						URL: http://www.orbisresearch.com/reports/index/'.$report_name[0]->page_urls.'<br>';
						$this->send_mail('rahul@orbisresearch.com',$msg);		
						$this->send_mail('ranjeet@orbisresearch.com',$msg);		
						$this->send_mail('abhinay@orbisresearch.com',$msg);						
					 	$this->send_mail('khushru@orbisresearch.com',$msg);					
			
		if($name == '')
		{
			redirect(base_url()."test/TestSsl.php?total=$amount&full_name=$full_name&contact_no=$contact_no&email=$email&order_code=$order_code");
			 
		}
		else{
			
			redirect(base_url()."paypal/paypal.php?total=$amount&full_name=$full_name&contact_no=$contact_no&email=$email&order_code=$order_code");
		}
		
		
	}
	function confirm_orders($amount)
	{	
		//print_r($_POST); die();
		$guestArrays =  $this->session->userdata('guestArray');
		$user_id = $this->session->userdata('user_id');
		if(empty($guestArrays) && ''!= $user_id){
		$data['user_info'] = $this->profile_model->getRecord($user_id);
		$username = $data['user_info']['username'];
		$full_name = $data['user_info']['last_name']." ".$data['user_info']['first_name'];
		$email = $data['user_info']['email'];
		$contact_no = $data['user_info']['mobile'];

			
		$order_code = 'OR'.time();
		$order_date = date('Y-m-d');
		$payment_status = 'pending';
		$order_status = 'pending';
		$status = '1';
		$report_code = "";
		$publisher_id = "";

		$data = array('user_id'=>$user_id,'order_code'=>$order_code,'amount'=>$amount,'order_date'=>$order_date,'payment_status'=>$payment_status,
            'order_status'=>$order_status,'status'=>'1');
		$this->db->insert('orders',$data);
		$order_id  =  $this->db->insert_id();

		$cart=$this->session->userdata('cart');
		//print_r($cart);die();
		for($i=0 ; $i<count($cart) ; $i++)
		{
			$report_id = $cart[$i]['report_id'];
			$price = $cart[$i]['report_price'];
			$status = '1';
			$reportdata['reports'] = $this->report_model->getReportData($report_id);
			//print_r($reportdata); die();
			$report_code = $reportdata['reports'][0]->report_code;
			$publisher_id = $reportdata['reports'][0]->publisher_id;
			//echo $report_code; echo $publisher_id;
			$data = array('order_id'=>$order_id,'user_id'=>$user_id,'report_id'=>$report_id,'price'=>$price,'status'=>'1');
			$this->db->insert('order_details',$data);

			$request = array('username'=>$username,'full_name'=>$full_name,'email'=>$email,'contact_no'=>$contact_no,'report_code'=>$report_code,'publisher_id'=>$publisher_id,'status'=>'1');
			$this->db->insert('request',$request);

		}
		$this->session->unset_userdata('cart'); 
		$this->session->set_flashdata('success', 'Thank you for purchasing our report. Your order has been successfully placed,We will deliver your order within next 24-48 hours.');
		redirect(base_url()."home/home");
		
		}else if(!empty($guestArrays)){		
		
		$order_id =  $this->session->userdata('order_code');		
		$result = $this->db->query("update guest_order set payment_status = 'done' where order_no = '".$order_id."'");		 

		unset($guestArrays);
		unset($order_id);
		$this->session->unset_userdata('guestArray');
		$this->session->unset_userdata('order_code');
			
			$this->session->set_flashdata('success', 'Thank you for your purchase. Your order has been successfully placed, We will deliver your order within 24 hours');
			/* redirect(base_url()."home/home");*/
			redirect(base_url().'reports/reports/thanks?url=purchase&repid='.$report_id);
		}else{ 
			$this->session->unset_userdata('guestArray');
			$this->session->set_flashdata('success', 'There is some problem in purchase');
			/* redirect(base_url()."home/home");*/
			redirect(base_url().'reports/reports/thanks?url=purchase&repid='.$report_id);
		}
	}
	public function cancel_order()
	{
		$order_id =  $this->session->userdata('order_code');
		$guestArrays =  $this->session->userdata('guestArray');
		unset($guestArrays);
		unset($order_id);
		$this->session->unset_userdata('guestArray');
		$this->session->unset_userdata('order_code');
		redirect(base_url()."home/home");
	}
		public function wiredTransfer()
		{
			$guestArrays =  $this->session->userdata('guestArray');
			$order_id = 'WT'.time();
			$guestArrayData = array(
				'order_no'=> $order_id,
				'full_name'=>$guestArrays['gfull_name'],
				'email'=>$guestArrays['gemail'],
				'phone'=>$guestArrays['gphone'],
				'address'=>$guestArrays['gaddress'],
				'city'=>$guestArrays['gcity'],
				'state'=>$guestArrays['gstate'],
				'zip_code'=>$guestArrays['gzip'],
				'username'=>$guestArrays['gusername'],
				'status'=>'0'
				);
		$this->db->insert('guest',$guestArrayData);
		$guest_id  =  $this->db->insert_id();
		$greport_id = $guestArrays['greport_id'];
		$guestArrayOD = array(
				'order_no'=> $order_id,
				'guest_id'=>$guest_id,
				'report_id'=>$greport_id,
				'price'=>'0',
				'status'=>'0'	
				);
		$this->db->insert('guest_order',$guestArrayOD);
		$report_id  =  $this->db->insert_id();
		
		$reports = $this->report_model->getReports1($guestArrays['greport_id']);
		
		$msg = $guestArrays['gfull_name'].' ('.$guestArrays['gemail'].'/'.$guestArrays['gphone'].') has attempted to buy a report  <strong>'.$reports[0]->report_name.' </strong> <br><br>
						 
						 
						 
						Name: <strong>'.$guestArrays['gfull_name'].'</strong><br>
						Email: <strong>'.$guestArrays['gemail'].'</strong><br>
						Number: <strong>'.$guestArrays['gphone'].'</strong><br>
						Country: <strong>'.$guestArrays['gcountry'].'</strong><br>
						Request type: <strong>Wire Transfer</strong><br>
						Report: <strong>'.$reports[0]->report_name.'</strong><br>
						URL: http://www.orbisresearch.com/reports/index/'.$reports[0]->page_urls.'<br>';
							$this->send_mail('rahul@orbisresearch.com',$msg);
							$this->send_mail('ranjeet@orbisresearch.com',$msg);
							$this->send_mail('abhinay@orbisresearch.com',$msg);
							$this->send_mail('sales@orbisresearch.com',$msg);
							$this->send_mail('khushru@orbisresearch.com',$msg);
		
		
		
		
		
		
		
		
		unset($guestArrays);
		unset($order_id);
		$this->session->unset_userdata('guestArray');
		$this->session->unset_userdata('order_code');
			
		$this->session->set_flashdata('success', 'Thank you for your purchase. Your order has been successfully placed, We will deliver your order within 24 hours');
		redirect(base_url()."home/home");
		}


	public function get_sort()
	{
		$sort=$_POST['sort'];
		$suid=$_POST['suid'];
			
		if($sort=='date_desc')
		{
			//    		echo $sort.$suid;
			//    	die();
			$data['sortlist'] = $this->report_model->getReportListDateDesc($suid);

			$data['category'] = $this->report_model->getCategory();
			$data['subcategory'] = $this->report_model->getSubCategory();
			$data['featured'] = $this->report_model->getFeaturedReports();
			$data['footernews'] = $this->report_model->getFooterNews();
			$data['footerreport'] = $this->report_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		elseif ($sort=='date_asc')
		{
			$data['sortlist'] = $this->report_model->getReportListDateAsc($suid);

			$data['category'] = $this->report_model->getCategory();
			$data['subcategory'] = $this->report_model->getSubCategory();
			$data['featured'] = $this->report_model->getFeaturedReports();
			$data['footernews'] = $this->report_model->getFooterNews();
			$data['footerreport'] = $this->report_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		elseif ($sort=='price_asc')
		{
			$data['sortlist'] = $this->report_model->getReportListPriceAsc($suid);

			$data['category'] = $this->report_model->getCategory();
			$data['subcategory'] = $this->report_model->getSubCategory();
			$data['featured'] = $this->report_model->getFeaturedReports();
			$data['footernews'] = $this->report_model->getFooterNews();
			$data['footerreport'] = $this->report_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		elseif ($sort=='price_desc')
		{
			$data['sortlist'] = $this->report_model->getReportListPriceDesc($suid);

			$data['category'] = $this->report_model->getCategory();
			$data['subcategory'] = $this->report_model->getSubCategory();
			$data['featured'] = $this->report_model->getFeaturedReports();
			$data['footernews'] = $this->report_model->getFooterNews();
			$data['footerreport'] = $this->report_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		elseif ($sort=='title_desc')
		{
			$data['sortlist'] = $this->report_model->getReportListTitleDesc($suid);

			$data['category'] = $this->report_model->getCategory();
			$data['subcategory'] = $this->report_model->getSubCategory();
			$data['featured'] = $this->report_model->getFeaturedReports();
			$data['footernews'] = $this->report_model->getFooterNews();
			$data['footerreport'] = $this->report_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		elseif ($sort=='title_asc')
		{
			$data['sortlist'] = $this->report_model->getReportListTitleAsc($suid);

			$data['category'] = $this->report_model->getCategory();
			$data['subcategory'] = $this->report_model->getSubCategory();
			$data['featured'] = $this->report_model->getFeaturedReports();
			$data['footernews'] = $this->report_model->getFooterNews();
			$data['footerreport'] = $this->report_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		else
		{
			$data['reportlist'] = $this->report_model->getReportList($suid);

			$data['category'] = $this->report_model->getCategory();
			$data['subcategory'] = $this->report_model->getSubCategory();
			$data['featured'] = $this->report_model->getFeaturedReports();
			$data['footernews'] = $this->report_model->getFooterNews();
			$data['footerreport'] = $this->report_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
			
	}
	function add_cart()
	{
			
			$id=$_POST['report_id'];
        	$report_price=$_POST['report_price'];
            if($id!="" && $id!=null && $id!=0)
			{
			//	$cart1= $this->session->userdata('cart');
				
                $result=$this->report_model->getReportData($id);

                $cart=$this->session->userdata('cart');
				unset($cart);
                if(is_array($cart))
                {
                    if(count($cart))
                    {
                        $flag=true;
                        $pos=0;
                        for($k=0;$k<count($cart);$k++)
                        {
                            if($cart[$k]['report_id']!=$id)
                            {
                                $flag=true;
                            }
                            else
                            {
                                $flag=false;
                                $pos=$k;
                                break;
                            }
                        }
                        if($flag)
                        {
//                            $final_price=$discounted_price*$quantity;
                            $product=array('report_id'=>$id,
                                            'report_price'=>$report_price);
                            array_push($cart, $product);
                            $this->session->set_userdata('cart',$cart);
                        }
                        else
                        {
                            unset($cart[$pos]);
//                           $final_price=$discounted_price*$quantity;
                            $cart=  array_values($cart);
                            $this->session->set_userdata('cart',$cart);
                            $product=array('report_id'=>$id,
                                            'report_price'=>$report_price);
                            array_push($cart, $product);
                            $this->session->set_userdata('cart',$cart);
                        }
                    }
                    else
                    {
                        //$final_price=$discounted_price*$quantity;
                        $product=array('report_id'=>$id,
                                            'report_price'=>$report_price);
                        
                        array_push($cart, $product);
                        $this->session->set_userdata('cart',$cart);
                    }
                    
                }
                else
                {
                    if(!is_array($cart))
                    {
                        //$final_price=$discounted_price*$quantity;
                        $product=array('report_id'=>$id,
                                            'report_price'=>$report_price);
                        $arr=array('cart'=>array($product));
                        $this->session->set_userdata($arr);
                    }
                }
            }
            
            //redirect($this->agent->referrer());
        redirect('reports/report/checkout');
	}

	public function checkout()
	{
		//$user_id = $this->session->userdata('user_id');
		//if(isset($user_id) && $user_id!=0)
		//{
			//  $data['users'] = $this->category_model->getUsers();
			$data['category'] = $this->report_model->getCategory();
			$data['clients'] = $this->report_model->getUsers();
			$data['countries'] = $this->contact_model->getCountryList();
			$data['sub_category'] = $this->report_model->getSubCategory();
			$data['footernews'] = $this->report_model->getFooterNews();
			$data['footerreport'] = $this->report_model->getFooterReport();
			$cart=$this->session->userdata('cart');
			if(is_array($cart))
			{
				$data['product']=$cart;
			}
			else
			{
				$data['product']=array();
			}
			$data['include'] = 'reports/checkout';
			$this->load->view('frontend/container',$data);
		// }
		// else
		// {
			// redirect('register/login');
		// }
	}
	function remove_cart($id)
	{
		$cart=$this->session->userdata('cart');
		//print_r($cart);
		for($i=0;$i<count($cart);$i++)
		{
			if(in_array($id, $cart[$i]))
			{
				unset($cart[$i]);
				$cart=  array_values($cart);
				$this->session->set_userdata('cart',$cart);
			}
		}
		redirect($this->agent->referrer());
	}

	/*  <!---------------------------------- Latest Report Pagination Starting----------------------------> */
	public function getData()
	{

	 $sql=$_POST['sql'];

	 if($_POST['page'])
	 {
	 	$page = $_POST['page'];
	 	$cur_page = $page;
	 	$page -= 1;
	 	$per_page = 5;
	 	$previous_btn = true;
	 	$next_btn = true;
	 	$first_btn = true;
	 	$last_btn = true;
	 	$start = $page * $per_page;


	 	$query_pag_data = $sql." LIMIT $start, $per_page";
	 	
	 	$result_pag_data = $this->db->query($query_pag_data);
	 	$reportlist=$result_pag_data->result();

	 	$msg ='';
	 	if(count($reportlist)==0)
	 	{

	 		$msg .='<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle"><a href="reports_details.php" class="transicion">No data available</a></h3>
                      </div>
           </div>
            </article> ';
	 	}
	 	else
	 	{
	 		for($i=0;$i<count($reportlist);$i++)
	 		{
	 			$result=$this->db->query("select display_name from users where id='".$reportlist[$i]->publisher_id."'");
	 			$row = $result->result();
				$publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));
	 			$result_1=$this->db->query("select r.no_pages,r.category_id,c.category_name as category_name from report r INNER JOIN category c on(r.category_id = c.id) where page_urls='".$reportlist[$i]->page_urls."' AND r.status='1' LIMIT 0,1");
				$reports=$result_1->result();
	 				
	 			$msg .='<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle">
			<a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="transicion">'. $reportlist[$i]->report_name.'</a></h3>
					 <div class="row">
					<div class="col-lg-2 col-sm-3">
					   <div class="reportsImg">
							   <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'"> <img src="http://www.orbisresearch.com/themes/frontend/images/report_default.jpg" style="height: 165px;" class="img-post img-responsive" alt="'. $reportlist[$i]->report_name.'"></a>
					  </div>
					</div>
					<div class="col-lg-10 col-sm-9 post-content">
					
					<div class="reportsContent">
					  <p><i class="fa fa-user"></i> &nbsp; Published by <a href="'.base_url().'publisher/'.$publisher_name.'.html">'.$row[0]->display_name.'</a></p>
					<p><i class="fa fa-calendar"></i> &nbsp; '.date("M d Y", strtotime($reportlist[$i]->report_date)).'</p>';
					for($j=0;$j<count($reports);$j++)
	 		{
				$category_name =  str_replace(" ","-",strtolower($reports[$j]->category_name));
				$msg .= '<p><i class="fa fa-user"></i> &nbsp; Category <a href="'.base_url().'market-reports/'.$category_name.'.html"> '.$reports[$j]->category_name.'</a></p><p><i class="fa fa-file"></i> &nbsp;  Total Pages: '.$reports[$j]->no_pages.'</p>';
			}
					$msg .='
					
				   <h5><span class="label label-danger"> USD '. $reportlist[$i]->report_price.' </span></h5>
					</div>
				  
				   
				   
					</div>
					
					</div>
					</div>
				   </div>
					</article> ';
		 
	 			
	 			
	 		}
	 	}



	 	/* --------------------------------------------- */


	 	$q=$this->db->query("select id from report where status='1' order by id DESC LIMIT 0,100");
	 	$ro=$q->result();
	 	$count=$q->num_rows();
	 	$no_of_paginations=ceil($count / $per_page);
	 
	 	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	 	if ($cur_page >= 5) {
	 		$start_loop = $cur_page - 3;
	 		if ($no_of_paginations > $cur_page + 3)
	 		{
	 			$end_loop = $cur_page + 3;
	 		}
	 		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6)
	 		{
	 			$start_loop = $no_of_paginations - 6;
	 			$end_loop = $no_of_paginations;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	else
	 	{
	 		$start_loop = 1;
	 		if ($no_of_paginations > 7)
	 		{
	 			$end_loop = 7;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	/* ----------------------------------------------------------------------------------------------------------- */
	 	$msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";

	 	// FOR ENABLING THE FIRST BUTTON
	 	if ($first_btn && $cur_page > 1)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='active'>First</li>";
	 	}
	 	else if ($first_btn)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='inactive'>First</li>";
	 	}

	 	// FOR ENABLING THE PREVIOUS BUTTON
	 	if ($previous_btn && $cur_page > 1)
	 	{
	 		$pre = $cur_page - 1;
	 		$msg .= "<li p='$pre' onclick = 'loadData($pre);' class='active'>Previous</li>";
	 	}
	 	else if ($previous_btn)
	 	{
	 		$msg .= "<li class='inactive'>Previous</li>";
	 	}
	 	for ($i = $start_loop; $i <= $end_loop; $i++)
	 	{

	 		if ($cur_page == $i)
	 		{
	 			$msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
	 		}
	 		else
	 		{
	 			$msg .= "<li p='$i' onclick = 'loadData($i);'>{$i}</li>";
	 		}
	 	}

	 	// TO ENABLE THE NEXT BUTTON
	 	if ($next_btn && $cur_page < $no_of_paginations)
	 	{
	 		$nex = $cur_page + 1;
	 		$msg .= "<li p='$nex' onclick = 'loadData($nex);' class='active'>Next</li>";
	 	}
	 	else if ($next_btn)
	 	{
	 		$msg .= "<li class='inactive'>Next</li>";
	 	}

	 	// TO ENABLE THE END BUTTON
	 	if ($last_btn && $cur_page < $no_of_paginations)
	 	{
	 		$msg .= "<li p='$no_of_paginations' onclick = 'loadData($no_of_paginations);' class='active'>Last</li>";
	 	}
	 	else if ($last_btn)
	 	{
	 		$msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
	 	}
	 	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
	 	$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
	 	$msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
	 	echo $msg;
	 }



	}
	/*  <!---------------------------------- Latest Report Pagination Ending----------------------------> */


	/* <!---------------------------------- CategoryWise Report Pagination Starting---------------------------->*/

	public function getCategorywise()
	{

	 $sql=$_POST['sql'];
$category_id = $_POST['catid'];
	 if($_POST['page'])
	 {
	 	$page = $_POST['page'];
	 	$cur_page = $page;
	 	$page -= 1;
	 	$per_page = 5;
	 	$previous_btn = true;
	 	$next_btn = true;
	 	$first_btn = true;
	 	$last_btn = true;
	 	$start = $page * $per_page;


	 	$query_pag_data = $sql." LIMIT $start, $per_page";
	 	
	 	$result_pag_data = $this->db->query($query_pag_data);
	 	$reportlist=$result_pag_data->result();

	 	$msg ='';
	 	if(count($reportlist)==0)
	 	{

	 		$msg .='<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle"><a href="reports_details.php" class="transicion">No data available</a></h3>
                      </div>
           </div>
            </article> ';
	 	}
	 	else
	 	{
	 		for($i=0;$i<count($reportlist);$i++)
	 		{
	 			$result=$this->db->query("select display_name from users where id='".$reportlist[$i]->publisher_id."'");
	 			$row = $result->result();
				$publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));
	 			$result_1=$this->db->query("select r.no_pages,r.category_id,c.category_name as category_name from report r INNER JOIN category c on(r.category_id = c.id) where page_urls='".$reportlist[$i]->page_urls."' AND r.status='1' LIMIT 0,1");
				$reports=$result_1->result();
	 				
	 			$msg .='<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle">
			<a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="transicion">'. $reportlist[$i]->report_name.'</a></h3>
					 <div class="row">
					<div class="col-lg-3 col-sm-5">
					   <div class="reportsImg">
							   <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'"> <img src="http://www.orbisresearch.com/themes/frontend/images/report_default.jpg" style="height: 165px;" class="img-post img-responsive" alt="'. $reportlist[$i]->report_name.'"></a>
					  </div>
					</div>
					<div class="col-lg-9 col-sm-7 post-content">
					
					<div class="reportsContent">
					  <p><i class="fa fa-user"></i> &nbsp; Published by <a href="'.base_url().'publisher/'.$publisher_name.'.html">'.$row[0]->display_name.'</a></p>
					<p><i class="fa fa-calendar"></i> &nbsp; '.date("M d Y", strtotime($reportlist[$i]->report_date)).'</p>';
					for($j=0;$j<count($reports);$j++)
	 		{
				$category_name =  str_replace(" ","-",strtolower($reports[$j]->category_name));
				$msg .= '<p><i class="fa fa-user"></i> &nbsp; Category <a href="'.base_url().'market-reports/'.$category_name.'.html"> '.$reports[$j]->category_name.'</a></p><p><i class="fa fa-file"></i> &nbsp;  Total Pages: '.$reports[$j]->no_pages.'</p>';
			}
					$msg .='
					
				   <h5><span class="label label-danger"> USD '. $reportlist[$i]->report_price.' </span></h5>
					</div>
				  
				   
				   
					</div>
					
					</div>
					</div>
				   </div>
					</article> ';
		 
	 			
	 			
	 		}
	 	}



	 	/* --------------------------------------------- */


	 	$q=$this->db->query("select id from report where category_id = '$category_id' and status='1' order by id DESC");
	 	$ro=$q->result();
	 	$count=$q->num_rows();
	 	$no_of_paginations=ceil($count / $per_page);
	 
	 	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	 	if ($cur_page >= 5) {
	 		$start_loop = $cur_page - 3;
	 		if ($no_of_paginations > $cur_page + 3)
	 		{
	 			$end_loop = $cur_page + 3;
	 		}
	 		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6)
	 		{
	 			$start_loop = $no_of_paginations - 6;
	 			$end_loop = $no_of_paginations;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	else
	 	{
	 		$start_loop = 1;
	 		if ($no_of_paginations > 7)
	 		{
	 			$end_loop = 7;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	/* ----------------------------------------------------------------------------------------------------------- */
	 	$msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";

	 	// FOR ENABLING THE FIRST BUTTON
	 	if ($first_btn && $cur_page > 1)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='active'>First</li>";
	 	}
	 	else if ($first_btn)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='inactive'>First</li>";
	 	}

	 	// FOR ENABLING THE PREVIOUS BUTTON
	 	if ($previous_btn && $cur_page > 1)
	 	{
	 		$pre = $cur_page - 1;
	 		$msg .= "<li p='$pre' onclick = 'loadData($pre);' class='active'>Previous</li>";
	 	}
	 	else if ($previous_btn)
	 	{
	 		$msg .= "<li class='inactive'>Previous</li>";
	 	}
	 	for ($i = $start_loop; $i <= $end_loop; $i++)
	 	{

	 		if ($cur_page == $i)
	 		{
	 			$msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
	 		}
	 		else
	 		{
	 			$msg .= "<li p='$i' onclick = 'loadData($i);'>{$i}</li>";
	 		}
	 	}

	 	// TO ENABLE THE NEXT BUTTON
	 	if ($next_btn && $cur_page < $no_of_paginations)
	 	{
	 		$nex = $cur_page + 1;
	 		$msg .= "<li p='$nex' onclick = 'loadData($nex);' class='active'>Next</li>";
	 	}
	 	else if ($next_btn)
	 	{
	 		$msg .= "<li class='inactive'>Next</li>";
	 	}

	 	// TO ENABLE THE END BUTTON
	 	if ($last_btn && $cur_page < $no_of_paginations)
	 	{
	 		$msg .= "<li p='$no_of_paginations' onclick = 'loadData($no_of_paginations);' class='active'>Last</li>";
	 	}
	 	else if ($last_btn)
	 	{
	 		$msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
	 	}
	 	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
	 	$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
	 	$msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
	 	echo $msg;
	 }





	}
	/* <!---------------------------------- CategoryWise Report End---------------------------->*/


	/* <!---------------------------------- CountryWise Report Pagination Starting---------------------------->*/

	public function getcountrywise()
	{

	 $sql=$_POST['sql'];
	 $contry_id=$_POST['cid'];
 
	 if($_POST['page'])
	 {
	 	$page = $_POST['page'];
	 	$cur_page = $page;
	 	$page -= 1;
	 	$per_page = 5;
	 	$previous_btn = true;
	 	$next_btn = true;
	 	$first_btn = true;
	 	$last_btn = true;
	 	$start = $page * $per_page;


	 	$query_pag_data = $sql." LIMIT $start, $per_page";
	 	
	 	$result_pag_data = $this->db->query($query_pag_data);
	 	$reportlist=$result_pag_data->result();

	 	$msg ='';
	 	if(count($reportlist)==0)
	 	{

	 		$msg .='<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle"><a href="reports_details.php" class="transicion">No data available</a></h3>
                      </div>
           </div>
            </article> ';
	 	}
	 	else
	 	{
	 		for($i=0;$i<count($reportlist);$i++)
	 		{
	 			$result=$this->db->query("select display_name from users where id='".$reportlist[$i]->publisher_id."'");
	 			$row = $result->result();
				$publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));
	 			$result_1=$this->db->query("select r.no_pages,r.category_id,c.category_name as category_name from report r INNER JOIN category c on(r.category_id = c.id) where page_urls='".$reportlist[$i]->page_urls."' AND r.status='1' LIMIT 0,1");
				$reports=$result_1->result();
	 				
	 			$msg .='<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle">
			<a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="transicion">'. $reportlist[$i]->report_name.'</a></h3>
					 <div class="row">
					<div class="col-lg-3 col-sm-5">
					   <div class="reportsImg">
							   <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'"> <img src="http://www.orbisresearch.com/themes/frontend/images/report_default.jpg" style="height: 165px;" class="img-post img-responsive" alt="'. $reportlist[$i]->report_name.'"></a>
					  </div>
					</div>
					<div class="col-lg-9 col-sm-7 post-content">
					
					<div class="reportsContent">
					  <p><i class="fa fa-user"></i> &nbsp; Published by <a href="'.base_url().'publisher/'.$publisher_name.'.html">'.$row[0]->display_name.'</a></p>
					<p><i class="fa fa-calendar"></i> &nbsp; '.date("M d Y", strtotime($reportlist[$i]->report_date)).'</p>';
					for($j=0;$j<count($reports);$j++)
	 		{
				$category_name =  str_replace(" ","-",strtolower($reports[$j]->category_name));
				$msg .= '<p><i class="fa fa-user"></i> &nbsp; Category <a href="'.base_url().'market-reports/'.$category_name.'.html"> '.$reports[$j]->category_name.'</a></p><p><i class="fa fa-file"></i> &nbsp;  Total Pages: '.$reports[$j]->no_pages.'</p>';
			}
					$msg .='
					
				   <h5><span class="label label-danger"> USD '. $reportlist[$i]->report_price.' </span></h5>
					</div>
				  
				   
				   
					</div>
					
					</div>
					</div>
				   </div>
					</article> ';
		 
	 			
	 			
	 		}
	 	}



	 	/* --------------------------------------------- */


	 	$q=$this->db->query("select id from report where country_id = '$contry_id' and status='1' order by id DESC");
	 	$ro=$q->result();
	 	$count=$q->num_rows();
	 	$no_of_paginations=ceil($count / $per_page);
	 
	 	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	 	if ($cur_page >= 5) {
	 		$start_loop = $cur_page - 3;
	 		if ($no_of_paginations > $cur_page + 3)
	 		{
	 			$end_loop = $cur_page + 3;
	 		}
	 		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6)
	 		{
	 			$start_loop = $no_of_paginations - 6;
	 			$end_loop = $no_of_paginations;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	else
	 	{
	 		$start_loop = 1;
	 		if ($no_of_paginations > 7)
	 		{
	 			$end_loop = 7;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	/* ----------------------------------------------------------------------------------------------------------- */
	 	$msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";

	 	// FOR ENABLING THE FIRST BUTTON
	 	if ($first_btn && $cur_page > 1)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='active'>First</li>";
	 	}
	 	else if ($first_btn)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='inactive'>First</li>";
	 	}

	 	// FOR ENABLING THE PREVIOUS BUTTON
	 	if ($previous_btn && $cur_page > 1)
	 	{
	 		$pre = $cur_page - 1;
	 		$msg .= "<li p='$pre' onclick = 'loadData($pre);' class='active'>Previous</li>";
	 	}
	 	else if ($previous_btn)
	 	{
	 		$msg .= "<li class='inactive'>Previous</li>";
	 	}
	 	for ($i = $start_loop; $i <= $end_loop; $i++)
	 	{

	 		if ($cur_page == $i)
	 		{
	 			$msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
	 		}
	 		else
	 		{
	 			$msg .= "<li p='$i' onclick = 'loadData($i);'>{$i}</li>";
	 		}
	 	}

	 	// TO ENABLE THE NEXT BUTTON
	 	if ($next_btn && $cur_page < $no_of_paginations)
	 	{
	 		$nex = $cur_page + 1;
	 		$msg .= "<li p='$nex' onclick = 'loadData($nex);' class='active'>Next</li>";
	 	}
	 	else if ($next_btn)
	 	{
	 		$msg .= "<li class='inactive'>Next</li>";
	 	}

	 	// TO ENABLE THE END BUTTON
	 	if ($last_btn && $cur_page < $no_of_paginations)
	 	{
	 		$msg .= "<li p='$no_of_paginations' onclick = 'loadData($no_of_paginations);' class='active'>Last</li>";
	 	}
	 	else if ($last_btn)
	 	{
	 		$msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
	 	}
	 	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
	 	$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
	 	$msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
	 	echo $msg;
	 }





	}
	/* <!---------------------------------- CategoryWise Report End---------------------------->*/


	/* <!---------------------------------- Sub category Wise Report Pagination Starting---------------------------->*/

	public function getsubcategorywise()
	{

	 $sql=$_POST['sql'];

	 if($_POST['page'])
	 {
	 	$page = $_POST['page'];
	 	$cur_page = $page;
	 	$page -= 1;
	 	$per_page = 5;
	 	$previous_btn = true;
	 	$next_btn = true;
	 	$first_btn = true;
	 	$last_btn = true;
	 	$start = $page * $per_page;


	 	$query_pag_data = $sql." LIMIT $start, $per_page";
	 	
	 	$result_pag_data = $this->db->query($query_pag_data);
	 	$reportlist=$result_pag_data->result();

	 	$msg ='';
	 	if(count($reportlist)==0)
	 	{

	 		$msg .='<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle"><a href="reports_details.php" class="transicion">No data available</a></h3>
                      </div>
           </div>
            </article> ';
	 	}
	 	else
	 	{
	 		for($i=0;$i<count($reportlist);$i++)
	 		{
	 			$result=$this->db->query("select display_name from users where id='".$reportlist[$i]->publisher_id."'");
	 			$row = $result->result();
				$publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));
	 			$result_1=$this->db->query("select r.no_pages,r.category_id,c.category_name as category_name from report r INNER JOIN category c on(r.category_id = c.id) where page_urls='".$reportlist[$i]->page_urls."' AND r.status='1' LIMIT 0,1");
				$reports=$result_1->result();
	 				
	 			$msg .='<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle">
			<a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="transicion">'. $reportlist[$i]->report_name.'</a></h3>
					 <div class="row">
					<div class="col-lg-3 col-sm-5">
					   <div class="reportsImg">
							   <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'"> <img src="http://www.orbisresearch.com/themes/frontend/images/report_default.jpg" style="height: 165px;" class="img-post img-responsive" alt="'. $reportlist[$i]->report_name.'"></a>
					  </div>
					</div>
					<div class="col-lg-9 col-sm-7 post-content">
					
					<div class="reportsContent">
					  <p><i class="fa fa-user"></i> &nbsp; Published by <a href="'.base_url().'publisher/'.$publisher_name.'.html">'.$row[0]->display_name.'</a></p>
					<p><i class="fa fa-calendar"></i> &nbsp; '.date("M d Y", strtotime($reportlist[$i]->report_date)).'</p>';
					for($j=0;$j<count($reports);$j++)
	 		{
				$category_name =  str_replace(" ","-",strtolower($reports[$j]->category_name));
				$msg .= '<p><i class="fa fa-user"></i> &nbsp; Category <a href="'.base_url().'market-reports/'.$category_name.'.html"> '.$reports[$j]->category_name.'</a></p><p><i class="fa fa-file"></i> &nbsp;  Total Pages: '.$reports[$j]->no_pages.'</p>';
			}
					$msg .='
					
				   <h5><span class="label label-danger"> USD '. $reportlist[$i]->report_price.' </span></h5>
					</div>
				  
				   
				   
					</div>
					
					</div>
					</div>
				   </div>
					</article> ';
		 
	 			
	 			
	 		}
	 	}



	 	/* --------------------------------------------- */


	 	$q=$this->db->query("select id from report where sub_category_id = '$sub_category_id' and status='1' order by id DESC");
	 	$ro=$q->result();
	 	$count=$q->num_rows();
	 	$no_of_paginations=ceil($count / $per_page);
	 
	 	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	 	if ($cur_page >= 5) {
	 		$start_loop = $cur_page - 3;
	 		if ($no_of_paginations > $cur_page + 3)
	 		{
	 			$end_loop = $cur_page + 3;
	 		}
	 		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6)
	 		{
	 			$start_loop = $no_of_paginations - 6;
	 			$end_loop = $no_of_paginations;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	else
	 	{
	 		$start_loop = 1;
	 		if ($no_of_paginations > 7)
	 		{
	 			$end_loop = 7;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	/* ----------------------------------------------------------------------------------------------------------- */
	 	$msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";

	 	// FOR ENABLING THE FIRST BUTTON
	 	if ($first_btn && $cur_page > 1)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='active'>First</li>";
	 	}
	 	else if ($first_btn)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='inactive'>First</li>";
	 	}

	 	// FOR ENABLING THE PREVIOUS BUTTON
	 	if ($previous_btn && $cur_page > 1)
	 	{
	 		$pre = $cur_page - 1;
	 		$msg .= "<li p='$pre' onclick = 'loadData($pre);' class='active'>Previous</li>";
	 	}
	 	else if ($previous_btn)
	 	{
	 		$msg .= "<li class='inactive'>Previous</li>";
	 	}
	 	for ($i = $start_loop; $i <= $end_loop; $i++)
	 	{

	 		if ($cur_page == $i)
	 		{
	 			$msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
	 		}
	 		else
	 		{
	 			$msg .= "<li p='$i' onclick = 'loadData($i);'>{$i}</li>";
	 		}
	 	}

	 	// TO ENABLE THE NEXT BUTTON
	 	if ($next_btn && $cur_page < $no_of_paginations)
	 	{
	 		$nex = $cur_page + 1;
	 		$msg .= "<li p='$nex' onclick = 'loadData($nex);' class='active'>Next</li>";
	 	}
	 	else if ($next_btn)
	 	{
	 		$msg .= "<li class='inactive'>Next</li>";
	 	}

	 	// TO ENABLE THE END BUTTON
	 	if ($last_btn && $cur_page < $no_of_paginations)
	 	{
	 		$msg .= "<li p='$no_of_paginations' onclick = 'loadData($no_of_paginations);' class='active'>Last</li>";
	 	}
	 	else if ($last_btn)
	 	{
	 		$msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
	 	}
	 	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
	 	$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
	 	$msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
	 	echo $msg;
	 }





	}
	/* <!---------------------------------- Sub category Wise Report End---------------------------->*/


	/* <!---------------------------------- Publisher Wise Report Pagination Starting---------------------------->*/

	public function getPublisherDataList()
	{

	 $sql=$_POST['sql'];
	 $publisher_id=$_POST['pid'];

 

	 if($_POST['page'])
	 {
	 	$page = $_POST['page'];
	 	$cur_page = $page;
	 	$page -= 1;
	 	$per_page = 5;
	 	$previous_btn = true;
	 	$next_btn = true;
	 	$first_btn = true;
	 	$last_btn = true;
	 	$start = $page * $per_page;


	 	$query_pag_data = $sql." LIMIT $start, $per_page";
	 	
	 	$result_pag_data = $this->db->query($query_pag_data);
	 	$reportlist=$result_pag_data->result();

	 	$msg ='';
	 	if(count($reportlist)==0)
	 	{

	 		$msg .='<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle"><a href="reports_details.php" class="transicion">No data available</a></h3>
                      </div>
           </div>
            </article> ';
	 	}
	 	else
	 	{
	 		for($i=0;$i<count($reportlist);$i++)
	 		{
	 			$result=$this->db->query("select display_name from users where id='".$reportlist[$i]->publisher_id."'");
	 			$row = $result->result();
				$publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));
	 			$result_1=$this->db->query("select r.no_pages,r.category_id,c.category_name as category_name from report r INNER JOIN category c on(r.category_id = c.id) where page_urls='".$reportlist[$i]->page_urls."' AND r.status='1' LIMIT 0,1");
				$reports=$result_1->result();
	 				
	 			$msg .='<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle">
			<a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="transicion">'. $reportlist[$i]->report_name.'</a></h3>
					 <div class="row">
					<div class="col-lg-3 col-sm-5">
					   <div class="reportsImg">
							   <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'"> <img src="http://www.orbisresearch.com/themes/frontend/images/report_default.jpg" style="height: 165px;" class="img-post img-responsive" alt="'. $reportlist[$i]->report_name.'"></a>
					  </div>
					</div>
					<div class="col-lg-9 col-sm-7 post-content">
					
					<div class="reportsContent">
					  <p><i class="fa fa-user"></i> &nbsp; Published by <a href="'.base_url().'publisher/'.$publisher_name.'.html">'.$row[0]->display_name.'</a></p>
					<p><i class="fa fa-calendar"></i> &nbsp; '.date("M d Y", strtotime($reportlist[$i]->report_date)).'</p>';
					for($j=0;$j<count($reports);$j++)
	 		{
				$category_name =  str_replace(" ","-",strtolower($reports[$j]->category_name));
				$msg .= '<p><i class="fa fa-user"></i> &nbsp; Category <a href="'.base_url().'market-reports/'.$category_name.'.html"> '.$reports[$j]->category_name.'</a></p><p><i class="fa fa-file"></i> &nbsp;  Total Pages: '.$reports[$j]->no_pages.'</p>';
			}
					$msg .='
					
				   <h5><span class="label label-danger"> USD '. $reportlist[$i]->report_price.' </span></h5>
					</div>
				  
				   
				   
					</div>
					
					</div>
					</div>
				   </div>
					</article> ';
		 
	 			
	 			
	 		}
	 	}



	 	/* --------------------------------------------- */


	 	$q=$this->db->query("select id from report where publisher_id = '$publisher_id' and status='1' order by report_date DESC");
	 	$ro=$q->result();
	 	$count=$q->num_rows();
	 	$no_of_paginations=ceil($count / $per_page);
	 
	 	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	 	if ($cur_page >= 5) {
	 		$start_loop = $cur_page - 3;
	 		if ($no_of_paginations > $cur_page + 3)
	 		{
	 			$end_loop = $cur_page + 3;
	 		}
	 		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6)
	 		{
	 			$start_loop = $no_of_paginations - 6;
	 			$end_loop = $no_of_paginations;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	else
	 	{
	 		$start_loop = 1;
	 		if ($no_of_paginations > 7)
	 		{
	 			$end_loop = 7;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	/* ----------------------------------------------------------------------------------------------------------- */
	 	$msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";

	 	// FOR ENABLING THE FIRST BUTTON
	 	if ($first_btn && $cur_page > 1)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='active'>First</li>";
	 	}
	 	else if ($first_btn)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='inactive'>First</li>";
	 	}

	 	// FOR ENABLING THE PREVIOUS BUTTON
	 	if ($previous_btn && $cur_page > 1)
	 	{
	 		$pre = $cur_page - 1;
	 		$msg .= "<li p='$pre' onclick = 'loadData($pre);' class='active'>Previous</li>";
	 	}
	 	else if ($previous_btn)
	 	{
	 		$msg .= "<li class='inactive'>Previous</li>";
	 	}
	 	for ($i = $start_loop; $i <= $end_loop; $i++)
	 	{

	 		if ($cur_page == $i)
	 		{
	 			$msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
	 		}
	 		else
	 		{
	 			$msg .= "<li p='$i' onclick = 'loadData($i);'>{$i}</li>";
	 		}
	 	}

	 	// TO ENABLE THE NEXT BUTTON
	 	if ($next_btn && $cur_page < $no_of_paginations)
	 	{
	 		$nex = $cur_page + 1;
	 		$msg .= "<li p='$nex' onclick = 'loadData($nex);' class='active'>Next</li>";
	 	}
	 	else if ($next_btn)
	 	{
	 		$msg .= "<li class='inactive'>Next</li>";
	 	}

	 	// TO ENABLE THE END BUTTON
	 	if ($last_btn && $cur_page < $no_of_paginations)
	 	{
	 		$msg .= "<li p='$no_of_paginations' onclick = 'loadData($no_of_paginations);' class='active'>Last</li>";
	 	}
	 	else if ($last_btn)
	 	{
	 		$msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
	 	}
	 	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
	 	$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
	 	$msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
	 	echo $msg;
	 }




	}
	
	/* <!---------------------------------- Publisher Wise Report End---------------------------->*/
	
	public function setGuestData()
	{
		$full_name = $_POST['full_name'];
		$email = $_POST['email'];
		$phone =$_POST['phone'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$state =$_POST['state'];
		$country =$_POST['country'];
		$zip = $_POST['zip'];
		$username = $_POST['username'];
		$report_id = $_POST['report_id'];
		$designation = $_POST['designation'];
		$guestArray = array(
		'gfull_name'=>$full_name,
		'gemail'=>$email,
		'gphone'=>$phone,
		'gaddress'=>$address,
		'gcity'=>$city,
		'gstate'=>$state,
		'gcountry'=>$country,
		'gzip'=>$zip,
		'gusername'=>$username,
		'greport_id'=>$report_id,
		'gdesignation'=>$designation
		);
	$this->session->set_userdata('guestArray',$guestArray);
		$guestArrays =  $this->session->userdata('guestArray');
		print_r($guestArrays);
		
	}


}