<?php 
class Reports extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('reports/report_model');
		$this->load->model('profile/profile_model');
		$this->load->model('contact/contact_model');
		$this->load->model('home/home_model');
		$this->load->model('category/category_model');
		$this->load->library('user_agent');
		$this->load->helper('url');
		$this->load->library('email');
	}
	public function index($report_id)
	{
		$data['reports'] = $this->report_model->getReports($report_id);
		$data['sub_category'] = $this->category_model->getSubCategory();
		//$data['featured'] = $this->report_model->getcatgoryreports($data['reports'][0]->category_id);
		$data['featured'] = $this->report_model->getSearchKeywordWiseReports($data['reports'][0]->search_keywords);
		
		$data['countries'] = $this->contact_model->getCountryList();
		$data['include'] = 'reports/report';
		$this->load->view('frontend/container',$data);
	}
	
	public function reportEnquiry($report_id)
	{
		//echo $id; die();
		$data['publisher'] = $this->report_model->getPublisher();
		$data['reports'] = $this->report_model->getReports1($report_id);
		$data['category'] = $this->report_model->getCategory();
		$data['subcategory'] = $this->report_model->getSubCategory();
		$data['featured'] = $this->report_model->getFeaturedReports();
		$data['testimonials'] = $this->report_model->getTestimonials();
		$data['footernews'] = $this->report_model->getFooterNews();
		$data['countries'] = $this->contact_model->getCountryList();
		$data['footerreport'] = $this->report_model->getFooterReport();
		$data['form'] = 'enquiry';
		$data['include'] = 'reports/reportEnquiry';
		$this->load->view('frontend/container',$data);
		//$this->thanks();
	}
	public function discount($report_id){
		$data['reports'] = $this->report_model->getReports1($report_id);
		$data['countries'] = $this->contact_model->getCountryList();
		$data['get_report_id_from_url']=$report_id;
		$data['include'] = 'reports/discount';
		$this->load->view('frontend/container',$data);
		}
	public function requestsample($report_id){
		$data['reports'] = $this->report_model->getReports1($report_id);
		$data['countries'] = $this->contact_model->getCountryList();
		$data['get_report_id_from_url']=$report_id;
		$data['include'] = 'reports/requestsample';
		$this->load->view('frontend/container',$data);
		}
	public function enquirybeforebuying($report_id){
		$data['reports'] = $this->report_model->getReports1($report_id);
		$data['countries'] = $this->contact_model->getCountryList();
		$data['get_report_id_from_url']=$report_id;
		$data['include'] = 'reports/enquirybeforebuying';
		$this->load->view('frontend/container',$data);
	}
	
	//Start Generate Template for SEO Team

	public function generateTemplate1($id){

		$data['reports'] = $this->report_model->getTemplateReportInfo($id);
		//$data['include'] = 'reports/getTemplate';
		$this->load->view('reports/getTemplate1',$data);
	}

	public function generateTemplate2($id){

		$data['reports'] = $this->report_model->getTemplateReportInfo($id);
		//$data['include'] = 'reports/getTemplate';
		$this->load->view('reports/getTemplate2',$data);
	}

	public function report_pdf($report_id){

		$data['report_data'] = $this->report_model->getReportData($report_id);
		// print_r($data);exit;
		$this->load->view('reports/generate_pdf',$data);
	}

	//End Generate Template for SEO Team
	
	public function send_enquiry1()
	{

		$id_report=$_POST['id'];
		$get_report_id_from_url=$_POST['get_report_id_from_url'];
		$page_urls=$_POST['page_urls'];
		$publisher_id=$_POST['publisher_id'];
		$publisher_name1 = $this->report_model->getPublisherName($publisher_id);
		$id = $this->report_model->send_enquiry();
		
		if($id)
		{
			
			$username= $_POST['cf_864'];
			$email=$_POST['email'];
			$phone=$_POST['cf_868'];
			$company_name = $_POST['company'];
			$message=mysql_real_escape_string($_POST['cf_876']);// query on report
			$report_id=$_POST['id'];
			$country=$_POST['cf_858'];
			$request_type=$_POST['request_type'];
			//Extra parameter given for curl function
			$__vtrftk=$_POST['__vtrftk'];
			$publicid=$_POST['publicid'];
			$urlencodeenable=$_POST['urlencodeenable'];
			$name=$_POST['name'];
			$reportname=$_POST['cf_860'];
			$designation=$_POST['cf_870'];//job title
			$publisher_name=$publisher_name1[0]->publisher_name;
			$date=$_POST['cf_882'];
			$time=$_POST['cf_884'];
			$report_code=$_POST['report_code'];
			$country_code=$_POST['country_code'];
			// $curl_data=array('cf_864'=>$username,'email'=>$email,'cf_868'=>$phone,'company'=>$company_name,
			// 	'cf_876'=>$message,'cf_858'=>$country,'__vtrftk'=>$__vtrftk,'publicid'=>$publicid,'urlencodeenable'=>$urlencodeenable,'name'=>$name,'cf_860'=>$reportname,'cf_870'=>$designation,'publisher_id'=>$publisher_id,'cf_874'=>$publisher_name,'id'=>'__vtigerWebForm','cf_880'=>"Enquiry",'cf_882'=>$date,'cf_884'=>$time);

			// $url='http://tempwebapp.orbisresearch.com/modules/Webforms/capture.php';
			// $handle = curl_init($url);
			// curl_setopt($handle, CURLOPT_POST, true);
			// curl_setopt($handle, CURLOPT_POSTFIELDS, $curl_data);
			// curl_exec($handle);
			// curl_close($handle);

			$region=getEmailsRegion($country,'region');
			$test_data=array('name'=>$username,'company'=>$company_name,'email'=>$email,'phone'=>$phone,'message'=>$message,'reportname'=>$reportname,'designation'=>$designation,'publisher_name'=>$publisher_name,'request_type'=>"Enquiry",'datetime'=>$date.' '. $time,'country'=>$country,'region'=>$region);

			//$url='http://tempwebapp.orbisresearch.com/modules/Webforms/capture.php';
			$url='https://crm.orbisresearch.com/orbiscrm/core/leadCapture.php';
			//$url='http://123.201.125.186/orbiscrm/core/leadCapture.php';
			$handle = curl_init($url);
			curl_setopt($handle, CURLOPT_POST, true);
			curl_setopt($handle, CURLOPT_POSTFIELDS, $test_data);
			curl_exec($handle);
			curl_close($handle);


			$report_name = $this->report_model->getReportData($report_id);

			if($report_name[0]->page_urls !=''){
				$report_url_or_id="URL : ".base_url()."reports/index/".$report_name[0]->page_urls;
			}
			else{
				$report_url_or_id="Report Id: <strong>".$get_report_id_from_url."</strong>";
			}
			
			$msg = $username."(".$email."/".$phone.")"." has enquired for a report <strong>".$report_name[0]->report_name."</strong>".$message;$msg = $username.' ('.$email.'/'.$phone.') has sent you an enquiry for report  <strong>'.$report_name[0]->report_name.' </strong> <br>			<br>			
			Name: <strong>'.$username.'</strong><br>				
			Email: <strong>'.$email.'</strong><br>		
			Company Name: <strong>'.$company_name.'</strong><br>
			Designation  : <strong>'.$designation.'</strong><br>
			Number: <strong>'.$country_code.' '.$phone.'</strong><br>	
			Country: <strong>'.$country.'</strong><br>	
			Request type: <strong>Report Enquiry</strong><br>	
			Report: <strong>'.$report_name[0]->report_name.'</strong><br>	
			Message: '.$message.'<br>				
			'.$report_url_or_id.'<br>';
			
			/*$this->send_mail('rahul@orbisresearch.com',$msg);
			
			$this->send_mail('ambika@orbisresearch.com',$msg);
			$this->send_mail('swara@orbisresearch.com',$msg);
			$this->send_mail('karan@orbisresearch.com',$msg);
			$this->send_mail('aditi@orbisresearch.com',$msg);
			$this->send_mail('aslam@orbisresearch.com',$msg);
			$this->send_mail('mayuresh.sonawane@orbisresearch.com',$msg);
			$this->send_mail('hrishikesh@orbisresearch.com',$msg);
			$this->send_mail('tressa@orbisresearch.com',$msg);
			$this->send_mail('shruti@orbisresearch.com',$msg);
			$this->send_mail('anjali@orbisresearch.com',$msg);

			*/

			//this functions are define in constant.php
			
			//$emails=getEmails($country);
			$emails=getEmailsRegion($country,'country');
			//$status=$this->send_mail($emails,$msg);
			send_email_subject($emails,$msg,$username,$company_name,$request_type);
		// $this->send_mail('leads@orbisresearch.com',$msg);
			
			$this->session->set_flashdata('success', 'Enquiry has been send successfully.');
			redirect(base_url().'reports/reports/thanks?url=Question&name='.$report_name[0]->page_urls.'&repid='.$report_id);
		}
		else
		{
			$this->session->set_flashdata('error', 'Unable to send Enquiry. Try again');
			redirect(base_url().'contacts/enquiry-before-buying/'.$id_report);
		}

	}

public function thanks()
{		
		$data['include'] = 'frontend/thankyou';
		$this->load->view('frontend/container',$data);
}
 
public function send_mail($to,$desc)
	{
			/*$subject = "Orbis Research";
                    $message = '<table border="1" cellspacing="0" width="100%">
                                <tr style="">
                                    <td style=" padding: 10px;">
                                        <strong>Orbis Research</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:10px; background-color: #ffffff;">
                                       	<p>Dear Admin,
                                        <p>'.$desc.' </p>
									</td>
                                </tr>
                            </table>';
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                    $headers .= 'From: <enquiry@orbisresearch.com>' . "\r\n";
                    mail($to,$subject,$message,$headers);*/
                    for($i = 0; $i < count($to); $i++){
                        $data = array(
                            'to_email' => $to[$i], 
                            'desc' => $desc
                        );
                    
                        $handle = curl_init('https://www.orbisresearch.com/email/');
                        curl_setopt($handle, CURLOPT_POST, true);
                        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
                        $result = curl_exec($handle);
                   }
                    return true;
	}
	public function send_email_pravin($myemail,$msg,$username,$company_name,$request_type){

		//echo "call mail";exit;

					for($i = 0; $i < count($myemail); $i++){
                        $data = array(
                            'to_email' => $myemail[$i], 
                            'desc' => $msg,
                            'username'=>$username,
                            'company_name'=>$company_name,
                            'request_type'=>$request_type
                        );
                   //print_r($data);exit;
                        $handle = curl_init('https://www.orbisresearch.com/email_pravin/');
                        curl_setopt($handle, CURLOPT_POST, true);
                        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
                        $result = curl_exec($handle);
                        //print_r($result);exit;

                   }
                    return true;

	}
	public function getSearchKeywordRelatedData()
	{
	 	if($_POST['page'])
	 	{
		 	$search_related_keywords=$_POST['related_keyword'];
		 	$page = $_POST['page'];
		 	$cur_page = $page;
		 	$page -= 1;
		 	$per_page = 10;
		 	$previous_btn = true;
		 	$next_btn = true;
		 	$first_btn = true;
		 	$last_btn = true;
		 	$start = $page * $per_page;
		 	$sql="select id, publisher_id, page_urls, category_id, report_name, report_date,report_price from report where search_keywords = '$search_related_keywords' AND status = '1' order by report_date desc";
		 	$query_pag_data = $sql." LIMIT $start, $per_page";
	 	
		 	$result_pag_data = $this->db->query($query_pag_data);
		 	$reportlist=$result_pag_data->result();

	 		$msg ='';
	 	if(count($reportlist)==0)
	 	{

	 		$msg .='<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle">No data available</h3>
            </div>
           </div>
            </article> ';
	 	}
	 	else
	 	{
	 		for($i=0;$i<count($reportlist);$i++)
	 		{
	 			$result=$this->db->query("select display_name from users where id='".$reportlist[$i]->publisher_id."'");
	 			$row = $result->result();
				$publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));
	 			$result_1=$this->db->query("select no_pages,category_id from report where page_urls='".$reportlist[$i]->page_urls."' LIMIT 0,1");
				$reports=$result_1->result();
	 				
	 			$msg .='<article class="post reportsList">
            			<div class="panel panel-default">
            			<div class="panel-body">
          				<div class="row">
						<div class="col-lg-3 col-sm-3">
					   	<div class="reportsImg">
						 <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'"> <img src="'.base_url().'uploads/category_report_image/'.$reportlist[$i]->category_id.'.jpg" style="height: 165px;" class="img-post img-responsive" alt="'. $reportlist[$i]->report_name.'"></a>
					  </div>
					</div>
					<div class="col-lg-9 col-sm-9 post-content">
					 <h3 class="post-title reportsTitle">
						<a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="transicion">'. $reportlist[$i]->report_name.'</a>
					</h3>
					<div class="reportsContent">
					<table class="table table-bordered">
					   <tr>
					       <td> <i class="fa fa-user"></i> &nbsp; Published by <a href="'.base_url().'publisher/'.$publisher_name.'.html">'.$row[0]->display_name.'</a></td>
						   <td><i class="fa fa-calendar"></i> &nbsp; '.date("M d Y", strtotime($reportlist[$i]->report_date)).'</td>
					   </tr>
					   <tr>
					   ';
				$category_name_array = $this->report_model->getCategoryById($reports[0]->category_id);
				$category_name =  str_replace(" ","-",strtolower($category_name_array['category_name']));
				$msg .= '<td><i class="fa fa-user"></i> &nbsp; Category <a href="'.base_url().'market-reports/'.$category_name.'.html"> '.$category_name_array['category_name'].'</a></td>
				<td><i class="fa fa-file"></i> &nbsp;  Total Pages: '.$reports[0]->no_pages.'</td>';
				$msg .='
				  	</tr>
					</table>
				   	 <a class="btn btn-primary" style="cursor: text;"> USD '. $reportlist[$i]->report_price.' </a> <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="btn btn-primary">View Report
				   	 </a>
					</div>
					</div>
					</div>
					</div>
				   </div>
					</article> ';
		 
	 			
	 			
	 		}
	 	}

	 	/* --------------------------------------------- */
	 	$sqlcount="select count(id) as cnt from report where search_keywords = '$search_related_keywords' AND status = '1' order by report_date desc";
	 	$q=$this->db->query($sqlcount);
	  	$count=$q->row()->cnt;
	 	$no_of_paginations=ceil($count / $per_page);
	 
	 	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	 	if ($cur_page >= 10) {
	 		$start_loop = $cur_page - 3;
	 		if ($no_of_paginations > $cur_page + 3)
	 		{
	 			$end_loop = $cur_page + 3;
	 		}
	 		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6)
	 		{
	 			$start_loop = $no_of_paginations - 6;
	 			$end_loop = $no_of_paginations;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	else
	 	{
	 		$start_loop = 1;
	 		if ($no_of_paginations > 7)
	 		{
	 			$end_loop = 7;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	/* ----------------------------------------------------------------------------------------------------------- */
	 	$msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";

	 	// FOR ENABLING THE FIRST BUTTON
	 	if ($first_btn && $cur_page > 1)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='active'>First</li>";
	 	}
	 	else if ($first_btn)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='inactive'>First</li>";
	 	}

	 	// FOR ENABLING THE PREVIOUS BUTTON
	 	if ($previous_btn && $cur_page > 1)
	 	{
	 		$pre = $cur_page - 1;
	 		$msg .= "<li p='$pre' onclick = 'loadData($pre);' class='active'>Previous</li>";
	 	}
	 	else if ($previous_btn)
	 	{
	 		$msg .= "<li class='inactive'>Previous</li>";
	 	}
	 	for ($i = $start_loop; $i <= $end_loop; $i++)
	 	{

	 		if ($cur_page == $i)
	 		{
	 			$msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
	 		}
	 		else
	 		{
	 			$msg .= "<li p='$i' onclick = 'loadData($i);'>{$i}</li>";
	 		}
	 	}

	 	// TO ENABLE THE NEXT BUTTON
	 	if ($next_btn && $cur_page < $no_of_paginations)
	 	{
	 		$nex = $cur_page + 1;
	 		$msg .= "<li p='$nex' onclick = 'loadData($nex);' class='active'>Next</li>";
	 	}
	 	else if ($next_btn)
	 	{
	 		$msg .= "<li class='inactive'>Next</li>";
	 	}

	 	// TO ENABLE THE END BUTTON
	 	if ($last_btn && $cur_page < $no_of_paginations)
	 	{
	 		$msg .= "<li p='$no_of_paginations' onclick = 'loadData($no_of_paginations);' class='active'>Last</li>";
	 	}
	 	else if ($last_btn)
	 	{
	 		$msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
	 	}
	 	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
	 	$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
	 	$msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
	 	echo $msg;
	 }
	}

}	
?>
