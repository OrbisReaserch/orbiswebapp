
<script src="<?php echo base_url();?>themes/frontend/js/ajax.googleapis.com.ajax.libs.jquery.1.4.2.jquery.min.js"></script>

  
  <script type="text/javascript">
function loadData(page)

    {

       var sql=document.getElementById("sql").value;

				$.post('<?php echo base_url()."reports/report/getData";?>',{sql:sql,page:page},function(data)

			{
				 $("#loading").css('display','none');
					$("#container").html(data);

				});

    }
$(document).ready(function(){
    loadData(1);  
});
</script>

<style type="text/css">
#container .pagination ul li.inactive,
#container .pagination ul li.inactive:hover{
    background-color:#ededed;
   color:#bababa;
    border:1px solid #bababa;
    cursor: default;
}
#container .data ul li{
    list-style: none;
    font-family: verdana;
    margin: 5px 0 5px 0;
    color: #000;
    font-size: 12px;
}
#container .pagination{
    height: 25px;
    margin-top:20px;
    float: left;
    padding: 0;
margin: 0;
width: 100%;
clear: both;
}

#container .pagination input{
    height: 30px;
    margin-right: 8px;
}

#container .pagination ul{
    list-style: none;
    float: left;
    padding: 0;
margin: 0;
}

#container .pagination ul li{
    list-style: none;
    float: left;
    border: 1px solid #1062AC;
    padding: 2px 6px 2px 6px;
    margin: 0 3px 0 3px;
    font-family: arial;
    font-size: 12px;
    color: #999;
    font-weight: bold;
    background-color: #ffffff;
}

#container .pagination ul li:hover{
    color: #fff;
    background-color: #1062AC;
    cursor: pointer;
}
            .go_button
            {
            background-color:#ffffff;border:1px solid #fb0455;color:#cc0000;padding:2px 6px 2px 6px;cursor:pointer;position:absolute;margin-top:-1px;
            }
            .total
            {
             float:right;font-family:arial;color:#bababa; font-size: 12px;
             margin:5px 25px;
            }
	#container .pagination ul li.active{
       background-color: #1062AC !important;
	   color:#fff !important;
      }		
</style>
 <header class="main-header">
    <div class="container">
        <h1 class="page-title"> Latest Reports</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active"> Latest Reports</li>
        </ol>
    </div>
</header>



<div class="container">
    <div class="row">
        <div class="col-md-8">
		

     

       <div id="loading">
		 <center>
			<img style = "height:100px;" src = "https://www.southeastairportshuttle.com.au/assets/img/ajax-loading-large.gif" />
		 <center>
	 </div>

            <div id="container">
                
                <div class="data"></div>

                <div class="pagination"></div>

            </div>

      
 
        </div> 
         <div class="col-md-4">
         
         <div class="panel panel-default">
          <div class="panel-heading">
           <h3 class="panel-title">Featured Research Reports</h3>
          </div>
          <div class="panel-body">
		  
		   <?php
        /* count($featured) */
				for($i=0; $i < count($featured);$i++)
				{
					 
				?>
                    <div class="media">
                    <a href="<?php echo base_url();?>reports/index/<?php echo $featured[$i]->page_urls; ?>" class="pull-left"><img width="70" height="70" alt="<?php echo $featured[$i]->report_name; ?>" src="<?php echo base_url();?>uploads/category_report_image/<?php echo $featured[$i]->category_id.".jpg"; ?>" class="media-object"></a>
                    <div class="media-body">
                    <p style="margin-top:5px;margin-bottom:0;"><?php echo substr($featured[$i]->report_name,0,60); ?></p>
                    <small><a href="<?php echo base_url();?>reports/index/<?php echo $featured[$i]->page_urls; ?>">Read more...</a></small>
                    </div>
                    </div>
				<?php }
        ?>           	
 
          </div>
       </div>
           
		 </div> 
    </div> 
</div>
 

<script>

var js=$.noConflict();

// Code that uses other library's $ can follow here.

</script>



 

<!---------------------------------- Latest Report Starting---------------------------->

<?php $sql = "select id,report_name,category_id,publisher_id,report_image,report_date,report_price,page_urls,SUBSTRING(`report_description`, 1, 100) from report WHERE status='1' ORDER BY report_date DESC"; ?>

  <input type="hidden" name="sql" id="sql" value="<?php echo $sql;?>">



<!---------------------------------- Latest Report Ending---------------------------->