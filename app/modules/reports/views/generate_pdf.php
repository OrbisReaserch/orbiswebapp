<?php 
//print_r($report_data[0]->id);
		require_once('PDF_CON_Astha/config/lang/eng.php');
        require_once('PDF_CON_Astha/tcpdf.php');

        // ini_set('max_execution_time', 500);

        class MYPDF extends TCPDF {
        	protected $processId = 0;
		    protected $header = '';
		    protected $footer = '';
		    static $errorMsg = '';

        	public function Header()
		    {
		    	$image_file = K_PATH_IMAGES.'logo.png';
        		$this->Image($image_file, 10, 10, 40, 15, 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		       $this->writeHTMLCell($w='', $h='', $x='', $y='', $this->header, $border=0, $ln=0, $fill=0, $reseth=true, $align='L', $autopadding=true);
		       // $this->SetLineStyle( array( 'width' => 0.25, 'color' => array(0, 0, 0)));

		       // $this->SetLineStyle(array('width' => 0.25 / $this->k, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
		       $this->Line(5, 5, $this->getPageWidth()-5, 5); 

		       $this->Line($this->getPageWidth()-5, 5, $this->getPageWidth()-5,  $this->getPageHeight()-5);
		       $this->Line(5, $this->getPageHeight()-5, $this->getPageWidth()-5, $this->getPageHeight()-5);
		       $this->Line(5, 5, 5, $this->getPageHeight()-5);
		       
		       // $this->writeHTML("<hr>", true, false, false, false, '');
		    }

    	 function Footer() {
        // $image_file = "img/bg_bottom_releve.jpg";
        // $this->Image($image_file, 11, 241, 189, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetY(-12);
    	$this->Line(15,$this->y,200,$this->y);
        $this->SetFont('helvetica', 'N',10);
        $this->Cell(0, 5, 'https://www.orbisresearch.com Copyright © 2019 Orbis Research Contact: enquiry@orbisresearch.com', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}
$pdf  = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('ORBIS');
        $pdf->SetTitle('ORBIS');
        $pdf->SetSubject('ORBIS');
        $pdf->SetKeywords('TCPDF, PDF, ORBIS, ORBIS, ORBIS');
        $pdf->setPrintHeader(TRUE);
        $pdf->setPrintFooter(TRUE);
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->SetFont('helvetica', '', 10);
        // set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, 50);
		// $pdf->setFooterData(PDF_FOOTER_LOGO, 50);

		//  function Footer(){
		// 	$this->SetY(-8);

		// 	$this->SetFont('helvetica','I',8);

		// 	$this->Cell(0,10,'https://www.orbisresearch.com Copyright © 2019 Orbis Research Contact: enquiry@orbisresearch.com');
		// }
		
		//set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(50);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		// function Footer()
		// {
		//      $this->writeHTML('https://www.orbisresearch.com Copyright © 2019 Orbis Research Contact: enquiry@orbisresearch.com', false, true, false, true);
		//     $this->SetY(8);
		   
		//     $this->SetFont('helvetica', 'I', 8);
		    
		// }
		$pdf->writeHTML('https://www.orbisresearch.com Copyright © 2019 Orbis Research Contact: enquiry@orbisresearch.com', false, true, false, true);
        $pdf->AddPage();
		// $report_data = $this->getReportData($report_id);
		 function removeslashes($string)
			{
			    $string1=implode("",explode("\\n",$string));
				$string2=implode("",explode("\\",$string1));
				$string3=implode("",explode("\\",$string2));
				$string4=implode("",explode("<table>",$string3));
			     $string5=implode("",explode("</table>",$string4));
			    return stripslashes(trim($string5));
			}
		function clean($string){

   		return preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
	 }
 $find0=['\n','\"'];
	 $replace0=['',''];
	 $report_description=str_replace($find0,$replace0,removeslashes($report_data[0]->report_description));

	 $find1=['Table of Content','\n','\"'];
	 $replace1=['','',''];
	 $report_containt=str_replace($find1,$replace1,removeslashes($report_data[0]->report_content));

	 $find2=['<h1>','</h1>','\n','\"'];
	 $replace2=['<h2>','</h2>','',''];
	 $list_of_tables=str_replace($find2,$replace2,removeslashes($report_data[0]->list_of_tables));

	 $request=base_url()."contacts/request-sample/".$report_data[0]->id;
	 $browse = base_url()."reports/index/".$report_data[0]->page_urls;

$aramexhtml = '<div style="">

<p style = "text-align:center;font-size: x-large;"><span style="font-weight: 400;"><b>'.$report_data[0]->report_name.'</b></span></p><hr>
<p></p>

<p><strong>No of Report Pages &ndash; </strong><span style="font-weight: 400;">'.$report_data[0]->no_pages.'</span>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<span style="float:right;"><strong>Publishing Date &ndash; </strong><span style="font-weight: 400;">'.date("M d Y", strtotime($report_data[0]->report_date)).'</span></span>
</p>
<p><strong>Request a PDF sample @ </strong><span style = "color: blue;"><a href="'.$request.'">'.$request.'</a></span>

<p><strong>Report Description</strong></p>
<p><span style="font-weight: 400;">'.$report_description.'</span></p>
<p><strong>Browse the full report @ </strong><span style = "color: blue;">'.base_url().'reports/index/'.$report_data[0]->page_urls.'</span></p>

<p><strong>Table of Contents:</strong></p>
<p><span style="font-weight: 400;">'.$report_containt.'</span></p> 
<br>
<strong>List Of Tables:</strong>
                    <p>';
                    $aramexhtml .= $list_of_tables;
					
					$aramexhtml .= '</p>
<p><strong>For Enquiry and Customization of Report, Contact @ </strong><span style = "color: blue;">'.base_url().'contacts/enquiry-before-buying/'.$report_data[0]->id.'</span></p>
<p><strong>Get Discount @ </strong><span style = "color: blue;">'.base_url().'contacts/discount/'.$report_data[0]->id.'</span></p>
<p>&nbsp;</p>
<p><strong>Report Details &ndash;</strong></p>
<p><strong>Single User PDF License &ndash; </strong><strong>$'.$report_data[0]->report_price.'</strong></p>
<p><strong>Corporate License &ndash; </strong><strong>$'.$report_data[0]->corporate_price.'</strong></p>
<p><strong>Buy this Report Now  @ </strong><span style = "color: blue;">'.base_url().'contact/purchase-single-user/'.$report_data[0]->id.'</span></p>

<p><strong>About Us:</strong></p>
<p><strong>Orbis Research</strong><span style="font-weight: 400;"> is a single point aid for all your Market research requirements. We have vast database of reports from the leading publishers and authors across the globe. We specialize in delivering customised reports as per the requirements of our clients. We have complete information about our publishers and hence are sure about the accuracy of the industries and verticals of their specialisation. This helps our clients to map their needs and we produce the perfect required Market research study for our clients.</span></p>
<p><strong>Contact Us:</strong><strong><br /></strong><span style="font-weight: 400;">Hector Costello </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Senior Manager &ndash; Client Engagements</span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">4144N Central Expressway, </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Suite 600, Dallas, </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;">Texas - 75204, U.S.A. </span><span style="font-weight: 400;"><br /></span><span style="font-weight: 400;"><strong>Phone No.</strong>: +1 (972)-362-8199; +91 895 659 5155</span></p>
<p><strong>Email ID: </strong><a href="mailto:sales@orbisresearch.com"><strong>sales@orbisresearch.com</strong></p>
</div>
<p><strong>Request a PDF sample @ </strong><span style = "color: blue;"><a href="'.$browse.'">'.$browse.'</a></span></p>

';
      
	  $pdf->writeHTML($aramexhtml, true, false, true, false, '');
	  //$pdf->writeHTMLCell(0, 0, '', '', 'https://www.orbisresearch.com Copyright © 2019 Orbis Research Contact: enquiry@orbisresearch.com', 0, 0, false,true, "L", true);
        $report_pdf = clean($report_data[0]->report_name)."-" . time() . ".pdf";
	  	ob_clean();
        $pdf->Output('./uploads/report_pdf/' .$report_pdf, 'F');
		$label = base_url().'uploads/report_pdf/'.$report_pdf;
        header('location:'.$label);
		 
// 	}	
 
?>  

	
