<?php 
  //echo base_url();
  echo index_page();
  if(empty($reports))
    {
    	redirect(base_url());
    }
  function removeslashes($string)
			{
			    $string1=implode("",explode("\\n",$string));
				$string2=implode("",explode("\\",$string1));
				$string3=implode("",explode("\\",$string2));
				$string4=implode("",explode("<table>",$string3));
			     $string5=implode("",explode("</table>",$string4));
			    return stripslashes(trim($string5));
			}
			
    $result=$this->db->query("select * from users where id='".$reports[0]->publisher_id."'");
         	$row = $result->result();
    ?><head>
<meta name="keywords" content="<?php echo $reports[0]->metatitle;?>" />
<meta name="Description" content="<?php echo str_replace("&nbsp;"," ",stripslashes(strip_tags($reports[0]->report_summery)));?>" />
<meta name = "title" content="<?php if ('' == $reports[0]->meta_title){echo $reports[0]->title;}else{ echo $reports[0]->meta_title;}?>"/>
<title><?php if ('NULL' == $reports[0]->page_title || '' == $reports[0]->page_title){echo $reports[0]->meta_title;}else{ echo $reports[0]->page_title;}?></title>
</head> 


<div id="slider_wrapper2">
  <div class="wrap">
    <h1 style="text-transform:none;"><span><?php echo $reports[0]->report_name; ?></span></h1>
  </div>
</div>
<div class="clear"></div>
<div class="content">
  <div class="wrap">
    <div class="fifth_remaining_new">
      <div class="blog-post">
        <div class="">
          <?php 
            /*$rms=$reports[0]->report_image;
            if($rms!='')
                    {
           ?>
                      <img src="<?php echo base_url(); ?>uploads/report_images/<?php echo $reports[0]->report_image;?>" width="288" height="192" alt="" class="blog_img"  />
                      <?php 
                    }
                    else 
                    {
             ?> 
                      <img src="<?php echo base_url(); ?>uploads/no-image.png" width="288" height="192" alt=""  class="blog_img" />
                      <?php } */
					  $publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));
					  $category_name =  str_replace(" ","-",strtolower($reports[0]->category_name));
					  ?>
         <div class="one_third marbot"> <center><img src="<?php echo base_url(); ?>themes/frontend/images/report_default.jpg" height="200"  alt="<?php echo $reports[0]->report_name;?>"  class="blog_img pull-lef" /></center></div><div class="fifth_remaining_new ab last marbot" >
          <ul>
            <li><i class="icon-user"></i>Published by <a href="<?php echo base_url();?>publisher/<?php echo $publisher_name.'.html';?>"><?php echo $row[0]->display_name;?></a></li>
            <li><i class="icon-time"></i><?php echo date("M d Y", strtotime($reports[0]->report_date));?></li>
            <li><i class="icon-user"></i>Category <a href="<?php echo base_url();?>market-reports/<?php echo $category_name.'.html';?>"><?php echo $reports[0]->category_name;?></a></li>
            <li><i class="icon-file"></i>
              <label style="float: right;color: black; margin-top:0px"> Total Pages: <?php echo $reports[0]->no_pages; ?> </label>
            </li>
          </ul>
          <?php $report_status=$reports[0]->report_status; 
	  if($report_status==1)
	  {
	  ?>
          <?php
	   }
	   else
	   {
	   ?>
          
          <?php } ?>
          <div class="clear"></div>
          
          <div class="space1"></div>
          <?php /*?><label style="font-size: 13px;color: red;">USD</label> &nbsp;&nbsp;&nbsp; <?php */?>
          <input type="hidden" class="price1" readonly value=" <?php echo $reports[0]->report_price; ?>" name="price" id="price">
          <p>
            <?php //echo removeslashes($reports[0]->report_summery);?>
          </p>
          <div class="clear"></div>
          <div class="space1"></div>
        </div> 
        <div class="clear"></div>
        <div class="tabs_panel">
        <div id="tabs" class="tab-container">
          <ul class="etabs" style="width: 100%;">
            <li class="tab" id = "descriptionbtn" ><a onclick = "displayDiv('description');" style="cursor:pointer;">Report Description</a></li>
            <li class="tab" id = "tocbtn"><a  onclick = "displayDiv('toc');" style="cursor:pointer;">Table of Content</a></li>
            <li class="tab" id = "chkdiscountbtn"><a onclick = "displayDiv('chkdiscount');" style="cursor:pointer;">Related Reports</a></li>
            <!--<li class="tab" id = "lotbtn"><a  onclick = "displayDiv('lot');" style="cursor:pointer;"><i class="icon-check"></i>List of Tables</a></li>
          <li class="tab" id = "rrbtn"><a  onclick = "displayDiv('rr');" style="cursor:pointer;"><i class="icon-check"></i>Related Reports</a></li>
          <li class="tab"><a href="<?php echo base_url();?>reports/reportEnquiry/<?php echo $reports[0]->id; ?>" style="cursor:pointer;"><i class="icon-check"></i>Enquiry</a></li>-->
            
          </ul>
           
          <div class="tab-item" id="description" style = "display:none;">
         
            <div id = "demo" class="one_full blackcolortext" style = "text-align: justify;">
              <p  ><?php echo substr(removeslashes($reports[0]->report_description),0,500);?>&nbsp;&nbsp;&nbsp;
                <?php if(strlen($reports[0]->report_description) > 500){?>
                <a onclick = "getMore();" style="font-weight:700; cursor:pointer;">Read more. . .</a>
                <?php }?>
              </p>
            </div>
            <div class="one_full blackcolortext" style = "text-align: justify; display:none;" id = "complete" > 
              <p ><?php echo str_replace('<li>','<li style = "list-style: initial;float: initial;">',removeslashes($reports[0]->report_description));?></p>
            </div>
          </div>
          <div class="tab-item" id="toc" style = "display:none;">
            <div class="one_full blackcolortext" style = "text-align: justify;">
              
              <p><?php echo removeslashes($reports[0]->report_content);?></p>
            </div>
            <div class="one_full blackcolortext" style = "text-align: justify;">
              <h2><strong>List Of Tables:</strong></h2>
              <p><?php 
				$a = str_replace("<h1>","<h2>",$reports[0]->list_of_tables);
				$b = str_replace("</h1>","</h2>",$a);
			  echo removeslashes($b); ?></p>
            </div>
          </div>
          <?php /* ?><div class="tab-item" id="lot" style = "display:none;">
          <div class="one_full blackcolortext" style = "text-align: justify;">
            <h1><strong>List Of Tables:</strong></h1>
            <p><?php //echo removeslashes($reports[0]->list_of_tables);?></p>
          </div>
        </div><?php */?>
          
          <!--  related report    echo str_replace('<p>','<p style = "color:black;">',   -->
          
          <div class="tab-item" id="rr" style = "display:none;"> </div>
          
          <!--  end related report      -->
          <div class="tab-item" id="requestsample" style = "display:none;">
            <div class="one_full">
            <strong>Request Sample:</strong>
              <span style="margin-left: 30px;">
              <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('errror')."</font>";
                            }
                            ?>
              </span>
              
            </div>
          </div>
          <?php /*?>
        <div class="tab-item" id="enquiry">
          <div class="one_full">
            <h1>Enquiry:</h1>
             <span style="margin-left: 30px;">
                            <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('errror')."</font>";
                            }
                            ?>
                        </span>
            <form id="contactform" action="<?php echo base_url();?>reports/report/send_enquiry" method="post">
             <table>
          <tr>
            <td><input type="text" id="name" style="font-size: 13px;" name="username" placeholder="Your Name" pattern="^[a-zA-Z ]+$" required /></td>
          </tr>
          <tr>
            <td><input type="text" id="name" style="font-size: 13px;" name="company_name" placeholder="Your Company Name" required /></td>
          </tr>
		  <tr>
            <td><input type="text" id="email" style="font-size: 13px;" name="email" placeholder="Your Email Address" required pattern="^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+$"/></td>
          </tr>
          <tr>
            <td><input type="text" id="email"  style="font-size: 13px;" name="phone" placeholder="Your Phone Number" required pattern="^[0-9 ]+$"/></td>
          </tr>
          <tr>
            <td><textarea id="message" placeholder="Your Message" style="font-size: 13px;" name="message" rows="5" cols="20"></textarea></td>
          </tr>
          <tr>
            <td>
	            <input type="hidden" value="<?php echo $reports[0]->id;?>" name="id">
				<input type="hidden" value="<?php echo $reports[0]->page_urls;?>" name="page_urls">
				
	            <input type="hidden" name="report_code" value="<?php echo $reports[0]->report_code;?>">
	            <input type="hidden" name="publisher_id" value="<?php echo $reports[0]->publisher_id;?>">
	            <input type="submit" value="Send" id="send" class="button small" />
	            <input type="reset" value="Reset" class="button small" />
            </td>
          </tr>
        </table>
                  </form>
          </div>
        </div>
		<?php */?>
          <div class="tab-item" id="chkdiscount" style = "display:none;">
            <div class="one_full">
              <p>
                <?php $subcat_id=$reports[0]->sub_category_id;

                        $query=$this->db->query("select * from report where status='1' and sub_category_id='$subcat_id' and id!='".$reports[0]->id."' LIMIT 0,6");
						//return $result->result();
						if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					
			$results=$this->db->query("select * from users where id='".$row->publisher_id."'");
         	$rowss = $results->result();
					//echo $row->publisher_id.'<br>';	
					
					?>
              
              <div class="blog-post">
                <div class="one_fifth marbot"> <a href="<?php echo base_url();?>reports/report/index/<?php echo $row->id;?>"> <img src="<?php echo base_url(); ?>themes/frontend/images/report_default.jpg"  alt="<?php echo $row->report_name; ?>"  class="blog_img" /> </a> </div>
              </div>
              <div class="two_thirds last marbot">
                <h3 style="margin-bottom: -2%;"><a href="<?php echo base_url();?>reports/index/<?php echo $row->page_urls;?>" style="color:rgb(0, 90, 171);;"><?php echo $row->report_name; ?></a></h3>
                <ul style = "float:right;">
                  <!-- <li><i class="icon-user"></i>Published by <a href="#"><?php echo $rowss[0]->last_name." ".$rowss[0]->first_name;?></a></li>--
            <li><i class="icon-time"></i><?php echo date("M d Y", strtotime($row->report_date));?></li>-->
                </ul>
                <div class="clear"></div>
                <br/>
                <p style= "color: #030303; text-align: justify;"> <?php echo strip_tags(removeslashes(substr($row->report_description,0,400)));?>... </p>
              </div>
              <div class="clear"></div>
              <div class="divider"></div>
              <?php 		
					
				}
			}
			else 
			{
				echo '<p style= "color:black">NA</p>';
			}
                        
                        ?>
              </p>
              <span>
              <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('errror')."</font>";
                            }
                            ?>
              </span>
              
            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
        </div>
        
        
      </div>
    </div>
    <div class="one_third last">
        <div class="reqsampleblock">
            <center>
              <p style="padding:10px 0px 10px 0px;"><strong>Choose License Type:</strong></p>
            </center>
            <center  class="pricedropdown"><select name="price_type" id="price_type" onchange="pricetype(this.value)" style="margin-bottom:23px;">
              <option value="single"><strong>Single User (USD <?php echo $reports[0]->report_price; ?>)</strong></option>
              <option value="multiple"><strong>Multiple User (USD <?php echo $reports[0]->multiple_price; ?>)</strong></option>
              <option value="global"><strong>Global User (USD <?php echo $reports[0]->global_price; ?>)</strong></option>
              <option value="corporate"><strong>Corporate User (USD <?php echo $reports[0]->corporate_price; ?>)</strong></option>
            </select></center>
            <div style="clear:both;"></div>
            
            <center><a href="#" class="button small reqsamplebtn" style="background-color:#005AAB !important;"  onclick="submit(<?php echo $reports[0]->id?>);"><i class="icon-shopping-cart pull-left" style="font-size:21px !important; color:#fff;"></i> Buy now 
            
            </a></center> </div>
            <div class="reqsampleblock">
        <!--<p>Do you wish to check the sample of the report? <br/>
          Order a <strong>FREE</strong> sample right now! </p>-->
        <!-- onclick = "displayDiv('requestsample');"  --> 
        <center><a  class="button small reqsamplebtn" href= "<?php echo base_url();?>contacts/request-sample/<?php echo $reports[0]->id; ?>"><i class="icon-file-alt pull-left" style="font-size:21px !important; color:#fff;"></i>  Request a Sample
        
        <div style="clear:both;"></div>
        </a> </center></div>
        <div class="reqsampleblock">
        <center><a  class="button small reqsamplebtn" href= "<?php echo base_url();?>contacts/enquiry-before-buying/<?php echo $reports[0]->id; ?>" style="background-color:#005AAB !important;"><i class="icon-envelope pull-left" style="font-size:21px !important; color:#fff;"></i> Enquire Before Buying</a></center>
        </div>
        
        
        <div class="reqsampleblock">
        <center><a  class="button small reqsamplebtn" href= "<?php echo base_url();?>contacts/discount/<?php echo $reports[0]->id; ?>"><i class="icon-envelope pull-left" style="font-size:21px !important; color:#fff;"></i> Check Discount</a></center>
        </div>
          <div class="enquirybox">
        <center>
          <div class="tab1"> <h5 style="color:#fff;"><i class="icon-envelope pull-left" style="font-size:21px !important; color:#fff;"></i> Make An Enquiry:</h5> </div>
        </center>
        <form id="contactform" action="<?php echo base_url();?>reports/send_enquiry1" method="post">
          <table style="margin:0 auto;">
            <tr>
              <td><input type="text" id="name" style="font-size: 13px; float:left;" name="username" placeholder="Your Name" pattern="^[a-zA-Z ]+$" required />
                <span style="color:red; float:left;">*</span>
                <div style="clear:both;"></div></td>
            </tr>
            <tr>
              <td><input type="text" id="name" style="font-size: 13px;float:left;" name="company_name" placeholder="Your Company Name" required />
                <span style="color:red;float:left;">*</span></td>
            </tr>
            <tr>
              <td><select name = "country" class="selectbox">
                  <?php 
			for($c = 0; $c<count($countries); $c++)
			{ 
				?>
                  <option value = "<?php echo $countries[$c]->country_name;?>"><?php echo $countries[$c]->country_name;?></option>
                  <?php
			}
			?>
                </select></td>
            </tr>
            <tr>
              <td><input type="text" id="email" style="font-size: 13px;float:left;" name="email" placeholder="Your Email Address" required pattern="^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+$"/>
                <span style="color:red;float:left;">*</span></td>
            </tr>
            <tr>
              <td><input type="text" id="email" style="font-size: 13px;float:left;" name="phone" placeholder="Your Phone Number" required pattern="^[0-9 ]+$"/>
                <span style="color:red">*</span></td>
            </tr>
            <tr>
              <td><input type="text" id="email" style="font-size: 13px; float:left;" name="designation" placeholder="Job Title" required />
                <span style="color:red;float:left;">*</span></td>
            </tr>
            <tr>
              <td><textarea id="message" style="font-size: 13px; float: left;" placeholder="Your Message" name="message" rows="5" cols="20"></textarea></td>
            </tr> <tr>
			  <td><input type="text" readonly id="txtCaptcha"
                                   style="background-image:url(<?php echo base_url(); ?>themes/frontend/images/captcha.JPG); text-align:center; border:none;
            font-weight:bold; font-family:Modern;width: 200px;height:35px;color: green !important;font-size: 25px;" />
                         </td> </tr> <tr>
          <td><input type="text" id="txtInput" placeholder="Captcha text" required style="float:left; height:25px; width:230px;"/> <input type="button" id="btnrefresh" class="pull-right" value="" onClick="DrawCaptcha();" style="height: 30px;width:30px;background-image:url(<?php echo base_url(); ?>themes/frontend/images/refresh.jpg);background-repeat:no-repeat; background-color:#ffffff;" />
                            <label id="lblError" style="color:red;"></label></td> </tr>
            <tr>
              <td><input type="hidden" value="<?php echo $reports[0]->id;?>" name="id">
                <input type="hidden" value="<?php echo $reports[0]->page_urls;?>" name="page_urls">
                <input type="hidden" name="report_code" value="<?php echo $reports[0]->report_code;?>">
                <input type="hidden" name="publisher_id" value="<?php echo $reports[0]->publisher_id;?>">
                <center><input type="submit" value="Send" id="send" class="button small" onclick="return CheckCaptcha()"/></center>
                
                <!-- <input type="reset" value="Reset" class="button small" />--></td>
            </tr>
          </table>
        </form>
        
        <!--  </div>-->
        <div class="clear"></div>
      </div>
        </div>
  </div>
  <div class="clear"></div>
  
</div>
<div style="clear:both;"></div>
</div>
</div>
<div class="clear"></div>
<div class="space1"></div>
<form id="myform" action="<?php echo base_url();?>contact/purchase/<?php echo $reports[0]->id;?>" method="post">
  <?php 
	 $cart=$this->session->userdata('cart');
	 ?>
  <input type='hidden' name="report_id" id="report_id" value="<?php echo $reports[0]->id;?>">
  <input type='hidden' name="report_price" id="report_price" value="<?php echo $reports[0]->report_price;?>">
</form>
<script>

function pricetype(pricetype)
{
//alert(pricetype);
	if(pricetype=='single')
	{
	//alert(<?php echo $reports[0]->report_price;?>);
	document.getElementById("price").value='<?php echo $reports[0]->report_price;?>';
	document.getElementById("report_price").value='<?php echo $reports[0]->report_price;?>';
	
	}
	else if(pricetype=='multiple')
	{
	//alert(<?php echo $reports[0]->multiple_price;?>);
	document.getElementById("price").value='<?php echo $reports[0]->multiple_price;?>';
	document.getElementById("report_price").value='<?php echo $reports[0]->multiple_price;?>';
	}
	else if(pricetype=='global')
	{
	//alert(<?php echo $reports[0]->global_price;?>);
	document.getElementById("price").value='<?php echo $reports[0]->global_price;?>';
	document.getElementById("report_price").value='<?php echo $reports[0]->global_price;?>';
	}
	else if(pricetype=='corporate')
	{
	//alert(<?php echo $reports[0]->corporate_price;?>);
	document.getElementById("price").value='<?php echo $reports[0]->corporate_price;?>';
	document.getElementById("report_price").value='<?php echo $reports[0]->corporate_price;?>';
	}
}

function submit(value)
{
//	alert("aaa");
	 document.getElementById("myform").submit();
}
$( window ).load(function() {
 displayDiv('description');
});
	function displayDiv(val)
	{
		//alert(val);
		if(val == 'description'){
			document.getElementById("description").style.display = 'block';
			document.getElementById("toc").style.display = 'none';
			//document.getElementById("rr").style.display = 'none';
			//document.getElementById("requestsample").style.display = 'none';
			document.getElementById("chkdiscount").style.display = 'none';
			//document.getElementById("lot").style.display = 'none';
			
			document.getElementById("descriptionbtn").style.backgroundColor  = '#ff9900';
			document.getElementById("tocbtn").style.backgroundColor = '#005aab';
			//document.getElementById("rrbtn").style.backgroundColor = '#005aab';
			//document.getElementById("requestsamplebtn").style.backgroundColor = '#005aab';
			document.getElementById("chkdiscountbtn").style.backgroundColor = '#005aab';
			//document.getElementById("lotbtn").style.backgroundColor = '#005aab';
			
			
		} else if(val== 'toc'){
		//document.getElementById("lot").style.display = 'none';
			document.getElementById("description").style.display = 'none';
			document.getElementById("toc").style.display = 'block';
			document.getElementById("tocbtn").style.backgroundColor  = '#ff9900';
			//document.getElementById("rr").style.display = 'none';
			//document.getElementById("requestsample").style.display = 'none';
			document.getElementById("chkdiscount").style.display = 'none';
			//document.getElementById("lotbtn").style.backgroundColor = '#005aab';
			document.getElementById("descriptionbtn").style.backgroundColor = '#005aab';
			//document.getElementById("rrbtn").style.backgroundColor = '#005aab';
			//document.getElementById("requestsamplebtn").style.backgroundColor = '#005aab';
			document.getElementById("chkdiscountbtn").style.backgroundColor = '#005aab';
		} else if(val== 'rr'){
		document.getElementById("chkdiscountbtn").style.backgroundColor = '#005aab';
		//document.getElementById("lot").style.display = 'none';
			document.getElementById("description").style.display = 'none';
			document.getElementById("toc").style.display = 'none';
			//document.getElementById("rr").style.display = 'block';
			//document.getElementById("rrbtn").style.backgroundColor  = 'red';
			//document.getElementById("requestsample").style.display = 'none';
			document.getElementById("chkdiscount").style.display = 'none';
			//document.getElementById("lotbtn").style.backgroundColor = '#005aab';
			document.getElementById("descriptionbtn").style.backgroundColor = '#005aab';
			document.getElementById("tocbtn").style.backgroundColor = '#005aab';
			//document.getElementById("requestsamplebtn").style.backgroundColor = '#005aab';
			
		} else if(val== 'requestsample'){
			document.getElementById("description").style.display = 'none';
			document.getElementById("toc").style.display = 'none';
			//document.getElementById("rr").style.display = 'none';
			//document.getElementById("requestsample").style.display = 'block';
			//document.getElementById("requestsamplebtn").style.backgroundColor  = 'red';
			document.getElementById("chkdiscount").style.display = 'none';
			//document.getElementById("lot").style.display = 'none';
			//document.getElementById("lotbtn").style.backgroundColor = '#005aab';
			document.getElementById("descriptionbtn").style.backgroundColor = '#005aab';
			document.getElementById("tocbtn").style.backgroundColor = '#005aab';
			//document.getElementById("rrbtn").style.backgroundColor = '#005aab';
			document.getElementById("chkdiscountbtn").style.backgroundColor = '#005aab';
		} else if(val== 'chkdiscount'){
			document.getElementById("description").style.display = 'none';
			document.getElementById("toc").style.display = 'none';
			//document.getElementById("rr").style.display = 'none';
			//document.getElementById("requestsample").style.display = 'none';
			//document.getElementById("lot").style.display = 'none';
			document.getElementById("chkdiscount").style.display = 'block';
			document.getElementById("chkdiscountbtn").style.backgroundColor  = '#ff9900';
			//document.getElementById("lotbtn").style.backgroundColor = '#005aab';
			document.getElementById("descriptionbtn").style.backgroundColor = '#005aab';
			document.getElementById("tocbtn").style.backgroundColor = '#005aab';
			//document.getElementById("rrbtn").style.backgroundColor = '#005aab';
		//	document.getElementById("requestsamplebtn").style.backgroundColor = '#005aab';
		} else if(val == 'lot'){
			document.getElementById("description").style.display = 'none';
			document.getElementById("toc").style.display = 'none';
			//document.getElementById("rr").style.display = 'none';
			//document.getElementById("requestsample").style.display = 'none';
			//document.getElementById("lot").style.display = 'block';
			document.getElementById("chkdiscount").style.display = 'none';
			document.getElementById("chkdiscountbtn").style.backgroundColor  = '#005aab';
			//document.getElementById("lotbtn").style.backgroundColor = 'red';
			document.getElementById("descriptionbtn").style.backgroundColor = '#005aab';
			document.getElementById("tocbtn").style.backgroundColor = '#005aab';
			//document.getElementById("rrbtn").style.backgroundColor = '#005aab';
			//document.getElementById("requestsamplebtn").style.backgroundColor = '#005aab';
		
		
		
		}
		
	}
	function getMore()
		{
			
			document.getElementById("demo").style.display = 'none';
			document.getElementById("complete").style.display = 'block';
			
		}
</script>

<script type="text/javascript">
//Created / Generates the captcha function
  <!--$(document).ready(function(){-->
   $(document).ready(function(){

 var a = Math.ceil(Math.random() * 10) + '';
            var b = Math.ceil(Math.random() * 10) + '';
            var c = Math.ceil(Math.random() * 10) + '';
            var d = Math.ceil(Math.random() * 10) + '';
            var e = Math.ceil(Math.random() * 10) + '';
            var f = Math.ceil(Math.random() * 10) + '';
            var g = Math.ceil(Math.random() * 10) + '';
            var code = a + '' + b + '' + '' + c + '' + d + '' + e + '' + f + '' + g;
            document.getElementById("txtCaptcha").value = code
			 
 });
        function DrawCaptcha() {
            var a = Math.ceil(Math.random() * 10) + '';
            var b = Math.ceil(Math.random() * 10) + '';
            var c = Math.ceil(Math.random() * 10) + '';
            var d = Math.ceil(Math.random() * 10) + '';
            var e = Math.ceil(Math.random() * 10) + '';
            var f = Math.ceil(Math.random() * 10) + '';
            var g = Math.ceil(Math.random() * 10) + '';
            var code = a + '' + b + '' + '' + c + '' + d + '' + e + '' + f + '' + g;
            document.getElementById("txtCaptcha").value = code
        }

        // Validate the Entered input aganist the generated security code function
        function ValidCaptcha() {
            var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
            var str2 = removeSpaces(document.getElementById('txtInput').value);
			var error_text;			
            if(str2.length==0)
			{
				error_text = document.getElementById('lblError');
                error_text.textContent = "Enter captcha value.";
				return false;
			}
			
			else if (str1 == str2) {
				error_text = document.getElementById('lblError');
                error_text.textContent = "";
				//alert('ok');
                return true;
            }
			
            else {

                error_text = document.getElementById('lblError');
                error_text.textContent = "Wrong captcha value.";
                return false;
            }
        }

        // Remove the spaces from the entered and generated code
        function removeSpaces(string) {
            return string.split(' ').join('');
        }
<!--    });-->
 function LoginControl() {
        var isValidated = $('#contactform').validationEngine('validate');
		var isval = false;
	if(isValidated == false)
	{
		isval = false;
	}
	else
	{
		isval = true;
	}
		var password = false;var password07 = false;
		var fstr = document.getElementById('ppass').value;
		var sstr = document.getElementById('cpass').value;
         if (fstr == sstr) {
		  
                    document.getElementById("lblpassword").innerHTML = '';
                    password = true;
          } else {
		  
					document.getElementById("lblpassword").innerHTML = 'Password does not match';
                    password = false;
                   
        }
		var minMaxLength = /^[\s\S]{8,32}$/,
        upper = /[A-Z]/,
        lower = /[a-z]/,
        number = /[0-9]/,
        special = /[ !"#$%&'()*+,\-./:;<=>?@[\\\]^_`{|}~]/;

		if (minMaxLength.test(fstr) &&
			upper.test(fstr) &&
			lower.test(fstr) &&
			number.test(fstr) &&
			special.test(fstr)
		) {
		    
			document.getElementById("lblpasswordsp").innerHTML = '';
            password07 = true;
		}else{
		 
		   document.getElementById("lblpasswordsp").innerHTML = 'Input Password and Submit [Minimum 8 characters which contain At least one upper case english letter, At least one lower case english letter, At least one digit, At least one special character,]';
         password07 = false;
		}
		if (isval == true && password == true) {
			if (ValidCaptcha()) 
			{
			  return true;
			} else {
			
			  return false;
			}
		} else {
		  return false;
		}
}
$( document ).ready(function() {
	$("#contactform").validationEngine({
		'custom_error_messages': {
					// Custom Error Messages for Validation Types
					'custom[integer]': {
						'message': "Not a valid Number"
					}
		}
	});

});
	function CheckCaptcha(){
	 
	if($("#txtCaptcha").val() == $("#txtInput").val()){
		return true;
	}else{
		alert("Please enter valid captcha");
		$("#txtInput").val("");
		return false;
	}	
	
	}
	
 
</script>
