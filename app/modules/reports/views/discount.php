<head> 

  <title>Check Discount: <?php echo $reports[0]->report_name; ?></title>
</head> 
<header class="main-header">

  <div class="container">
    <h1 class="page-title">Check Discount</h1>
  </div>
</header>

<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<form id="__vtigerWebForm" name="leads" action="<?php echo base_url();?>reports/report/check_discount" method="post" accept-charset="utf-8" enctype="multipart/form-data">

  <input type="hidden" name="__vtrftk" value="sid:97dfc85d7ba5194442b2d2fe8719c0db0e3f74eb,1547112527">
  <input type="hidden" name="publicid" value="6a8e52752db6d6c57c25b6a3312f427f">
  <input type="hidden" name="urlencodeenable" value="1">
  <input type="hidden" name="name" value="leads">
  <input type="hidden" name="id" value="<?php echo $reports[0]->id;?>" >
  <input type="hidden" name="get_report_id_from_url" value="<?php echo $get_report_id_from_url;?>" >
  <input type="hidden" name="page_urls" value="<?php echo $reports[0]->page_urls;?>" >
  <input type="hidden" name="report_code" value="<?php echo $reports[0]->report_code;?>">
  <input type="hidden" name="publisher_id" value="<?php echo $reports[0]->publisher_id;?>">
  <input type="hidden" name="cf_860" id="cf_860"  data-label="" value='<?php echo $reports[0]->report_name;?>'>
  <!--Request Type-->
  <input type="hidden" name="cf_880" data-label="" value=""></td></tr>
  <!--Publisher Name only defined here -->
  <input type="hidden" name="cf_874" data-label="" value="<?php echo $publisher_name[0]->publisher_name;?>"> 
  <input type="hidden" value="<?php echo $reports[0]->id;?>" name="id">
  <!--Date  (yyyy-mm-dd) -->
  <input type="hidden" name="cf_882" data-label="" value="<?php date_default_timezone_set("Asia/Calcutta");  echo date('Y-m-d');?>">
  <!--Time-->
  <input type="hidden" name="cf_884" data-label="" value="<?php date_default_timezone_set("Asia/Calcutta");  echo date('H:i:s');?>">
<input type="hidden" name="request_type" data-label="" value="Discount Request">
  <div class="content container">
   <div class="row">
    <div class="col-md-12">
     <div class="panel panel-default Purchasebox">
      <div class="panel-heading"><h4><strong>Report Name</strong> :</span> <a href="<?php echo base_url(); ?>reports/index/<?php echo $reports[0]->page_urls; ?>"><?php echo $reports[0]->report_name;?></a></h4></div>
      <div class="panel-body">

       <!--<form id = "guest_form" method = "post" action = "<?php //base_url();?>reports/report/saveGuest">-->

        <div class="row">

         <div class="col-md-4">
          <div class="form-group">
           <label for="InputFullName">Full Name <span class="redstar">*</span></label>
           <input type="text" id="full_name" name="cf_864" data-label="" value="" required="" placeholder="Your Name" pattern="^[a-zA-Z ]+$" class="form-control">
         </div>
         <div class="form-group" >
                           <label for="InputPhoneno" style="margin-right:56%;">Phone No.  <span class="redstar">*</span></label>
                           <input type="text" name="country_code" class="form-control" style="width:23%;display:inline;" value="+1" id="country_code1"  readonly />
                            <input type="text" id="phone" name="cf_868" placeholder="Your Phone Number" maxlength="100" required pattern="^[0-9 ]+$" class="form-control" style="width:75%;display:inline;"/>
                           
                        </div>
         <div class="form-group">
           <label for="Inputaddress">Company Name <span class="redstar">*</span></label>
           <input type="text" id="company_name" name="company" data-label="" value="" placeholder="Your Company Name" class="form-control">
         </div>
       </div>

       <div class="col-md-4">
        <div class="form-group">
         <label for="Inputaddress">Email <span class="redstar">*</span></label>
         <input type="email" id="email" name="email" data-label="" value="" required="" placeholder="Your Email Address" pattern="^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+$" class="form-control">
       </div>

       <div class="form-group">
         <label for="Inputaddress">Job Title <span class="redstar">*</span></label>
         <input type="text" id="designation" name="cf_870" data-label="" value="" placeholder="Job Title" maxlength="100"  class="form-control">
       </div>
         <div class="form-group">
      <label for="Inputaddress">Query</label>
      <textarea class="form-control" name="cf_876" placeholder="Your Message"></textarea>
    </div>
       
    </div>
    <div class="col-md-4">
       <div class="form-group">
                           <label for="Inputaddress">Country <span class="redstar">*</span></label>
                            <select name = "cf_858" class="form-control" id="country">
                               <option value ="">Select Country</option>
                                <?php 
                                for($c = 0; $c<count($countries); $c++)
                                { 
                                    ?>
                                    <option value = "<?php echo $countries[$c]->country_name;?>"><?php echo $countries[$c]->country_name;?></option>
                                    <?php
                                }
                                ?>
                            </select>
                           
                            
                        </div>
   
    <div class="form-group">
      <input type="text" readonly id="txtCaptcha"
      style="background-image:url(<?php echo base_url(); ?>themes/frontend/images/captcha.JPG);color: green;font-weight: 1000;text-align: center;font-size: 20px;font-weight: bold;height: 50px;" class="form-group" />


      <input class="form-group" type="button" id="btnrefresh" class="pull-right" value="" onClick="DrawCaptcha();" style="background-image:url(<?php echo base_url(); ?>themes/frontend/images/refresh.jpg);width: 33px;" />
      <input style="width: 78%;" type="text" id="txtInput" placeholder="Captcha text" required class="form-group"/> 
      <label id="lblError" style="color:red;"></label>
    </div>
    <div class="form-group">
      <!--<input type="submit" value="Submit Request" class="btn btn-md btn-primary" style="cursor: pointer;" onclick="return CheckCaptcha()">-->
      <button class="btn btn-md btn-primary" style="cursor: pointer;" onclick="return CheckCaptcha()">Submit Request</button>
    </div>

  </div>

</div>
</div>
</form>
</div>
</div>
</div>        
</div>
</div>


<script type="text/javascript">
//Created / Generates the captcha function
<!--$(document).ready(function(){-->
  DrawCaptcha();
  $(document).ready(function(){

   var a = Math.ceil(Math.random() * 10) + '';
   var b = Math.ceil(Math.random() * 10) + '';
   var c = Math.ceil(Math.random() * 10) + '';
   var d = Math.ceil(Math.random() * 10) + '';
   var e = Math.ceil(Math.random() * 10) + '';
   var f = Math.ceil(Math.random() * 10) + '';
   var g = Math.ceil(Math.random() * 10) + '';
   var code = a + '' + b + '' + '' + c + '' + d + '' + e + '' + f + '' + g;
   document.getElementById("txtCaptcha").value = code

 });
  function DrawCaptcha() {
    var a = Math.ceil(Math.random() * 10) + '';
    var b = Math.ceil(Math.random() * 10) + '';
    var c = Math.ceil(Math.random() * 10) + '';
    var d = Math.ceil(Math.random() * 10) + '';
    var e = Math.ceil(Math.random() * 10) + '';
    var f = Math.ceil(Math.random() * 10) + '';
    var g = Math.ceil(Math.random() * 10) + '';
    var code = a + '' + b + '' + '' + c + '' + d + '' + e + '' + f + '' + g;
    document.getElementById("txtCaptcha").value = code
  }

        // Validate the Entered input aganist the generated security code function
        function ValidCaptcha() {
          var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
          var str2 = removeSpaces(document.getElementById('txtInput').value);
          var error_text;			
          if(str2.length==0)
          {
            error_text = document.getElementById('lblError');
            error_text.textContent = "Enter captcha value.";
            return false;
          }

          else if (str1 == str2) {
            error_text = document.getElementById('lblError');
            error_text.textContent = "";
				//alert('ok');
        return true;
      }

      else {

        error_text = document.getElementById('lblError');
        error_text.textContent = "Wrong captcha value.";
        return false;
      }
    }

        // Remove the spaces from the entered and generated code
        function removeSpaces(string) {
          return string.split(' ').join('');
        }
        <!--    });-->
function LoginControl() {
  var isValidated = $('#__vtigerWebForm').validationEngine('validate');
  var isval = false;
  if(isValidated == false)
  {
    isval = false;
  }
  else
  {
    isval = true;
  }
  var password = false;var password07 = false;
  var fstr = document.getElementById('ppass').value;
  var sstr = document.getElementById('cpass').value;
  if (fstr == sstr) {

    document.getElementById("lblpassword").innerHTML = '';
    password = true;
  } else {

   document.getElementById("lblpassword").innerHTML = 'Password does not match';
   password = false;

 }
 var minMaxLength = /^[\s\S]{8,32}$/,
 upper = /[A-Z]/,
 lower = /[a-z]/,
 number = /[0-9]/,
 special = /[ !"#$%&'()*+,\-./:;<=>?@[\\\]^_`{|}~]/;

 if (minMaxLength.test(fstr) &&
   upper.test(fstr) &&
   lower.test(fstr) &&
   number.test(fstr) &&
   special.test(fstr)
   ) {

   document.getElementById("lblpasswordsp").innerHTML = '';
 password07 = true;
}else{

 document.getElementById("lblpasswordsp").innerHTML = 'Input Password and Submit [Minimum 8 characters which contain At least one upper case english letter, At least one lower case english letter, At least one digit, At least one special character,]';
 password07 = false;
}
if (isval == true && password == true) {
 if (ValidCaptcha()) 
 {
   return true;
 } else {

   return false;
 }
} else {
  return false;
}
}
$( document ).ready(function() {
	$("#__vtigerWebForm").validationEngine({
		'custom_error_messages': {
					// Custom Error Messages for Validation Types
					'custom[integer]': {
						'message': "Not a valid Number"
					}
        }
      });

});
function CheckCaptcha(){
  if($("#full_name").val() == '' || $("#email").val() == ''|| $("#phone").val() == ''|| $("#designation").val()== '' || $("#company_name").val()== ''|| $("#country").val()== '')
  { 
    alert("Please fill all details");
    return false;
  }

	if($("#txtCaptcha").val() == $("#txtInput").val()){
		return true;
	}else{
		alert("Please enter valid captcha");
		$("#txtInput").val("");
		return false;
	}	
	
}


</script>
<script  type="text/javascript">

  window.onload = function() 
  { 
    var N=navigator.appName, ua=navigator.userAgent, tem;
    var M=ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
    if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];M=M? [M[1], M[2]]: [N, navigator.appVersion, "-?"];
    var browserName = M[0];
    var form = document.getElementById("__vtigerWebForm"), inputs = form.elements; 
    form.onsubmit = function() 
    { 
      var required = [], att, val; 
      for (var i = 0; i < inputs.length; i++)
      {
        att = inputs[i].getAttribute("required"); 
        val = inputs[i].value; 
        type = inputs[i].type; 
        if(type == "email") 
        {
          if(val != "") {
            var elemLabel = inputs[i].getAttribute("label");
            var emailFilter = /^[_/a-zA-Z0-9]+([!"#$%&()*+,./:;<=>?\^_`{|}~-]?[a-zA-Z0-9/_/-])*@[a-zA-Z0-9]+([\_\-\.]?[a-zA-Z0-9]+)*\.([\-\_]?[a-zA-Z0-9])+(\.?[a-zA-Z0-9]+)?$/;var illegalChars= /[\(\)\<\>\,\;\:\"\[\]]/ ;
            if (!emailFilter.test(val))
            {
              alert("For "+ elemLabel +" field please enter valid email address");
              return false;
            }
            else if (val.match(illegalChars))
            {
              alert(elemLabel +" field contains illegal characters");
              return false;
            }
          }
        }
        if (att != null)
        { 
          if (val.replace(/^\s+|\s+$/g, "") == "")
          {
            required.push(inputs[i].getAttribute("label")); 
          } 
        } 
      } 
      if (required.length > 0)
      { 
        alert("The following fields are required: " + required.join()); 
        return false; 
      }
      var numberTypeInputs = document.querySelectorAll("input[type=number]");
      for (var i = 0; i < numberTypeInputs.length; i++) 
      {
       val = numberTypeInputs[i].value;var elemLabel = numberTypeInputs[i].getAttribute("label");
       var elemDataType = numberTypeInputs[i].getAttribute("datatype");
       if(val != "") 
       {
        if(elemDataType == "double") 
        {
          var numRegex = /^[+-]?\d+(\.\d+)?$/;
        }
        else
        {
          var numRegex = /^[+-]?\d+$/;
        }
        if (!numRegex.test(val)) 
        {
          alert("For "+ elemLabel +" field please enter valid number"); 
          return false;
        }
      }
    }
    var dateTypeInputs = document.querySelectorAll("input[type=date]");
    for (var i = 0; i < dateTypeInputs.length; i++)
    {
      dateVal = dateTypeInputs[i].value;var elemLabel = dateTypeInputs[i].getAttribute("label");
      if(dateVal != "")
      {
        var dateRegex = /^[1-9][0-9]{3}-(0[1-9]|1[0-2]|[1-9]{1})-(0[1-9]|[1-2][0-9]|3[0-1]|[1-9]{1})$/;
        if(!dateRegex.test(dateVal)) 
        {
          alert("For "+ elemLabel +" field please enter valid date in required format"); return false;
        }
      }
    }
    var inputElems = document.getElementsByTagName("input");
    var totalFileSize = 0;
    for(var i = 0; i < inputElems.length; i++) 
    {
      if(inputElems[i].type.toLowerCase() === "file") 
      {
        var file = inputElems[i].files[0];
        if(typeof file !== "undefined")
        {
          var totalFileSize = totalFileSize + file.size;
        }
      }
    }
    if(totalFileSize > 52428800) 
    {
      alert("Maximum allowed file size including all files is 50MB.");
      return false;
    }
  }; 
}
</script>
<script >
  
  $( "#country" ).change(function() {
  var country = $("#country").val()  
  // alert(country);
  // if(country !='')
  // {
  // $('#country_code').val(country);
  // }
 $.ajax({      
   type: "POST",                                    
   url: "<?php echo base_url().'reports/report/getCountryNameRelatedData'?>",

   data: {
     country_name:country
     

   }, 
    
   success: function(data){
    
    // alert(data);
    var obj = JSON.parse(data);
    // alert(obj);
    //var split = json_decode(json_encode(data), True);
    //
    // alert(JSON.stringify(split));
     // var split = data.split("<div class='clearfix'></div>"); 
     // alert(split);
     // alert(obj[0].country_code);
     $("#country_code1").val('+' +obj[0].country_code);
     // $("#pagination").html(split[1]); 
   }
 }); 

    });

   // }

</script>