  <?php 
  
  echo index_page();
  if(empty($reports))
    {
    	redirect(base_url());
    }
  function removeslashes($string)
			{
			    $string1=implode("",explode("\\n",$string));
				$string2=implode("",explode("\\",$string1));
				$string3=implode("",explode("\\",$string2));
				$string4=implode("",explode("<table>",$string3));
			     $string5=implode("",explode("</table>",$string4));
			    return stripslashes(trim($string5));
			}
			
    $result=$this->db->query("select * from users where id='".$reports[0]->publisher_id."'");
         	$row = $result->result();
			 $publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));
					  $category_name =  str_replace(" ","-",strtolower($reports[0]->category_name));
    ?><head>
<meta name="keywords" content="<?php echo $reports[0]->metatitle;?>" />
<meta name="Description" content="<?php echo str_replace("&nbsp;"," ",stripslashes(strip_tags($reports[0]->report_summery)));?>" />
<meta name = "title" content="<?php if ('' == $reports[0]->meta_title){echo $reports[0]->title;}else{ echo $reports[0]->meta_title;}?>"/>
<title><?php if ('NULL' == $reports[0]->page_title || '' == $reports[0]->page_title){echo $reports[0]->meta_title;}else{ echo $reports[0]->page_title;}?></title>
</head> 

 <header class="main-header">
    <div class="container">
        <h1 class="page-title">Report Details</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li><a href="<?php echo base_url();?>market-reports/<?php echo $category_name.'.html';?>"><?php echo $reports[0]->category_name;?></a></li>
			
			<?php	  
			/*print_r($sub_category); die();
				*/
				for ($j = 0; $j< sizeof($sub_category); $j++)
					{
					  $sub_category_name =  str_replace(" ","-",strtolower($sub_category[$j]->sub_category_name));
						if($sub_category[$j]->id == $reports[0]->sub_category_id)
							{
						?>
                            <li><a href="<?php echo base_url();?>market-reports/<?php echo $category_name.'/'.$sub_category_name.'.html' ;?>"><?php echo $sub_category[$j]->sub_category_name?></a></li>
					<?php } 
					}
			?>
			<!--<li><a href="<?php echo base_url(); ?>latest-market-research-reports.html">Reports</a></li>-->
            <li class="active">Report Details</li>
        </ol>
    </div>
</header>


<div class="container">
    <div class="row">
    
        <div class="col-md-8">
            <h3 class="page-header no-margin-top"><?php echo $reports[0]->report_name; ?></h3>
            <div class="row report_detailsBox1">
            <div class="col-md-2 col-sm-5">
            <div class="report_detailsBox1_img">
              <img src="<?php echo base_url();?>uploads/category_report_image/<?php echo $reports[0]->category_id.".jpg"; ?>" class="img-post img-responsive" alt="<?php echo $reports[0]->report_name;?>" >
            </div>
            </div>
            <div class="col-md-10 col-sm-7">
            
            <table class="table table-bordered">
            <tr>
                 <td><i class="fa fa-user"></i></span> <span>Published by <a href="<?php echo base_url();?>publisher/<?php echo $publisher_name.'.html';?>"><?php echo $row[0]->display_name;?></a></td>
                <td><i class="fa fa-clock-o"></i></span> <span> <?php echo date("M d Y", strtotime($reports[0]->report_date));?></td>
            </tr>
            <tr>
              <td><i class="fa fa-user"></i></span> <span>Category <a href="<?php echo base_url();?>market-reports/<?php echo $category_name.'.html';?>"><?php echo $reports[0]->category_name;?></a> </td>
               <td><i class="fa fa-file"></i></span> <span>
            Total Pages: <?php echo $reports[0]->no_pages; ?></td>
            </tr>
            </table>
                    
            
            
            </div>
             <input type="hidden" class="price1" readonly value=" <?php echo $reports[0]->report_price; ?>" name="price" id="price">
            </div>
            <ul class="nav nav-pills nav-justified ar-nav-pills center-block margin-bottom reportDetailsTabs">
            <li class="active"><a href="#windows" data-toggle="tab" aria-expanded="true"><i class="fa fa-list"></i> Report Description</a></li>
            <li class=""><a href="#mac" data-toggle="tab" aria-expanded="false"><i class="fa fa-table"></i> Table of Content</a></li>
            <li class=""><a href="#linux" data-toggle="tab" aria-expanded="false"><i class="fa fa-file"></i> Related Reports</a></li>
            </ul>
            <div class="tab-content margin-top">
            <div class="tab-pane active" id="windows">
             <div id = "demo">
              <p  ><?php echo substr(removeslashes($reports[0]->report_description),0,500);?>&nbsp;&nbsp;&nbsp;
                <?php if(strlen($reports[0]->report_description) > 500){?>
                <a onclick = "getMore();" style="font-weight:700; cursor:pointer;">Read more. . .</a>
                <?php }?>
              </p>
            </div>
            <div  style = "text-align: justify; display:none;" id = "complete" > 
              <p ><?php echo str_replace('<li>','<li style = "list-style: initial;float: initial;">',removeslashes($reports[0]->report_description));?></p>
            </div>
            </div>
            
            <div class="tab-pane" id="mac">
            
                <div  style = "text-align: justify; height:635px; overflow-y:scroll;">
                    <p><?php echo removeslashes($reports[0]->report_content);?></p>
                    <strong>List Of Tables:</strong>
                    <p><?php 
                    $a = str_replace("<h1>","<h2>",$reports[0]->list_of_tables);
                    $b = str_replace("</h1>","</h2>",$a);
                    echo removeslashes($b); ?></p>
                </div>
       
         </div>
         <div class="tab-pane" id="linux">
              <input type="hidden" name="related_keyword" id="related_keyword" value="<?php echo $reports[0]->search_keywords; ?>">
            
               <div id="container">
                <div class="data"></div>

                <div class="pagination"></div>
              </div>
             </div>
            </div>
       </div>

        <div class="col-md-4">
                 <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">  Choose License Type:</h3>
                    </div>
                    <div class="panel-body text-center">
                      <form role="form" class="">
                           
                           <div class="form-group">
                              <label for="Choose_License_Type" class="sr-only">Choose License Type</label>
                              
								<select name="price_type" id="price_type" onchange="pricetype(this.value)" class="form-control text-center">
								  <option value="single"><strong>Single User (USD <?php echo $reports[0]->report_price; ?>)</strong></option>
								  <option value="multiple"><strong>Multiple User (USD <?php echo $reports[0]->multiple_price; ?>)</strong></option>
								  <option value="global"><strong>Global User (USD <?php echo $reports[0]->global_price; ?>)</strong></option>
								  <option value="corporate"><strong>Corporate User (USD <?php echo $reports[0]->corporate_price; ?>)</strong></option>
								</select>
                           </div>
                           <a class="btn btn-lg btn-ar btn-block btn-primary" style="background:#ff7305;border-color:#ff7305;" onclick="submit(<?php echo $reports[0]->id?>);"><i class="fa fa-shopping-cart"></i> &nbsp; Buy Now</a>
                        </form>
                    </div>
                </div>
                 <div class="panel panel-default">
                   <div class="panel-body">
                        <a href = "<?php echo base_url();?>contacts/request-sample/<?php echo $reports[0]->id; ?>" class="btn btn-md btn-block btn-primary" type="submit"><i class="fa fa-file"></i> &nbsp; Request a Sample </a>
                        <a href = "<?php echo base_url();?>contacts/enquiry-before-buying/<?php echo $reports[0]->id; ?>" class="btn btn-md btn-block btn-primary" type="submit"><i class="fa fa-envelope"></i> &nbsp; Enquire Before Buying </a>
                        <a href = "<?php echo base_url();?>contacts/discount/<?php echo $reports[0]->id; ?>" class="btn btn-md btn-block btn-primary checkdiscountbtn" type="submit"><i class="fa fa-check-square-o"></i> &nbsp; Check Discount </a>

                       <?php $allow = array("103.93.194.90","123.201.125.186");
                    
                      if (in_array ($_SERVER['REMOTE_ADDR'], $allow)) {
                      
                        ?>
                        <a style="background:#ff7305;border-color:#ff7305;" href = "<?php echo base_url();?>reports/generateTemplate1/<?php echo $reports[0]->id; ?>" class="btn btn-md btn-block btn-primary " type="submit"><i class="fa fa-file-word-o"></i> &nbsp; Generate Template 1</a>

                        <a style="background:#ff7305;border-color:#ff7305;" href = "<?php echo base_url();?>reports/generateTemplate2/<?php echo $reports[0]->id; ?>" class="btn btn-md btn-block btn-primary " type="submit"><i class="fa fa-file-word-o"></i> &nbsp; Generate Template 2</a>

                         <a target="_blank" style="background:#ff7305;border-color:#ff7305;" href = "<?php echo base_url();?>reports/report_pdf/<?php echo $reports[0]->id; ?>" class="btn btn-md btn-block btn-primary " type="submit"><i class="fa fa-file-pdf-o"></i> &nbsp; Generate PDF</a>

                      <?php } ?>
                    </div>
                </div>
				<?php /* ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Make An Enquiry:</h3>
                    </div>
                    <div class="panel-body">
                      <form role="form" id="contactform" action="<?php echo base_url();?>reports/send_enquiry1" method="post">
                           <div class="form-group">
                            <label class="sr-only" for="name">Your Name</label>
                            <input class="form-control" id="name" placeholder="Your Name" pattern="^[a-zA-Z ]+$" required  placeholder="Your Name" type="text" name="name">
                          </div>
                            <div class="form-group">
                            <label class="sr-only" for="Companyname">Your Company Name</label>
                            <input class="form-control" id="Companyname" placeholder="Your Company Name" type="text" name="company_name" required>
                          </div>
                           <div class="form-group">
                              <label for="country" class="sr-only">Select Country</label>
                              <select class="form-control" id="country" name="country">
                                     <?php 
									for($c = 0; $c<count($countries); $c++)
									{ 
										?>
										  <option value = "<?php echo $countries[$c]->country_name;?>"><?php echo $countries[$c]->country_name;?></option>
										  <?php
									}
									?>
                              </select>
                           </div>
                           <div class="form-group">
                            <label class="sr-only" for="emailid">Your Email Address</label>
							<input type="email" id="email" class="form-control" name="email" placeholder="Your Email Address" required pattern="^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+$"/>
                             
                          </div>
                          <div class="form-group">
                            <label class="sr-only" for="phone">Your Phone No.</label>
							<input type="text" id="email" class="form-control" name="phone" placeholder="Your Phone Number" required pattern="^[0-9 ]+$"/>
                             
                          </div>
                          <div class="form-group">
                            <label class="sr-only" for="jobtitle">Job Title</label>
							<input type="text" id="email" class="form-control" name="designation" placeholder="Job Title" required />
                            
                          </div>
                          <div class="form-group">
                             
							<textarea id="message" class="form-control" rows="5" placeholder="Your Message" name="message" ></textarea>
                          </div>
                          <div class = "form-group">
							<input class="form-control" type="text" readonly id="txtCaptcha"
                                   style="background-image:url(<?php echo base_url(); ?>themes/frontend/images/captcha.JPG);color: green;font-weight: 1000;font-size: 26px;text-align: center;" />
						  </div>
						  <div class = "form-group">
						  <input type="text" class="form-control" id="txtInput" placeholder="Captcha text" required /> 
						  <input type="button" id="btnrefresh"  value="" onClick="DrawCaptcha();" class="form-control" style="background-image:url(<?php echo base_url(); ?>themes/frontend/images/refresh.jpg);background-repeat:no-repeat; background-color:#ffffff;width: 10%;height: 31px;margin: auto;margin-top: 4%;" />
                            <label id="lblError" style="color:red;"></label>
						  </div>
						  <input type="hidden" value="<?php echo $reports[0]->id;?>" name="id">
                <input type="hidden" value="<?php echo $reports[0]->page_urls;?>" name="page_urls">
                <input type="hidden" name="report_code" value="<?php echo $reports[0]->report_code;?>">
                <input type="hidden" name="publisher_id" value="<?php echo $reports[0]->publisher_id;?>">
                           <button class="btn btn-md btn-primary"  value="Send" id="send" onclick="return CheckCaptcha()" type="submit"><i class="fa fa-send"></i> &nbsp; Send</button>
                        </form>
                    </div>
                </div>
               <?php */ ?>
			   <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Featured Research Reports</h3>
                    </div>
                    <div class="panel-body">
                     <ul class="media-list">
					 <?php
        /* count($featured) */
				for($i=0; $i < 4;$i++)
				{
					 
				?>
                                <li class="media">
                                    <a class="pull-left" href="<?php echo base_url();?>reports/index/<?php echo $featured[$i]->page_urls; ?>"><img alt = "<?php echo $featured[$i]->report_name; ?>" src="<?php echo base_url();?>uploads/category_report_image/<?php echo $featured[$i]->category_id.".jpg"; ?>"   title = "<?php echo $featured[$i]->report_name; ?>" class="media-object" width="80" height="100"></a>
                                    <div class="media-body">
                                        <p style="margin-top: 4%;" class="media-heading"><?php echo substr($featured[$i]->report_name,0,250); ?></p>
                                        <small><a href="<?php echo base_url();?>reports/index/<?php echo $featured[$i]->page_urls; ?>">Read more...</a></small>
                                    </div>
                                </li>
                   <?php }
        ?>                   
                            </ul>
                    </div>
                </div>
        </div>
        
    </div>
</div>
<form id="myform" action="<?php echo base_url();?>contact/purchase/<?php echo $reports[0]->id;?>" method="post">
  <?php 
	 $cart=$this->session->userdata('cart');
	 ?>
  <input type='hidden' name="report_id" id="report_id" value="<?php echo $reports[0]->id;?>">
  <input type='hidden' name="report_price" id="report_price" value="<?php echo $reports[0]->report_price;?>">
</form>
<script>
DrawCaptcha();
function pricetype(pricetype)
{
//alert(pricetype);
	if(pricetype=='single')
	{
	//alert(<?php echo $reports[0]->report_price;?>);
	document.getElementById("price").value='<?php echo $reports[0]->report_price;?>';
	document.getElementById("report_price").value='<?php echo $reports[0]->report_price;?>';
	
	}
	else if(pricetype=='multiple')
	{
	//alert(<?php echo $reports[0]->multiple_price;?>);
	document.getElementById("price").value='<?php echo $reports[0]->multiple_price;?>';
	document.getElementById("report_price").value='<?php echo $reports[0]->multiple_price;?>';
	}
	else if(pricetype=='global')
	{
	//alert(<?php echo $reports[0]->global_price;?>);
	document.getElementById("price").value='<?php echo $reports[0]->global_price;?>';
	document.getElementById("report_price").value='<?php echo $reports[0]->global_price;?>';
	}
	else if(pricetype=='corporate')
	{
	//alert(<?php echo $reports[0]->corporate_price;?>);
	document.getElementById("price").value='<?php echo $reports[0]->corporate_price;?>';
	document.getElementById("report_price").value='<?php echo $reports[0]->corporate_price;?>';
	}
}

function submit(value)
{
    var pricetype=$("#price_type").val();

    if(pricetype=='single'){
      var url="<?php echo base_url();?>contact/purchase-single-user/<?php echo $reports[0]->id;?>";
      }
    else if(pricetype=='multiple'){
      var url="<?php echo base_url();?>contact/purchase-multiple-user/<?php echo $reports[0]->id?>";
      
    }
    else if(pricetype=='global'){
      var url="<?php echo base_url();?>contact/purchase-global-user/<?php echo $reports[0]->id?>";
     
    }
    else if(pricetype=='corporate'){
      var url="<?php echo base_url();?>contact/purchase-corporate-user/<?php echo $reports[0]->id?>";
      
    }
    //alert(report_price);
    $.ajax({
        type: "POST",
        url: url,
        data: { 
            
          },
        success: function(result) {
          //alert(result);
            window.location.href =url;
            ///$.redirect(url);
        },
        error: function(result) {
            alert('error');
        }
    });
}
$( window ).load(function() {
 displayDiv('description');
});
	function displayDiv(val)
	{
		//alert(val);
		if(val == 'description'){
			document.getElementById("description").style.display = 'block';
			document.getElementById("toc").style.display = 'none';
			//document.getElementById("rr").style.display = 'none';
			//document.getElementById("requestsample").style.display = 'none';
			document.getElementById("chkdiscount").style.display = 'none';
			//document.getElementById("lot").style.display = 'none';
			
			document.getElementById("descriptionbtn").style.backgroundColor  = '#ff9900';
			document.getElementById("tocbtn").style.backgroundColor = '#005aab';
			//document.getElementById("rrbtn").style.backgroundColor = '#005aab';
			//document.getElementById("requestsamplebtn").style.backgroundColor = '#005aab';
			document.getElementById("chkdiscountbtn").style.backgroundColor = '#005aab';
			//document.getElementById("lotbtn").style.backgroundColor = '#005aab';
			
			
		} else if(val== 'toc'){
		//document.getElementById("lot").style.display = 'none';
			document.getElementById("description").style.display = 'none';
			document.getElementById("toc").style.display = 'block';
			document.getElementById("tocbtn").style.backgroundColor  = '#ff9900';
			//document.getElementById("rr").style.display = 'none';
			//document.getElementById("requestsample").style.display = 'none';
			document.getElementById("chkdiscount").style.display = 'none';
			//document.getElementById("lotbtn").style.backgroundColor = '#005aab';
			document.getElementById("descriptionbtn").style.backgroundColor = '#005aab';
			//document.getElementById("rrbtn").style.backgroundColor = '#005aab';
			//document.getElementById("requestsamplebtn").style.backgroundColor = '#005aab';
			document.getElementById("chkdiscountbtn").style.backgroundColor = '#005aab';
		} else if(val== 'rr'){
		document.getElementById("chkdiscountbtn").style.backgroundColor = '#005aab';
		//document.getElementById("lot").style.display = 'none';
			document.getElementById("description").style.display = 'none';
			document.getElementById("toc").style.display = 'none';
			//document.getElementById("rr").style.display = 'block';
			//document.getElementById("rrbtn").style.backgroundColor  = 'red';
			//document.getElementById("requestsample").style.display = 'none';
			document.getElementById("chkdiscount").style.display = 'none';
			//document.getElementById("lotbtn").style.backgroundColor = '#005aab';
			document.getElementById("descriptionbtn").style.backgroundColor = '#005aab';
			document.getElementById("tocbtn").style.backgroundColor = '#005aab';
			//document.getElementById("requestsamplebtn").style.backgroundColor = '#005aab';
			
		} else if(val== 'requestsample'){
			document.getElementById("description").style.display = 'none';
			document.getElementById("toc").style.display = 'none';
			//document.getElementById("rr").style.display = 'none';
			//document.getElementById("requestsample").style.display = 'block';
			//document.getElementById("requestsamplebtn").style.backgroundColor  = 'red';
			document.getElementById("chkdiscount").style.display = 'none';
			//document.getElementById("lot").style.display = 'none';
			//document.getElementById("lotbtn").style.backgroundColor = '#005aab';
			document.getElementById("descriptionbtn").style.backgroundColor = '#005aab';
			document.getElementById("tocbtn").style.backgroundColor = '#005aab';
			//document.getElementById("rrbtn").style.backgroundColor = '#005aab';
			document.getElementById("chkdiscountbtn").style.backgroundColor = '#005aab';
		} else if(val== 'chkdiscount'){
			document.getElementById("description").style.display = 'none';
			document.getElementById("toc").style.display = 'none';
			//document.getElementById("rr").style.display = 'none';
			//document.getElementById("requestsample").style.display = 'none';
			//document.getElementById("lot").style.display = 'none';
			document.getElementById("chkdiscount").style.display = 'block';
			document.getElementById("chkdiscountbtn").style.backgroundColor  = '#ff9900';
			//document.getElementById("lotbtn").style.backgroundColor = '#005aab';
			document.getElementById("descriptionbtn").style.backgroundColor = '#005aab';
			document.getElementById("tocbtn").style.backgroundColor = '#005aab';
			//document.getElementById("rrbtn").style.backgroundColor = '#005aab';
		//	document.getElementById("requestsamplebtn").style.backgroundColor = '#005aab';
		} else if(val == 'lot'){
			document.getElementById("description").style.display = 'none';
			document.getElementById("toc").style.display = 'none';
			//document.getElementById("rr").style.display = 'none';
			//document.getElementById("requestsample").style.display = 'none';
			//document.getElementById("lot").style.display = 'block';
			document.getElementById("chkdiscount").style.display = 'none';
			document.getElementById("chkdiscountbtn").style.backgroundColor  = '#005aab';
			//document.getElementById("lotbtn").style.backgroundColor = 'red';
			document.getElementById("descriptionbtn").style.backgroundColor = '#005aab';
			document.getElementById("tocbtn").style.backgroundColor = '#005aab';
			//document.getElementById("rrbtn").style.backgroundColor = '#005aab';
			//document.getElementById("requestsamplebtn").style.backgroundColor = '#005aab';
		
		
		
		}
		
	}
	function getMore()
		{
			
			document.getElementById("demo").style.display = 'none';
			document.getElementById("complete").style.display = 'block';
			
		}
 
//Created / Generates the captcha function
  <!--$(document).ready(function(){-->
   $(document).ready(function(){

 var a = Math.ceil(Math.random() * 10) + '';
            var b = Math.ceil(Math.random() * 10) + '';
            var c = Math.ceil(Math.random() * 10) + '';
            var d = Math.ceil(Math.random() * 10) + '';
            var e = Math.ceil(Math.random() * 10) + '';
            var f = Math.ceil(Math.random() * 10) + '';
            var g = Math.ceil(Math.random() * 10) + '';
            var code = a + '' + b + '' + '' + c + '' + d + '' + e + '' + f + '' + g;
            document.getElementById("txtCaptcha").value = code
			 
 });
        function DrawCaptcha() {
            var a = Math.ceil(Math.random() * 10) + '';
            var b = Math.ceil(Math.random() * 10) + '';
            var c = Math.ceil(Math.random() * 10) + '';
            var d = Math.ceil(Math.random() * 10) + '';
            var e = Math.ceil(Math.random() * 10) + '';
            var f = Math.ceil(Math.random() * 10) + '';
            var g = Math.ceil(Math.random() * 10) + '';
            var code = a + '' + b + '' + '' + c + '' + d + '' + e + '' + f + '' + g;
            document.getElementById("txtCaptcha").value = code
        }

        // Validate the Entered input aganist the generated security code function
        function ValidCaptcha() {
            var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
            var str2 = removeSpaces(document.getElementById('txtInput').value);
			var error_text;			
            if(str2.length==0)
			{
				error_text = document.getElementById('lblError');
                error_text.textContent = "Enter captcha value.";
				return false;
			}
			
			else if (str1 == str2) {
				error_text = document.getElementById('lblError');
                error_text.textContent = "";
				//alert('ok');
                return true;
            }
			
            else {

                error_text = document.getElementById('lblError');
                error_text.textContent = "Wrong captcha value.";
                return false;
            }
        }

        // Remove the spaces from the entered and generated code
        function removeSpaces(string) {
            return string.split(' ').join('');
        }
<!--    });-->
 function LoginControl() {
        var isValidated = $('#contactform').validationEngine('validate');
		var isval = false;
	if(isValidated == false)
	{
		isval = false;
	}
	else
	{
		isval = true;
	}
		var password = false;var password07 = false;
		var fstr = document.getElementById('ppass').value;
		var sstr = document.getElementById('cpass').value;
         if (fstr == sstr) {
		  
                    document.getElementById("lblpassword").innerHTML = '';
                    password = true;
          } else {
		  
					document.getElementById("lblpassword").innerHTML = 'Password does not match';
                    password = false;
                   
        }
		var minMaxLength = /^[\s\S]{8,32}$/,
        upper = /[A-Z]/,
        lower = /[a-z]/,
        number = /[0-9]/,
        special = /[ !"#$%&'()*+,\-./:;<=>?@[\\\]^_`{|}~]/;

		if (minMaxLength.test(fstr) &&
			upper.test(fstr) &&
			lower.test(fstr) &&
			number.test(fstr) &&
			special.test(fstr)
		) {
		    
			document.getElementById("lblpasswordsp").innerHTML = '';
            password07 = true;
		}else{
		 
		   document.getElementById("lblpasswordsp").innerHTML = 'Input Password and Submit [Minimum 8 characters which contain At least one upper case english letter, At least one lower case english letter, At least one digit, At least one special character,]';
         password07 = false;
		}
		if (isval == true && password == true) {
			if (ValidCaptcha()) 
			{
			  return true;
			} else {
			
			  return false;
			}
		} else {
		  return false;
		}
}
$( document ).ready(function() {
	$("#contactform").validationEngine({
		'custom_error_messages': {
					// Custom Error Messages for Validation Types
					'custom[integer]': {
						'message': "Not a valid Number"
					}
		}
	});
});
	function CheckCaptcha(){
	 
	if($("#txtCaptcha").val() == $("#txtInput").val()){
		return true;
	}else{
		alert("Please enter valid captcha");
		$("#txtInput").val("");
		return false;
	}	
	
	}
	
 
</script>
<script type="text/javascript">
 $( document ).ready(function() {
  loadData(1);
 });
 
 function loadData(page)
 {
    var related_keyword= $("#related_keyword").val();
    if(related_keyword !==""){     
      $.ajax({      
            type: "POST",                                    
            url: "<?php echo base_url().'reports/reports/getSearchKeywordRelatedData'?>",
            data: {
                  related_keyword:related_keyword,
                   page:page
                  
              },     
             success: function(data){ 
              $(".data").html(data);
            } 
      });   
    }  
 }
</script>
<style type="text/css">
#container .pagination ul li.inactive,
#container .pagination ul li.inactive:hover{
    background-color:#ededed;
    color:#bababa;
    border:1px solid #bababa;
    cursor: default;
}
#container .data ul li{
    list-style: none;
    font-family: verdana;
    margin: 5px 0 5px 0;
    color: #000;
    font-size: 12px;
}

#container .pagination{

    height: 25px;
    margin-top:20px;
    float: left;
    padding: 0;
margin: 0;
width: 100%;
clear: both;
                    //margin-left:0px !important;
}
#container .pagination input{

    height: 30px;
    margin-right: 8px;
                    //margin-left:0px !important;
}
#container .pagination ul{
    list-style: none;
    float: left;
    padding: 0;
margin: 0;
}
#container .pagination ul li{
    list-style: none;
    float: left;
    border: 1px solid #1cc3c9;
    padding: 2px 6px 2px 6px;
    margin: 0 3px 0 3px;
    font-family: arial;
    font-size: 12px;
    color: #999;
    font-weight: bold;
    background-color: #ffffff;
}
#container .pagination ul li:hover{
    color: #fff;
    background-color: #1cc3c9;
    cursor: pointer;
}
  .go_button
  {
  background-color:#ffffff;border:1px solid #fb0455;color:#cc0000;padding:2px 6px 2px 6px;cursor:pointer;position:absolute;margin-top:-1px;
  }
  .total
  {
   float:right;font-family:arial;color:#bababa; font-size: 12px;
   margin:5px 25px;
  }

</style>


     