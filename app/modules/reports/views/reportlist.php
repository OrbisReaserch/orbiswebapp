
<script src="<?php echo base_url();?>themes/frontend/js/ajax.googleapis.com.ajax.libs.jquery.1.4.2.jquery.min.js"></script>

  
  <script type="text/javascript">
function loadData(page)
    {
        
           
        var sql=document.getElementById("sql").value;
        var category_id=document.getElementById("category_id").value;
        
        $.post('<?php echo base_url()."reports/report/getCategorywise";?>',{sql:sql,page:page,catid:category_id},function(data)
	{
        	 
            $("#loading").css('display','none');
            $("#container").html(data);
        });
    }
$(document).ready(function(){
    loadData(1);  
});
</script>
<style type="text/css">

#container .pagination ul li.inactive,
#container .pagination ul li.inactive:hover{
    background-color:#ededed;
    color:#bababa;
    border:1px solid #bababa;
    cursor: default;
}
#container .data ul li{
    list-style: none;
    font-family: verdana;
    margin: 5px 0 5px 0;
    color: #000;
    font-size: 12px;
}

#container .pagination{

    height: 25px;
    margin-top:20px;
    float: left;
    padding: 0;
margin: 0;
width: 100%;
clear: both;
                    //margin-left:0px !important;
}
#container .pagination input{

    height: 30px;
    margin-right: 8px;
                    //margin-left:0px !important;
}
#container .pagination ul{
    list-style: none;
    float: left;
    padding: 0;
margin: 0;
}
#container .pagination ul li{
    list-style: none;
    float: left;
    border: 1px solid #1cc3c9;
    padding: 2px 6px 2px 6px;
    margin: 0 3px 0 3px;
    font-family: arial;
    font-size: 12px;
    color: #999;
    font-weight: bold;
    background-color: #ffffff;
}
#container .pagination ul li:hover{
    color: #fff;
    background-color: #1cc3c9;
    cursor: pointer;
}
            .go_button
            {
            background-color:#ffffff;border:1px solid #fb0455;color:#cc0000;padding:2px 6px 2px 6px;cursor:pointer;position:absolute;margin-top:-1px;
            }
            .total
            {
             float:right;font-family:arial;color:#bababa; font-size: 12px;
             margin:5px 25px;
            }

</style>
<?php 
	$resultp=$this->db->query("select * from category where id='".$category_id."'");
	$rowp = $resultp->result();
  
?>

<?php 
 $category_name_rss =  str_replace(" ","-",strtolower($rowp[0]->category_name));
?>
<form action = "<?php echo base_url();?>market-reports/<?php echo $category_name_rss; ?>/feed" id = "rssForm" method = "post">
<input type = "hidden" name = "cond" value = "category_id = ~<?php print_r($category_id);?>~"/>
</form>

  <header class="main-header">
    <div class="container">
        <h1 class="page-title">Latest <?php echo $rowp[0]->category_name; ?> Market Research Reports</h1>
		<a href = "#" onclick = "javascript:$('#rssForm').submit();"><img src = "https://cdn0.iconfinder.com/data/icons/yooicons_set01_socialbookmarks/512/social_rss_button_orange.png" height = "30" width = "30"/></a>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
			  <li><a href="<?php echo base_url(); ?>market-research-reports-categories.html">Categories</a></li>
            <li class="active"><?php echo $rowp[0]->category_name; ?></li>
        </ol>
    </div>
</header>

<div class="container">


    <div class="row"> 
        <div class="col-md-8">
		<div style="text-align: justify; margin-bottom: 2%; display: block; border: 1px solid #e5e5e5; padding: 5px; background: #f3f3f3;" id = "demo">
			<?php echo substr($rowp[0]->short_desc,0,300);  ?>
			<br>
			<?php if(strlen($rowp[0]->short_desc) > 300){ ?>
				<a onclick = "getMore();" style="font-weight:400; cursor:pointer;">Show more. . .</a>
			<?php } ?>
		  </div>
		  <div style="text-align: justify; margin-bottom: 2%; display: block; border: 1px solid #e5e5e5; padding: 5px; background: #f3f3f3;display:none;" id = "complete" >
			<?php echo $rowp[0]->short_desc;  ?>
			<br>
			<a onclick = "getLess();" style="font-weight:400; cursor:pointer;">Show less. . .</a>
		  </div>
     <div id="loading" >
		 <center>
			<img style = "height:100px;" src = "https://www.southeastairportshuttle.com.au/assets/img/ajax-loading-large.gif" />
		 <center>
	 </div>
            <div id="container">
                <div class="data"></div>
                <div class="pagination"></div>
            </div>
 
   </div> 
        <div class="col-md-4">
         
         <div class="panel panel-default">
          <div class="panel-heading">
           <h3 class="panel-title">Featured Reports</h3>
          </div>
          <div class="panel-body">
		  
		   <?php
        /* count($featured) */
				for($i=0; $i < count($featured);$i++)
				{
					 
				?>
                    <div class="media">
                    <a href="<?php echo base_url();?>reports/index/<?php echo $featured[$i]->page_urls; ?>" class="pull-left"><img width="70" height="70" alt="<?php echo $featured[$i]->report_name; ?>" src="<?php echo base_url();?>uploads/category_report_image/<?php echo $featured[$i]->category_id.".jpg"; ?>" class="media-object"></a>
                    <div class="media-body">
                    <p style="margin-top:5px;margin-bottom:0;"><?php echo substr($featured[$i]->report_name,0,60); ?></p>
                    <small><a href="<?php echo base_url();?>reports/index/<?php echo $featured[$i]->page_urls; ?>">Read more...</a></small>
                    </div>
                    </div>
				<?php }
        ?>           	
 
          </div>
       </div>
           
		 </div> 
    </div> 
</div>

<!---------------------------------- CategoryWise Report Starting---------------------------->
<?php $sql = "select id,report_name,category_id,publisher_id,report_date,report_price,page_urls from report where category_id = '$category_id' and status='1' order by id DESC"; ?>
  <input type="hidden" name="sql" id="sql" value="<?php echo $sql;?>">
  <input type="hidden" name="sql" id="category_id" value="<?php echo $category_id;?>">
 

<!---------------------------------- CategoryWise Report Ending---------------------------->
<script>
function getMore()
{
	
	document.getElementById("demo").style.display = 'none';
	document.getElementById("complete").style.display = 'block';
	
}
function getLess(){
	document.getElementById("demo").style.display = 'block';
	document.getElementById("complete").style.display = 'none';
}
</script>