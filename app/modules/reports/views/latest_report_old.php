<?php //print_r($reportlist);die();?>
<style type="text/css">

#container .pagination ul li.inactive,
#container .pagination ul li.inactive:hover{
    background-color:#ededed;
    color:#bababa;
    border:1px solid #bababa;
    cursor: default;
}
#container .data ul li{
    list-style: none;
    font-family: verdana;
    margin: 5px 0 5px 0;
    color: #000;
    font-size: 12px;
}

#container .pagination{

    height: 25px;
    margin-top:20px;
    float: left;
    padding: 0;
margin: 0;
width: 100%;
clear: both;
                    //margin-left:0px !important;
}
#container .pagination input{

    height: 30px;
    margin-right: 8px;
                    //margin-left:0px !important;
}
#container .pagination ul{
    list-style: none;
    float: left;
    padding: 0;
margin: 0;
}
#container .pagination ul li{
    list-style: none;
    float: left;
    border: 1px solid #1cc3c9;
    padding: 2px 6px 2px 6px;
    margin: 0 3px 0 3px;
    font-family: arial;
    font-size: 12px;
    color: #999;
    font-weight: bold;
    background-color: #ffffff;
}
#container .pagination ul li:hover{
    color: #fff;
    background-color: #1cc3c9;
    cursor: pointer;
}
            .go_button
            {
            background-color:#ffffff;border:1px solid #fb0455;color:#cc0000;padding:2px 6px 2px 6px;cursor:pointer;position:absolute;margin-top:-1px;
            }
            .total
            {
             float:right;font-family:arial;color:#bababa; font-size: 12px;
             margin:5px 25px;
            }

</style>
<div id="slider_wrapper2" style="background: white;text-align: left;">
  <div class="wrap">
    <h5 style="color: black;"><a href = "<?php echo base_url(); ?>">Home</a> > Latest Reports</h5>
  </div>
</div>
<div class="clear"></div>

<div class="content">
  <div class="wrap">
    <div class="one_full">
      
     
       <div id="loading"></div>
            <div id="container">
                <div class="data"></div>
                <div class="pagination"></div>
            </div>
      
      
    </div>
  </div>
</div>
<div class="clear"></div>
<div class="space1"></div>
<!--<script src="<?php echo theme_url();?>js/main.js"></script>-->

<script>
var js=$.noConflict();
// Code that uses other library's $ can follow here.
</script>

 
<!---------------------------------- Latest Report Starting---------------------------->
<?php $sql = "select id,report_name,category_id,publisher_id,report_image,report_date,report_price,page_urls,SUBSTRING(`report_description`, 1, 100) from report WHERE status='1' ORDER BY report_date DESC"; ?>
  <input type="hidden" name="sql" id="sql" value="<?php echo $sql;?>">
   <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>-->
   <script src="<?php echo theme_url();?>js/ajax.googleapis.com.ajax.libs.jquery.1.4.2.jquery.min.js"></script>
  <script type="text/javascript">
$(document).ready(function(){
    function loadData(page)
    {
       var sql=document.getElementById("sql").value;
				$.post('<?php echo base_url()."reports/report/getData";?>',{sql:sql,page:page},function(data)
			{
				// alert(data);
					$("#container").html(data);
				});
    }
    loadData(1);  // For first time page load default result
    $('#container .pagination li.active').live('click',function(){
	
        var page = $(this).attr('p');
        loadData(page);

    });           
    $('#go_btn').live('click',function(){
	//	alert("hi");
        var page = parseInt($('.goto').val());
        var no_of_pages = parseInt($('.total').attr('a'));
        if(page != 0 && page <= no_of_pages){
	
            loadData(page);
        }else{
            alert('Enter a PAGE between 1 and '+no_of_pages);
            $('.goto').val("").focus();
            return false;
        }

    });
});
</script>

<!---------------------------------- Latest Report Ending---------------------------->
