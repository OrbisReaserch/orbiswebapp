 <header class="main-header">
    <div class="container">
        <h1 class="page-title">Purchase</h1>
    </div>
</header>
 
 <div class="container">
  <div class="row ourClient">
            
             <div class="col-md-2 col-sm-3 text-center">
                  <h3 class="section-title">Our Clients</h3>
             </div>
            
               <div class="col-md-10 col-sm-9">
                <div class="bx-wrapper" style="max-width:100%;">
                     <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: auto;">
                        <ul id="bx5" class="bxslider" style="width: 4215%; position: relative; left: -1337.55px;">
<?php
			for($j=0;$j<count($clients);$j++)
			{
		  ?>                          
						  <li style="float: left; list-style: outside none none; position: relative; width: 100%; margin-right: 10px;"> <a class="thumbnail"> <img alt="Image" src="<?php echo base_url();?>uploads/<?php echo $clients[$j]->brand_logo;?>" style = "height:70px;"> </a> </li>
                           <?php }
        ?>
                        </ul>
                     </div>
                  </div>
               </div>
               
            </div>
   <div class="row">
   <div class="col-md-12">
       <div class="panel panel-default Purchasebox">
                <div class="panel-heading"><h4 style="color:#1062AC;">ORDER SUMMARY</h4></div>
                <div class="panel-body">
									<?php
				$user_id = $this->session->userdata('user_id');
				if($user_id == ''){
				 ?> 
						<form id = "guest_form" method = "post" action = "<?php base_url();?>reports/report/saveGuest">

                    <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="InputFullName">Full Name <span class="redstar">*</span></label>

              <input type="text" name = "full_name" id = "full_name" class="form-control" required />
            </div>
             <div class="form-group">
                  <label for="InputPhoneno">Phone No. <span class="redstar">*</span></label><br/>
                   <input type="text" name="country_code" class="form-control" style="width:23%;display:inline;" value="+1" id="country_code1"  readonly />
                  <input type="text" name="phone" placeholder="Your Phone Number" maxlength="100" required pattern="^[0-9 ]+$" class="form-control" style="width:75%;display:inline;" id = "phone" />
                </div>
            <div class="form-group">
              <label for="Inputaddress">Address <span class="redstar">*</span></label>

              <input type="text" name = "address" id = "address"  class="form-control"  required/>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="InputState">State <span class="redstar">*</span></label>
              <input type="text"  name = "state" id = "state"   class="form-control" required />
            </div>
            <div class="form-group">
              <label for="InputZip">Zip Code <span class="redstar">*</span></label>
              <input type="text" name = "zip" id = "zip"  class="form-control" required/>
            </div>
            <div class="form-group">
              <label for="Inputemail">Email <span class="redstar">*</span></label>
              <input type="text" name = "email" id = "email" pattern="^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+$" class="form-control"  required />
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="country">Country <span class="redstar">*</span> </label>

              <select class="form-control" name = "country" id = "country" required>
               <?php 
               ?>
               <option value="">Select Country </option>
              <?php 
              for($c = 0; $c<count($countries); $c++)
               {
                ?>
                
                <option value = "<?php echo $countries[$c]->country_name;?>"><?php echo $countries[$c]->country_name;?></option>
                <?php
              }
              ?>
            </select>
          </div>
           
            <div class="form-group">
              <label for="Inputcity">City <span class="redstar">*</span></label>
              <input type="text" name = "city" id = "city"  class="form-control"  required/>

            </div>
             <div class="form-group">
              <label for="InputJobtitle">Job Title <span class="redstar">*</span></label>
              <input type="text" name = "designation" id = "designation"  class="form-control"  required />
            </div>
        </div>
      </div>
                    <br>
                    </form>
					<?php }?>
                    <div class="row">
                      <div class="col-md-12">
                      <div class="purchaseDetails table-responsive">
                          <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th  width="70%">Report Title:</th>
                          <th width="15%">Price:</th>
                          <th width="15%">Action: </th>
                        </tr>
                      </thead>
                      <tbody>
                      
						   <?php 
          
            $total = 0;
            if(count($product)>0)
            {
               
                    $result_report = $this->db->query("select * from report where id='".$product."'");
                    $row_report = $result_report->result();
                    $total=$report_price;
                ?>  <tr>
                           <td valign="middle">
                           <table>
                               <tr>
                                  <td valign="top">
								  <div class="purchasereportimg">
								  <img src="<?php echo base_url();?>themes/frontend/images/report_default.jpg" width="70%">
								  </div>
								  </td>
                                   <td valign="top">
								   <div class="purchasereportName">
								   <a href="<?php echo base_url(); ?>reports/index/<?php echo $row_report[0]->page_urls; ?>"><b>
								   <?php echo $row_report[0]->report_name; ?> </b></a>
								   </div></td>
                               </tr>
							   
							   
                           </table>
                          </td>
                           <td valign="middle"><span><b>USD <?php echo $report_price;?>  </b></span></td>
                          <td valign="middle" ><a class="btn btn-danger" href = "<?php echo base_url();?>reports/report/remove_cart/<?php echo $product;?>"><i class="fa fa-times"></i>  Delete</a></td>
                        </tr>
<!----update button only orbis access--->
              <?php $allow = array("103.93.194.90","123.201.125.186");
                    // echo $_SERVER['REMOTE_ADDR'];
                  if (in_array ($_SERVER['REMOTE_ADDR'], $allow)) {
                      // exit();
                   ?>
             <tr>
              <form method='POST' action = "" id = "update_price">
             <td valign="middle">
               <table>
                 <tr>
                  <td valign="top">
                    <div class="purchasereportimg">
                      <img src="<?php echo base_url();?>themes/frontend/images/report_default.jpg" width="70%">
                    </div>
                  </td>
                  <td valign="top">
                   <div class="purchasereportName">
                     <a href="<?php echo base_url(); ?>reports/index/<?php echo $row_report[0]->page_urls; ?>">
                       <?php echo $row_report[0]->report_name; ?> </a>
                     </div></td>
                   </tr>
                   
                   
                 </table>
               </td>
               <td valign="middle">
                
                <input type="text" id= "update_report_price" name="update_report_price" value="<?php echo $report_price;?>"> </td>
               <td valign="middle" >
                <input type="hidden" id= "report_price_type" name="report_price_type" value="<?php  echo $price_type;?>">
               <input type="submit" class="btn btn-primary" onchange="submit_update(<?php echo $row_report[0]->id?>)" name="submit" id="submit" value="Update">
               
              
               </td>
              </form>
             </tr>
              <?php } ?>
             <!---- End update button only orbis access--->
                        

                        <tr>
						<td colspan="3">
                            <span class="pull-right"> <h5>Grand total:   <strong>USD <?php echo $report_price;?></strong></h5> </span>
                          </td>
                           
                        </tr>
                        <tr>
                           <td><b>Payment Procedures</b></td>
                          
                          <td colspan="2"><b>Please Select a Payment Options: </b></td>
                        </tr>
                         <tr>
                           <td valign="middle"><ul>

                          <!-- <li><p><b>PayPal: </b>This is another secured payment option, especially if you are an overseas client. PayPal accepts payments via all international cards. <b>(AMEX, VISA, Master Card, Discover etc)</b>.</p></li> -->

                          <!-- <li><p><b>PayPal: </b>This is our most secured, trusted, user friendly, universal online payment gateway. Most of our clients across the globe prefer making payments via PayPal. The best part is you do not need to create a new account whereas you can make a payment as a guest user. PayPal accepts payments via all international cards.<b> (AMEX, VISA, Master Card, Discover etc).</b></p></li>

                          <li><p><b>Bank/Wire Transfers:</b> Our clients have also been provided with the alternative of effecting direct money transfers into the bank account of Orbis Research. An invoice will be issued immediately with our bank details. </p></li>
                          
                          <li><p><b>Online:</b> You may pay online, especially when you make a direct purchase, Via Debit/Credit Card <b>(VISA, Master Card and Discover)</b>.</p></li> -->

                          <li><p><b>PayPal: </b>This is another secured payment option, especially if you are an overseas client. PayPal accepts payments via all international cards. <b>(AMEX, VISA, Master Card, Discover etc)</b>.</p></li>


                            <li><p><b>Razorpay:</b> We motivate our clients to pay via Razorpay which is one of our swift and most user friendly online payment method, especially when you make a direct purchase Via Debit/Credit Card <b>(VISA, Master Card, AMEX and Discover).</b></p></li>


                             <li><p><b>Bank/Wire Transfers:</b> Our clients have also been provided with the alternative of effecting direct money transfers into the bank account of Orbis Research. An invoice will be issued immediately with our bank details. </p></li>

                          

                              <li><p><b>Online:</b> This is our another online payment gateway where you can pay Via Debit/Credit Card <b>(VISA, Master Card and Discover).</b></p></li>

                        </ul> </td>
                          <td colspan="2">
                            <table>

                              <tr>
                     <td align="center" style="padding-top:10px;">

                    <a <?php if($user_id == ''){?>href="#"  onclick = "redirectUrl('paypal');"<?php } else{?>href="<?php echo base_url();?>reports/report/confirm_order/<?php echo $total;?>/<?php echo $this->session->userdata('user_id');?>"<?php }?>>

                     <img  alt="Click here" title="Click here" style="border: solid 1px grey;"  src="<?php echo base_url();?>themes/frontend/images/paypal.jpg" width="100" />

                   </a>
                    
                    </td>
                    <td align="center" style="padding-top:10px;">

                     <a href="#"  onclick = "redirectUrl('wire');" <?php if ($user_id != ''){echo 'style=display:none';}?>>

                       <img alt="Click here" title="Click here" style="border: solid 1px grey;margin-left: -43%;"  src="<?php echo base_url();?>themes/frontend/images/wiretransfer.gif"  width="100" />

                     </a>
                   </td>          
                  
                 </tr>
                 <tr>

                 
                   <td align="center" style="padding-top:20px;">
                     
                     <a href="#"  onclick = "redirectUrl('razorpaypg');" <?php if ($user_id != ''){echo 'style=display:none';}?>>
                       
                       <img alt="Click here" title="Click here" style="border: solid 1px grey;" src="<?php echo base_url();?>themes/frontend/images/razorpay_logo.jpg" id="rzor"   />
                       
                     </a>
                   </td>

                    
                   <td align="center" style="padding-top:20px;">
                     
                     <a href="#" onclick = "redirectUrl('hdfc');">
                      <img alt="Click here" title="Click here" style="border: solid 1px grey;margin-left: -43%;" src="<?php echo base_url();?>themes/frontend/images/hdfcpay.jpg" width="95" />
                    </a>
                     <!-- <a href="#"  onclick = "redirectUrl('hdfc_fss');" <?php if ($user_id != ''){echo 'style=display:none';}?>>
                       
                       <img alt="Click here" style="border: solid 1px grey;margin-left: -43%;" title="Click here" src="<?php echo base_url();?>themes/frontend/images/hdfc_pg.jpg"  width="95" height="60" />
                       
                     </a> -->
                   </td>
                   <!-- <td align="center" style="padding-top:20px;">
                     <a href="#" onclick = "redirectUrl('hdfc');">
                      <img alt="Click here" title="Click here" style="border: solid 1px grey;margin-left: -43%;" src="<?php echo base_url();?>themes/frontend/images/hdfcpay.jpg" width="95" />
                    </a> 
                  </td> -->
                   
                 </tr>



                    <!-- this code commented by pravin -->         
                   <!-- <tr>
                     <td align="center">

                    <a <?php if($user_id == ''){?>href="#"  onclick = "redirectUrl('paypal');"<?php } else{?>href="<?php echo base_url();?>reports/report/confirm_order/<?php echo $total;?>/<?php echo $this->session->userdata('user_id');?>"<?php }?>>

                     <img  alt="Click here" title="Click here" style="bord
                     er: solid 1px grey;"  src="<?php echo base_url();?>themes/frontend/images/paypal.jpg" width="100" />

                   </a>
                    
                    </td>

                     <td align="center" style="padding-top:8px;">

                     <a href="#"  onclick = "redirectUrl('wire');" <?php if ($user_id != ''){echo 'style=display:none';}?>>

                       <img alt="Click here" title="Click here" style="border: solid 1px grey;"  src="<?php echo base_url();?>themes/frontend/images/wiretransfer.gif"  width="100" />

                     </a>
                   </td>
                    <td align="center">
                     <a href="#" onclick = "redirectUrl('hdfc');">
                      <img alt="Click here" title="Click here" style="border: solid 1px grey;" src="<?php echo base_url();?>themes/frontend/images/hdfcpay.jpg" width="95" />
                    </a> 
                  </td>
                 
                  
                 </tr>
                 <?php //$allow = array("103.93.194.90","123.201.125.186");
                    // echo $_SERVER['REMOTE_ADDR'];
                  //if (in_array ($_SERVER['REMOTE_ADDR'], $allow)) {
                      // exit();
                   ?>
                 <tr>
                  
                   <td align="center" style="padding-top:20px;">
                     
                     <a href="#"  onclick = "redirectUrl('razorpaypg');" <?php if ($user_id != ''){echo 'style=display:none';}?>>
                       
                       <img alt="Click here" title="Click here" style="border: solid 1px grey;" src="<?php echo base_url();?>themes/frontend/images/razorpay_logo.jpg" id="rzor"/>
                       
                     </a>
                   </td>
                 
                 </tr> -->
                 <?php //} ?>
               </table>
                          
                         
                          </td>
                        </tr>
						          
		  <?php }
				else
				{ ?>
					<tr>
						<td colspan="4">
							 No Products added to cart.
						</td>
						
					</tr>
	   <?php   }
		  ?>
            
                      </tbody>
                    </table>
                      </div>
                     </div>
                     
                    </div>
                      
                    
                </div>
            </div>
    </div>        
   </div>
</div>



     <head>
		<title>Purchase  a copy: <?php echo $row_report[0]->report_name; ?></title>
		</head>  
   <div id = "hdfcdiv" style = "display:none;">     
    <form method='POST' action = "<?php echo base_url();?>reports/report/confirm_order/<?php echo $total;?>/hdfc" id = "hdfcForm">
			<table border='0'>
				<tr> <td> Key : </td> <td> <input name='key' type='text' value='71tFEF'> </td>
				<tr> <td> Transaction Id : </td> <td> <input name='txnid' type='text' value='<?php echo uniqid( "animesh_" );?>'> </td>			
				<tr> <td> Amount : </td> <td> <input name='amount' type='text' value='<?php echo $total;?>'> </td>
				<tr> <td> Firstname : </td> <td> <input name='firstname' type='text' value='animesh'> </td>
				<tr> <td> Email : </td> <td> <input name='email' type='text' value='animesh.kundu@payu.in'> </td>
				<tr> <td> Phone : </td> <td> <input name='phone' type='text' value='1234567890'> </td>
				<tr> <td> Product Info : </td> <td> <input name='productinfo' type='text' value='Just another test site'> </td> 
			</table>
			
			<input type="submit" value="Submit">
		</form>     
	</div>
 
<script>
	function redirectUrl(val){
	var urlIcici = '<?php echo base_url();?>reports/report/confirm_order/<?php echo $total;?>';
	var urlHdfc = '<?php echo base_url();?>reports/report/confirm_order/<?php echo $total;?>/hdfc';
  var urlRazorpay='<?php echo base_url();?>reports/report/confirm_order/<?php echo $total;?>/razorpaypg';
	var urlWire = '<?php echo base_url();?>reports/report/wiredTransfer/';
	var full_name = document.getElementById("full_name").value;
	var email = document.getElementById("email").value;
	var designation = document.getElementById("designation").value;
	var username = full_name.substring(0, 4);
	var u = username.replace(" ","_");
	var urlPaypal = '<?php echo base_url();?>reports/report/confirm_order/<?php echo $total;?>/paypal';
	// var phone = document.getElementById("phone").value;
  var country_code=document.getElementById("country_code1").value;
  var mobile=document.getElementById("phone").value;
  var phone = country_code+' '+mobile;
	var address = document.getElementById("address").value;
	var city = document.getElementById("city").value;
	var state = document.getElementById("state").value;		var country = document.getElementById("country").value;
	var zip = document.getElementById("zip").value;
	var report_id = <?php echo $row_report[0]->id; ?>;
	if(document.getElementById("full_name").value == '' || document.getElementById("email").value == ''|| document.getElementById("phone").value == ''|| document.getElementById("address").value == ''|| document.getElementById("city").value == ''|| document.getElementById("state").value  == ''|| document.getElementById("zip").value== ''|| document.getElementById("designation").value== '' || document.getElementById("country").value == '')
	{
		alert("Insert All data");
		return false;
	}else{
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>reports/report/setGuestData",
			data:'full_name='+full_name+'&email='+email+'&phone='+phone+'&address='+address+'&city='+city+'&state='+state+'&zip='+zip+'&username='+u+'&report_id='+report_id+'&designation='+designation+'&country='+country,
			success: function(result)
			{
				 
				if(val == 'paypal'){
					location.href = urlPaypal;
				}else if(val == 'icici'){
			
				location.href = urlIcici;
				}else if(val == 'wire'){
					location.href = urlWire;
				}else if(val == 'razorpaypg'){
          location.href = urlRazorpay;
        }else if(val == 'hdfc'){
					$("#hdfcForm").submit();
				}
			 }

			});
		}

	}
</script>
<script>
    
    $("#update_price").submit(function (e) {
        e.preventDefault();
        var url="<?php echo base_url();?>reports/report/update_report_price/<?php echo $row_report[0]->id;?>";
        var up_report_price = $("#update_report_price").val();
        var price_type = $("#report_price_type").val();

        if(price_type=='single'){
            redirect_url="<?php echo base_url();?>contact/purchase-single-user/<?php  echo $row_report[0]->id; ?>";
        }
        else if (price_type=='multiple'){
            redirect_url="<?php echo base_url();?>contact/purchase-multiple-user/<?php  echo $row_report[0]->id; ?>";
        }
        else if (price_type=='global'){
            redirect_url="<?php echo base_url();?>contact/purchase-global-user/<?php  echo $row_report[0]->id; ?>";
        }
        else if (price_type=='corporate'){
            redirect_url="<?php echo base_url();?>contact/purchase-corporate-user/<?php  echo $row_report[0]->id; ?>";
        }

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: { 
                up_report_price:up_report_price,price_type:price_type,
            },
            success: function(result) {
                //debugger;
                //alert(result.message);
                 window.location.href=redirect_url;

            },
            error: function (result) {
               //alert(result.message);
              window.location.href=redirect_url;
            } 
        });
     });

</script>
  <script >
  
  $( "#country" ).change(function() {
  var country = $("#country").val()  
  // alert(country);
  // if(country !='')
  // {
  // $('#country_code').val(country);
  // }
 $.ajax({      
   type: "POST",                                    
   url: "<?php echo base_url().'reports/report/getCountryNameRelatedData'?>",

   data: {
     country_name:country
     

   }, 
    
   success: function(data){
    
    // alert(data);
    var obj = JSON.parse(data);
    // alert(obj);
   
     $("#country_code1").val('+' +obj[0].country_code);
     // $("#pagination").html(split[1]); 
   }
 }); 

    });

   // }

</script>