<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class publishersitemap extends CI_Controller
{

    function __construct ()
    {

        parent::__construct();

         $this->load->library('xml_writer');

    }

    function index ()
    {
        
        $priority='0.7';
        $chengefeq='daily';
        $xml = new Xml_writer();
        $xml->initiate();       
		$xml->startBranch('urlset');
        
		$query=$this->db->query("select display_name,join_date from users where user_type = 'publisher' AND status = '1' order by id ASC;");
		 
		$u=$query->result_array();
		$i=1;
		foreach($u as $url)
    	{ 	
    		//$date1=str_replace(' ', 'T', $url['join_date']);
    		//$date=$date1."+00:00";
            //

    		$publisher_name =  str_replace(" ","-",strtolower($url['display_name']));
    		$xml->startBranch('url');     
    		$xml->addNode('loc', base_url().'publisher/'.$publisher_name.'.html',true); 
            //$xml->addNode('lastmod',$date,true);
            $xml->addNode('changefreq',$chengefeq,true); 
            $xml->addNode('priority',$priority,true); 
    		$xml->endBranch();	
    		$i++;
    	}
        
        
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml', $data);
    }
}
