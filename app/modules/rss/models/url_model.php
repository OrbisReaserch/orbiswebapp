<?php
 
class Url_model extends CI_Model{
 
    public function __construct(){
        $this->load->database();
    }
    public function getURLS(){
 
        $this->db->select('*');
        $query=$this->db->get('report');
        return $query->result_array();
    }
	
	public function getPublisherWise($id)
	{
 		$query_2 = $this->db->get_where('report', array('publisher_id' => $id));
		return $query_2->result_array();
    }
		public function getCategoryWise($id)
	{
 		$query_2 = $this->db->get_where('report', array('country_id' => $id));
		return $query_2->result_array();
    }
	public function getKeywordWise($id)
	{
 		$this->db->like('metatitle', $id);
		$query=$this->db->get('report');
		return $query->result_array();
    }
}
?>