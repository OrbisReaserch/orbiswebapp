<?php 
echo '<?xml version="1.0" encoding="utf-8"?>' . "\n";

?>
<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" version="2.0"    
    >
    <channel>
    <title><?php echo $feed_name; ?></title>
    <link><?php echo $feed_url; ?></link>    
    <description><?php echo $page_description; ?></description>
    <dc:language><?php echo $page_language; ?></dc:language>
    <dc:creator><?php echo $creator_email; ?></dc:creator>
    <dc:rights>Copyright <?php echo gmdate("Y", time()); ?></dc:rights>
    <admin:generatorAgent rdf:resource="http://www.codeigniter.com/" />
   <?php
	if(is_array($ARTICLE_DETAILS) && count($ARTICLE_DETAILS) ) {					
		foreach($ARTICLE_DETAILS as $loop){ 
  ?>    
        <item>
          <title><?php echo xml_convert($loop->report_name); ?></title>
          <link><?php echo xml_convert($loop->report_name); ?></link>
		  <guid><?php echo xml_convert($loop->report_name); ?></guid>
          <description><?php echo xml_convert($loop->report_name); ?></description>      
        </item>        
   <?php } } ?>    
    </channel>
</rss>