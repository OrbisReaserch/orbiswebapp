<?php
 
Class Rss extends CI_Controller {
 
    public function __construct(){
        parent::__construct();
        $this->load->model('rss/url_model');
		$this->load->helper('url');
    }
 
	public function index()
	{
	$data['urlslist'] = $this->url_model->getURLS();
	$this->load->view("sitemap_view",$data);
	}
			
	public function publisher($id)
    {
        $data['urlslist'] = $this->url_model->getPublisherWise($id);
        $this->load->view("sitemap_view",$data);
    }
	public function category($id)
    {
        $data['urlslist'] = $this->url_model->getCategoryWise($id);
        $this->load->view("sitemap_view",$data);
    }
	
	public function keyword($id)
    {
        $data['urlslist'] = $this->url_model->getKeywordWise($id);
        $this->load->view("sitemap_view",$data);
    }
}
 
?>