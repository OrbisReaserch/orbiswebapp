<?php
class Rss extends CI_Controller
{

   public function __construct()
    {
        parent::__construct();
		$this->load->helper('xml');
    }

   public function index($cond = "",$type = "",$id = "")
    {		 
        
         /*---------------------------------------------------------Set Title-------------------------------------------------------------*/
        $title = "";
		if($type == "category"){
			
			$queryTitle = $this->db->query('SELECT category_name FROM category where id = "'.$id.'";');		
			$uTitle = $queryTitle->row_array();
			 
			$title = $uTitle['category_name']." Market Research Reports";
			 
		} else if($type == "sub_category"){
		
			$queryTitle = $this->db->query('SELECT sub_category_name FROM sub_category where id = "'.$id.'";');		
			$uTitle = $queryTitle->row_array();
			$title = $uTitle['sub_category_name']." Market Research Reports";
			
		}else if($type == "publisher"){
			
			$queryTitle = $this->db->query('SELECT display_name FROM users where id = "'.$id.'";');		
			$uTitle = $queryTitle->row_array();
			$title = $uTitle['display_name']." Market Research Reports";
			
		}else if($type == "country"){
			
			$queryTitle = $this->db->query('SELECT country_name FROM country where id = "'.$id.'";');		
			$uTitle = $queryTitle->row_array();
			$title = $uTitle['country_name']." Market Research Reports";
			
		}	
		 
		/*---------------------------------------------------------Set Title END-------------------------------------------------------------*/	
		
        $this->load->library('xml_writer');
        
         
        $xml = new Xml_writer();
        $xml->setRootName('rss');
		
 
        $xml->initiate();
        

          $xml->startBranch('channel');
			$xml->addNode('title', strip_tags($title),true);
		$xml->addNode('language', "en-us",true);
	 
        if($cond == ""){
				$query=$this->db->query('SELECT id,report_name,report_date,page_urls,SUBSTRING(`report_description`, 1, 500) as report_description FROM report where status = "1" order by id desc;');		
			}else{		
				$query=$this->db->query("SELECT id,report_name,report_date,page_urls,SUBSTRING(`report_description`, 1, 500) as report_description  FROM report where status = '1' AND ".urldecode(str_replace("~","'",$cond))." order by id desc;");	
		}
		
		
		 
		$u=$query->result_array();
		$i=1;
		foreach($u as $url)
	{ 
			
			$xml->startBranch('item'); 
		    $xml->addNode('title', $this->RemoveJunkHtml($url['report_name']),true);
			$xml->addNode('link', base_url()."reports/index/".$url['page_urls'] ,true);
			$xml->addNode('pubDate', $url['report_date'],true);
			$xml->addNode('description', $this->RemoveJunkHtml($url['report_description'])."<br><br>Purchase a copy  @ <a href='".base_url()."contact/purchase/".$url['id']."'> ".base_url()."contact/purchase/".$url['id']."</a>",true);
		 
			 
			
			$xml->endBranch();
			  
			 
						 
			$i++;
	}
             
                
        $xml->endBranch();
               
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
		 
        $this->load->view('xml', $data);
    }
	public function feed($type = "",$id = ""){
		if(!isset($_POST['cond'])){
			$cond = $type."_id = ~".$id."~";	
		}else{
			$cond = $_POST['cond'];	
		}
		
		/* $this->index2($cond,$type,$id); 
		$this->demoRss($cond,$type,$id); */
		$this->index($cond,$type,$id);
	}
	public function RemoveJunkHtml($string){ 
			$a = str_replace("&rsquo;","",$string);
			$b = str_replace("&nbsp;","",$a);
			$c = str_replace("\n","",$b);
			$d = str_replace("&lsquo;","",$c);
			$e = str_replace("&","",$d);
			$f = str_replace("amp;","",$e);
			
			return xml_convert(strip_tags($f));
	}
	
	/* public function demoRss($cond,$type = "",$id = ""){
		$title = "";
		if($type == "category"){
			
			$queryTitle = $this->db->query('SELECT category_name FROM category where id = "'.$id.'";');		
			$uTitle = $queryTitle->row_array();
			 
			$title = $uTitle['category_name']." Market Research Reports";
			 
		} else if($type == "sub_category"){
		
			$queryTitle = $this->db->query('SELECT sub_category_name FROM sub_category where id = "'.$id.'";');		
			$uTitle = $queryTitle->row_array();
			$title = $uTitle['sub_category_name']." Market Research Reports";
			
		}else if($type == "publisher"){
			
			$queryTitle = $this->db->query('SELECT display_name FROM users where id = "'.$id.'";');		
			$uTitle = $queryTitle->row_array();
			$title = $uTitle['display_name']." Market Research Reports";
			
		}else if($type == "country"){
			
			$queryTitle = $this->db->query('SELECT country_name FROM country where id = "'.$id.'";');		
			$uTitle = $queryTitle->row_array();
			$title = $uTitle['country_name']." Market Research Reports";
			
		}			
		
		 
        if($cond == ""){
				$query=$this->db->query('SELECT report_name,report_date,page_urls,SUBSTRING(`report_description`, 1, 100) as report_description FROM report where status = "1" order by id desc;');		
			}else{		
				$query=$this->db->query("SELECT report_name,report_date,page_urls,SUBSTRING(`report_description`, 1, 100) as report_description  FROM report where status = '1' AND ".urldecode(str_replace("~","'",$cond))." order by id desc;");	
		}
		 
		$u = $query->result_array();
		$i=1;
		
		$data = "<title>".strip_tags($title)."</title><language>en-us</language>";
		foreach($u as $url)
		{ 
			
			
			$data .= "<item>
						<title>".$this->RemoveJunkHtml($url['report_name'])."</title>
						<link>".base_url()."reports/index/".$url['page_urls']."</link>
						<pubDate>".$url['report_date']."</pubDate>
						<description>".$this->RemoveJunkHtml($url['report_description'])."</description>
					  </item>"; 
			$i++;
		}
		
		
        if(CreateRss("app/modules/rss/views/rss.xml",$data)){
			
			  
			 header("location:".base_url()."app/modules/rss/views/rss.xml");
			 
		}else{
			echo "Fail";
		}
		   
	}
	  
	public function index2($cond = "",$type = "",$id = ""){
		$data['encoding'] = 'utf-8';
	        $data['feed_name'] = 'www.technicalkeeda.com';
	        $data['feed_url'] = 'http://www.technicalkeeda.com';
	        $data['page_description'] = 'Welcome to www.technicalkeeda.com feed url page';
	        $data['page_language'] = 'en-ca';
	        $data['creator_email'] = 'yashwantchavan@gmail.com';
	        
	        $data['ARTICLE_DETAILS'] = null;
			if($cond == ""){
				$query=$this->db->query('SELECT report_name,report_date,page_urls,SUBSTRING(`report_description`, 1, 100) as report_description FROM report where status = "1" order by id desc;');		
			}else{		
				$query=$this->db->query("SELECT report_name,report_date,page_urls,SUBSTRING(`report_description`, 1, 100) as report_description  FROM report where status = '1' AND ".urldecode(str_replace("~","'",$cond))." order by id desc;");	
		}
		 
		$query = $query->result_array();
		
		if($query){
			$data['ARTICLE_DETAILS'] =  $query;
		}  
	        header("Content-Type: application/rss+xml");
	        $this->load->view('rss', $data);
	}
	*/
}
