<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends CI_Controller
{

    function __construct ()
    {
        parent::__construct();
    }

    function index ()
    {
        
        
        $this->load->library('xml_writer');
        
 
        $xml = new Xml_writer();
        $xml->setRootName('Orbis-Reports');
        $xml->initiate();
        

        
        $xml->startBranch('sitemapindex');
        
    
		
		  
		 		
	$xml->startBranch('sitemap');     		
	$xml->addNode('loc', base_url().'allcategorysitemap.xml',true);     		
	$xml->endBranch();			
	$xml->startBranch('sitemap');     		
	$xml->addNode('loc', base_url().'latestreportsitemap',true);     			
	$xml->endBranch();	
	$xml->startBranch('sitemap');    
	$xml->addNode('loc', base_url().'publishersitemap.xml',true);     	
	$xml->endBranch();						
 
	$xml->startBranch('sitemap');     		
	$xml->addNode('loc', base_url().'countrysitemap.xml',true);     
	$xml->endBranch();					
	$xml->startBranch('sitemap');     		
	$xml->addNode('loc', base_url().'newssitemap.xml',true);     	
	$xml->endBranch();	
	 
        
        
        
        
        $xml->endBranch();
        
     
          
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml', $data);
    }
}
