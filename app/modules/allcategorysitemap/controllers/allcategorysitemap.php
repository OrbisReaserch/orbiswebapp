<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class allcategorysitemap extends CI_Controller
{

    function __construct ()
    {

        parent::__construct();

         $this->load->library('xml_writer');

    }


    function index ()
    {
        $priority='0.8'; 
        $lastmodify="2019-03-19";
        $chengefeq='always';   
        $xml = new Xml_writer();
        $xml->initiate();
        $xml->startBranch('urlset');
        
		$query=$this->db->query('SELECT id,category_name FROM category where status = "1" ORDER BY id ASC;');
		 
		$u=$query->result_array();     
		
		foreach($u as $url)
	{ 		
		$category_name =  str_replace(" ","-",strtolower($url['category_name']));


		$query1=$this->db->query("select sub_category_name from sub_category where status = '1' and category_id = '".$url['id']."' ORDER BY id ASC;");
		$u1=$query1->result_array();

		$xml->startBranch('url');       
		$xml->addNode('loc',base_url().'market-reports/'.$category_name.'.html',true);
		$xml->addNode('lastmod',$lastmodify,true);
        $xml->addNode('changefreq',$chengefeq,true);
        $xml->addNode('priority',$priority,true);        
		$xml->endBranch();		
		$i++;	
				foreach($u1 as $url1)
			{ 		
				$sub_category_name =  str_replace(" ","-",strtolower($url1['sub_category_name']));
				$xml->startBranch('url');       
				$xml->addNode('loc',base_url().'market-reports/'.$category_name.'/'.$sub_category_name.'.html',true); 
				$xml->addNode('lastmod',$lastmodify,true);
                $xml->addNode('changefreq',$chengefeq,true);
        		$xml->addNode('priority',$priority,true);        
				$xml->endBranch();		
			}
	}
        
        $xml->endBranch();
        
     
        
        
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);

        $this->load->view('xml', $data);
    }
    
    public function newxmldata(){
    $query=$this->db->query('SELECT id,category_name FROM category where status = "1" ORDER BY id DESC;');
    $u=$query->result_array();
    //print_r($u); die();
    $data["u"] = $u;
    $this->load->view('sitemap_view', $data);
    }
}