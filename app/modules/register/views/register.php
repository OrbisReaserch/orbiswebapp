<header class="main-header">
    <div class="container">
        <h1 class="page-title">Register With OrbisResearch.com</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Register</li>
        </ol>
    </div>
</header>

<div class="container">
   <div class="row">
   <div class="col-md-8 col-md-offset-2">
       <div class="panel panel-default registerbox">
                <div class="panel-heading"><h4>New Client ! Register With OrbisResearch.com</h4></div>
                <div class="panel-body">
				
					<?php
						   if($this->session->flashdata('success'))
						   {
							  echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
						   }
						   else if($this->session->flashdata('error'))
						   {
							 echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
						   }
					?>
                    <form  method="post" action="<?php echo base_url();?>register/register">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label for="InputUserName">User Name</label>
                            
							 <input type="text" id="name" name="username" class="form-control" onblur="get_username(this.value)" required/>
                        </div>
                        <div class="form-group">
                            <label for="Inputpassword">Password</label>
                            <input class="form-control" type="password" id="name" name="password" required/>
                        </div>
						 <div class="form-group">
                            <label for="Inputpassword">Display Name</label>
                            <input type="text" id="name" name="display_name" class="form-control" pattern="^[a-zA-Z ]+$" required/> 
                        
                        </div>
                        <div class="form-group">
                            <label for="InputEmail">Email</label>
                            
							<input type="email" id="name" name="email" class="form-control" onblur="get_email(this.value)" required/>
                        </div>
                        <div class="form-group">
                            <label for="companyName">Company Name</label>
                            <input class="form-control" type="text" id="name"  name="company_name" >
                        </div>
                        <div class="form-group">
                            <label for="fName">First Name</label>
                            <input class="form-control"  type="text" id="name" name="first_name" pattern="^[a-zA-Z ]+$" >
                        </div>
                        <div class="form-group">
                            <label for="fName">Middle Name</label>
                            <input class="form-control"  type="text" id="name" name="middle_name" pattern="^[a-zA-Z ]+$" >
                        </div>
                        <div class="form-group">
                            <label for="lName">Last Name</label>
                            <input class="form-control" type="text" id="name"   name="last_name" pattern="^[a-zA-Z ]+$">
                        </div>
                        <div class="form-group">
                            <label for="mobino">Mobile Number</label>
                            <input class="form-control" id="mobino" type="tel" name="mobile" pattern="^[0-9 ]+$" maxlength="10"  >
                        </div>
                        <div class="form-group">
                            <label for="fax">Fax</label>
                            <input class="form-control" id="name" name="fax" pattern="^[0-9 ]+$">
                        </div>
                        <div class="form-group">
                            <label for="date">Date</label>
                            <input class="form-control" id="date" type="text" placeholder="<?php echo date("Y-m-d"); ?>" disabled>
							 <input type="hidden" name="user_type"  readonly value = "client" />
                        </div>
                      </div>
                       <div class="col-md-6">
                        
                        <div class="form-group">
                        <label for="zCode">Zip Code</label>
                        <input class="form-control" name="zipcode" pattern="^[0-9 ]+$"  type="text">
                        </div>
                        <div class="form-group">
                        <label for="cityname">City</label>
                        <input class="form-control" name="city" pattern="^[a-zA-Z ]+$" id="cityname" type="text">
                        </div>
                        <div class="form-group">
                        <label for="stateName">State</label>
                        <input class="form-control"  name="state"  pattern="^[a-zA-Z ]+$" id="stateName" type="text">
                        </div>
                        <div class="form-group">
                        <label for="countryName">Country</label>
                        <input class="form-control" id="countryName" name="country" pattern="^[a-zA-Z ]+$" type="text">
                        </div>
                        <div class="form-group">
                        <label for="currentAddress">Current Address</label>
                        
						 <textarea id="message" class="form-control" name="address1" rows="4" ></textarea>

                        </div>
                        <div class="form-group">
                        <label for="currentAddress">Present Address</label>
                        
						 <textarea id="message" class="form-control" name="address2" rows="4" ></textarea>

                        </div>
                        <div class="form-group">
                        <label for="PhoneNumber">Phone Number</label>
                        <input class="form-control" id="PhoneNumber" type="tel"  name="phone" pattern="^[0-9 ]+$" maxlength="10">
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                        <div class="checkbox checkbox-inline">
                        <input id="inlineCheckbox1" value="option1" type="checkbox">
                        <label for="inlineCheckbox1">I read <a href="<?php echo base_url(); ?>termsandconditions">Terms and Conditions</a>.</label>
                        </div>
                        </div>
                        <br><br>
                        <div class="col-md-12 text-right">
                        <button type="submit" value="Register" name="submit" class="btn btn-primary">Register</button>
                        <a href="<?php echo base_url(); ?>" class="btn btn-primary">Cancel</a>
                        </div>
                        </div>
                      </div>
                    </div>
                      
                    </form>
                </div>
            </div>
    </div>        
   </div>
</div>

<script>

           function get_username(val)

           {
    	   $.post('<?php echo base_url()."register/register/get_usernames";?>',{username:val},function(data)
    			   {
			   if(data!='')
			   {
				   document.register.username.value='';
				   document.register.username.focus();
				   alert(data);
			   }
    			   });	
           }

           function get_email(val)
           {
    	   $.post('<?php echo base_url()."register/register/get_email";?>',{email:val},function(data)
    			   {
			   if(data!='')
			   {
				   document.register.email.value='';
				   document.register.email.focus();
 			 alert(data);
			   }
    			   });	
           }  
 </script>
