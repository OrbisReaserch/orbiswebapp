
 <header class="main-header">
    <div class="container">
        <h1 class="page-title">Log In to OrbisResearch</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="index.php">Home</a></li>
            <li class="active">Log In</li>
        </ol>
    </div>
</header>


<div class="container">
    <div class="center-block logig-form">
        <div class="panel panel-default loginbox">
            <div class="panel-heading"><h4>Login Form</h4></div>
            <div class="panel-body">
			 <?php
				   if($this->session->flashdata('success'))
				   {
					  echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
				   }
				   else if($this->session->flashdata('error'))
				   {
					 echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
				   }
			?>
                <form action="<?php echo base_url();?>user/check_login" method="post">
                    <div class="form-group">
                        <div class="input-group login-input">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input class="form-control" id="name" name="username" placeholder="Username" type="text" required />
                        </div>
                        <br>
                        <div class="input-group login-input">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input class="form-control" placeholder="Password" type="password" id="name" name="password" required />
                        </div>
                        <br>
                        <button value="Login" name="submit" class="btn btn-primary pull-right">Login</button>
                     <div class="clearfix"></div>
                        <hr class="dotted margin-10">
                        <a href="<?php echo base_url(); ?>register/register" class="pull-right">Create Account</a>
                        <a href="<?php echo base_url(); ?>register/forgot_password" class="">Password Recovery</a>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
