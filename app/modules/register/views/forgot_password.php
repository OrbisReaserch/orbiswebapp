
 <header class="main-header">
    <div class="container">
        <h1 class="page-title">Log In to OrbisResearch</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Log In</li>
        </ol>
    </div>
</header>


<div class="container">
    <div class="center-block logig-form">
        <div class="panel panel-default loginbox">
            <div class="panel-heading"><h4>Forgot Password?</h4></div>
            <div class="panel-body">
			 <?php
           if($this->session->flashdata('success'))
           {
              echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
           }
           else if($this->session->flashdata('error'))
           {
             echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
           }
    ?>
                <form role="form" action="<?php echo base_url();?>register/forgot_password" method="post">
                    <div class="form-group">
                        <div class="input-group login-input">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input class="form-control" type="text" name="username" id="name" onblur ="return CheckEmail();"  placeholder="Username" required>
                        </div>
                    
                       <br>
                       <div class="text-right">
                          <button type="submit" value="Submit" class="btn btn-primary" onclick="return CheckEmail();">Submit</button>
                        <a href="<?php echo base_url(); ?>"  class="btn btn-primary">Cancel</a>
                       </div>
              
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>

           function CheckEmail()

           {
			var name = document.getElementById("name").value;
			
    	   $.post('<?php echo base_url()."register/register/get_usernames";?>',{username:name},function(data)

    			   {
					
			   if(data == '')

			   {
					document.getElementById("name").value='';
					alert("User not registered");
					return false; 
				} 
	   });	
}
</script>