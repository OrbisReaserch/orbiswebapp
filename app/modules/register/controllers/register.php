<?php
class Register extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('register/register_model');
		$this->load->helper('form');

	}

	public function index()
	{
			
			
		//$data['publisher'] = $this->register_model->getPublisher();
		//$data['reports'] = $this->register_model->getReports($report_id);
			
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->register_model->getFooterNews();
		$data['footerreport'] = $this->register_model->getFooterReport();
		$data['include'] = 'register/register';
		$this->load->view('frontend/container',$data);

		if(isset($_POST['submit']))
		{

			$k=$this->register_model->register();
			if($k)
			{

				$this->session->set_flashdata('success', 'Registration has been done successfully.');
				redirect(base_url().'register/register');
			}
			else
			{

				$this->session->set_flashdata('success', 'Registration not done successfully. Please Try Again.');
				$this->load->view('register/register',$data);
			}


		}
		
	}
	public function register_new()
	{
			
			
		//$data['publisher'] = $this->register_model->getPublisher();
		//$data['reports'] = $this->register_model->getReports($report_id);
			
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->register_model->getFooterNews();
		$data['footerreport'] = $this->register_model->getFooterReport();
		$data['include'] = 'register/register_new';
		$this->load->view('frontend/container',$data);

		if(isset($_POST['submit']))
		{

			$k=$this->register_model->register();
			if($k)
			{

				$this->session->set_flashdata('success', 'Registration has been done successfully.');
				redirect(base_url().'register/register');
			}
			else
			{

				$this->session->set_flashdata('success', 'Registration not done successfully. Please Try Again.');
				$this->load->view('register/register',$data);
			}


		}
		
	}
	public function forgot_password_new(){

	if(isset($_POST['submit']))	
		{			
			$username = $_POST['username'];		
			$result_user = $this->db->query("select * from users where username='".$username."'"); 	
			if ($result_user->num_rows() > 0)		
				{				
					$row = $result_user->result();			 	
					$email = $row[0]->email;			
					$msg =' Your password is '.$row[0]->password;		 		
					$k = $this->send_mail($email,$msg);		
					if($k)			
	{						
		$this->session->set_flashdata('success', 'Password has been sent successfully on your registered email ID.');		
		//echo "done"; die();
		redirect(base_url());	
	}		
	else				
		{						
		$this->session->set_flashdata('success', 'Please Try Again.');				
		$data['include'] = 'register/forgot_password_new';	
		$this->load->view('frontend/container',$data);	
	}			
}else{				
	$this->session->set_flashdata('error', 'Please enter valid username');	
		$data['include'] = 'register/forgot_password_new';	
		$this->load->view('frontend/container',$data);				
	}		 			
}else{				
	$data['include'] = 'register/forgot_password_new';		
	$this->load->view('frontend/container',$data);			
}		
}	
	public function forgot_password(){

	if(isset($_POST['submit']))	
		{			
			$username = $_POST['username'];		
			$result_user = $this->db->query("select * from users where username='".$username."'"); 	
			if ($result_user->num_rows() > 0)		
				{				
					$row = $result_user->result();			 	
					$email = $row[0]->email;			
					$msg =' Your password is '.$row[0]->password;		 		
					$k = $this->send_mail($email,$msg);		
					if($k)			
	{						
		$this->session->set_flashdata('success', 'Password has been sent successfully on your registered email ID.');		
		//echo "done"; die();
		redirect(base_url());	
	}		
	else				
		{						
		$this->session->set_flashdata('success', 'Please Try Again.');				
		$data['include'] = 'register/forgot_password';	
		$this->load->view('frontend/container',$data);	
	}			
}else{				
	$this->session->set_flashdata('error', 'Please enter valid username');	
		$data['include'] = 'register/forgot_password';	
		$this->load->view('frontend/container',$data);				
	}		 			
}else{				
	$data['include'] = 'register/forgot_password';		
	$this->load->view('frontend/container',$data);			
}		
}	
	public function login()
	{
			
			
		//$data['publisher'] = $this->register_model->getPublisher();
		//$data['reports'] = $this->register_model->getReports($report_id);
			
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->register_model->getFooterNews();
		$data['footerreport'] = $this->register_model->getFooterReport();
		$data['include'] = 'register/login';
		$this->load->view('frontend/container',$data);

		if(isset($_POST['submit']))
		{

			$k=$this->register_model->login();
			if($k)
			{

				$this->session->set_flashdata('success', 'Registration has been done successfully.');
				redirect(base_url().'register/login');
			}
			else
			{

				$this->session->set_flashdata('success', 'Registration not done successfully. Please Try Again.');
				$this->load->view('register/login',$data);
			}


		}
			


	}
	public function login_new()
	{
			
			
		//$data['publisher'] = $this->register_model->getPublisher();
		//$data['reports'] = $this->register_model->getReports($report_id);
			
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->register_model->getFooterNews();
		$data['footerreport'] = $this->register_model->getFooterReport();
		$data['include'] = 'register/login_new';
		$this->load->view('frontend/container',$data);

		if(isset($_POST['submit']))
		{

			$k=$this->register_model->login();
			if($k)
			{

				$this->session->set_flashdata('success', 'Registration has been done successfully.');
				redirect(base_url().'register/login_new');
			}
			else
			{

				$this->session->set_flashdata('success', 'Registration not done successfully. Please Try Again.');
				$this->load->view('register/login_new',$data);
			}


		}
			


	}
	public function get_usernames()
	{
		$username = $_REQUEST['username'];
	
		$result_user = $this->db->query("select * from users where username LIKE '".$username."'");

		if ($result_user->num_rows() > 0)
		{

			echo 'Username already exists';
		}
			
	}
	public function get_email()
	{
		$email = $_POST['email'];
		//echo $username;
		$result_user=$this->db->query("select * from users where email='".$email."'");
		if ($result_user->num_rows() > 0)
		{
			echo 'Email address already exists';
		}
			
	}public function send_mail($to,$desc)	{		
		$subject = "Orbis Research";                
		    $message = '<table border="1" cellspacing="0" width="100%">       
		                             <tr style="">                          
		                                       <td style=" padding: 10px;">                                        <strong>Orbis Research</strong>                                    </td>                                </tr>                                <tr>                                    <td style="padding:10px; background-color: #ffffff;">                                       	<p>Dear Customer,                                        <p>'.$desc.' </p>									</td>                                </tr>                            </table>';                    $headers = "MIME-Version: 1.0" . "\r\n";                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";                    $headers .= 'From: <enquiry@orbisresearch.com>' . "\r\n";                    mail($to,$subject,$message,$headers);	

		                   return true;                }
}