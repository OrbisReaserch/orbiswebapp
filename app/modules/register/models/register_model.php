<?php
class Register_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function register()
	{
		//print_r($_POST); exit;
		$email = $_POST['email'];
		$username = $_POST['username'];
		$display_name = $_POST['display_name'];
		$password = $_POST['password'];
		$company_name = $_POST['company_name'];
		$first_name = $_POST['first_name'];
		$middle_name = $_POST['middle_name'];
		$last_name = $_POST['last_name'];
		$address1 = $_POST['address1'];
		$address2 = $_POST['address2'];
		$zipcode = $_POST['zipcode'];
		$city = $_POST['city'];
		$country = $_POST['country'];
		$state = $_POST['state'];
		$phone = $_POST['phone'];
		$user_type = $_POST['user_type'];
		$mobile = $_POST['mobile'];
		$fax = $_POST['fax'];
		$d=date('y-m-d');
		$data = array('email'=>$email,'username'=>$username,'display_name'=>$display_name,'password'=>$password,'user_type'=>$user_type,'company_name'=>$company_name,
            'first_name'=>$first_name,'middle_name'=>$middle_name,'last_name'=>$last_name,'address1'=>$address1,'address2'=>$address2,'zipcode'=>$zipcode,
            'city'=>$city,'country'=>$country,'state'=>$state,'phone'=>$phone,'mobile'=>$mobile,'fax'=>$fax,'join_date'=>$d,'status'=>'1');
		//print_r($data);exit;
		$this->db->insert('users',$data);
		return $this->db->insert_id();
	}

	public function getCategory()
	{
		$result=$this->db->query("select * from category where status='1'");
		return $result->result();
	}
	public function getSubCategory()
	{
		$result=$this->db->query("select * from sub_category where status='1'");
		return $result->result();
	}
	public function getFeaturedReports()
	{
		$result=$this->db->query("SELECT * FROM report AS r1 JOIN (SELECT CEIL(RAND() * (SELECT MAX(id)   FROM report)) AS id) AS r2 WHERE r1.id >= r2.id ORDER BY r1.id ASC LIMIT 0,3");
		return $result->result();
	}
	public function getTestimonials()
	{
		$result=$this->db->query("select * from testimonials where status='1' order by id DESC limit 1");
		return $result->result();
	}
	public function getFooterNews()
	{
		$result=$this->db->query("select * from news where status='1' order by id desc limit 2");
		return $result->result();
	}
	public function getFooterReport()
	{
		$result=$this->db->query("select * from report where status='1' order by id desc limit 5");
		return $result->result();

	}
	/* public function getUsers()
	 {
		$result=$this->db->query("select * from users");
		return $result->result();
		}
		public function getReports($report_id)
		{
		$result=$this->db->query("select * from report where id='$report_id'");
		return $result->result();
		}

		public function getProductImage()
		{
		$result=$this->db->query("select * from product_images where status='1' limit 1");
		return $result->result();
		}
		public function getPublisher()
		{
		$result=$this->db->query("select * from users where status='1'");
		return $result->result();
		}
		public function getReportList($report_id)
		{
		$result=$this->db->query("select * from report where sub_category_id='$report_id' order by id DESC");
		return $result->result();
		}
		//-------------- Repourt Sorting Start------------


		public function getReportListPriceDesc($subcategoryid)
		{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_price DESC");
		return $result->result();
		}
		public function getReportListPriceAsc($subcategoryid)
		{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_price ASC");
		return $result->result();
		}

		public function getReportListDateDesc($subcategoryid)
		{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_date DESC");
		return $result->result();
		}
		public function getReportListDateAsc($subcategoryid)
		{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_date ASC");
		return $result->result();
		}

		public function getReportListTitleDesc($subcategoryid)
		{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_name DESC");
		return $result->result();
		}
		public function getReportListTitleAsc($subcategoryid)
		{

		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_name ASC");
		return $result->result();
		}
		//-------------- Repourt Sorting End------------

		*/

}
?>