<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class newssitemap extends CI_Controller
{

    function __construct ()
    {
        parent::__construct();

         $this->load->library('xml_writer');
    }

    function index ()
    {

        $priority='0.7';
        $changefreq='daily'; 
        $xml = new Xml_writer();
        $xml->initiate();
        $xml->startBranch('urlset');
		
		$query=$this->db->query("select news_title,news_date from news WHERE status='1' order by id ASC");
		 
		$u=$query->result_array();

		foreach($u as $url)
	{ 		
		$news_name =  str_replace(" ","-",strtolower($url['news_title']));
		$xml->startBranch('url');    
		$xml->addNode('loc', base_url().'news/'.$news_name,true);
        $xml->addNode('changefreq',$changefreq,true);
        $xml->addNode('lastmod',$url['news_date'],true); 
        $xml->addNode('priority',$priority,true);     
		$xml->endBranch();		
		
	}
        
        $xml->endBranch();
        
     
        
        
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);
        $this->load->view('xml', $data);
    }
}
