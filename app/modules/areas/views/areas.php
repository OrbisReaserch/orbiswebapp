
 <header class="main-header">
    <div class="container">
        <h1 class="page-title">Continents and Countries</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="index.php">Home</a></li>
            <li class="active">Continents and Countries</li>
        </ol>
    </div>
</header>

<?php /* ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
        
          <h3 class="post-title">Continent Reports</h3>
         
          <ul class="countriesSection">
		   <?php 
				$a=1;
      for ($j = 0; $j< sizeof($areas); $j++)
                  {
  
if($areas[$j]->isContinent == "1" ){
			?> 
            <li>
                <img src="<?php echo base_url(); ?>uploads/country_images/<?php echo $areas[$j]->flag_image; ?>" alt="<?php echo $areas[$j]->country_name." Market Reports"; ?>" title="<?php echo $areas[$j]->country_name." Market Reports"; ?>" width="35" height="25">
                <a href="<?php echo base_url().strtolower(str_replace(" ","-",$areas[$j]->country_name)).'-market-reports.html'; ?>" ><?php echo $areas[$j]->country_name; ?></a>
            </li>
              <?php
}
            $a++;
 }

 ?>
          </ul>
           
          <div class="clearfix"></div>
          <br>
           <h3 class="post-title">Country Reports</h3>
         
          <ul class="countriesSection">
             <?php 
 
                     for ($j = 0; $j< sizeof($areas); $j++)

                  {
if($areas[$j]->isContinent == "0" ){
	 ?>
			<li>
                <img src="<?php echo base_url(); ?>uploads/country_images/<?php echo $areas[$j]->flag_image; ?>" alt="<?php echo $areas[$j]->country_name." Market Reports"; ?>" title="<?php echo $areas[$j]->country_name." Market Reports"; ?>" width="35" height="25">
                <a href="<?php echo base_url().strtolower(str_replace(" ","-",$areas[$j]->country_name)).'-market-reports.html'; ?>" ><?php echo $areas[$j]->country_name; ?></a>
            </li>
			  <?php
}            $a++;
            }

            ?>
          </ul>
		  </div>
        </div>
    </div> <!-- row -->
   
</div>
<?php */ ?>
<div class="container">
         <div class="row">
            <div class="col-md-12">
               <h3 class="post-title">Continent Reports</h3>
               <ul class="countriesSection">
				<?php 
					$alphabets = array();
					 
					 for ($j = 0; $j< sizeof($areas); $j++)
						{
							$tmpname = $areas[$j]->country_name;
							array_push($alphabets,$tmpname[0]);
							if($areas[$j]->isContinent == "1" ){
							?> 
								<li>
									<img src="<?php echo base_url(); ?>uploads/country_images/<?php echo $areas[$j]->flag_image; ?>" alt="<?php echo $areas[$j]->country_name." Market Reports"; ?>" title="<?php echo $areas[$j]->country_name." Market Reports"; ?>" width="35" height="25">
									<a href="<?php echo base_url().strtolower(str_replace(" ","-",$areas[$j]->country_name)).'-market-reports.html'; ?>" ><?php echo $areas[$j]->country_name; ?></a>
								</li>
							  <?php
							}
							 
						}
					$uniqueAlpha = array_values(array_unique($alphabets));
					 
				 ?>                
				</ul>
               <div class="clearfix"></div>
               <br>
               <h3 class="post-title">Country Reports</h3>
               <div class="row">
                  <div class="col-md-12">
                     <?php /* ?><button class="btn btn-small btn-primary" data-toggle="portfilter" data-target="all"> All </button><?php */ ?>
					 <?php 
					 for ($k = 0; $k < sizeof($uniqueAlpha); $k++)
						{ ?>
                     <button id = "alpha_<?php echo $k; ?>" class="btn btn-small btn-primary" data-toggle="portfilter" data-target="<?php echo strtoupper($uniqueAlpha[$k]); ?>"> <?php echo strtoupper($uniqueAlpha[$k]); ?> </button>
                     <?php 
						}
					 ?>
                  </div>
                  <hr>
                  <br>
                  <div class="thumbnails gallery">
				  
						
						
				<?php 
					 for ($m = 0; $m < sizeof($uniqueAlpha); $m++)
						{ 
						$tmpAlpha = $uniqueAlpha[$m];
					?>
						 <div class="col-md-12" data-tag="<?php echo strtoupper($tmpAlpha); ?>">
							<ul class="list-unstyled countriesSection">
							  <?php 
 
                     for ($i = 0; $i< sizeof($areas); $i++)
					{
						$tmpname2 = $areas[$i]->country_name;
						if($areas[$i]->isContinent == "0" && $tmpAlpha == $tmpname2[0]){
						?>
							  <li>
								<img src="<?php echo base_url(); ?>uploads/country_images/<?php echo $areas[$i]->flag_image; ?>" alt="<?php echo $areas[$i]->country_name." Market Reports"; ?>" title="<?php echo $areas[$i]->country_name." Market Reports"; ?>" width="35" height="25">
								<a href="<?php echo base_url().strtolower(str_replace(" ","-",$areas[$i]->country_name)).'-market-reports.html'; ?>" ><?php echo $areas[$i]->country_name; ?></a>
							</li>
							   <?php
						}           
					 
					} ?>
							</ul>
						 </div>
                      <?php 
						}
					 ?>
                      
                  </div>
               </div>
            </div>
         </div>
         <!-- row --> 
		 </div>
		 
<script>

function clickAlpha(){
	
    document.getElementById("alpha_0").click();
}
</script>