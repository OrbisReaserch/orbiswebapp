<?php
class Areas extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('areas/areas_model');
	}

	public function index()
	{
		 
		$data['areas'] = $this->areas_model->getCountry(); 
		$data['include'] = 'areas/areas';
		$this->load->view('frontend/container',$data);
	}public function areas_new()	{ 		$data['areas'] = $this->areas_model->getCountry();		$data['include'] = 'areas/areas_new';		$this->load->view('frontend/container',$data);	}

	function clear_cache()
	{
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
	}

}