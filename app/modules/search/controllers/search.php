<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Search extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();	

		$this->load->database();
		$this->load->model("category/category_model");
		$this->load->model('search_model');
		$this->load->model('reports/report_model');
		//$this->load->model('profile_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

	}

	//==================== all page session check =====================

	public function Checklogin()
	{
		if ($this->session->userdata('admin_email') == '')
		{
			redirect('siteadmin/');
		}
	}

	public function index()
	{
		if(isset($_REQUEST['submit']))
		{	
			$key1 = mysql_real_escape_string($_REQUEST['search_keyword']);
			$key=preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $key1);	
			$cat = $_REQUEST['category'];	
			//$data['report_key'] = mysql_real_escape_string($_REQUEST['search_keyword']);
			$data['report_key'] =$key;	
			$data['cat'] =$_REQUEST['category'];	
			$data['featured'] = $this->report_model->getAllFeaturedReports();
			// if( (!empty($_REQUEST['category'])) || ('0' != ($_REQUEST['category'])) ){	
			// $data['res_report'] = $this->search_model->getKetCatreports($key,$cat); 
			// }		
			// else {	
			// $data['res_report'] = $this->search_model->getKeyreports($key); 	
			// } 		
			$data['include'] = 'search/searchlist';    	
			$this->load->view('frontend/container',$data);	
		}	
	}
public function auto_search()
{

			$key = $_POST['search_keyword'];
			$cat = $_POST['category'];
			
			$data['report_key'] =$_POST['search_keyword'];
			$data['cat'] =$_POST['category'];
			
			if( (!empty($_POST['category'])) || ('0' != ($_POST['category'])) ){
			
			$data['res_report'] = $this->search_model->getKetCatreports_key($key); 
			}
			else {
			$data['res_report'] = $this->search_model->getKetCatreports_key($key); 
			}
		
			$data['publisher'] = $this->search_model->getPublisher();
        	//$data['reports'] = $this->search_model->getReports($report_id);
            $data['category'] = $this->search_model->getCategory();
            $data['subcategory'] = $this->search_model->getSubCategory();
			$data['featured'] = $this->search_model->getFeaturedReports();
			$data['testimonials'] = $this->search_model->getTestimonials();
			$data['footernews'] = $this->search_model->getFooterNews();
		    $data['footerreport'] = $this->search_model->getFooterReport();
			echo $data['res_report'][0]->page_urls;
			//$data['include'] = 'search/searchlist';    


			//$this->load->view('frontend/container',$data);
}


	public function get_sort()
	{
		$data['mx']=$_POST['maxprice'];
		$data['mn']=$_POST['minprice'];
		$data['co']=$_POST['country'];
		$data['report_year']=$_POST['report_year'];
		$data['sort']=$_POST['sort'];
		
		$data['category'] = $this->category_model->getMenuCategory();
		$data['sub_category'] = $this->category_model->getSubCategory();
		$data['testimonials'] = $this->search_model->getTestimonials();
		$data['footernews'] = $this->search_model->getFooterNews();
		$data['footerreport'] = $this->search_model->getFooterReport();
		// echo "sdfds"; exit();
		//$data['info'] = $this->profile_model->getRecord($id);
		if(isset($_POST['submit']))
		{
			
		//$data['reportlist'] = $this->search_model->getSortReports();
		
			// $data['category'] = $this->search_model->getCategory();
			 $data['subcategory'] = $this->category_model->getSubCategory();
			 $data['featured'] = $this->category_model->getFeaturedReports();
			
			$data['include'] = 'search/sortlist';    
			$this->load->view('frontend/container',$data);
		}
		else
		{
			$data['include'] = 'home';
			$this->load->view('frontend/container', $data);
		}
	}
	
	/*  <!---------------------------------- Latest Report Pagination Starting----------------------------> */
    public function getSortList()
	{
		$sql=$_POST['sql'];
	    $mx=$_POST['mx'];
		$mn=$_POST['mn'];
		$co=$_POST['country'];
		$report_year=isset($_POST['report_year']);
		$sort=$_POST['sort'];
	
            if($_POST['page'])
            {
//            	print_r($_POST);
//	die();
            $page = $_POST['page'];
            $cur_page = $page;
            $page -= 1;
            $per_page = 5;
            $previous_btn = true;
            $next_btn = true;
            $first_btn = true;
            $last_btn = true;
            $start = $page * $per_page;
            

          //  $query_pag_data = $sql." LIMIT $start, $per_page";
		    $query_pag_data = $sql;
//            echo $query_pag_data;
//            die();
            $result_pag_data = $this->db->query($query_pag_data);
            $reportlist=$result_pag_data->result();
            
            $msg ='';
             if(count($reportlist)==0)
         {
         	
     $msg .='<div class="blog-post">
        
        <div class="two_thirds last">
          <h3><a href="#">No data Available</a></h3>
          <div class="clear"></div>
      </div>
      <div class="clear"></div>
      <div class="divider"></div>';
         }
         else 
         {
            for($i=0;$i<count($reportlist);$i++) 
            {
            $result=$this->db->query("select * from users where id='".$reportlist[$i]->publisher_id."'");
         	$row = $result->result();
             $publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));
             $result_1=$this->db->query("select r.*,c.category_name as category_name from report r INNER JOIN category c on(r.category_id = c.id) where page_urls='".$reportlist[$i]->page_urls."' AND r.status='1' LIMIT 0,1");
			 $reports=$result_1->result();


            	$msg .='<div class="blog-post">
        <div class="one_third marbot"> 
       <center> <a href='.base_url().'reports/index/'.$reportlist[$i]->page_urls.'>';
            	$rm=$reportlist[$i]->report_image;
                  	$msg.='<img src="'.base_url().'uploads/category_report_image/'.$reportlist[$i]->category_id.'.jpg" height="200" class="blog_img"  alt="" />';
           $msg .='</a></center>
         </div>
        <div class="two_thirds last marleftminus12">
          <h3 style = "text-transform: none;"><a style = "color:black;" style = "color:black;" href="'.base_url().'reports/index/'. $reportlist[$i]->page_urls.'">'. $reportlist[$i]->report_name.'</a></h3>
          <ul class="reportcatlist1">
            <li><i class="icon-user"></i>Published by <a href="'.base_url().'publisher/'.$publisher_name.'.html">'.$row[0]->display_name.'</a></li>
            <li><i class="icon-time"></i>'.date("M d Y", strtotime($reportlist[$i]->report_date)).'</li>';
            for($j=0;$j<count($reports);$j++)
	 		{
			$category_name =  str_replace(" ","-",strtolower($reports[$j]->category_name));
            		$msg .='<li><i class="icon-user"></i>Category <a href="'.base_url().'market-reports/'.$category_name.'.html">'.$reports[$j]->category_name.'</a></li>
            		<li><i class="icon-file"></i> Total Pages: '.$reports[$j]->no_pages.'</li>';
        	}
          $msg .='</ul>
          <div class="clear"></div>
           <a class="btn btn-primary" style="cursor: text;"> USD '. $reportlist[$i]->report_price.' </a> <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="btn btn-primary">View Report</a>
          </div>
      
      </div>
       <div class="clear"></div>
      <div class="divider"></div>';
            	
            	
               
                                 
            }
         }
            


            /* --------------------------------------------- */
            
 // $cid=;
            if($sort=='price')
		{
			$param="";
			if($mx != "" && $mn == "" )
			{
				//echo "into maxprice";
				//$maxprice=$_POST['maxprice'];
			//	$minprice= "0";
				$param.=" and report_price <= '".$mx."' ";
			} 
			else if($mn != "" && $mx == "")
			{
				//echo "into minprice";
				///$minprice=$_POST['minprice'];
				//	$minprice= "0";
				$param.=" and report_price >= '".$mn."' ";
			}
			
			else if($mx != "" && $mn != "")
			{
				//echo "into both";
				//$minprice=$_POST['minprice'];
				//$maxprice=$_POST['maxprice'];
				$param.=" and report_price BETWEEN '".$mn."' and'".$mx."' ";
			}
				
		   else 
			{
			$param.="";	
			}
			
		 if($co!=0)
			{
				//echo "into country";
				//$country=$_POST['country'];
				$param.=" and country_id='".$co."' ";
			}
		if($report_year!="")
			{
				//echo "into country";
				//$country=$_POST['country'];
				$param.=" and report_year='".$report_year."' ";
			}
			
			$que="select * from report where status='1'";
			$sqls=$que.$param;
			//echo $sql; die();
		
		$q=$this->db->query($sqls."order by report_price desc");
		  //  return $result->result();
		}
		else
		{
			$param="";
			if($mx != "" && $mn == "" )
			{
				//echo "into maxprice";
				//$maxprice=$_POST['maxprice'];
			//	$minprice= "0";
				$param.=" and report_price <= '".$mx."' ";
			} 
			else if($mn != "" && $mx == "")
			{
				//echo "into minprice";
				//$minprice=$_POST['minprice'];
				//	$minprice= "0";
				$param.=" and report_price >= '".$mn."' ";
			}
			
			else if($mx != "" && $mn != "")
			{
				//echo "into both";
				//$minprice=$_POST['minprice'];
				///$maxprice=$_POST['maxprice'];
				$param.=" and report_price BETWEEN '".$mn."' and'".$mx."' ";
			}
				
		   else 
			{
			$param.="";	
			}
			
		   if($co!=0)
			{
				//echo "into country";
				//$country=$_POST['country'];
				$param.=" and country_id='".$co."' ";
			}
			if($report_year!="")
			{
				//echo "into country";
				//$country=$_POST['country'];
				$param.=" and report_year='".$report_year."' ";
			}
			
			$que="select * from report where status='1'";
			$sqls=$que.$param;
			//echo $sql."order by report_date desc"; die();
		 $q=$this->db->query($sqls."order by report_date desc");
		//$result=$this->db->query("select * from report where status='1' order by report_date desc");
		//return $result->result();

            
}
         
             
             $ro=$q->result();   
                $count=$q->num_rows();   
                 $no_of_paginations=ceil($count / $per_page);
          //  $count = count($row_page_data);
          
            //echo $no_of_paginations = ceil($count / $per_page);
           // echo '<br>'.$cur_page;
// die();
//$no_of_paginations=2;
            /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
            if ($cur_page >= 5) {
                $start_loop = $cur_page - 3;
                if ($no_of_paginations > $cur_page + 3)
                {
                    $end_loop = $cur_page + 3;
                }
                else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) 
                {
                    $start_loop = $no_of_paginations - 6;
                    $end_loop = $no_of_paginations;
                } 
                else 
                {
                    $end_loop = $no_of_paginations;
                }
            } 
            else 
            {
                $start_loop = 1;
                if ($no_of_paginations > 7)
                {
                    $end_loop = 7;
                }
                else
                {
                    $end_loop = $no_of_paginations;
                }
            }
            /* ----------------------------------------------------------------------------------------------------------- */
            $msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";

            // FOR ENABLING THE FIRST BUTTON
            if ($first_btn && $cur_page > 1) 
            {
                $msg .= "<li p='1' class='active'>First</li>";
            } 
            else if ($first_btn) 
            {
                $msg .= "<li p='1' class='inactive'>First</li>";
            }

            // FOR ENABLING THE PREVIOUS BUTTON
            if ($previous_btn && $cur_page > 1) 
            {
                $pre = $cur_page - 1;
                $msg .= "<li p='$pre' class='active'>Previous</li>";
            } 
            else if ($previous_btn) 
            {
                $msg .= "<li class='inactive'>Previous</li>";
            }
            for ($i = $start_loop; $i <= $end_loop; $i++) 
            {

                if ($cur_page == $i)
                {
                    $msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
                }
                else
                {
                    $msg .= "<li p='$i' class='active'>{$i}</li>";
                }
            }

            // TO ENABLE THE NEXT BUTTON
            if ($next_btn && $cur_page < $no_of_paginations) 
            {
                $nex = $cur_page + 1;
                $msg .= "<li p='$nex' class='active'>Next</li>";
            } 
            else if ($next_btn) 
            {
                $msg .= "<li class='inactive'>Next</li>";
            }

            // TO ENABLE THE END BUTTON
            if ($last_btn && $cur_page < $no_of_paginations) 
            {
                $msg .= "<li p='$no_of_paginations' class='active'>Last</li>";
            } 
            else if ($last_btn) 
            {
                $msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
            }
            $goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
            $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
            $msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
            echo $msg;
            }
		
		
		
	}
	/*  <!---------------------------------- Latest Report Pagination Ending----------------------------> */
	
	
	
	/* <!---------------------------------- Search Pagination Starting---------------------------->*/
	public function getData()	
	{
	 $sql=$_POST['sql'];
	 $cid=$_POST['cid'];
	 $rk=$_POST['rk'];
 	 if($_POST['page'])	 
 	 {
 	 	 	$page = $_POST['page'];	 	
 	 	 	$cur_page = $page;	 	
 	 	 	$page -= 1;	 	
 	 	 	$per_page = 5;	 	
 	 	 	$previous_btn = true;	 	
 	 	 	$next_btn = true;	 	
 	 	 	$first_btn = true;	 	
 	 	 	$last_btn = true;	 	
 	 	 	$start = $page * $per_page;	 	
 	 	 	$query_pag_data = $sql." LIMIT $start, $per_page";	 		 	
 	 	 	$result_pag_data = $this->db->query($query_pag_data);	 	
 	 	 	$reportlist=$result_pag_data->result();	 	
 	 	 	$msg ='';	 	
 	 	 	if(count($reportlist)==0)	 	
 	 	 	{	 		
 	 	 	  $msg .='<article class="post reportsList">
 	 	 	  <div class="panel panel-default">            
 	 	 	  <div class="panel-body">            
 	 	 	  <h3 class="post-title reportsTitle"><a href="reports_details.php" class="transicion">No data available</a></h3>                      
 	 	 	  </div>           
 	 	 	  </div>            
 	 	 	  </article> ';	 	
 	 	 	}	
 	 	 	  else	 	
 	 	 	{	 		
 	 	 		for($i=0;$i<count($reportlist);$i++)	 		
 	 	 		{	 			
		 	 	 	$result=$this->db->query("select display_name from users where id='".$reportlist[$i]->publisher_id."'");	 			
		 	 	 	$row = $result->result();				
		 	 	 	$publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));	 			
 	 	 	      $result_1=$this->db->query("select r.no_pages,r.category_id,c.category_name as category_name from report r INNER JOIN category c on(r.category_id = c.id) where page_urls='".$reportlist[$i]->page_urls."' AND r.status='1' LIMIT 0,1");				
 	 	 	      $reports=$result_1->result();	 					 			
 	 	 	      $msg .='<article class="post reportsList">            
 	 	 	      <div class="panel panel-default">            
 	 	 	      <div class="panel-body"> <div class="row">					
 	 	 	      <div class="col-lg-3 col-sm-3">					  
 	 	 	       <div class="reportsImg">							   
 	 	 	       <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'"> <img src="'.base_url().'uploads/category_report_image/'.$reportlist[$i]->category_id.'.jpg" style="height: 165px;" class="img-post img-responsive" alt="'. $reportlist[$i]->report_name.'">
 	 	 	       </a>					  
 	 	 	       </div>					
 	 	 	       </div>					
 	 	 	       <div class="col-lg-9 col-sm-9 post-content">
                   <h3 class="post-title reportsTitle">			
                   <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="transicion">'. $reportlist[$i]->report_name.'</a></h3>										
                <div class="reportsContent">			
              <table class="table table-bordered">
		       <tr>
		           <td> <i class="fa fa-user"></i> &nbsp; Published by <a href="'.base_url().'publisher/'.$publisher_name.'.html">'.$row[0]->display_name.'</a>
		           </td>
			       <td> <i class="fa fa-calendar"></i> &nbsp; '.date("M d Y", strtotime($reportlist[$i]->report_date)).'</td>
		       </tr>
		        <tr>';
		           for($j=0;$j<count($reports);$j++)
	                   {
	                $category_name =  str_replace(" ","-",strtolower($reports[$j]->category_name));
	                 $msg .= '<td><i class="fa fa-user"></i> &nbsp; Category <a href="'.base_url().'market-reports/'.$category_name.'.html"> '.$reports[$j]->category_name.'</a></td>
	              <td><i class="fa fa-file"></i> &nbsp;  Total Pages: '.$reports[$j]->no_pages.'</td>';
                     }
				$msg .='
		    	 </tr>
		    	 </table>
                 <a class="btn btn-primary" style="cursor: text;"> USD '. $reportlist[$i]->report_price.' </a> <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="btn btn-primary">View Report</a>
                 </div>				  				   				   			
                 </div>										
                 </div>					
                 </div>				   
                 </div>					
                 </article> ';		 	 				 				 		
			}	
	   	}	 	
    	if( (!empty($cid)) || ('0' != ($cid)) )			
    	{			
    		$q=$this->db->query("select id from report where (report_name like '%$rk%' or report_description like '%$rk%' or report_content like '%$rk%') AND (status='1') AND (category_id = '$cid') LIMIT 0,100");			
    	}			
    	else 			
    	{				
    	 $q=$this->db->query("select id from report where (report_name like '%$rk%' or report_description like '%$rk%' or report_content like '%$rk%') AND (status='1') LIMIT 0,100");			
    	}	 	
    	$ro=$q->result();	 	
    	$count=$q->num_rows();	 	
    	$no_of_paginations=ceil($count / $per_page);

	    /* ---------------Calculating the starting and endign values for the loop----------------------------------- */	 	

    	if ($cur_page >= 5) 
    	{	 		
    		$start_loop = $cur_page - 3;	 		
    		if ($no_of_paginations > $cur_page + 3)	 		
    			{	 			
    				$end_loop = $cur_page + 3;	 		
    			}	 		
    			else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6)	 		
    				{	 			
    					$start_loop = $no_of_paginations - 6;	 			$end_loop = $no_of_paginations;	 
    				}	 		
    				else	 		
    				{	 			
                     $end_loop = $no_of_paginations;	 		
                 }	 	
             }	 	
             else	 	
             	{	 		
             		$start_loop = 1;	 		
             		if ($no_of_paginations > 7)	 		
             			{	 			
             			$end_loop = 7;	 		
             		}	 		
             		else	 		
             			{	 			
             				$end_loop = $no_of_paginations;	 		
             			}	 	
             		}	 
			/* ----------------------------------------------------------------------------------------------------------- */	 	

	       $msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";	 	
	       if ($first_btn && $cur_page > 1)	 	
	     	{	 		
	       $msg .= "<li p='1' onclick = 'loadData(1);' class='active'>First</li>";	 	
	         }	 
	        else if ($first_btn)	 	
	        {	 		
	       $msg .= "<li p='1' onclick = 'loadData(1);' class='inactive'>First</li>";	 	
	         }	 		
 	        if ($previous_btn && $cur_page > 1)	 	
 	        {	 		
 	        $pre = $cur_page - 1;	 	
	        $msg .= "<li p='$pre' onclick = 'loadData($pre);' class='active'>Previous</li>";
	    }	 	
		else if ($previous_btn)	 	
		{	 		
			$msg .= "<li class='inactive'>Previous</li>";	 	
		}	 
       for ($i = $start_loop; $i <= $end_loop; $i++)	 	
       	{	 		
	       	if ($cur_page == $i)	 		
	       	{	 			
	       	$msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>
	       	{$i}</li>";	 		
	       }	 		
	       else	 		
	       {	 			
	       	$msg .= "<li p='$i' onclick = 'loadData($i);'>{$i}</li>";	 		
	       }	 	
	    }	 	 	
	    if ($next_btn && $cur_page < $no_of_paginations)	 	
	    {	 		
	   		 $nex = $cur_page + 1;	 		
	   		 $msg .= "<li p='$nex' onclick = 'loadData($nex);' class='active'>Next</li>";	 	
	    }	 	
	    else if ($next_btn)	 	
	    {	 		
	    	$msg .= "<li class='inactive'>Next</li>";	 	
	    }	 		
	    if ($last_btn && $cur_page < $no_of_paginations)	 	
	    {	 		
	      $msg .= "<li p='$no_of_paginations' onclick = 'loadData($no_of_paginations);' class='active'>Last</li>";	 	
	   }	 	
	  	else if ($last_btn)	 	
	  	{	 		
	  		$msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";	 	
	  	}	 	
	  	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";	 	
	  	$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";	 	$msg = $msg . "</ul>" . $total_string . "</div></div>";  	
	  	echo $msg;	
	 }	
 	}
		
 public function getSearchList()
	{
		$data['country_list'] = $this->search_model->getCountryList();
	 $sql=$_POST['sql'];
	 $cid=$_POST['cid'];
	 $rk=$_POST['rk'];
	
            if($_POST['page'])
            {
            $page = $_POST['page'];
            $cur_page = $page;
            $page -= 1;
            $per_page = 5;
            $previous_btn = true;
            $next_btn = true;
            $first_btn = true;
            $last_btn = true;
            $start = $page * $per_page;
            

            $query_pag_data = $sql." LIMIT $start, $per_page";
//            echo $query_pag_data;
//            die();
            $result_pag_data = $this->db->query($query_pag_data);
            $reportlist=$result_pag_data->result();
            
            $msg ='';
             if(count($reportlist)==0)
         {
         	
     $msg .='<div class="blog-post" style="" align="center">
	  <h3>Send Us A Query !</h3>
      <p>Please fill in the details and we will get back to you within 24 hours</p>
      <div class="clear"></div>
      
      <form id="contactform" method="post" action="'. base_url().'contact/send_contact">
        <table>
          <tr>
            <td><input type="text" id="name" name="name" placeholder="Your Name"  pattern="^[a-zA-Z ]+$" required/></td>
          </tr>
          <tr>
            <td><input type="email" id="email" name="email" placeholder="Your Email Address"  required/></td>
          </tr>
		   <tr>
            <td><input type="text" id="name" name="phone" placeholder="Your Phone Number"  pattern="^[0-9+- ]+$" maxlength="15" required/></td>
          </tr>
		  <tr>
            <td>
			<select name = "country">
			<option>---Select Country---</option>';
			
			for($c = 0; $c<count($data['country_list']); $c++)
			{
				
				$msg .='<option value = "'.$data['country_list'][$c]->country_name.'">'. $data['country_list'][$c]->country_name.'</option>';
				
			}
			
			$msg .='</select>
			</td>
          </tr>
		  <tr>
            <td><input type="text" id="name" name="company" placeholder="Your Company"  /></td>
          </tr>
          <tr>
            <td><textarea id="message" placeholder="Your Message" name="comment" rows="5" cols="20" required></textarea></td>
          </tr>
           
          
          
         <tr>
            <td>
           <center> <input type="submit" value="Send" id="send" name="submit" class="button small"  /></center>
<!--            <input type="reset" value="Reset" class="button small" onclick="return chack_captcha()" />-->
            </td>
          </tr>
        </table>
        
      </form>
       </div>';
	  
         }
         else 
         {
            for($i=0;$i<count($reportlist);$i++) 
            {
            $result=$this->db->query("select * from users where id='".$reportlist[$i]->publisher_id."'");
         	$row = $result->result();
                $publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));
         	$result_1=$this->db->query("select r.*,c.category_name as category_name from report r INNER JOIN category c on(r.category_id = c.id) where page_urls='".$reportlist[$i]->page_urls."' AND r.status='1' LIMIT 0,1");
			$reports=$result_1->result();

            	$msg .='<div class="blog-post">
        <div class="one_fifth marbot"> 
        <center><a href='.base_url().'reports/index/'.$reportlist[$i]->page_urls.'>';
            	$rm=$reportlist[$i]->report_image;
        
        $msg .='<img src="'.base_url().'uploads/category_report_image/'.$reportlist[$i]->category_id.'.jpg" height="200" class="blog_img"  alt="" />';
                  
        $msg .=' </a></center>
         </div>
        <div class="two_thirds last marleftminus12">
          <h3 style = "text-transform: none;"><a style = "color:black;" href="'.base_url().'reports/index/'. $reportlist[$i]->page_urls.'">'. $reportlist[$i]->report_name.'</a></h3>
          <ul class="reportcatlist1">
            <li><i class="icon-user"></i>Published by <a href="'.base_url().'publisher/'.$publisher_name.'.html">'.$row[0]->display_name.'</a></li>
            <li><i class="icon-time"></i>'.date("M d Y", strtotime($reportlist[$i]->report_date)).'</li>';
            for($j=0;$j<count($reports);$j++)
	 		{
			$category_name =  str_replace(" ","-",strtolower($reports[$j]->category_name));
            		$msg .='<li><i class="icon-user"></i>Category <a href="'.base_url().'market-reports/'.$category_name.'.html">'.$reports[$j]->category_name.'</a></li>
            		<li><i class="icon-file"></i> Total Pages: '.$reports[$j]->no_pages.'</li>';
        	}
          $msg .='</ul>
          <div class="clear"></div>
           <a class="btn btn-primary" style="cursor: text;"> USD '. $reportlist[$i]->report_price.' </a> <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="btn btn-primary">View Report</a>
       
          </div>
      
      </div>
       <div class="clear"></div>
      <div class="divider"></div>';
            	
            	
               
                                 
            }
         }
            


            /* --------------------------------------------- */
  if( (!empty($cid)) || ('0' != ($cid)) )
{
$q=$this->db->query("select * from report where (report_name like '%$rk%' or report_description like '%$rk%' or report_content like '%$rk%') AND (status='1') AND (category_id = '$cid') LIMIT 0,100");
}
else 
{
	$q=$this->db->query("select * from report where (report_name like '%$rk%' or report_description like '%$rk%' or report_content like '%$rk%') AND (status='1') LIMIT 0,100");
}
  

  
           // $q=$this->db->query("select * from report where category_id = '$category_id' and status='1' order by id DESC");
             $ro=$q->result();   
                $count=$q->num_rows();   
                 $no_of_paginations=ceil($count / $per_page);
          //  $count = count($row_page_data);
          
            //echo $no_of_paginations = ceil($count / $per_page);
           // echo '<br>'.$cur_page;
// die();
//$no_of_paginations=2;
            /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
            if ($cur_page >= 5) {
                $start_loop = $cur_page - 3;
                if ($no_of_paginations > $cur_page + 3)
                {
                    $end_loop = $cur_page + 3;
                }
                else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) 
                {
                    $start_loop = $no_of_paginations - 6;
                    $end_loop = $no_of_paginations;
                } 
                else 
                {
                    $end_loop = $no_of_paginations;
                }
            } 
            else 
            {
                $start_loop = 1;
                if ($no_of_paginations > 7)
                {
                    $end_loop = 7;
                }
                else
                {
                    $end_loop = $no_of_paginations;
                }
            }
			
            /* ----------------------------------------------------------------------------------------------------------- */
            $msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";
		    if(count($reportlist)>=1)
         {
            // FOR ENABLING THE FIRST BUTTON
            if ($first_btn && $cur_page > 1) 
            {
                $msg .= "<li p='1' class='active'>First</li>";
            } 
            else if ($first_btn) 
            {
                $msg .= "<li p='1' class='inactive'>First</li>";
            }

            // FOR ENABLING THE PREVIOUS BUTTON
            if ($previous_btn && $cur_page > 1) 
            {
                $pre = $cur_page - 1;
                $msg .= "<li p='$pre' class='active'>Previous</li>";
            } 
            else if ($previous_btn) 
            {
                $msg .= "<li class='inactive'>Previous</li>";
            }
            for ($i = $start_loop; $i <= $end_loop; $i++) 
            {

                if ($cur_page == $i)
                {
                    $msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
                }
                else
                {
                    $msg .= "<li p='$i' class='active'>{$i}</li>";
                }
            }

            // TO ENABLE THE NEXT BUTTON
            if ($next_btn && $cur_page < $no_of_paginations) 
            {
                $nex = $cur_page + 1;
                $msg .= "<li p='$nex' class='active'>Next</li>";
            } 
            else if ($next_btn) 
            {
                $msg .= "<li class='inactive'>Next</li>";
            }

            // TO ENABLE THE END BUTTON
            if ($last_btn && $cur_page < $no_of_paginations) 
            {
                $msg .= "<li p='$no_of_paginations' class='active'>Last</li>";
            } 
            else if ($last_btn) 
            {
                $msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
            }
			}
            $goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
            $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
            $msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
            echo $msg;
            }
			

		
		
	}
	/* <!---------------------------------- Search Report End---------------------------->*/
	
	public function NewAutoComplete(){
		$q = strtolower($_GET["q"]);
			if (!$q) {return;}
			$sql = "SELECT * FROM report where report_name LIKE '%$q%'";
			$row = $this->db->query($sql);
			$result =  $row->result();
		
			for($i = 0; $i < couint($result);$i++ ) {
				$cname = $result[0]->report_name;
				echo "$cname\n";
			}
	}

	public function AutoComplete() {

			$key =trim($_REQUEST['search_keyword']);
		
			if($key !== '')
			{
				$data['reports'] = $this->report_model->getReportsByKeyword($key);
			
				echo  json_encode($data);
			}
		}
	public function getSearchKeywordWiseData()
	{
	 	if($_POST['page'])
	 	{
		 	$page = $_POST['page'];
		 	$search_keyword=$_POST['search_keyword'];
		 	$categoryid=$_POST['category_id'];
		 	$cur_page = $page;
		 	$page -= 1;
		 	$per_page = 10;
		 	$previous_btn = true;
		 	$next_btn = true;
		 	$first_btn = true;
		 	$last_btn = true;
		 	$start = $page * $per_page;
		 	$sqlwhere='';
		 	$sqlwhere.=(!empty($search_keyword)) ? " report_name like '%$search_keyword%' and " :"1 and ";
		 	$sqlwhere.=(!empty($categoryid)) ? " category_id = '$categoryid' " :" 1 ";
		 	
		 	$sql="select id,report_name,country_id,category_id,publisher_id,sub_category_id,report_date,featured_report,page_urls,country_id,report_price,multiple_price,global_price,corporate_price,report_status,is_requested_for_delete from report where ".$sqlwhere." order by report_date DESC";
		 	
		 	$query_pag_data = $sql." LIMIT $start, $per_page";
			$result_pag_data = $this->db->query($query_pag_data);
	 		$reportlist=$result_pag_data->result();
	 		$msg ='';
		 	if(count($reportlist)==0)
		 	{

		 		$msg .='<article class="post reportsList">
	            <div class="panel panel-default">
	            <div class="panel-body">
	            <h3 class="post-title reportsTitle">No data available</h3>
	            </div>
	           </div>
	            </article> ';
		 	}
	 	else
	 	{
	 		for($i=0;$i<count($reportlist);$i++)
	 		{
	 			$result=$this->db->query("select display_name from users where id='".$reportlist[$i]->publisher_id."'");
	 			$row = $result->result();
				$publisher_name =  str_replace(" ","-",strtolower($row[0]->display_name));
	 			$result_1=$this->db->query("select no_pages,category_id from report where page_urls='".$reportlist[$i]->page_urls."' LIMIT 0,1");
				$reports=$result_1->result();
	 				
	 			$msg .='<article class="post reportsList">
            			<div class="panel panel-default">
            			<div class="panel-body">
          				<div class="row">
						<div class="col-lg-3 col-sm-3">
					   	<div class="reportsImg">
						 <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'"> <img src="'.base_url().'uploads/category_report_image/'.$reportlist[$i]->category_id.'.jpg" style="height: 165px;" class="img-post img-responsive" alt="'. $reportlist[$i]->report_name.'"></a>
					  </div>
					</div>
					<div class="col-lg-9 col-sm-9 post-content">
					 <h3 class="post-title reportsTitle">
						<a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="transicion">'. $reportlist[$i]->report_name.'</a>
					</h3>
					<div class="reportsContent">
					<table class="table table-bordered">
					   <tr>
					       <td> <i class="fa fa-user"></i> &nbsp; Published by <a href="'.base_url().'publisher/'.$publisher_name.'.html">'.$row[0]->display_name.'</a></td>
						   <td><i class="fa fa-calendar"></i> &nbsp; '.date("M d Y", strtotime($reportlist[$i]->report_date)).'</td>
					   </tr>
					   <tr>
					   ';
				$category_name_array = $this->report_model->getCategoryById($reports[0]->category_id);
				$category_name =  str_replace(" ","-",strtolower($category_name_array['category_name']));
				$msg .= '<td><i class="fa fa-user"></i> &nbsp; Category <a href="'.base_url().'market-reports/'.$category_name.'.html"> '.$category_name_array['category_name'].'</a></td>
				<td><i class="fa fa-file"></i> &nbsp;  Total Pages: '.$reports[0]->no_pages.'</td>';
				$msg .='
				  	</tr>
					</table>
				   	 <a class="btn btn-primary" style="cursor: text;"> USD '. $reportlist[$i]->report_price.' </a> <a href="'.base_url().'reports/index/'.$reportlist[$i]->page_urls.'" class="btn btn-primary">View Report
				   	 </a>
					</div>
					</div>
					</div>
					</div>
				   </div>
					</article> ';
	 		}
	 	}

	 	/* --------------------------------------------- */
	 		
		$sqlcount="select count(id) as cnt from report where ".$sqlwhere." order by report_date desc";
		$q=$this->db->query($sqlcount);
		$count=$q->row()->cnt;
		$no_of_paginations=ceil($count / $per_page);
	 
	 	/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
	 	if ($cur_page >= 10) {
	 		$start_loop = $cur_page - 3;
	 		if ($no_of_paginations > $cur_page + 3)
	 		{
	 			$end_loop = $cur_page + 3;
	 		}
	 		else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6)
	 		{
	 			$start_loop = $no_of_paginations - 6;
	 			$end_loop = $no_of_paginations;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	else
	 	{
	 		$start_loop = 1;
	 		if ($no_of_paginations > 7)
	 		{
	 			$end_loop = 7;
	 		}
	 		else
	 		{
	 			$end_loop = $no_of_paginations;
	 		}
	 	}
	 	/* ----------------------------------------------------------------------------------------------------------- */
	 	$msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";

	 	// FOR ENABLING THE FIRST BUTTON
	 	if ($first_btn && $cur_page > 1)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='active'>First</li>";
	 	}
	 	else if ($first_btn)
	 	{
	 		$msg .= "<li p='1' onclick = 'loadData(1);' class='inactive'>First</li>";
	 	}

	 	// FOR ENABLING THE PREVIOUS BUTTON
	 	if ($previous_btn && $cur_page > 1)
	 	{
	 		$pre = $cur_page - 1;
	 		$msg .= "<li p='$pre' onclick = 'loadData($pre);' class='active'>Previous</li>";
	 	}
	 	else if ($previous_btn)
	 	{
	 		$msg .= "<li class='inactive'>Previous</li>";
	 	}
	 	for ($i = $start_loop; $i <= $end_loop; $i++)
	 	{

	 		if ($cur_page == $i)
	 		{
	 			$msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
	 		}
	 		else
	 		{
	 			$msg .= "<li p='$i' onclick = 'loadData($i);'>{$i}</li>";
	 		}
	 	}

	 	// TO ENABLE THE NEXT BUTTON
	 	if ($next_btn && $cur_page < $no_of_paginations)
	 	{
	 		$nex = $cur_page + 1;
	 		$msg .= "<li p='$nex' onclick = 'loadData($nex);' class='active'>Next</li>";
	 	}
	 	else if ($next_btn)
	 	{
	 		$msg .= "<li class='inactive'>Next</li>";
	 	}

	 	// TO ENABLE THE END BUTTON
	 	if ($last_btn && $cur_page < $no_of_paginations)
	 	{
	 		$msg .= "<li p='$no_of_paginations' onclick = 'loadData($no_of_paginations);' class='active'>Last</li>";
	 	}
	 	else if ($last_btn)
	 	{
	 		$msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
	 	}
	 	$goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
	 	$total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
	 	$msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
	 	echo $msg;
	 }
	}

}
