<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Search extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();	

		$this->load->database();
		$this->load->model("category/category_model");
		$this->load->model('search_model');
		$this->load->model('reports/report_model');
		//$this->load->model('profile_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

	}

	//==================== all page session check =====================

	public function Checklogin()
	{
		if ($this->session->userdata('admin_email') == '')
		{
			redirect('siteadmin/');
		}
	}

	public function index()
	{
		//$this->Checklogin();
		
		if(isset($_REQUEST['submit']))
		{
			
			$key = $_REQUEST['search_keyword'];
			$cat = $_REQUEST['category'];
			
			$data['report_key'] =$_REQUEST['search_keyword'];
			$data['cat'] =$_REQUEST['category'];
			
			if( (!empty($_REQUEST['category'])) || ('0' != ($_REQUEST['category'])) ){
			
			$data['res_report'] = $this->search_model->getKetCatreports($key,$cat); 
			}
			else {
			$data['res_report'] = $this->search_model->getKeyreports($key); 
			}
		
			$data['publisher'] = $this->search_model->getPublisher();
        	//$data['reports'] = $this->search_model->getReports($report_id);
            $data['category'] = $this->search_model->getCategory();
            $data['subcategory'] = $this->search_model->getSubCategory();
			$data['featured'] = $this->search_model->getFeaturedReports();
			$data['testimonials'] = $this->search_model->getTestimonials();
			$data['footernews'] = $this->search_model->getFooterNews();
		    $data['footerreport'] = $this->search_model->getFooterReport();
			$data['include'] = 'search/searchlist';    
			$this->load->view('frontend/container',$data);
		}
		
		
		/* $data['result'] = $this->profile_model->getall();
		$data['include'] = 'profile/view_profile';
		$data['admin_section'] = 'view_profile';
		$this->load->view('frontend/container', $data);  */
	}
public function AutoCompleteNew(){
			$q=$_POST['search'];
			//echo "select report_name from report where report_name like '%$q%';"; die();
			$r = $this->db->query("select report_name from report where report_name like '%$q%' ");
			$result = $r->result();
			for($i = 0; $i < count($result); $i++)
			{
			$report_name=$result[$i]->report_name;
			echo '<div class="show" align="left"><span class="name">'.$report_name.'</span> </div>';
			}
}
public function auto_search()
{

			$key = $_POST['search_keyword'];
			$cat = $_POST['category'];
			
			$data['report_key'] =$_POST['search_keyword'];
			$data['cat'] =$_POST['category'];
			
			if( (!empty($_POST['category'])) || ('0' != ($_POST['category'])) ){
			
			$data['res_report'] = $this->search_model->getKetCatreports_key($key); 
			}
			else {
			$data['res_report'] = $this->search_model->getKetCatreports_key($key); 
			}
		
			$data['publisher'] = $this->search_model->getPublisher();
        	//$data['reports'] = $this->search_model->getReports($report_id);
            $data['category'] = $this->search_model->getCategory();
            $data['subcategory'] = $this->search_model->getSubCategory();
			$data['featured'] = $this->search_model->getFeaturedReports();
			$data['testimonials'] = $this->search_model->getTestimonials();
			$data['footernews'] = $this->search_model->getFooterNews();
		    $data['footerreport'] = $this->search_model->getFooterReport();
			echo $data['res_report'][0]->page_urls;
			//$data['include'] = 'search/searchlist';    


			//$this->load->view('frontend/container',$data);
}


	public function get_sort()
	{
		$data['mx']=$_POST['maxprice'];
		$data['mn']=$_POST['minprice'];
		$data['co']=$_POST['country'];
		$data['report_year']=$_POST['report_year'];
		$data['sort']=$_POST['sort'];
		
		$data['category'] = $this->category_model->getMenuCategory();
		$data['sub_category'] = $this->category_model->getSubCategory();
		$data['testimonials'] = $this->search_model->getTestimonials();
		$data['footernews'] = $this->search_model->getFooterNews();
		$data['footerreport'] = $this->search_model->getFooterReport();
		// echo "sdfds"; exit();
		//$data['info'] = $this->profile_model->getRecord($id);
		if(isset($_POST['submit']))
		{
			
		//$data['reportlist'] = $this->search_model->getSortReports();
		
			// $data['category'] = $this->search_model->getCategory();
			 $data['subcategory'] = $this->category_model->getSubCategory();
			 $data['featured'] = $this->category_model->getFeaturedReports();
			
			$data['include'] = 'search/sortlist';    
			$this->load->view('frontend/container',$data);
		}
		else
		{
			$data['include'] = 'home';
			$this->load->view('frontend/container', $data);
		}
	}
	
	/*  <!---------------------------------- Latest Report Pagination Starting----------------------------> */
    public function getSortList()
	{
		$sql=$_POST['sql'];
	    $mx=$_POST['mx'];
		$mn=$_POST['mn'];
		$co=$_POST['country'];
		$report_year=isset($_POST['report_year']);
		$sort=$_POST['sort'];
	
            if($_POST['page'])
            {
//            	print_r($_POST);
//	die();
            $page = $_POST['page'];
            $cur_page = $page;
            $page -= 1;
            $per_page = 5;
            $previous_btn = true;
            $next_btn = true;
            $first_btn = true;
            $last_btn = true;
            $start = $page * $per_page;
            

          //  $query_pag_data = $sql." LIMIT $start, $per_page";
		    $query_pag_data = $sql;
//            echo $query_pag_data;
//            die();
            $result_pag_data = $this->db->query($query_pag_data);
            $reportlist=$result_pag_data->result();
            
            $msg ='';
             if(count($reportlist)==0)
         {
         	
     $msg .='<div class="blog-post">
        
        <div class="two_thirds last">
          <h3><a href="#">No data Available</a></h3>
          <div class="clear"></div>
      </div>
      <div class="clear"></div>
      <div class="divider"></div>';
         }
         else 
         {
            for($i=0;$i<count($reportlist);$i++) 
            {
            $result=$this->db->query("select * from users where id='".$reportlist[$i]->publisher_id."'");
         	$row = $result->result();
             
             $result_1=$this->db->query("select r.*,c.category_name as category_name from report r INNER JOIN category c on(r.category_id = c.id) where page_urls='".$reportlist[$i]->page_urls."' AND r.status='1' LIMIT 0,1");
			 $reports=$result_1->result();


            	$msg .='<div class="blog-post">
        <div class="one_third"> 
        <center><a href='.base_url().'reports/index/'.$reportlist[$i]->page_urls.'>';
            	$rm=$reportlist[$i]->report_image;
                  	$msg.='<img src="'. base_url().'themes/frontend/images/report_default.jpg" height="200" class="blog_img"  alt="" />';
           $msg .='</a></center>
         </div>
        <div class="two_thirds last" style="margin-left: -12%;">
          <h3 style = "text-transform: none; height: 0px;"><a style = "color:black;" style = "color:black;" href="'.base_url().'reports/index/'. $reportlist[$i]->page_urls.'">'. $reportlist[$i]->report_name.'</a></h3>
          <ul style="width: 50%;margin-top: 5%;">
            <li><i class="icon-user"></i>Published by <a href="#">'.$row[0]->display_name.'</a></li>
            <li><i class="icon-time"></i>'.date("M d Y", strtotime($reportlist[$i]->report_date)).'</li>';
            for($j=0;$j<count($reports);$j++)
	 		{
            		$msg .='<li><i class="icon-user"></i>Category <a href="'.base_url().'reports/report/getCategoryReport/'.$reports[$j]->category_id.'">'.$reports[$j]->category_name.'</a></li>
            		<li><i class="icon-file"></i> Total Pages: '.$reports[$j]->no_pages.'</li>';
        	}
          $msg .='</ul>
          <div class="clear"></div>
           <p class="price1"> USD '. $reportlist[$i]->report_price.' </p>
          </div>
      
      </div>
       <div class="clear"></div>
      <div class="divider"></div>';
            	
            	
               
                                 
            }
         }
            


            /* --------------------------------------------- */
            
 // $cid=;
            if($sort=='price')
		{
			$param="";
			if($mx != "" && $mn == "" )
			{
				//echo "into maxprice";
				//$maxprice=$_POST['maxprice'];
			//	$minprice= "0";
				$param.=" and report_price <= '".$mx."' ";
			} 
			else if($mn != "" && $mx == "")
			{
				//echo "into minprice";
				///$minprice=$_POST['minprice'];
				//	$minprice= "0";
				$param.=" and report_price >= '".$mn."' ";
			}
			
			else if($mx != "" && $mn != "")
			{
				//echo "into both";
				//$minprice=$_POST['minprice'];
				//$maxprice=$_POST['maxprice'];
				$param.=" and report_price BETWEEN '".$mn."' and'".$mx."' ";
			}
				
		   else 
			{
			$param.="";	
			}
			
		 if($co!=0)
			{
				//echo "into country";
				//$country=$_POST['country'];
				$param.=" and country_id='".$co."' ";
			}
		if($report_year!="")
			{
				//echo "into country";
				//$country=$_POST['country'];
				$param.=" and report_year='".$report_year."' ";
			}
			
			$que="select * from report where status='1'";
			$sqls=$que.$param;
			//echo $sql; die();
		
		$q=$this->db->query($sqls."order by report_price desc");
		  //  return $result->result();
		}
		else
		{
			$param="";
			if($mx != "" && $mn == "" )
			{
				//echo "into maxprice";
				//$maxprice=$_POST['maxprice'];
			//	$minprice= "0";
				$param.=" and report_price <= '".$mx."' ";
			} 
			else if($mn != "" && $mx == "")
			{
				//echo "into minprice";
				//$minprice=$_POST['minprice'];
				//	$minprice= "0";
				$param.=" and report_price >= '".$mn."' ";
			}
			
			else if($mx != "" && $mn != "")
			{
				//echo "into both";
				//$minprice=$_POST['minprice'];
				///$maxprice=$_POST['maxprice'];
				$param.=" and report_price BETWEEN '".$mn."' and'".$mx."' ";
			}
				
		   else 
			{
			$param.="";	
			}
			
		   if($co!=0)
			{
				//echo "into country";
				//$country=$_POST['country'];
				$param.=" and country_id='".$co."' ";
			}
			if($report_year!="")
			{
				//echo "into country";
				//$country=$_POST['country'];
				$param.=" and report_year='".$report_year."' ";
			}
			
			$que="select * from report where status='1'";
			$sqls=$que.$param;
			//echo $sql."order by report_date desc"; die();
		 $q=$this->db->query($sqls."order by report_date desc");
		//$result=$this->db->query("select * from report where status='1' order by report_date desc");
		//return $result->result();

            
}
         
             
             $ro=$q->result();   
                $count=$q->num_rows();   
                 $no_of_paginations=ceil($count / $per_page);
          //  $count = count($row_page_data);
          
            //echo $no_of_paginations = ceil($count / $per_page);
           // echo '<br>'.$cur_page;
// die();
//$no_of_paginations=2;
            /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
            if ($cur_page >= 5) {
                $start_loop = $cur_page - 3;
                if ($no_of_paginations > $cur_page + 3)
                {
                    $end_loop = $cur_page + 3;
                }
                else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) 
                {
                    $start_loop = $no_of_paginations - 6;
                    $end_loop = $no_of_paginations;
                } 
                else 
                {
                    $end_loop = $no_of_paginations;
                }
            } 
            else 
            {
                $start_loop = 1;
                if ($no_of_paginations > 7)
                {
                    $end_loop = 7;
                }
                else
                {
                    $end_loop = $no_of_paginations;
                }
            }
            /* ----------------------------------------------------------------------------------------------------------- */
            $msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";

            // FOR ENABLING THE FIRST BUTTON
            if ($first_btn && $cur_page > 1) 
            {
                $msg .= "<li p='1' class='active'>First</li>";
            } 
            else if ($first_btn) 
            {
                $msg .= "<li p='1' class='inactive'>First</li>";
            }

            // FOR ENABLING THE PREVIOUS BUTTON
            if ($previous_btn && $cur_page > 1) 
            {
                $pre = $cur_page - 1;
                $msg .= "<li p='$pre' class='active'>Previous</li>";
            } 
            else if ($previous_btn) 
            {
                $msg .= "<li class='inactive'>Previous</li>";
            }
            for ($i = $start_loop; $i <= $end_loop; $i++) 
            {

                if ($cur_page == $i)
                {
                    $msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
                }
                else
                {
                    $msg .= "<li p='$i' class='active'>{$i}</li>";
                }
            }

            // TO ENABLE THE NEXT BUTTON
            if ($next_btn && $cur_page < $no_of_paginations) 
            {
                $nex = $cur_page + 1;
                $msg .= "<li p='$nex' class='active'>Next</li>";
            } 
            else if ($next_btn) 
            {
                $msg .= "<li class='inactive'>Next</li>";
            }

            // TO ENABLE THE END BUTTON
            if ($last_btn && $cur_page < $no_of_paginations) 
            {
                $msg .= "<li p='$no_of_paginations' class='active'>Last</li>";
            } 
            else if ($last_btn) 
            {
                $msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
            }
            $goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
            $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
            $msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
            echo $msg;
            }
		
		
		
	}
	/*  <!---------------------------------- Latest Report Pagination Ending----------------------------> */
	
	
	
	/* <!---------------------------------- Search Pagination Starting---------------------------->*/
	
 public function getSearchList()
	{
		$data['country_list'] = $this->search_model->getCountryList();
	 $sql=$_POST['sql'];
	 $cid=$_POST['cid'];
	 $rk=$_POST['rk'];
	
            if($_POST['page'])
            {
            $page = $_POST['page'];
            $cur_page = $page;
            $page -= 1;
            $per_page = 5;
            $previous_btn = true;
            $next_btn = true;
            $first_btn = true;
            $last_btn = true;
            $start = $page * $per_page;
            

            $query_pag_data = $sql." LIMIT $start, $per_page";
//            echo $query_pag_data;
//            die();
            $result_pag_data = $this->db->query($query_pag_data);
            $reportlist=$result_pag_data->result();
            
            $msg ='';
             if(count($reportlist)==0)
         {
         	
     $msg .='<div class="blog-post" style="margin-left:100px;" align="center">
	  <h3>Send Us A Query !</h3>
      <p>Please fill in the details and we will get back to you within 24 hours</p>
      <div class="clear"></div>
      
      <form id="contactform" method="post" action="'. base_url().'contact/send_contact">
        <table>
          <tr>
            <td><input type="text" id="name" name="name" placeholder="Your Name"  pattern="^[a-zA-Z ]+$" required/></td>
          </tr>
          <tr>
            <td><input type="email" id="email" name="email" placeholder="Your Email Address"  required/></td>
          </tr>
		   <tr>
            <td><input type="text" id="name" name="phone" placeholder="Your Phone Number"  pattern="^[0-9+- ]+$" maxlength="15" required/></td>
          </tr>
		  <tr>
            <td>
			<select name = "country">
			<option>---Select Country---</option>';
			
			for($c = 0; $c<count($data['country_list']); $c++)
			{
				
				$msg .='<option value = "'.$data['country_list'][$c]->country_name.'">'. $data['country_list'][$c]->country_name.'</option>';
				
			}
			
			$msg .='</select>
			</td>
          </tr>
		  <tr>
            <td><input type="text" id="name" name="company" placeholder="Your Company"  /></td>
          </tr>
          <tr>
            <td><textarea id="message" placeholder="Your Message" name="comment" rows="5" cols="20" required></textarea></td>
          </tr>
           
          
          
         <tr>
            <td>
            <input type="submit" value="Send" id="send" name="submit" class="button small"  />
<!--            <input type="reset" value="Reset" class="button small" onclick="return chack_captcha()" />-->
            </td>
          </tr>
        </table>
        
      </form>
       </div>';
	  
         }
         else 
         {
            for($i=0;$i<count($reportlist);$i++) 
            {
            $result=$this->db->query("select * from users where id='".$reportlist[$i]->publisher_id."'");
         	$row = $result->result();
                
         	$result_1=$this->db->query("select r.*,c.category_name as category_name from report r INNER JOIN category c on(r.category_id = c.id) where page_urls='".$reportlist[$i]->page_urls."' AND r.status='1' LIMIT 0,1");
			$reports=$result_1->result();

            	$msg .='<div class="blog-post">
        <div class="one_third"> 
        <a href='.base_url().'reports/index/'.$reportlist[$i]->page_urls.'>';
            	$rm=$reportlist[$i]->report_image;
        
        $msg .='<img src="'. base_url().'themes/frontend/images/report_default.jpg" height="200" class="blog_img"  alt="" />';
                  
        $msg .=' </a>
         </div>
        <div class="two_thirds last">
          <h3 style = "text-transform: none;height: 0px;"><a style = "color:black;" href="'.base_url().'reports/index/'. $reportlist[$i]->page_urls.'">'. $reportlist[$i]->report_name.'</a></h3>
          <ul style="width: 50%;margin-top: 5%;">
            <li><i class="icon-user"></i>Published by <a href="#">'.$row[0]->display_name.'</a></li>
            <li><i class="icon-time"></i>'.date("M d Y", strtotime($reportlist[$i]->report_date)).'</li>';
            for($j=0;$j<count($reports);$j++)
	 		{
            		$msg .='<li><i class="icon-user"></i>Category <a href="'.base_url().'reports/report/getCategoryReport/'.$reports[$j]->category_id.'">'.$reports[$j]->category_name.'</a></li>
            		<li><i class="icon-file"></i> Total Pages: '.$reports[$j]->no_pages.'</li>';
        	}
          $msg .='</ul>
          <div class="clear"></div>
           <p class="price1"> USD '. $reportlist[$i]->report_price.' </p>
       
          </div>
      
      </div>
       <div class="clear"></div>
      <div class="divider"></div>';
            	
            	
               
                                 
            }
         }
            


            /* --------------------------------------------- */
  if( (!empty($cid)) || ('0' != ($cid)) )
{
$q=$this->db->query("select * from report where (report_name like '%$rk%' or report_description like '%$rk%' or report_content like '%$rk%') AND (status='1') AND (category_id = '$cid') LIMIT 0,100");
}
else 
{
	$q=$this->db->query("select * from report where (report_name like '%$rk%' or report_description like '%$rk%' or report_content like '%$rk%') AND (status='1') LIMIT 0,100");
}
  

  
           // $q=$this->db->query("select * from report where category_id = '$category_id' and status='1' order by id DESC");
             $ro=$q->result();   
                $count=$q->num_rows();   
                 $no_of_paginations=ceil($count / $per_page);
          //  $count = count($row_page_data);
          
            //echo $no_of_paginations = ceil($count / $per_page);
           // echo '<br>'.$cur_page;
// die();
//$no_of_paginations=2;
            /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
            if ($cur_page >= 5) {
                $start_loop = $cur_page - 3;
                if ($no_of_paginations > $cur_page + 3)
                {
                    $end_loop = $cur_page + 3;
                }
                else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) 
                {
                    $start_loop = $no_of_paginations - 6;
                    $end_loop = $no_of_paginations;
                } 
                else 
                {
                    $end_loop = $no_of_paginations;
                }
            } 
            else 
            {
                $start_loop = 1;
                if ($no_of_paginations > 7)
                {
                    $end_loop = 7;
                }
                else
                {
                    $end_loop = $no_of_paginations;
                }
            }
			
            /* ----------------------------------------------------------------------------------------------------------- */
            $msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";
		    if(count($reportlist)>=1)
         {
            // FOR ENABLING THE FIRST BUTTON
            if ($first_btn && $cur_page > 1) 
            {
                $msg .= "<li p='1' class='active'>First</li>";
            } 
            else if ($first_btn) 
            {
                $msg .= "<li p='1' class='inactive'>First</li>";
            }

            // FOR ENABLING THE PREVIOUS BUTTON
            if ($previous_btn && $cur_page > 1) 
            {
                $pre = $cur_page - 1;
                $msg .= "<li p='$pre' class='active'>Previous</li>";
            } 
            else if ($previous_btn) 
            {
                $msg .= "<li class='inactive'>Previous</li>";
            }
            for ($i = $start_loop; $i <= $end_loop; $i++) 
            {

                if ($cur_page == $i)
                {
                    $msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
                }
                else
                {
                    $msg .= "<li p='$i' class='active'>{$i}</li>";
                }
            }

            // TO ENABLE THE NEXT BUTTON
            if ($next_btn && $cur_page < $no_of_paginations) 
            {
                $nex = $cur_page + 1;
                $msg .= "<li p='$nex' class='active'>Next</li>";
            } 
            else if ($next_btn) 
            {
                $msg .= "<li class='inactive'>Next</li>";
            }

            // TO ENABLE THE END BUTTON
            if ($last_btn && $cur_page < $no_of_paginations) 
            {
                $msg .= "<li p='$no_of_paginations' class='active'>Last</li>";
            } 
            else if ($last_btn) 
            {
                $msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
            }
			}
            $goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
            $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
            $msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
            echo $msg;
            }
			

		
		
	}
	/* <!---------------------------------- Search Report End---------------------------->*/
	
	public function NewAutoComplete(){
		$q = strtolower($_GET["q"]);
			if (!$q) {return;}
			$sql = "SELECT * FROM report where report_name LIKE '%$q%'";
			$row = $this->db->query($sql);
			$result =  $row->result();
		
			for($i = 0; $i < couint($result);$i++ ) {
				$cname = $result[0]->report_name;
				echo "$cname\n";
			}
	}
	
}
