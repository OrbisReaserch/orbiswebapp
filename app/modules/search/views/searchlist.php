 <script src="<?php echo base_url();?>themes/frontend/js/ajax.googleapis.com.ajax.libs.jquery.1.4.2.jquery.min.js"></script>
 <style type="text/css">
#container .pagination ul li.inactive,
#container .pagination ul li.inactive:hover{
    background-color:#ededed;
    color:#bababa;
    border:1px solid #bababa;
    cursor: default;
}
#container .data ul li{
    list-style: none;
    font-family: verdana;
    margin: 5px 0 5px 0;
    color: #000;
    font-size: 12px;
}

#container .pagination{

    height: 25px;
    margin-top:20px;
    float: left;
    padding: 0;
margin: 0;
width: 100%;
clear: both;
                   
}
#container .pagination input{
   height: 30px;
   margin-right: 8px;
   
}
#container .pagination ul{
    list-style: none;
    float: left;
    padding: 0;
margin: 0;
}
#container .pagination ul li{
    list-style: none;
    float: left;
    border: 1px solid #1cc3c9;
    padding: 2px 6px 2px 6px;
    margin: 0 3px 0 3px;
    font-family: arial;
    font-size: 12px;
    color: #999;
    font-weight: bold;
    background-color: #ffffff;
}
#container .pagination ul li:hover{
    color: #fff;
    background-color: #1cc3c9;
    cursor: pointer;
}
.go_button
{
background-color:#ffffff;border:1px solid #fb0455;color:#cc0000;padding:2px 6px 2px 6px;cursor:pointer;position:absolute;margin-top:-1px;
}
.total
{
 float:right;font-family:arial;color:#bababa; font-size: 12px;
 margin:5px 25px;
}

</style>
 <header class="main-header">
    <div class="container">
        <h1 class="page-title">Search list</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active"> Search list</li>
        </ol>
    </div>
</header>

<div class="container">   
 <div class="row">        
 <div class="col-md-8">		    
 
<div id="loading">
		 <center>
			<img style = "height:100px;" src = "https://www.southeastairportshuttle.com.au/assets/img/ajax-loading-large.gif" />
		 <center>
	 </div> 
 <div id="container">               
 <div class="data"></div>              
 <div class="pagination"></div>        
 </div>   
 </div>  
 <div class="col-md-4">
         
         <div class="panel panel-default">
          <div class="panel-heading">
           <h3 class="panel-title">Featured Reports</h3>
          </div>
          <div class="panel-body">
		  
		   <?php
        /* count($featured) */
				for($i=0; $i < count($featured);$i++)
				{
					 
				?>
                    <div class="media">
                    <a href="<?php echo base_url();?>reports/index/<?php echo $featured[$i]->page_urls; ?>" class="pull-left"><img width="70" height="70" alt="<?php echo $featured[$i]->report_name; ?>" src="<?php echo base_url();?>uploads/category_report_image/<?php echo $featured[$i]->category_id.".jpg"; ?>" class="media-object"></a>
                    <div class="media-body">
                    <p style="margin-top:5px;margin-bottom:0;"><?php echo substr($featured[$i]->report_name,0,60); ?></p>
                    <small><a href="<?php echo base_url();?>reports/index/<?php echo $featured[$i]->page_urls; ?>">Read more...</a></small>
                    </div>
                    </div>
				<?php }
        ?>           	
 
          </div>
       </div>
           
		 </div>  
 </div> </div>
  <input type="hidden" name="categoryid" id="category_id" value="<?php echo $cat;?>">
  <input type="hidden" name="reportkey" id="report_key" value="<?php echo $report_key;?>">
  <script type="text/javascript">    
 function loadData(page)    
 {    
    var category_id=$("#category_id").val();
    var search_keyword=$("#report_key").val();
   
     $.ajax({
        type: "POST",
        url: '<?php echo base_url() ?>search/getSearchKeywordWiseData',
        data: {
             page:page,
             category_id:category_id,
             search_keyword:search_keyword,
            
        },
        success: function(data) {
             $("#loading").css('display','none');
             $("#container").html(data);
        }
    });   
}   
 $(document).ready(function(){   
        loadData(1); 
 });
 </script>
   

