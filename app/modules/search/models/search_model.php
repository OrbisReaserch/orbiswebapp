<?php
class Search_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();

	}
	public function getUsers()
	{
		$result=$this->db->query("select * from users status='1'");
		return $result->result();
	}
	public function getReports($report_id)
	{
		$result=$this->db->query("select * from report where id='$report_id' AND status='1' order by report_date DESC");
		return $result->result();
	}
	public function getCategory()
	{
		$result=$this->db->query("select * from category where status='1'");
		return $result->result();
	}
	public function getCountryList()
	{
		$result=$this->db->query("select * from country where status='1'");
		return $result->result();
		// print_r($result);
		// die();
	}
	public function getSubCategory()
	{
		$result=$this->db->query("select * from sub_category where status='1'");
		return $result->result();
	}
	public function getFeaturedReports()
	{
		$result=$this->db->query("SELECT * FROM report AS r1 JOIN (SELECT CEIL(RAND() * (SELECT MAX(id)   FROM report)) AS id) AS r2 WHERE r1.id >= r2.id ORDER BY r1.id ASC LIMIT 0,3");
		return $result->result();
	}
	public function getProductImage()
	{
		$result=$this->db->query("select * from product_images where status='1' limit 1");
		return $result->result();	
	}
	public function getPublisher()
	{
		$result=$this->db->query("select * from users where status='1'");
		return $result->result();	
	}
	public function getReportList($report_id)
	{
		$result=$this->db->query("select * from report where sub_category_id = '$report_id' order by report_date DESC");
		return $result->result();
	}
	
	//Search 
	// public function getKeyreports($report_key)
	// {
	// // echo "select id,report_name,category_id,publisher_id,report_image,report_date,report_price,page_urls,SUBSTRING(`report_description`, 1, 100) from report where (report_name like '%$report_key%' or report_description like '%$report_key%' or report_content like '%$report_key%') AND (status='1') order by report_date DESC LIMIT 500"; die();
	// 	$result=$this->db->query("select id,report_name,category_id,publisher_id,report_image,report_date,report_price,page_urls,SUBSTRING(`report_description`, 1, 100) from report where (report_name like '%$report_key%' or report_description like '%$report_key%' or report_content like '%$report_key%') AND (status='1') order by report_date DESC LIMIT 500");
	// 	return $result->result();
	// }
	// public function getKetCatreports($report_key,$cat)
	// {
	// 	//$result=$this->db->query("select id,report_name,category_id,publisher_id,report_image,report_date,report_price,page_urls,SUBSTRING(`report_description`, 1, 100) from report where (report_name like '%$report_key%' or report_description like '%$report_key%' or report_content like '%$report_key%') AND (status='1') AND (category_id = '$cat') order by report_date DESC LIMIT 0,500");
	// 	$result=$this->db->query("select id,report_name,category_id,publisher_id,report_image,report_date,report_price,page_urls,SUBSTRING(`report_description`, 1, 100) from report where (report_name like '%$report_key%') AND (status='1') AND (category_id = '$cat') order by report_date DESC LIMIT 0,500");
	// 	return $result->result();
	// }
	
public function getKetCatreports_key($report_key)
	{
		/*echo "select * from report where (report_name like '%$report_key%' or report_description like '%$report_key%' or report_content like '%$report_key%') AND (status='1') AND (category_id = '$cat')";
		die();*/
		$result=$this->db->query("select id,report_name,page_urls,report_date from report where report_name like '%$report_key%'  order by report_date DESC LIMIT 0,100");
		return $result->result();
	}

	//Sorting
	public function getallreports()
	{
		$result=$this->db->query("select * from report where status='1' order by report_date DESC");
		return $result->result();
	}
	
	public function get_price_reports($price)
	{
		$result=$this->db->query("select * from report where report_price < $price AND status='1' order by report_date DESC");
		return $result->result();
	}
	
	public function get_date_reports($date_search)
	{
		$result=$this->db->query("select * from report where report_date > '$date_search' AND status='1' order by report_date DESC");
		return $result->result();
	}
	
	public function get_date_price_reports($date_search,$price_range)
	{
		//echo "select * from report where report_date > '$date_search' AND status='1'";
		$result=$this->db->query("select * from report where report_date > '$date_search' AND  report_price < $price_range AND status='1' order by report_date DESC");
		return $result->result();
	}
	public function getTestimonials()
	{
		$result=$this->db->query("select * from testimonials where status='1' order by id DESC limit 1");
		return $result->result();
	}
public function getFooterNews()
	{
		$result=$this->db->query("select * from news where status='1' order by id desc limit 2");
		return $result->result();
	}
	public function getFooterReport()
	{
		$result=$this->db->query("select * from report where status='1' order by id desc limit 5");
		return $result->result();
	
	}
	public function getSortReports()
	{
		$mx=$_POST['maxprice'];
		$mn=$_POST['minprice'];
		$co=$_POST['country'];
		$report_year=$_POST['report_year'];
		
//		echo $mx."=======".$mn; die();
		$sort=$_POST['sort'];
		if($sort=='price')
		{
			$param="";
			if($mx != "" && $mn == "" )
			{
				//echo "into maxprice";
				$maxprice=$_POST['maxprice'];
			//	$minprice= "0";
				$param.=" and report_price <= '".$maxprice."' ";
			} 
			else if($mn != "" && $mx == "")
			{
				//echo "into minprice";
				$minprice=$_POST['minprice'];
				//	$minprice= "0";
				$param.=" and report_price >= '".$minprice."' ";
			}
			
			else if($mx != "" && $mn != "")
			{
				//echo "into both";
				$minprice=$_POST['minprice'];
				$maxprice=$_POST['maxprice'];
				$param.=" and report_price BETWEEN '".$minprice."' and'".$maxprice."' ";
			}
				
		   else 
			{
			$param.="";	
			}
			
		 if($co!=0)
			{
				//echo "into country";
				$country=$_POST['country'];
				$param.=" and country_id='".$country."' ";
			}
		if($report_year!="")
			{
				//echo "into country";
				//$country=$_POST['country'];
				$param.=" and report_year='".$report_year."' ";
			}
			
			$que="select * from report where status='1'";
			$sql=$que.$param;
			//echo $sql; die();
		$result=$this->db->query($sql."order by report_price desc");
		return $result->result();	
		}
		else
		{
			$param="";
			if($mx != "" && $mn == "" )
			{
				//echo "into maxprice";
				$maxprice=$_POST['maxprice'];
			//	$minprice= "0";
				$param.=" and report_price <= '".$maxprice."' ";
			} 
			else if($mn != "" && $mx == "")
			{
				//echo "into minprice";
				$minprice=$_POST['minprice'];
				//	$minprice= "0";
				$param.=" and report_price >= '".$minprice."' ";
			}
			
			else if($mx != "" && $mn != "")
			{
				//echo "into both";
				$minprice=$_POST['minprice'];
				$maxprice=$_POST['maxprice'];
				$param.=" and report_price BETWEEN '".$minprice."' and'".$maxprice."' ";
			}
				
		   else 
			{
			$param.="";	
			}
			
		   if($co!=0)
			{
				//echo "into country";
				$country=$_POST['country'];
				$param.=" and country_id='".$country."' ";
			}
			if($report_year!="")
			{
				//echo "into country";
				//$country=$_POST['country'];
				$param.=" and report_year='".$report_year."' ";
			}
			
			$que="select * from report where status='1'";
			$sql=$que.$param;
			//echo $sql."order by report_date desc"; die();
		$result=$this->db->query($sql."order by report_date desc");
		//$result=$this->db->query("select * from report where status='1' order by report_date desc");
		return $result->result();	
		}
	}

}