
<head>
 <title><?php 
if ('NULL' == $news[0]->page_title || '' == $news[0]->page_title){echo $news[0]->meta_title;}else{ echo $news[0]->page_title;}?></title>
<meta name="title" content="<?php echo  $news[0]->meta_title; ?>" /> 

</head>
<header class="main-header">
    <div class="container">
        <h1 class="page-title">Press Release Details</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li><a href="<?php echo base_url(); ?>news">Press Release</a></li>
            <li class="active">Press Release Details</li>
        </ol>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-sm-5">
            <section>
                <h3 class="page-header no-margin-top"><?php echo $news[0]->news_title;?></h3>
                <div class="newsdetailsimg"><span class="news_date"><i class="fa fa-calendar"></i> &nbsp; <?php echo date("d-M-Y", strtotime($news[0]->news_date));?> </span>
				        <?php 
                      $rm=$news[0]->news_image;
                    if($rm!='')
                            {
                   ?>
              <img src="<?php echo base_url(); ?>uploads/news_image/<?php echo $news[0]->news_image;?>" alt = "<?php echo $news[0]->news_title;?>" title = "<?php echo $news[0]->news_title;?>" class="img-responsive imageborder" width="300" height="192" alt="<?php echo $news[0]->news_title;?>">
              <?php 
                            }
                            else 
                            {
                     ?>
              <img src="<?php echo base_url(); ?>uploads/no-image.png"  title = "<?php echo $news[0]->news_title;?>"  class="img-responsive imageborder" width="300" height="192" alt="<?php echo $news[0]->news_title;?>" />
              <?php } ?>
    
				 
				
				</div>
                <p><?php echo $news[0]->news_description;  ?></p>
      </section>
  </div>
	
        <div class="col-md-4 col-sm-7">
              <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Latest News</h3>
                    </div>
                    <div class="panel-body">
                     <ul class="media-list">
					 <?php
         
				for($i=0;$i<count($newslist);$i++)
				{
					$news_name =  str_replace(" ","-",strtolower($newslist[$i]->news_title));
				?>
                                <li class="media">
                                    <a class="pull-left" href="<?php echo base_url(); ?>news/<?php echo $news_name; ?>"><img src="<?php echo base_url();?>uploads/news_image/<?php echo $newslist[$i]->news_image;?>" alt="<?php echo $newslist[$i]->news_title; ?>" title = "<?php echo $newslist[$i]->news_title; ?>" class="media-object" width="80" height="80"></a>
                                    <div class="media-body">
                                        <p class="media-heading"><a href="<?php echo base_url(); ?>news/<?php echo $news_name; ?>"><?php echo $newslist[$i]->news_title; ?></a></p>
                                        <small><?php echo date("d-M-Y", strtotime($newslist[$i]->news_date));?></small>
                                    </div>
                                </li>
                   <?php }
        ?>                   
                            </ul>
                    </div>
                </div>
        </div>
    </div>
</div>
