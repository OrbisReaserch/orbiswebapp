<style>
    #container .pagination ul li.inactive,
    #container .pagination ul li.inactive:hover {
        background-color: #ededed;
        color: #bababa;
        border: 1px solid #bababa;
        cursor: default;
    }
    #container .data ul li {
        list-style: none;
        font-family: verdana;
        margin: 5px 0 5px 0;
        color: #000;
        font-size: 12px;
    }
    #container .pagination {
        height: 25px;
        margin-top: 20px;
        float: left;
        padding: 0;
        margin: 0;
        width: 100%;
        clear: both;
    }
    #container .pagination input {
        height: 30px;
        margin-right: 8px;
    }
    #container .pagination ul {
        list-style: none;
        float: left;
        padding: 0;
        margin: 0;
    }
    #container .pagination ul li {
        list-style: none;
        float: left;
        border: 1px solid #1062AC;
        padding: 2px 6px 2px 6px;
        margin: 0 3px 0 3px;
        font-family: arial;
        font-size: 12px;
        color: #999;
        font-weight: bold;
        background-color: #ffffff;
    }
    #container .pagination ul li:hover {
        color: #fff;
        background-color: #1062AC;
        cursor: pointer;
    }
    .go_button {
        background-color: #ffffff;
        border: 1px solid #fb0455;
        color: #cc0000;
        padding: 2px 6px 2px 6px;
        cursor: pointer;
        position: absolute;
        margin-top: -1px;
    }
    .total {
        float: right;
        font-family: arial;
        color: #bababa;
        font-size: 12px;
        margin: 5px 25px;
    }
</style>
<title><?php 
if ('NULL' == $news[0]->page_title || '' == $news[0]->page_title){echo $news[0]->meta_title;}else{ echo $news[0]->page_title;}?></title>
<meta name="title" content="<?php echo  $news[0]->meta_title; ?>" /> 
<header class="main-header">
    <div class="container">
        <h1 class="page-title">Press Release</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Press Release</li>
        </ol>
    </div>
</header>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div id="loading"></div>
            <div id="container">
                <div class="data" id = "data"></div>
                <div class="pagination" id = "pagination"></div>
            </div>
        </div> 
    </div> 
</div>
      
<script>
    var js = $.noConflict(); // Code that uses other library's $ can follow here.
</script>

<?php $sql="select * from news WHERE status='1' and news_type = 'press_release' ORDER BY id DESC" ; ?>

<input type="hidden" name="sql" id="sql" value="<?php echo $sql; ?>">

<script src="<?php echo base_url();?>themes/frontend/js/ajax.googleapis.com.ajax.libs.jquery.1.4.2.jquery.min.js"></script>

<script>

    GetData("1");
    function GetData(page){

    $.post(
        '<?php echo base_url()."news/GetReleaseReports/";?>'+page, 
        function(data) {
    		var output = data.split("~~~");
    		$("#data").html(output[0]);
    		$("#pagination").html(output[1]);
    	});
    }

</script>