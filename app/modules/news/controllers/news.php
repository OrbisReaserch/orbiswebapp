<?php
class News extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('news/news_model');
		$this->load->model('home/home_model');
	}

	public function index()
	{
			
			
		$data['publisher'] = $this->news_model->getPublisher();
		$data['news'] = $this->news_model->getNews();
		$data['category'] = $this->news_model->getCategory();
		$data['subcategory'] = $this->news_model->getSubCategory();
		$data['featured'] = $this->news_model->getFeaturedReports();
		$data['testimonials'] = $this->news_model->getTestimonials();
		$data['footernews'] = $this->news_model->getFooterNews();
		$data['footerreport'] = $this->news_model->getFooterReport();
		$data['include'] = 'news/newslist';
		$this->load->view('frontend/container',$data);
	}
	public function newslist_old()
	{
			
			
		$data['publisher'] = $this->news_model->getPublisher();
		$data['news'] = $this->news_model->getNews();
		$data['category'] = $this->news_model->getCategory();
		$data['subcategory'] = $this->news_model->getSubCategory();
		$data['featured'] = $this->news_model->getFeaturedReports();
		$data['testimonials'] = $this->news_model->getTestimonials();
			$data['footernews'] = $this->news_model->getFooterNews();
		$data['footerreport'] = $this->news_model->getFooterReport();
		$data['include'] = 'news/newslist_old';
		$this->load->view('frontend/container',$data);
	}
	public function getNewsDetails($id)
	{
			
		$data['news'] = $this->news_model->getNewsDetails($id);
		$data['newslist'] = $this->home_model->getNews();
		$data['include'] = 'news/news';
		$this->load->view('frontend/container',$data);
	}
	


	public function confirm_order($amount)
	{
		$user_id = $this->session->userdata('user_id');
		$data['user_info'] = $this->profile_model->getRecord($user_id);
		$username = $data['user_info']['username'];
		$full_name = $data['user_info']['last_name']." ".$data['user_info']['first_name'];
		$email = $data['user_info']['email'];
		$contact_no = $data['user_info']['mobile'];
		
			
		$order_code = 'OR'.time();
		$order_date = date('Y-m-d');
		$payment_status = 'pending';
		$order_status = 'pending';
		$status = '1';
		$report_code = "";
		$publisher_id = "";
		
		$data = array('user_id'=>$user_id,'order_code'=>$order_code,'amount'=>$amount,'order_date'=>$order_date,'payment_status'=>$payment_status,
            'order_status'=>$order_status,'status'=>'1');
		$this->db->insert('orders',$data);
		$order_id  =  $this->db->insert_id();

		$cart=$this->session->userdata('cart');
		//print_r($cart);die();
		for($i=0 ; $i<count($cart) ; $i++)
		{
			$report_id = $cart[$i]['report_id'];
			$price = $cart[$i]['report_price'];
			$status = '1';
			$reportdata['reports'] = $this->news_model->getReports($report_id);
			//print_r($reportdata); die();
			$report_code = $reportdata['reports'][0]->report_code;
			$publisher_id = $reportdata['reports'][0]->publisher_id; 
			//echo $report_code; echo $publisher_id; 			
			$data = array('order_id'=>$order_id,'user_id'=>$user_id,'report_id'=>$report_id,'price'=>$price,'status'=>'1');
			$this->db->insert('order_details',$data);
			
			$request = array('username'=>$username,'full_name'=>$full_name,'email'=>$email,'contact_no'=>$contact_no,'report_code'=>$report_code,'publisher_id'=>$publisher_id,'status'=>'1');
			$this->db->insert('request',$request);
			
		}
		$this->session->unset_userdata('cart');
		//		$data['include'] = 'reports/reportlist';
		//		$this->load->view('frontend/container',$data);
		$this->session->set_flashdata('success', 'Order placed successfully, Your order will deliver within 5-6 working days');
		redirect(base_url()."home/home");
	}

		




	public function get_sort()
	{
		$sort=$_POST['sort'];
		$suid=$_POST['suid'];
			
		if($sort=='date_desc')
		{
			//    		echo $sort.$suid;
			//    	die();
			$data['sortlist'] = $this->news_model->getReportListDateDesc($suid);

			$data['category'] = $this->news_model->getCategory();
			$data['subcategory'] = $this->news_model->getSubCategory();
			$data['featured'] = $this->news_model->getFeaturedReports();
				$data['footernews'] = $this->news_model->getFooterNews();
		$data['footerreport'] = $this->news_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		elseif ($sort=='date_asc')
		{
			$data['sortlist'] = $this->news_model->getReportListDateAsc($suid);

			$data['category'] = $this->news_model->getCategory();
			$data['subcategory'] = $this->news_model->getSubCategory();
			$data['featured'] = $this->news_model->getFeaturedReports();
				$data['footernews'] = $this->news_model->getFooterNews();
		$data['footerreport'] = $this->news_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		elseif ($sort=='price_asc')
		{
			$data['sortlist'] = $this->news_model->getReportListPriceAsc($suid);

			$data['category'] = $this->news_model->getCategory();
			$data['subcategory'] = $this->news_model->getSubCategory();
			$data['featured'] = $this->news_model->getFeaturedReports();
				$data['footernews'] = $this->news_model->getFooterNews();
		$data['footerreport'] = $this->news_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		elseif ($sort=='price_desc')
		{
			$data['sortlist'] = $this->news_model->getReportListPriceDesc($suid);

			$data['category'] = $this->news_model->getCategory();
			$data['subcategory'] = $this->news_model->getSubCategory();
			$data['featured'] = $this->news_model->getFeaturedReports();
				$data['footernews'] = $this->news_model->getFooterNews();
		$data['footerreport'] = $this->news_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		elseif ($sort=='title_desc')
		{
			$data['sortlist'] = $this->news_model->getReportListTitleDesc($suid);

			$data['category'] = $this->news_model->getCategory();
			$data['subcategory'] = $this->news_model->getSubCategory();
			$data['featured'] = $this->news_model->getFeaturedReports();
				$data['footernews'] = $this->news_model->getFooterNews();
		$data['footerreport'] = $this->news_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		elseif ($sort=='title_asc')
		{
			$data['sortlist'] = $this->news_model->getReportListTitleAsc($suid);

			$data['category'] = $this->news_model->getCategory();
			$data['subcategory'] = $this->news_model->getSubCategory();
			$data['featured'] = $this->news_model->getFeaturedReports();
				$data['footernews'] = $this->news_model->getFooterNews();
		$data['footerreport'] = $this->news_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
		else
		{
			$data['reportlist'] = $this->news_model->getReportList($suid);

			$data['category'] = $this->news_model->getCategory();
			$data['subcategory'] = $this->news_model->getSubCategory();
			$data['featured'] = $this->news_model->getFeaturedReports();
				$data['footernews'] = $this->news_model->getFooterNews();
		$data['footerreport'] = $this->news_model->getFooterReport();
			$data['include'] = 'reports/reportlist';
			$this->load->view('frontend/container',$data);
		}
			
	}
	function add_cart()
	{
		if(isset($_POST['report_id']))
		{
			$id=$_POST['report_id'];
			$cart=$this->session->userdata('cart');
			if(is_array($cart))
			{
				if(count($cart))
				{
					for($k=0;$k<count($cart);$k++)
					{
						if($cart[$k]['report_id']!=$id)
						{

							$report = array('report_id'=>$_POST['report_id'],
                                                        'report_price'=>$_POST['report_price']
							);

							array_push($cart, $report);
							$this->session->set_userdata('cart',$cart);
							break;
						}
						else
						{
							unset($cart[$k]);
							$cart=  array_values($cart);
							$this->session->set_userdata('cart',$cart);


							$report = array('report_id'=>$_POST['report_id'],
                                                        'report_price'=>$_POST['report_price'],
							);

							$arr=array('cart'=>array($report));
							$this->session->set_userdata($arr);
							break;
						}
					}
				}
				else
				{
					$report = array('report_id'=>$_POST['report_id'],
                                                        'report_price'=>$_POST['report_price'],
					);
					array_push($cart, $report);
					$this->session->set_userdata('cart',$cart);
				}

			}
			else
			{
				if(!is_array($cart))
				{

					$report = array('report_id'=>$_POST['report_id'],
                                                        'report_price'=>$_POST['report_price'],
					);
					$arr=array('cart'=>array($report));
					$this->session->set_userdata($arr);
				}
			}
		}

		redirect($this->agent->referrer());
	}
	public function checkout()
	{
		$user_id = $this->session->userdata('user_id');
		if(isset($user_id) && $user_id!=0)
		{
			//  $data['users'] = $this->category_model->getUsers();
			$data['category'] = $this->news_model->getCategory();
			$data['clients'] = $this->news_model->getUsers();
			$data['sub_category'] = $this->news_model->getSubCategory();
			$cart=$this->session->userdata('cart');
			if(is_array($cart))
			{
				$data['product']=$cart;
			}
			else
			{
				$data['product']=array();
			}
			$data['include'] = 'reports/checkout';
			$this->load->view('frontend/container',$data);
		}
		else
		{
			redirect('register/register');
		}
	}
	function remove_cart($id)
	{
		$cart=$this->session->userdata('cart');
		//print_r($cart);
		for($i=0;$i<count($cart);$i++)
		{
			if(in_array($id, $cart[$i]))
			{
				unset($cart[$i]);
				$cart=  array_values($cart);
				$this->session->set_userdata('cart',$cart);
			}
		}
		redirect($this->agent->referrer());
	}
	
    public function getData()
	{
		
	$sql=$_POST['sql'];
            if($_POST['page'])
            {
            $page = $_POST['page'];
            $cur_page = $page;
            $page -= 1;
            $per_page = 5;
            $previous_btn = true;
            $next_btn = true;
            $first_btn = true;
            $last_btn = true;
            $start = $page * $per_page;
            

            $query_pag_data = $sql." LIMIT $start, $per_page";
 
            $result_pag_data = $this->db->query($query_pag_data);
            $news=$result_pag_data->result();
            $msg = "";
            for($i=0;$i<count($news);$i++) 
            {
			$news_name =  str_replace(" ","-",strtolower($news[$i]->news_title));
                
            	$msg.=' <div class="blog-post">
        <div class="one_fifth marbot"> 
        <a href='. base_url().'news/'.$news_name.'>';
         
            $rm=$news[$i]->news_image;
            if($rm!='')
                    {
         
        $msg.='<img src='.base_url().'uploads/news_image/'. $news[$i]->news_image.' class="blog_img" alt="'. $news[$i]->alt_tag.'" />'; 
       
                    }
                    else 
                    {
             
             
  $msg.='<img src="https://orbisresearch.com/uploads/no-image.png" class="blog_img"  alt="" />';
             }
//substr($news[$i]->news_title,0,52)			 
       $msg.=' </a> </div>
        <div class="two_thirds last">
          <h3><a href='. base_url().'news/'.$news_name.'>'.$news[$i]->news_title.'</a></h3>
          <ul>

            <li><i class="icon-time"></i>'.date("d-M-Y", strtotime($news[$i]->news_date)).'</li>
          </ul>
          <div class="clear"></div>
          <p>';$repo=$news[$i]->news_description;   
              
              
                $summary = str_replace("<span>", "", $repo);
                $summary1 = str_replace("</span>", "", $summary);
                $summary2 = str_replace("<div>", "", $summary1);
                $summary3 = str_replace("</div>", "", $summary2);
                $summary4 = str_replace("<p>", "", $summary3);
                $summary5 = str_replace("</p>", "", $summary4);
             
            $msg.=substr($summary5,0,600).'.... 
            <a href='.base_url().'news/'.$news_name.'>Read More</a></p>
           </div>
      </div>
      <div class="clear"></div>
      <div class="divider"></div>';
            	
            	
               
                                 
            }
            


            /* --------------------------------------------- */
            
  
            $q=$this->db->query("select * from news WHERE status='1'");
             $ro=$q->result();   
                $count=$q->num_rows();   
                 $no_of_paginations=ceil($count / $per_page);
          
            /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
            if ($cur_page >= 5) {
                $start_loop = $cur_page - 3;
                if ($no_of_paginations > $cur_page + 3)
                {
                    $end_loop = $cur_page + 3;
                }
                else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) 
                {
                    $start_loop = $no_of_paginations - 6;
                    $end_loop = $no_of_paginations;
                } 
                else 
                {
                    $end_loop = $no_of_paginations;
                }
            } 
            else 
            {
                $start_loop = 1;
                if ($no_of_paginations > 7)
                {
                    $end_loop = 7;
                }
                else
                {
                    $end_loop = $no_of_paginations;
                }
            }
            /* ----------------------------------------------------------------------------------------------------------- */
            $msg .= "<div class='clearfix'></div><div class='col-lg-12 col-md-12 col-sm-12'><div class='pagination'><ul>";

            
            if ($first_btn && $cur_page > 1) 
            {
                $msg .= "<li p='1' class='active'>First</li>";
            } 
            else if ($first_btn) 
            {
                $msg .= "<li p='1' class='inactive'>First</li>";
            }

            
            if ($previous_btn && $cur_page > 1) 
            {
                $pre = $cur_page - 1;
                $msg .= "<li p='$pre' class='active'>Previous</li>";
            } 
            else if ($previous_btn) 
            {
                $msg .= "<li class='inactive'>Previous</li>";
            }
            for ($i = $start_loop; $i <= $end_loop; $i++) 
            {

                if ($cur_page == $i)
                {
                    $msg .= "<li p='$i' style='color:#fff;background-color:#1cc3c9;' class='active'>{$i}</li>";
                }
                else
                {
                    $msg .= "<li p='$i' class='active'>{$i}</li>";
                }
            }

         
            if ($next_btn && $cur_page < $no_of_paginations) 
            {
                $nex = $cur_page + 1;
                $msg .= "<li p='$nex' class='active'>Next</li>";
            } 
            else if ($next_btn) 
            {
                $msg .= "<li class='inactive'>Next</li>";
            }

         
            if ($last_btn && $cur_page < $no_of_paginations) 
            {
                $msg .= "<li p='$no_of_paginations' class='active'>Last</li>";
            } 
            else if ($last_btn) 
            {
                $msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
            }
            $goto = "<input type='text' class='goto' size='1' style='margin-top:-1px;margin-left:60px;'/><input type='button' id='go_btn' class='go_button' value='Go'/>";
            $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
            $msg = $msg . "</ul>" . $total_string . "</div></div>";  // Content for pagination
            echo $msg;
            }
		
		
		
	}
	public function GetReports($page){
		$upperLimit = ($page * 5) - 5;
		$nextpage = $page + 1;
		if("1" == $page){
			$prevpage = $page - 1;
		}else{
		$prevpage = $page - 1;
		}
		$limit = "LIMIT ".$upperLimit.",5";
		
		$news = $this->news_model->getNewsData($limit);
		$resultcnt = $this->news_model->getNewsCount();
		
		$data = ""; $pagination = '';
		if(empty($news)){
				$data .= '<article class="post reportsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title reportsTitle"><a href="reports_details.php" class="transicion">No data available</a></h3>
                      </div>
           </div>
            </article> ';	
						
		$pagination .= '<ul><li  onclick = "GetData(1);">   <span aria-hidden="true">First</span>   </li>
		<li onclick = "GetData('.round($resultcnt['cnt']/5).');">  <span aria-hidden="true">Previous</span>   </li>';
		 			
		$pagination  .=	'
		<li onclick = "GetData('.round($resultcnt['cnt']/5).');">  <span aria-hidden="true">Last</span>   </li> </ul>';
			
					
		}else{
		for($i = 0; $i < count($news); $i++){
			 
			 
			$news_name =  str_replace(" ","-",strtolower($news[$i]->news_title));
                
            	$data.=' <article class="post newsList">
            <div class="panel panel-default">
            <div class="panel-body">
            <h3 class="post-title newsTitle"><a href="'. base_url().'news/'.$news_name.'" class="transicion">'.$news[$i]->news_title.'</a></h3>
            <div class="row">
            <div class="col-lg-3 col-sm-5">
            <div class="newsImg">
			<a href="'. base_url().'news/'.$news_name.'">';
			
			  $rm=$news[$i]->news_image;
            if($rm!='')
                    {
						$data.='<img src='.base_url().'uploads/news_image/'. $news[$i]->news_image.' class="img-post img-responsive" alt="'. $news[$i]->news_title.'" title = "'.$news[$i]->news_title.'"/>'; 
					}else{
						$data.='<img src='. base_url().'uploads/no-image.png class="img-post img-responsive"  alt="'. $news[$i]->news_title.'" />';
					}
			$data .= '
             </a>
            </div>
           </div>
            <div class="col-lg-9 col-sm-7 post-content">
             <div class="newsContent">
            <p>';
			$repo=$news[$i]->news_description;   
              
              
                $summary = str_replace("<span>", "", $repo);
                $summary1 = str_replace("</span>", "", $summary);
                $summary2 = str_replace("<div>", "", $summary1);
                $summary3 = str_replace("</div>", "", $summary2);
                $summary4 = str_replace("<p>", "", $summary3);
                $summary5 = str_replace("</p>", "", $summary4);
             
            $data.= substr($summary5,0,700).'...</p>
             </div>
            </div>
            </div>
            </div>
            <div class="panel-footer post-info-b">
            <div class="row">
            <div class="col-lg-10 col-md-9 col-sm-8">
            <i class="fa fa-clock-o"></i> '.date("d-M-Y", strtotime($news[$i]->news_date)).' <i class="fa fa-user"> </i>Orbis Research
            </div>
            <div class="col-lg-2 col-md-3 col-sm-4">
            <a href="'. base_url().'news/'.$news_name.'" class="pull-right">Read more »</a>
            </div>
            </div>
            </div>
            </div>
            </article> 
			 
        ';
         
		}
		
		 
				$pagination .= '<ul><li onclick = "GetData(1);">  <span aria-hidden="true">First</span> </a> </li>
				<li onclick = "GetData('.$prevpage.');">  <span aria-hidden="true">Previous</span>   </li>';
				 
				$j = $page;
				$pagination .= '<li onclick = "GetData('.$j.');">  '.$j.' </li>'; 
				if($j <= round($resultcnt['cnt']/5) ){
					$j++;
					$pagination .= '<li onclick = "GetData('.$j.');">  '.$j.' </li>'; 
				}
				if($j <= round($resultcnt['cnt']/5) ){
					$j++;
					$pagination .= '<li onclick = "GetData('.$j.');">  '.$j.' </li>'; 
				}
				if($j <= round($resultcnt['cnt']/5) ){
					$j++;
					$pagination .= '<li onclick = "GetData('.$j.');">  '.$j.' </li>'; 
				}
				if($j <= round($resultcnt['cnt']/5) ){
					$j++;
					$pagination .= '<li onclick = "GetData('.$j.');">  '.$j.' </li>'; 
				}	
				$pagination  .=	'<li onclick = "GetData('.$nextpage.');"> <span aria-hidden="true">Next</span> </li>
				<li onclick = "GetData('.round($resultcnt['cnt']/5).');">  <span aria-hidden="true">Last</span>   </li></ul>';
			 
		}
			
			echo $data."~~~".$pagination;
	
	}

	public function pressReleases()
	{		
		$data['publisher'] = $this->news_model->getPublisher();
		$data['news'] = $this->news_model->getPressRelease();
		$data['category'] = $this->news_model->getCategory();
		$data['subcategory'] = $this->news_model->getSubCategory();
		$data['featured'] = $this->news_model->getFeaturedReports();
		$data['testimonials'] = $this->news_model->getTestimonials();
		$data['footernews'] = $this->news_model->getFooterNews();
		$data['footerreport'] = $this->news_model->getFooterReport();
		$data['include'] = 'news/press_release_list';
		$this->load->view('frontend/container',$data);
	}

	public function getPressReleaseDetails($id)
	{
		$data['news'] = $this->news_model->getNewsDetails($id);
		$data['newslist'] = $this->home_model->getPressrelease();
		$data['include'] = 'news/press_release';
		$this->load->view('frontend/container',$data);
	}

	public function GetReleaseReports($page){
		$upperLimit = ($page * 5) - 5;
		$nextpage = $page + 1;
		if("1" == $page){
			$prevpage = $page - 1;
		}else{
			$prevpage = $page - 1;
		}
		$limit = "LIMIT ".$upperLimit.",5";
		
		$news = $this->news_model->getPressReleaseData($limit);
		$resultcnt = $this->news_model->getPressReleaseCount();
		
		$data = ""; $pagination = '';
		
		if(empty($news)){
			$data .= '
				<article class="post reportsList">
	        		<div class="panel panel-default">
	            		<div class="panel-body">
	            			<h3 class="post-title reportsTitle"><a href="reports_details.php" class="transicion">No data available</a></h3>
	                  	</div>
           			</div>
            	</article> ';	
						
				$pagination .= '
					<ul>
						<li  onclick = "GetData(1);">   <span aria-hidden="true">First</span>   </li>
						<li onclick = "GetData('.round($resultcnt['cnt']/5).');">  <span aria-hidden="true">Previous</span>   </li>';
			 			
				$pagination  .=	'
						<li onclick = "GetData('.round($resultcnt['cnt']/5).');">  <span aria-hidden="true">Last</span>   </li> 
					</ul>';
					
		} else {
			for($i = 0; $i < count($news); $i++) {
			
				$news_name =  str_replace(" ","-",strtolower($news[$i]->news_title));
                
            	$data.=' 
            		<article class="post newsList">
            			<div class="panel panel-default">
            				<div class="panel-body">
            					<h3 class="post-title newsTitle"><a href="'. base_url().'press-release/'.$news_name.'" class="transicion">'.$news[$i]->news_title.'</a></h3>
        						<div class="row">
            						<div class="col-lg-3 col-sm-5">
            							<div class="newsImg">
											<a href="'. base_url().'news/'.$news_name.'">';
			
				$rm=$news[$i]->news_image;
            
				if($rm!='') {
					$data.='<img src='.base_url().'uploads/news_image/'. $news[$i]->news_image.' class="img-post img-responsive" alt="'. $news[$i]->news_title.'" title = "'.$news[$i]->news_title.'"/>'; 
				} else {
					$data.='<img src='. base_url().'uploads/no-image.png class="img-post img-responsive"  alt="'. $news[$i]->news_title.'" />';
				}

				$data .= '
             								</a>
            							</div>
           							</div>
            						<div class="col-lg-9 col-sm-7 post-content">
             							<div class="newsContent">
            								<p>';
				
				$repo=$news[$i]->news_description;   

                $summary = str_replace("<span>", "", $repo);
                $summary1 = str_replace("</span>", "", $summary);
                $summary2 = str_replace("<div>", "", $summary1);
                $summary3 = str_replace("</div>", "", $summary2);
                $summary4 = str_replace("<p>", "", $summary3);
                $summary5 = str_replace("</p>", "", $summary4);
             
            	$data.= substr($summary5,0,700).'...</p>
             							</div>
            						</div>
            					</div>
            				</div>
            				<div class="panel-footer post-info-b">
            					<div class="row">
            						<div class="col-lg-10 col-md-9 col-sm-8">
            							<i class="fa fa-clock-o"></i> '.date("d-M-Y", strtotime($news[$i]->news_date)).' <i class="fa fa-user"> </i>Orbis Research
            						</div>
            						<div class="col-lg-2 col-md-3 col-sm-4">
            							<a href="'. base_url().'news/'.$news_name.'" class="pull-right">Read more »</a>
            						</div>
            					</div>
            				</div>
            			</div>
            		</article>';
         
			}
		
			$pagination .= '
				<ul>
					<li onclick = "GetData(1);">  <span aria-hidden="true">First</span> </a> </li>
					<li onclick = "GetData('.$prevpage.');">  <span aria-hidden="true">Previous</span>   </li>';
				 
				$j = $page;
				$pagination .= '<li onclick = "GetData('.$j.');">  '.$j.' </li>'; 
				
				if($j <= round($resultcnt['cnt']/5) ){
					$j++;
					$pagination .= '<li onclick = "GetData('.$j.');">  '.$j.' </li>'; 
				}
				
				if($j <= round($resultcnt['cnt']/5) ){
					$j++;
					$pagination .= '<li onclick = "GetData('.$j.');">  '.$j.' </li>'; 
				}
				
				if($j <= round($resultcnt['cnt']/5) ){
					$j++;
					$pagination .= '<li onclick = "GetData('.$j.');">  '.$j.' </li>'; 
				}
				
				if($j <= round($resultcnt['cnt']/5) ){
					$j++;
					$pagination .= '<li onclick = "GetData('.$j.');">  '.$j.' </li>'; 
				}	
				
				$pagination  .=	'
						<li onclick = "GetData('.$nextpage.');"> <span aria-hidden="true">Next</span> </li>
						<li onclick = "GetData('.round($resultcnt['cnt']/5).');">  <span aria-hidden="true">Last</span>   </li>
					</ul>';
		}
		
		echo $data."~~~".$pagination;
	
	}
}