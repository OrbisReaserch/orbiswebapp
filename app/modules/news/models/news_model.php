<?php
class News_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function getNews()
	{
		$result=$this->db->query("select * from news where status='1' and news_type = 'news' order by id desc");
		return $result->result();
	}		
	public function getNewsData($limit)	{
		$result=$this->db->query("select * from news where status='1' and news_type = 'news' order by id desc ".$limit.";");	
		return $result->result();		 	
		}
		public function getNewsCount()	{	
		
		$result=$this->db->query("select count(id) as cnt from news where status='1' and news_type = 'news';");	
		return $result->row_array();
		}
	public function getNewsDetails($news_id)
	{
		$result=$this->db->query("select * from news where id='$news_id' AND status='1'");
		return $result->result();
	}
	
	public function getUsers()
	{
		$result=$this->db->query("select * from users where status='1'");
		return $result->result();
	}
	public function getReports($report_id)
	{
		$result=$this->db->query("select * from report where id='$report_id' AND status='1'");
		return $result->result();
	}
	public function getlatestReportList()
	{
		$result=$this->db->query("select * from report where status='1' order by id desc");
		return $result->result();
	}
	
	
	public function getCategory()
	{
		$result=$this->db->query("select * from category where status='1'");
		return $result->result();
	}
	public function getSubCategory()
	{
		$result=$this->db->query("select * from sub_category where status='1'");
		return $result->result();
	}
	public function getFeaturedReports()
	{
		$result=$this->db->query("SELECT * FROM report AS r1 JOIN (SELECT CEIL(RAND() * (SELECT MAX(id)   FROM report)) AS id) AS r2 WHERE r1.id >= r2.id ORDER BY r1.id ASC LIMIT 0,3");
		return $result->result();
	}
	public function getProductImage()
	{
		$result=$this->db->query("select * from product_images where status='1' limit 1");
		return $result->result();	
	}
	public function getPublisher()
	{
		$result=$this->db->query("select * from users where status='1'");
		return $result->result();	
	}
	public function getReportList($report_id)
	{
		$result=$this->db->query("select * from report where sub_category_id = '$report_id' and status='1' order by id DESC");
		return $result->result();
	}
	public function getCategoryReportList($category_id)
	{
	$result=$this->db->query("select * from report where category_id = '$category_id' and status='1' order by id DESC");
	return $result->result();	
	}
	
	public function getTestimonials()
	{
		$result=$this->db->query("select * from testimonials where status='1' order by id DESC limit 1");
		return $result->result();
	}
	public function getFooterNews()
	{
		$result=$this->db->query("select * from news where status='1' and news_type = 'news' order by id desc limit 2");
		return $result->result();
	}
	public function getFooterReport()
	{
		$result=$this->db->query("select * from report where status='1' order by id desc limit 5");
		return $result->result();
	
	}
	public function getPressRelease()
	{
		$result=$this->db->query("select * from news where status='1' and news_type = 'press_release' order by id desc");
		return $result->result();
	}
	public function getPressReleaseData($limit)
	{
		$result=$this->db->query("select * from news where status='1' and news_type = 'press_release' order by id desc ".$limit.";");	
		return $result->result();		 	
	}
	public function getPressReleaseCount()
	{		
		$result=$this->db->query("select count(id) as cnt from news where status='1' and news_type = 'press_release';");	
		return $result->row_array();
	}
}
?>