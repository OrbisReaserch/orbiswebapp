<?php
class Publisher_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function register()
	{
		//print_r($_POST); exit;
		
		$username = $_POST['username'];
		$password = $_POST['password'];
		$company_name = $_POST['company_name'];
		$user_type = $_POST['user_type'];
		$title = $_POST['title'];
		$name = $_POST['name'];
		$email = $_POST['email'];
		$phone = $_POST['phone'];
		$fax = $_POST['fax'];
		$address1 = $_POST['address1'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$country = $_POST['country'];
		$zipcode = $_POST['zipcode'];
		$year = $_POST['year'];
		$website = mysql_real_escape_string($_POST['website']);
		$production_details = mysql_real_escape_string($_POST['production_details']);
		$specialisation = mysql_real_escape_string($_POST['specialisation']);
		$message = mysql_real_escape_string($_POST['message']);
		$cost_range = $_POST['cost_range'];
		$d=date('y-m-d');
	
		
		
		$data = array('email'=>$email,'username'=>$username,'display_name'=>$name,'password'=>$password,'company_name'=>$company_name,'first_name'=>$name,
           'user_type'=>$user_type,'address1'=>$address1,'title'=>$title,'zipcode'=>$zipcode,
            'city'=>$city,'country'=>$country,'state'=>$state,'phone'=>$phone,'year_founded'=>$year,'website'=>$website,
            'production_details'=>$production_details,'specialisation'=>$specialisation,'message'=>$message,'cost_range'=>$cost_range,'fax'=>$fax,'join_date'=>$d,'status'=>'0');
		//print_r($data);exit;
		$this->db->insert('users',$data);
		return $this->db->insert_id();
	}

	
public function getPublisherData()
	{
	//echo "select * from content where page_name='Become a publisher' AND status = '1'";die();
		$result=$this->db->query("select * from content where page_name='Become a publisher' AND status = '1'");
		return $result->result();
	}
	
	public function getCategory()
	{
		$result=$this->db->query("select * from category where status='1'");
		return $result->result();
	}
	public function getSubCategory()
	{
		$result=$this->db->query("select * from sub_category where status='1'");
		return $result->result();
	}
	public function getFeaturedReports()
	{
		$result=$this->db->query("SELECT * FROM report AS r1 JOIN (SELECT CEIL(RAND() * (SELECT MAX(id)   FROM report)) AS id) AS r2 WHERE r1.id >= r2.id ORDER BY r1.id ASC LIMIT 0,3");
		return $result->result();
	}
	public function getTestimonials()
	{
		$result=$this->db->query("select * from testimonials where status='1' order by id DESC limit 1");
		return $result->result();
	}
	public function getFooterNews()
	{
		$result=$this->db->query("select * from news where status='1' order by id desc limit 2");
		return $result->result();
	}
	public function getFooterReport()
	{
		$result=$this->db->query("select * from report where status='1' order by id desc limit 5");
		return $result->result();

	}	
	public function getAllPublishers()	{		
		$result=$this->db->query("select * from users where user_type = 'Publisher' AND status='1' order by display_name ASC");		
		return $result->result();	
	}
}
?>