<?php
class Publisher extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('publisher/publisher_model');
		$this->load->helper('form');

	}

	public function index()
	{
		$data['category'] = $this->publisher_model->getCategory();
		$data['subcategory'] = $this->publisher_model->getSubCategory();
		$data['featured'] = $this->publisher_model->getFeaturedReports();
		$data['testimonials'] = $this->publisher_model->getTestimonials();
		$data['footernews'] = $this->publisher_model->getFooterNews();
		$data['footerreport'] = $this->publisher_model->getFooterReport();
		$data['publisherdata'] = $this->publisher_model->getPublisherData();
		$data['include'] = 'publisher/publisher';
		$this->load->view('frontend/container',$data);

		if(isset($_POST['submit']))
		{

			$k=$this->publisher_model->register();
			if($k)
			{

				$this->session->set_flashdata('success', 'Registration has been done successfully.');
				redirect(base_url().'publisher/publisher');
			}
			else
			{

				$this->session->set_flashdata('success', 'Registration not done successfully. Please Try Again.');
				$this->load->view('publisher/publisher',$data);
			}


		}
		//$this->load->view('register/register',$data);

			


	}
	public function get_usernames()
	{
		$username = $_POST['username'];
		//echo $username;
			
			

		$result_user=$this->db->query("select * from users where username='".$username."'");
		if ($result_user->num_rows() > 0)
		{

			echo 'Username already exists';
		}
			
	}
	public function get_email()
	{
		$email = $_POST['email'];
		//echo $username;
		$result_user=$this->db->query("select * from users where email='".$email."'");
		if ($result_user->num_rows() > 0)
		{
			echo 'Email address already exists';
		}
			
	}		public function all_publishers(){				$data['publishers'] = $this->publisher_model->getAllPublishers();		$data['include'] = 'publisher/publisher_list';		$this->load->view('frontend/container',$data);		 	}	
}