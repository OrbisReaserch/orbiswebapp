 <header class="main-header">
    <div class="container">
        <h1 class="page-title">Publishers </h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Publishers</li>
        </ol>
    </div>
</header>


<?php /* ?>
<div class="container">
   <div class="row masonry-container">
 <div class="col-md-4 col-sm-4 col-xs-12 masonry-item">
                     <div class="panel panel-default sitemap">
                         
                        <div class="panel-body">
                          <ul>
	<?php	  
	for($i = 0; $i< sizeof($publishers); $i++)
	{
	if($i <= 9){
	$publisher_name =  str_replace(" ","-",strtolower($publishers[$i]->display_name));
		?>
			<li><a href="<?php echo base_url().'publisher/'.$publisher_name.'.html'; ?>"><i class="fa fa-angle-right"></i> &nbsp; <?php echo $publishers[$i]->display_name; ?></a></li>
	<?php 
	}
	}
	?>
						  </ul>
                        </div>
                    </div>
                </div>
				<div class="col-md-4 col-sm-4 col-xs-12 masonry-item">
                     <div class="panel panel-default sitemap">
                         
                        <div class="panel-body">
                          <ul>
	<?php	  
	for($i = 0; $i< sizeof($publishers); $i++)
	{
		if($i > 9  && $i <= 19){
	$publisher_name =  str_replace(" ","-",strtolower($publishers[$i]->display_name));
		?>
			<li><a href="<?php echo base_url().'publisher/'.$publisher_name.'.html'; ?>"><i class="fa fa-angle-right"></i> &nbsp; <?php echo $publishers[$i]->display_name; ?></a></li>
	<?php 
		}
	} ?>
						  </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 masonry-item">
                     <div class="panel panel-default sitemap">
                         
                        <div class="panel-body">
                          <ul>
	<?php	  
	for($i = 0; $i< sizeof($publishers); $i++)
	{
		if($i > 19){
			$publisher_name =  str_replace(" ","-",strtolower($publishers[$i]->display_name));
		?>
			<li><a href="<?php echo base_url().'publisher/'.$publisher_name.'.html'; ?>"><i class="fa fa-angle-right"></i> &nbsp; <?php echo $publishers[$i]->display_name; ?></a></li>
	<?php 
	} 
	} ?>
						  </ul>
                        </div>
                    </div>
                </div>
				  
    </div>
</div>

 <?php */ ?>  


<div class="container">
         <div class="row">
            <div class="col-md-12">
               <?php 
					$alphabets = array();
					 
					 for ($j = 0; $j< sizeof($publishers); $j++)
						{
							$tmpname = $publishers[$j]->display_name;
							array_push($alphabets,strtoupper($tmpname[0]));
						}
					$uniqueAlpha = array_values(array_unique($alphabets)); 
					  
				 ?>                
				 
              
               <h3 class="post-title">Publishers</h3>
               <div class="row">
                  <div class="col-md-12">
                     <button id="fbutton" class="btn btn-small btn-primary" data-toggle="portfilter" data-target="9"> 9 </button>
					<?php for ($k = 0; $k < sizeof($uniqueAlpha); $k++)
						{
						if($uniqueAlpha[$k] != '9'){
					?>
                     <button id = "alpha_<?php echo $k; ?>" class="btn btn-small btn-primary" data-toggle="portfilter" data-target="<?php echo strtoupper($uniqueAlpha[$k]); ?>"> <?php echo strtoupper($uniqueAlpha[$k]); ?> </button>
                     <?php 
						}
						}
					 ?>
                  </div>
                  <hr>
                  <br>
				   
                  <div class="thumbnails gallery">
				  
						
						
				<?php 
				
					 for ($m = 0; $m < sizeof($uniqueAlpha); $m++)
						{ 
						$tmpAlpha = $uniqueAlpha[$m];
					?>
						 <div class="col-md-12" data-tag="<?php echo strtoupper($tmpAlpha); ?>" style="margin-bottom: 4%;">
						 <h5 class="post-title"><?php echo strtoupper($tmpAlpha); ?></h5>
						 	<ul class="list-unstyled countriesSection">
							  <?php 
 
                     for ($i = 0; $i< sizeof($publishers); $i++)
					{
						$publisher_name =  str_replace(" ","-",strtolower($publishers[$i]->display_name));
						$tmpname2 = $publishers[$i]->display_name;
						if($tmpAlpha == $tmpname2[0]){
							
						?>
							  <li><a href="<?php echo base_url().'publisher/'.$publisher_name.'.html'; ?>"><i class="fa fa-angle-right"></i> &nbsp; <?php echo $publishers[$i]->display_name; ?></a></li>
							   <?php
						}           
					 
					} ?>
							</ul>
						 </div>
						  
                      <?php 
						}
					 ?>
                      
                  </div>
               </div>
            </div>
         </div>
         <!-- row --> 
		 </div>
		 
<script>

function clickAlpha(){
	
    //document.getElementById("fbutton").click();
}
</script> 