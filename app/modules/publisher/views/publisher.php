 <header class="main-header">
    <div class="container">
        <h1 class="page-title">Become a publisher</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Become a publisher</li>
        </ol>
    </div>
</header>


<div class="container">
    <div class="row">
      <form role="form" method="post" name='register' action="<?php echo base_url();?>publisher/publisher">
	  
    <?php

           if($this->session->flashdata('success'))

           {

              echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";

           }

           else if($this->session->flashdata('error'))

           {

             echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";

           }

    ?>
         <div class="col-md-4">
           
                    <div class="form-group">
                        <label for="InputUserName">User Name</label>
                        <input class="form-control" id="InputUserName" type="text" name="username" onblur="get_username(this.value)" required>
                    </div>
                    <div class="form-group">
                        <label for="InputPassword">Password</label>
                        <input class="form-control" id="InputPassword" type="password" name="password" required>
                    </div>
                     <div class="form-group">
                        <label for="InputCompanyName">Company Name</label>
                        <input class="form-control" id="InputCompanyName" type="text"  name="company_name" required>
                    </div> 
                     <div class="form-group">
                        <label for="InputPublisher">Publisher</label>
                        <input class="form-control" id="InputPublisher" readonly value="Publisher" type="text" name="user_type" required>
                    </div> 
                     <div class="form-group">
                        <label for="InputTitle">Title</label>
                        <input class="form-control" id="InputTitle" pattern="^[a-zA-Z ]+$"  type="text" name="title" required>
                    </div> 
                     <div class="form-group">
                        <label for="InputName">Name</label>
                        <input class="form-control" id="InputName" type="text" pattern="^[a-zA-Z ]+$"  name="name" required>
                    </div> 
                     <div class="form-group">
                        <label for="InputEmail">Email</label>
                        <input class="form-control" id="InputEmail" type="email" name="email" onblur="get_email(this.value)" required>
                    </div> 
                     <div class="form-group">
                        <label for="InputPhone">Phone Number</label>
                        <input class="form-control" id="InputPhone" type="tel" name="phone" pattern="^[0-9 ]+$"  >
                    </div>
              </div>
         
         <div class="col-md-4">
            <div class="form-group">
                        <label for="InputFax">Fax</label>
                        <input class="form-control" id="InputFax" type="text" name="fax" pattern="^[0-9 ]+$" >
                    </div>
                     <div class="form-group">
                        <label for="InputCity">City</label>
                        <input class="form-control" id="InputCity" type="text" pattern="^[a-zA-Z ]+$"  name="city" >
                    </div>
                     <div class="form-group">
                        <label for="InputState">State</label>
                        <input class="form-control" id="InputState" type="text" pattern="^[a-zA-Z ]+$"  name="state" >
                    </div>
                     <div class="form-group">
                        <label for="InputCountry">Country</label>
                        <input class="form-control" id="InputCountry" type="text"  pattern="^[a-zA-Z ]+$"  name="country">
                    </div>
                     <div class="form-group">
                        <label for="InputZip">Zip Code</label>
                        <input class="form-control" id="InputZip" type="text" name="zipcode" pattern="^[0-9 ]+$" >
                    </div>
                     <div class="form-group">
                        <label for="InputFoundedYear">Year of organisation being founded</label>
                        <input class="form-control" id="InputFoundedYear" type="text" name="year" pattern="^[0-9 ]+$"  >
                    </div>
                     <div class="form-group">
                        <label for="InputWebsiteUrl">URL for your website</label>
                        <input class="form-control" id="InputWebsiteUrl" type="text" name="website" >
                    </div>
                     <div class="form-group">
                        <label for="InputCostRange">Cost range of these studies</label>
                        <input class="form-control" id="InputCostRange" name="cost_range" type="text" pattern="^[0-9 ]+$" >
                    </div>
                      
         </div>
         
           <div class="col-md-4">
                    <div class="form-group">
                        <label for="InputDate">Date</label>
                        <input class="form-control" id="InputDate" value="<?php echo date("Y-m-d"); ?>"  name = "join_date" readonly type="text">
                    </div>
                      <div class="form-group">
                        <label for="InputPresentAddress">Present Address</label>
                        <textarea class="form-control" id="InputPresentAddress" rows="4" name="address1" ></textarea>
                    </div>
                       <div class="form-group">
                        <label for="InputProductionDetails">Production details</label>
                        <textarea class="form-control" id="InputProductionDetails" rows="4" name="production_details"></textarea>
                    </div>
                       <div class="form-group">
                        <label for="InputSpecialisation">Specialisation of the studiess</label>
                        <textarea class="form-control" id="InputSpecialisation" rows="4" name="specialisation"></textarea>
                    </div>
                     <div class="form-group">
                        <label for="InputMessage">Message/Comments</label>
                        <textarea class="form-control" id="InputMessage" rows="4" name="message" ></textarea>
                    </div>
                    
                    <button type="submit" class="btn btn-lg btn-primary" name="submit">Register</button>
         </div>
         
       </form>  
    </div> <!-- row -->
   
</div>

     