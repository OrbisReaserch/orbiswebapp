<?php
class Profile_model extends CI_Model {

	public function __construct()
	{

		parent::__construct();

	}


	public function getall()
	{
		$query = $this->db->get('users');

		return $query->result();

	}
	public function getMenuCategory()
	{
		$result=$this->db->query("select * from category where status='1'");
		return $result->result();
	}
	public function add_user()
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		$display_name = $_POST['display_name'];
		$user_type = $_POST['usertype'];
		$email = $_POST['email'];
		$company_name = $_POST['company_name'];
		$first_name = $_POST['first_name'];
		$middle_name = $_POST['middle_name'];
		$last_name = $_POST['last_name'];
		$address1 = $_POST['address1'];
		$address2 = $_POST['address2'];
		$zipcode = $_POST['zipcode'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$country = $_POST['country'];
		$mobile = $_POST['mobile'];
		$phone = $_POST['phone'];
		$fax = $_POST['fax'];
		$date=date("Y-m-d h-i-s");
		$data = array('username'=>$username,'display_name'=>$display_name,'password'=>$password,'user_type'=>$user_type,
				'email'=>$email,'company_name'=>$company_name,'first_name'=>$first_name,'middle_name'=>$middle_name,
				'last_name'=>$last_name,'address1'=>$address1,'address2'=>$address2,'zipcode'=>$zipcode,
				'city'=>$city,'country'=>$country,'state'=>$state,'mobile'=>$mobile
		,'phone'=>$phone,'fax'=>$fax,'join_date'=>$date,'status'=>'1');
		$this->db->insert('users',$data);
		return $this->db->insert_id();
	}

	public function update_profile()
	{
		$user_type = $_POST['usertype'];
		
		if($user_type=='Client')
		{
		$user = $_POST['user_id'];
		$display_name = $_POST['display_name'];
		$email = $_POST['email'];
		$company_name = $_POST['company_name'];
		$first_name = $_POST['first_name'];
		$middle_name = $_POST['middle_name'];
		$last_name = $_POST['last_name'];
		$address1 = $_POST['address1'];
		$address2 = $_POST['address2'];
		$zipcode = $_POST['zipcode'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$country = $_POST['country'];
		$mobile = $_POST['mobile'];
		$phone = $_POST['phone'];
		$fax = $_POST['fax'];
		$data = array('display_name'=>$display_name,'user_type'=>$user_type,
				'email'=>$email,'company_name'=>$company_name,'first_name'=>$first_name,'middle_name'=>$middle_name,
				'last_name'=>$last_name,'address1'=>$address1,'address2'=>$address2,'zipcode'=>$zipcode,
				'city'=>$city,'country'=>$country,'state'=>$state,'mobile'=>$mobile
		,'phone'=>$phone,'fax'=>$fax);
		//print_r($data);exit;
		$this->db->where('id',$user);
		$this->db->update('users',$data);
		}
		elseif ($user_type=='Publisher')
		{
			
			
		$user = $_POST['user_id'];
		$display_name = $_POST['display_name'];
		$email = $_POST['email'];
		$company_name = $_POST['company_name'];
//		$first_name = $_POST['first_name'];
//		$middle_name = $_POST['middle_name'];
//		$last_name = $_POST['last_name'];
		$address1 = $_POST['address1'];
		$address2 = $_POST['address2'];
		$zipcode = $_POST['zipcode'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$country = $_POST['country'];
		$mobile = $_POST['mobile'];
		$phone = $_POST['phone'];
		$fax = $_POST['fax'];
		
	    $title = $_POST['title'];
		$year_founded = $_POST['year_founded'];
		$website = $_POST['website'];
		$production_details = mysql_real_escape_string($_POST['production_details']);
		$specialisation = mysql_real_escape_string($_POST['message']);
		$cost_range = $_POST['cost_range'];
		$message = mysql_real_escape_string($_POST['message']);
		
		$data = array('display_name'=>$display_name,'user_type'=>$user_type,
				'email'=>$email,'company_name'=>$company_name,'address1'=>$address1,'address2'=>$address2,'zipcode'=>$zipcode,
				'city'=>$city,'country'=>$country,'state'=>$state,'mobile'=>$mobile
		,'phone'=>$phone,'fax'=>$fax,'title'=>$title,'year_founded'=>$year_founded,'website'=>$website,'production_details'=>$production_details,
		'specialisation'=>$specialisation,'cost_range'=>$cost_range,'message'=>$message);
		//print_r($data);exit;
		$this->db->where('id',$user);
		$this->db->update('users',$data);	
		}
		else 
		{
			
		}

		return TRUE;

	}

        public function change_password($id,$new_password)
        {
            $data = array('password'=>$new_password);
            //print_r($data);exit;
            $this->db->where('id',$id);
            $this->db->update('users',$data);           
        }

        public function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('users');
		return true;
	}
	public function getRecord($id)
	{ 
		$q= $this->db->get_where('users',array('id'=>$id));
		return $q->row_array();
	}

	public function activate_user($id)
	{
		$data = array('status'=>'1');
		$this->db->where('id',$id);
		$this->db->update('users',$data);
	}

	public function deactivate_user($id)
	{
		$data = array('status'=>'0');
		$this->db->where('id',$id);
		$this->db->update('users',$data);
	}

	public function getVendors()
	{
		$result=$this->db->query("select * from users where status='1' and user_type = 'Vendor'");
		return $result->result();
	}

	public function getTestimonials()
	{
		$result=$this->db->query("select * from testimonials where status='1' order by id DESC limit 1");
		return $result->result();
	}
	
	public function getAllUsers()
	{
		$result=$this->db->query("select * from users where status='1'");
		return $result->result();
	}
public function getFooterNews()
	{
		$result=$this->db->query("select * from news where status='1' order by id desc limit 2");
		return $result->result();
	}
	public function getFooterReport()
	{
		$result=$this->db->query("select * from report where status='1' order by id desc limit 5");
		return $result->result();
	
	}

}