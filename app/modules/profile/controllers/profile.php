<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Profile extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->model("category/category_model");
		$this->load->model('profile_model');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
                $this->load->library('email');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	//==================== all page session check =====================

	public function Checklogin()
	{
		if ($this->session->userdata('admin_email') == '')
		{
			redirect('siteadmin/');
		}
	}

	public function index()
	{
		$this->Checklogin();
		$data['result'] = $this->profile_model->getall();
		$data['footernews'] = $this->profile_model->getFooterNews();
		$data['footerreport'] = $this->profile_model->getFooterReport();
		$data['include'] = 'profile/view_profile';
		$data['admin_section'] = 'view_profile';
		$this->load->view('frontend/container', $data);
	}

	public function forgot_pass()
        {
            $data['category'] = $this->category_model->getMenuCategory();
            $data['subcategory'] = $this->category_model->getSubCategory();
            $data['featured'] = $this->category_model->getFeaturedReports();
            $data['footernews'] = $this->profile_model->getFooterNews();
			$data['footerreport'] = $this->profile_model->getFooterReport();
            $data['include'] = 'profile/forgot_password';
            $this->load->view('frontend/container',$data);
        }
        
        public function forgot_password()
        {   
            $to = $_POST['email'];
            $q = $this->db->get_where('users',array('email'=>$to));       
            
            if($q->num_rows() == 1)
            {
                $row = $q->result();
                $password = $row[0]->password; 
                
                $this->email->from('info@orbis.com', 'ORBIS');
                $this->email->to($to);
                $this->email->cc('sandeep@velociters.com'); 
               // $this->email->bcc('them@their-example.com'); 
                $this->email->subject('Orbis - Password recovery');
                $this->email->message('Your Password is : '.$password);	

                $this->email->send();

                $this->email->print_debugger();
                
                $this->session->set_flashdata('success', 'Email sent to inbox !');
                redirect(base_url()."profile/forgot_pass");
            }
            else 
            {
                $this->session->set_flashdata('error', 'Entered Email is not valid');
                redirect(base_url()."profile/forgot_pass");
            }
        }
        
	public function change_password($id)
	{
           // echo "hiii"; exit();
		$data['category'] = $this->category_model->getMenuCategory();
		$data['subcategory'] = $this->category_model->getSubCategory();
		//$data['user_info'] = $this->profile_model->getRecord($id);
		$data['featured'] = $this->category_model->getFeaturedReports();
		$data['footernews'] = $this->profile_model->getFooterNews();
		$data['footerreport'] = $this->profile_model->getFooterReport();
		$data['include'] = 'profile/change_password';
		$this->load->view('frontend/container',$data);
		//$this->load->view('profile/change_password', $data);
	}

	public function change_pass()
	{
		$id = $_POST['user_id'];
		$old_password 	   = $_POST['old_password'];
		$new_password      = $_POST['new_password'];
		$confirm_password  = $_POST['confirm_password'];
		                
//                $query = $this->db->get('users', array('id' => $id));
//		return $query->result();
                $q = $this->db->get_where('users',array('id'=>$id));
                $row = $q->result(); 
              //  print_r($q); exit();
                
//                $qry = mysql_query("select * from users where id = '".$id."'");
//                $fet = mysql_fetch_array($qry);
                
                $original_password = $row[0]->password; 
                  
                if($original_password != $old_password)
                {
                    $this->session->set_flashdata('error', 'Old Password does not match.');
                    redirect(base_url()."profile/change_password/".$id);
                }
		else if($new_password != $confirm_password)
		{
			$this->session->set_flashdata('error', 'New and Confirm Password does not match.');
			redirect(base_url()."profile/change_password/".$id);
		}
                else 
		{
			$this->profile_model->change_password($id,$new_password);
			$this->session->set_flashdata('success', 'Password changed successfully.');
			redirect(base_url()."profile/change_password/".$id);
		}
	}


	public function edit_user($id)
	{
		$data['category'] = $this->category_model->getMenuCategory();
		$data['sub_category'] = $this->category_model->getSubCategory();
		$data['featured'] = $this->category_model->getFeaturedReports();
		$data['testimonials'] = $this->profile_model->getTestimonials();
		$data['info'] = $this->profile_model->getRecord($id);
		$data['footernews'] = $this->profile_model->getFooterNews();
		$data['footerreport'] = $this->profile_model->getFooterReport();
		if(isset($_POST['submit']))
		{
			$this->Checklogin();
			$data['admin_section'] = 'profile';
			$edit = $this->profile_model->edit_user();

			if($edit)
			{
				//$this->session->set_flashdata('success', 'User has been update successfully.');
				redirect(base_url().'profile/view_profile');
			}
			else
			{
				//$this->session->set_flashdata('success', 'Unable to update User.');
				$data['include'] = 'profile/view_profile';
				$this->load->view('frontend/container', $data);
			}
		}
		else
		{
			$data['featured'] = $this->category_model->getFeaturedReports();
			$data['category'] = $this->category_model->getMenuCategory();
			$data['sub_category'] = $this->category_model->getSubCategory();
			$data['footernews'] = $this->profile_model->getFooterNews();
		$data['footerreport'] = $this->profile_model->getFooterReport();
			$data['include'] = 'profile/view_profile';
			$this->load->view('frontend/container', $data);
		}
	}

	public function update_profile($id)
	{
		$data['featured'] = $this->category_model->getFeaturedReports();
		$data['category'] = $this->category_model->getMenuCategory();
		$data['sub_category'] = $this->category_model->getSubCategory();
		$data['testimonials'] = $this->profile_model->getTestimonials();
		$data['info'] = $this->profile_model->getRecord($id);
		$data['footernews'] = $this->profile_model->getFooterNews();
		$data['footerreport'] = $this->profile_model->getFooterReport();
		if(isset($_POST['submit']))
		{

			//$this->Checklogin();
			//$data['admin_section'] = 'profile';
			$id=$_POST['user_id'];
			$edit = $this->profile_model->update_profile();

			if($edit)
			{
				$this->session->set_flashdata('success', 'Profile Successfully Updated.');
				redirect(base_url().'profile/update_profile/'.$id);

			}
			else
			{
				$this->session->set_flashdata('error', 'Unable to update User Profile.');
				$data['include'] = 'profile/profile';
				$this->load->view('frontend/container', $data);
			}
		}
		else
		{
			$data['category'] = $this->category_model->getMenuCategory();
			$data['sub_category'] = $this->category_model->getSubCategory();
			$data['featured'] = $this->category_model->getFeaturedReports();
			$data['footernews'] = $this->profile_model->getFooterNews();
		$data['footerreport'] = $this->profile_model->getFooterReport();
			$data['include'] = 'profile/profile';
			$this->load->view('frontend/container', $data);
		}
	}

	public function delete($id)
	{
		if ($this->advertisement_model->delete($id))
		{
			$this->session->set_flashdata('success', 'Record has been deleted successfully.');
			redirect(base_url() . 'siteadmin/advertisement');
		}
	}

	public function activate_user($id)
	{
		$this->user_model->activate_user($id);
		$this->session->set_flashdata('success', 'User has been activated successfully');
		redirect('siteadmin/user_management');
	}

	public function deactivate_user($id)
	{
		$this->user_model->deactivate_user($id);
		$this->session->set_flashdata('success', 'User has been deactivated successfully');
		redirect('siteadmin/user_management');
	}

}
