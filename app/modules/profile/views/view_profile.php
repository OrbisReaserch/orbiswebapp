<div class="clear"></div>
<div id="slider_wrapper2">
  <div class="wrap">
    <h1>My Account</h1>
  </div>
</div>
<div class="clear"></div>
<div class="content">
  <div class="wrap">
    <div class="one_full">
      <div class="blog-post">
        <div class="one_fourth"> <img src="<?php echo theme_url(); ?>images/adminname.jpg"	 class="blog_img" alt="" /> </div>
        <div class="two_thirds last">
          <h4><?php echo strtoupper($info['display_name']);?></h4>
          <p>Your current account information is displayed below. If you have any questions about your account or need assistance, please contact our Customer Service.</p>
          <div class="clear"></div>
          <div class="tabs_panel">
            <div id="tabs" class="tab-container">
              <ul class="etabs">
                <li class="tab"><a href="#logininfo">Login information</a></li>
                <li class="tab"><a href="#acinfo">Account Information</a></li>
<!--                <li class="tab"><a href="#paymentinfo">Payment Information</a></li>-->
                <li class="tab"><a href="#address">Address</a></li>
              </ul>
              <div class="tab-item" id="logininfo">
                <div class="one_full">
                  <h1>Login Informarion:</h1>
                  <p><i class="icon-envelope"></i> <a href="mailto:<?php echo $info['email'];?>" id="LuckyAnchor_543398753_74"><?php echo $info['email'];?></a></p>
                </div>
              </div>
              <div class="tab-item" id="acinfo">
                <div class="one_full">
                  <h1>Account Information:</h1>
                  <p>Email Id : <?php echo $info['email'];?></p>
                  <p>User Type : <?php echo $info['user_type'];?></p>
                  <p>Company Name : <?php echo $info['company_name'];?></p>
                  <p>First Name : <?php echo $info['first_name'];?></p>
                  <p>Middle Name : <?php echo $info['middle_name'];?></p>
                  <p>Last Name : <?php echo $info['last_name'];?></p>
                  <p>City : <?php echo $info['city'];?></p>
                  <p>State: <?php echo $info['state'];?></p>
                  <p>Country : <?php echo $info['country'];?></p>
                  <p>Mobile Number: <?php echo $info['mobile'];?></p>
                  <p>Phone Number : <?php echo $info['phone'];?></p>
                  <p>Fax : <?php echo $info['fax'];?></p>
                  <p>Join Date : <?php echo $info['join_date'];?></p>
                </div>
              </div>
<!--              <div class="tab-item" id="paymentinfo">-->
<!--                <div class="one_full">-->
<!--                  <h1>Payment Information:</h1>-->
<!--                  <p>Comming soon</p>-->
<!--                </div>-->
<!--              </div>-->
              <div class="tab-item" id="address">
                <div class="one_half">
                  <h3>Shipping Address</h3>
                  <ul>
                    <li><i class="icon-home"></i><?php echo $info['address1'];?></li>
                    <li><i class="icon-envelope"></i><?php echo $info['email'];?></li>
                    <li><i class="icon-phone"></i><?php echo $info['mobile']; ?>&nbsp; &nbsp; </li> 
                    <li><i class="icon-headphones"></i><?php echo $info['phone'];?></li>
<!--                    <li><i class="icon-twitter-sign"></i>Twitter: @username</li>-->
<!--                    <li><i class="icon-facebook-sign"></i>Facebook: username</li>-->
                  </ul>
                </div>
                <div class="one_half last">
                  <h3>Billing Address</h3>
                  <ul>
                    <li><i class="icon-home"></i><?php echo $info['address2'];?></li>
                    <li><i class="icon-envelope"></i><?php echo $info['email'];?></li>
                    <li><i class="icon-phone"></i><?php echo $info['mobile'];?>&nbsp; &nbsp; </li>
                    <li><i class="icon-headphones"></i><?php echo $info['phone'];?></li>
<!--                    <li><i class="icon-twitter-sign"></i>Twitter: @username</li>-->
<!--                    <li><i class="icon-facebook-sign"></i>Facebook: username</li>-->
                  </ul>
                </div>
              </div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clear"></div>
<div class="space1"></div>