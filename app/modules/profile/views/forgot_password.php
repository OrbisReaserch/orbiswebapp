  <div id="main" class="defaultContentWidth">
    <div id="wrapper-row">
      <div id="primary" class="content">
        <div id="content" role="main">
        <?php $user_id=$this->session->userdata('user_id');?>
        
        <header class="entry-header">
              <h1 class="entry-title"><a href="#" title="Permalink to Bon Appetit!" rel="bookmark">Recover Password</a></h1>
        </header> 
                <span style="margin-left: 30px;"></span>
                        <?php
                            if($this->session->flashdata('success'))
                            {
                                echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                            }
                            else if($this->session->flashdata('error'))
                            {
                                echo "<font style='color:red;'>".$this->session->flashdata('error')."</font>";
                            }
                            ?>
            <form name="ait-login-form-widget" id="ait-login-form-widget" action="<?php echo base_url();?>profile/forgot_password" method="post">
                <p class="login-username"><input type="text" name="email" id="email" placeholder="Email ID" class="textboxes" value="" size="20"></p>
                <p class="login-submit"><input type="submit" name="submit" class="btn" value="Submit"></p>
            </form>
          </article>
        </div>
      </div>
    <div id="secondary" class="widget-area" role="complementary">
        <div id="sidebar">
          <div class="widget">
            <h4>CATEGORIES</h4>
            <div id="accordion">
              <?php
               for ($j=0;$j<count($category);$j++)
               {
               	
               ?>
              <h5><a href="<?php echo base_url();?>category/categories"><?php echo $category[$j]->category_name;?> </a></h5>
              <div>
                <ul>
                <?php
               for ($s=0;$s<count($subcategory);$s++)
               {
               	if($category[$j]->id == $subcategory[$s]->category_id)
               	{
               ?>
               
                  <a href="<?php echo base_url();?>reports/report/getSubcategoryInfo/<?php echo $subcategory[$s]->id;?>"> 
                  <li><?php echo $subcategory[$s]->sub_category_name;?> </li> 
                  </a>
                <?php
               	}
               }
                ?>
                </ul>
              </div>
              <?php 
               }
              ?>
              
            </div>
          </div>
          
          <div class="widget">
            <h4>Featured Reports</h4>
            <div class="featured">
              <ul>
                <?php 
                for ($f=0;$f<count($featured);$f++)
                {
                ?>
                <li class="clearfix">
                  <figure> 
	                 <a href="<?php echo base_url();?>reports/report/index/<?php echo $featured[$f]->id;?>">
	                 <img src="<?php echo base_url();?>uploads/report_images/<?php echo $featured[$f]->report_image; ?>" alt="" />
	                  </a> 
                  </figure>
                  <div>
                  <a href="<?php echo base_url();?>reports/report/index/<?php echo $featured[$f]->id;?>" style="color:#737373;">
                    <h5><?php echo substr($featured[$f]->report_name,0,40).'..'; ?></h5>
                    </a>
                    <span>Rs&nbsp;<?php echo $featured[$f]->report_price; ?></span> </div>
                </li>
                <?php 
                }
                ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script>
           function get_username(val)
           {
    	      if(data!='')
			   {
				   document.getElementById("username").value='';
				   document.getElementById("username").focus();
 					 alert(data);
			   }
           } 
 </script>