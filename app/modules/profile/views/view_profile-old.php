  <div id="main" class="defaultContentWidth">
    <div id="wrapper-row">
      <div id="primary" class="content">
        <div role="main">
         
            <div id="product_tabs">
              <ul class="clearfix">
                <li><a href="#tabs-1">My Account</a></li>                
              </ul>
              <!--TABS-->
              <div id="tabs-1" class="tab">
                <div>
                  <div>Profile</div>
                  <h2  style="color: black;"><?php echo strtoupper($info['display_name']);?></h2>
                  Your current account information is displayed below. If you have any questions about your account or need assistance, please contact our <a href="http://www.marketresearch.com/corporate/help/help.asp?SID=31605333-612092200-630819709" id="LuckyAnchor_543398753_67">Customer Service</a>.
                  <table width="100%" border="0" cellspacing="0" cellpadding="4">
                    <tbody>
                      <tr>
                        <th><b>Login Information</b>&nbsp; </td>
                <th><!-- <b>Social Media Login</b> --></td>
                      </tr>
                      <tr>
                          <td>E-mail: <a href="mailto:<?php echo $info['email'];?>" id="LuckyAnchor_543398753_74"><?php echo $info['email'];?></a></td>
                          <td style="text-align:top"><!--  <a href="#" onclick="openpopup('linkedin');" id="LuckyAnchor_543398753_68">Link your account</a>--></td>
                      </tr>
                      <tr>
                        <th width="50%"><b>Account Information</b>&nbsp; </th>
                        <th width="50%"><b>Payment Information</b>&nbsp; </th>
                      </tr>
                      <tr>
                        <td width="50%" valign="top"><?php echo $info['first_name'].' '.$info['middle_name'].' '.$info['last_name'];?></td>
                        <td width="50%" valign="top"></td>
                      </tr>
                      <tr>
                        <th width="50%"><b>Shipping Address</b>&nbsp; </th>
                        <th width="50%"><b>Billing Address</b>&nbsp; </th>
                      </tr>
                      <tr>
                        <td width="50%" valign="top"><?php echo $info['address1'];?><br>
                            Pincode : <?php echo $info['zipcode'];?><br>
                            City : <?php echo $info['city'];?><br>
                            State : <?php echo $info['state'];?><br>
                            Country : <?php echo $info['country'];?><br>
                            Mobile : <?php echo $info['mobile'];?><br>
                            Landline : <?php echo $info['phone'];?><br>
                            Fax : <?php echo $info['fax'];?></td>
                        <td width="50%" valign="top"><?php echo $info['address2'];?><br>
                            Pincode : <?php echo $info['zipcode'];?><br>
                            City : <?php echo $info['city'];?><br>
                            State : <?php echo $info['state'];?><br>
                            Country : <?php echo $info['country'];?><br>
                            Mobile : <?php echo $info['mobile'];?><br>
                            Landline : <?php echo $info['phone'];?><br>
                            Fax : <?php echo $info['fax'];?></td>
                      </tr>                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </article>
          
        </div>
<!--         <a href="<?php echo base_url()?>profile/update_profile/<?php echo $info['id']?>">Update Profile</a>-->
      </div>
      <div id="secondary" class="widget-area" role="complementary">
        
         <aside id="posts-widget-5" class="widget widget_posts">
          <h3 class="widget-title"><span>Testimonials</span></h3>
          <div class="postitems-wrapper"> 
            <?php
               for ($j=0;$j<count($testimonials);$j++)
               {
               	echo $testimonials[$j]->testimonials_message;
               	
               	echo $testimonials[$j]->testimonials_name;
                ?>
          &nbsp;
          <?php 
               echo $testimonials[$j]->testimonials_occupation;
                }
               ?>
         
            </div>
        </aside> 
         <div id="sidebar"> <div class="widget">
            <h4>CATEGORIES</h4>
              <div id="accordion">
           
            <?php for($i = 0; $i< sizeof($category); $i++)
            {?>  
              <h5><a href="#"><?php echo $category[$i]->category_name;?></a></h5>
              <div>
                <ul>
                  <?php for ($j = 0; $j< sizeof($sub_category); $j++)
                  {
                  	if($sub_category[$j]->category_id == $category[$i]->id)
                  	{
                  ?>
                  <a href="<?php echo base_url();?>reports/report/getSubcategoryInfo/<?php echo $sub_category[$j]->id;?>"> 
                  <li><?php echo $sub_category[$j]->sub_category_name?></li>
                  </a>
                  <?php 
                  	} 
                  }
            ?>
                </ul>
              </div>
            <?php }?>
            
            </div>
          </div>
       <div class="widget">
            <h4>Featured Reports</h4>
                 <div class="featured">
              <ul>
			<?php 
			for($f = 0; $f< sizeof($featured); $f++)
			{
                ?>
                <li class="clearfix">
                  <figure> 
	                  <a href="<?php echo base_url();?>reports/report/index/<?php echo $featured[$f]->id;?>">
	                 <img src="<?php echo base_url();?>uploads/report_images/<?php echo $featured[$f]->report_image; ?>" alt="" />
	                 
	                  </a> 
                  </figure>
                  <div>
                  <a href="<?php echo base_url();?>reports/report/index/<?php echo $featured[$f]->id;?>" style="color:#737373;">
                    <h5><?php echo substr($featured[$f]->report_name,0,40).'..'; ?></h5>
                    </a>
                    <span>Rs&nbsp;<?php echo $featured[$f]->report_price;  ?></span> </div>
                </li>
                <?php 
                }?>
              </ul>
            </div>
          </div>
        </div>
              
      </div>
    </div>
  </div>