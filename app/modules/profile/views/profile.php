<div class="clear"></div>
<div id="slider_wrapper2">
  <div class="wrap">
    <h1>Edit Profile</h1>
  </div>
</div>
<div class="clear"></div>
<div class="content">
  <div class="wrap">
    
    <div class="clear"></div>
   
    
    <div class="slider_wrapper2">
    

    
     <?php $user_id=$this->session->userdata('user_id');
     //$user_type=$this->session->userdata('user_type');
     $que=$this->db->query("SELECT * FROM users WHERE id='$user_id'");
     $row=$que->result();
    $user_type= $row[0]->user_type;
    if($user_type=='Client')
    {
     ?>    
          
          
          <?php
           if($this->session->flashdata('success'))
           {
              echo "<font style='color:green; font-weight:bold;'>".$this->session->flashdata('success')."</font>";
           }
           else if($this->session->flashdata('error'))
           {
             echo "<font style='color:red;font-weight:bold;'>".$this->session->flashdata('error')."</font>";
           }
    ?>
    
          
                <form id="contactform" method="post" name='register' action="<?php echo base_url();?>profile/update_profile/<?php echo $row[0]->id;?>">
       <?php /* ?>
         <div class="one_third text_center">
           <img src="<?php echo theme_url(); ?>images/adminname.jpg"/><br/>
            Change photo
            <table align="center" style="width: 70%;margin: 0 auto;">
              <tr>
                <td><input type="file" name="photo" style="width:95%"></td>
              </tr>
            </table>
          </div>
          <?php */ ?>
          <div class="two_thirds last">
       
        <table>
          <tr>
            <td><input type="text" id="name" name="username" value="<?php echo $row[0]->username; ?>" readonly style="background-color:#eeeeee;"/></td>
          </tr>
          <tr>
            <td>
              <input type="text" id="name" name="display_name" placeholder="Display Name" pattern="^[a-zA-Z ]+$" value="<?php echo $row[0]->display_name; ?>" required/>
              </td>
          </tr>
          
            <tr>
            <td>
              <input type="text" id="name" name="usertype" readonly style="background-color:#eeeeee;" value="<?php echo $row[0]->user_type; ?>" />
              </td>
          </tr>
          
          
            <tr>
            <td>
              <input type="email" id="name" name="email"   required value="<?php echo $row[0]->email; ?>"  readonly style="background-color:#eeeeee;"/>
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="company_name" placeholder="Company Name"  value="<?php echo $row[0]->company_name; ?>"/>
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="first_name" placeholder="First Name" pattern="^[a-zA-Z ]+$" value="<?php echo $row[0]->first_name; ?>" />
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="middle_name" placeholder="Middle Name" pattern="^[a-zA-Z ]+$"  value="<?php echo $row[0]->middle_name; ?>"/>
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="last_name" placeholder="Last Name" pattern="^[a-zA-Z ]+$" value="<?php echo $row[0]->last_name; ?>" />
              </td>
          </tr>
          
          
          <tr>
            <td>
              <textarea id="message" placeholder="Current Address" name="address1" rows="5" cols="20"><?php echo $row[0]->address1; ?></textarea>
              </td>
          </tr>
          
          <tr>
            <td>
              <textarea id="message" placeholder="Permanent Address" name="address2" rows="5" cols="20" ><?php echo $row[0]->address2; ?></textarea>
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="zipcode" placeholder="Zip Code" pattern="^[0-9 ]+$" value="<?php echo $row[0]->zipcode; ?>" />
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" placeholder="City"  name="city" pattern="^[a-zA-Z ]+$" value="<?php echo $row[0]->city; ?>" />
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="state" placeholder="State"  pattern="^[a-zA-Z ]+$" value="<?php echo $row[0]->state; ?>" />
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="country" placeholder="Country" pattern="^[a-zA-Z ]+$" value="<?php echo $row[0]->country; ?>" />
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" placeholder="Mobile Number"  name="mobile" pattern="^[0-9 ]+$" maxlength="10" value="<?php echo $row[0]->mobile; ?>" />
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="phone" placeholder="Phone Number"  pattern="^[0-9 ]+$" maxlength="10" value="<?php echo $row[0]->phone; ?>"  />
              </td>
          </tr>
          
          <tr>
            <td>
              <input type="text" id="name" name="fax" placeholder="Fax" pattern="^[0-9 ]+$" value="<?php echo $row[0]->fax; ?>"/>
              </td>
          </tr>
          
          <tr>
            <td>
              <input type="text" id="name"  readonly style="background-color:#eeeeee;" value="<?php echo date("M d Y", strtotime($row[0]->join_date)); ?>" />
              </td>
          </tr>
          
          <tr>
            <td>
            <input type="hidden" id="name" name="user_id" value="<?php echo $row[0]->id; ?>" />
              <input type="submit" value="Update" class="button small" name="submit"/></td>
          </tr>
          
          
          
        </table>
          </div>
      </form>
      <?php 
    }
    elseif ($user_type=='Publisher')
    {
      ?>
     
          
           <?php
           if($this->session->flashdata('success'))
           {
              echo "<font style='color:green; font-weight:bold;'>".$this->session->flashdata('success')."</font>";
           }
           else if($this->session->flashdata('error'))
           {
             echo "<font style='color:red;font-weight:bold;'>".$this->session->flashdata('error')."</font>";
           }
    ?>
          
          
                   
                <form id="contactform" method="post" name='register' action="<?php echo base_url();?>profile/update_profile/<?php echo $row[0]->id;?>">
        
         <?php /* ?>
         <div class="one_third text_center">
           <img src="<?php echo theme_url(); ?>images/adminname.jpg"/><br/>
            Change photo
            <table align="center" style="width: 70%;margin: 0 auto;">
              <tr>
                <td><input type="file" name="photo" style="width:95%"></td>
              </tr>
            </table>
          </div>
          <?php */ ?>
          <div class="two_thirds last">
        
        <table>
          <tr>
            <td><input type="text" id="name" name="username" onblur="" required value="<?php echo $row[0]->username; ?>" readonly style="background-color:#eeeeee;"/></td>
          </tr>
          
          <tr>
            <td>
              <input type="text" id="name" name="company_name" placeholder="Company Name" value="<?php echo $row[0]->company_name; ?>" />
              </td>
          </tr>
          
           <tr>
            <td>
              <input type="text" id="name" name="usertype" readonly value="<?php echo $row[0]->user_type; ?>" style="background-color:#eeeeee;" />
              </td>
          </tr>
          
          <tr>
            <td>
              <input type="text" id="name" name="title" pattern="^[a-zA-Z ]+$" placeholder="Title" required value="<?php echo $row[0]->title; ?>"/>
              </td>
          </tr>
          
          <tr>
            <td>
              <input type="text" id="name" name="display_name" placeholder="Display Name" pattern="^[a-zA-Z ]+$" required value="<?php echo $row[0]->display_name; ?>"/>
              </td>
          </tr>
          
           
          
          
            <tr>
            <td>
              <input type="email" id="name" name="email"  onblur="get_email(this.value)" placeholder="E-mail" required value="<?php echo $row[0]->email; ?>" readonly  style="background-color:#eeeeee;"/>
              </td>
          </tr>
              
               <tr>
            <td>
              <input type="text" id="name" name="mobile" placeholder="Mobile Number"  pattern="^[0-9 ]+$" maxlength="10" value="<?php echo $row[0]->mobile; ?>" />
              </td>
          </tr>
              
              <tr>
            <td>
              <input type="text" id="name" name="phone" placeholder="Phone Number"  pattern="^[0-9 ]+$" maxlength="10" value="<?php echo $row[0]->phone; ?>" />
              </td>
          </tr>
          
          <tr>
            <td>
              <input type="text" id="name" name="fax" placeholder="Fax" pattern="^[0-9 ]+$" value="<?php echo $row[0]->fax; ?>"/>
              </td>
          </tr>
                    
          <tr>
            <td>
              <textarea id="message" placeholder="Present Address" name="address1" rows="5" cols="20" ><?php echo $row[0]->address1; ?></textarea>
              </td>
          </tr>
          
          <tr>
            <td>
              <textarea id="message" placeholder="Permanent Address" name="address2" rows="5" cols="20" ><?php echo $row[0]->address2; ?></textarea>
              </td>
          </tr>
          
            <tr>
            <td>
              <input type="text" id="name" placeholder="City"  name="city" pattern="^[a-zA-Z ]+$"  value="<?php echo $row[0]->city; ?>"/>
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="state" placeholder="State"  pattern="^[a-zA-Z ]+$" value="<?php echo $row[0]->state; ?>" />
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="country" placeholder="Country" pattern="^[a-zA-Z ]+$" value="<?php echo $row[0]->country; ?>" />
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="zipcode" placeholder="Zip Code" pattern="^[0-9 ]+$" value="<?php echo $row[0]->zipcode; ?>" />
              </td>
          </tr>
          
          
          <tr>
            <td>
              <input type="text" id="name" name="year_founded" placeholder="Year of organisation being founded" pattern="^[0-9 ]+$" value="<?php echo $row[0]->year_founded; ?>"/>
              </td>
          </tr>
          
          <tr>
            <td>
              <input type="text" id="name" name="website" placeholder="URL for your website" value="<?php echo $row[0]->website; ?>"/>
              </td>
          </tr>
                           
          <tr>
            <td>
              <textarea id="message" placeholder="Production details" name="production_details" rows="5" cols="20" ><?php echo $row[0]->production_details; ?></textarea>
              </td>
          </tr>
          
          <tr>
            <td>
              <textarea id="message" placeholder="Specialisation of the studiess" name="specialisation" rows="5" cols="20" ><?php echo $row[0]->specialisation; ?></textarea>
              </td>
          </tr>
          
           <tr>
            <td>
              <input type="text" id="name" placeholder="Cost range of these studies"  name="cost_range" pattern="^[0-9 ]+$" maxlength="10"  value="<?php echo $row[0]->cost_range; ?>" />
              </td>
          </tr>
          
          
          <tr>
            <td>
              <textarea id="message" placeholder="Message/Comments" name="message" rows="5" cols="20" ><?php echo $row[0]->message; ?></textarea>
              </td>
          </tr>
          
         
          
          
          
          
          <tr>
            <td>
              <input type="text" id="name" readonly  style="background-color:#eeeeee;" value="<?php echo date("M d Y", strtotime($row[0]->join_date)); ?>" />
              </td>
          </tr>
          
          <tr>
            <td>
            <input type="hidden" id="name" name="user_id" value="<?php echo $row[0]->id; ?>" />
              <input type="submit" value="Update" class="button small" name="submit"/></td>
          </tr>
          
          
          
        </table>
        </div>
      </form>
      
      <?php 
    }
    else 
    {
    	
    	?>
    	
    	 <div class="two_thirds last">
          <h3><a href="#">Sorry...! You Can't Edit your Profile..... </a></h3>
          <div class="clear"></div>
      </div>
    
    <?php 
    }
      ?>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear"></div>
<div class="space1"></div>


<script>
           function get_username(val)
           {
        	   
         // var counter=document.getElementById("username").value;
           
        	  //alert(val); 
        	 
    	   $.post('<?php echo base_url()."register/register/get_usernames";?>',{username:val},function(data)
    			   {
			   if(data!='')
			   {
				   document.register.username.value='';
				   document.register.username.focus();
//				   document.getElementById("username_reg").value='';
//				   document.getElementById("username_reg").focus();
 			 alert(data);
			   }
    			   });	
                  
           }



           function get_email(val)
           {
        	   
         // var counter=document.getElementById("username").value;
           
        	  //alert(val); 
        	 
    	   $.post('<?php echo base_url()."register/register/get_email";?>',{email:val},function(data)
    			   {
			   if(data!='')
			   {
				   document.register.email.value='';
				   document.register.email.focus();
				   
//				   document.getElementById("email").value='';
//				   document.getElementById("email").focus();
 			 alert(data);
			   }
    			   });	
                  
           }  
 </script>
