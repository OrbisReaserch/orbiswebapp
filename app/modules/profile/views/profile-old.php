 <div id="main" class="defaultContentWidth">
    <div id="wrapper-row">
      <div id="primary" class="content">
        <div id="content" role="main">
        <header class="entry-header">
              <h1 class="entry-title"><a href="#" title="Permalink to Bon Appetit!" rel="bookmark">Edit Profile</a></h1>
            </header> 
            <form name="ait-login-form-widget" id="ait-login-form-widget" action="<?php echo base_url();?>profile/update_profile/<?php echo $info['id']?>" method="post">
                  <p class="login-username"><input type="text" name="username" id="user_login" placeholder="Username" class="textboxes" value="<?php echo $info['username'];?>" size="20"></p>
                  <p class="login-password"><input type="password" name="password" id="user_pass" placeholder="Password" class="textboxes" value="<?php echo $info['password'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="display_name" id="user_login" placeholder="Display name" class="textboxes" value="<?php echo $info['display_name'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="usertype" id="user_login" placeholder="User type" readonly="" class="textboxes" value="<?php echo $info['user_type'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="email" id="user_login" placeholder="Email" class="textboxes" value="<?php echo $info['email'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="company_name" id="user_login" placeholder="Company Name" class="textboxes" value="<?php echo $info['company_name'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="first_name" id="user_login" placeholder="First Name" class="textboxes" value="<?php echo $info['first_name'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="middle_name" id="user_login" placeholder="Middle Name" class="textboxes" value="<?php echo $info['middle_name'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="last_name" id="user_login" placeholder="Last Name" class="textboxes" value="<?php echo $info['last_name'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="address1" id="user_login" placeholder="Address 1" class="textboxes" value="<?php echo $info['address1'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="address2" id="user_login" placeholder="Address 2" class="textboxes" value="<?php echo $info['address2'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="zipcode" id="user_login" placeholder="Zipcode" class="textboxes" value="<?php echo $info['zipcode'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="city" id="user_login" placeholder="City" class="textboxes" value="<?php echo $info['city'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="state" id="user_login" placeholder="State" class="textboxes" value="<?php echo $info['state'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="country" id="user_login" placeholder="Country" class="textboxes" value="<?php echo $info['country'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="mobile" id="user_login" placeholder="mobile" class="textboxes" value="<?php echo $info['mobile'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="phone" id="user_login" placeholder="Landline" class="textboxes" value="<?php echo $info['phone'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="fax" id="user_login" placeholder="Fax" class="textboxes" value="<?php echo $info['fax'];?>" size="20"></p>
                  <p class="login-username"><input type="text" name="join_date" id="user_login" placeholder="Join Date" readonly="" class="textboxes" value="<?php echo $info['join_date'];?>" size="20"></p>
                  
                  <p class="login-submit" align="right">
                    <input type="submit" class="btn" name="submit" value="Submit">
                    <input type="hidden" name="user_id" value="<?php echo $info['id']?>">
                  </p>
                </form>
          </article>
        </div>
      </div>
      <div id="secondary" class="widget-area" role="complementary">
        
         <aside id="posts-widget-5" class="widget widget_posts">
          <h3 class="widget-title"><span>Testimonials</span></h3>
          <div class="postitems-wrapper"> 
          
           <?php
               for ($j=0;$j<count($testimonials);$j++)
               {
               	echo $testimonials[$j]->testimonials_message;
               	
               	echo $testimonials[$j]->testimonials_name;
                ?>
          &nbsp;
          <?php 
               echo $testimonials[$j]->testimonials_occupation;
                }
               ?>
         
          
             </div>
        </aside> 
        <div id="sidebar">
          <div class="widget">
            <h4>CATEGORIES</h4>
            <div id="accordion">
              <?php
              for($i = 0; $i< sizeof($category); $i++)
            {?>  
              <h5><a href="#"><?php echo $category[$i]->category_name;?></a></h5>
              <div>
                <ul>
                  <?php for ($j = 0; $j< sizeof($sub_category); $j++)
                  {
                  	if($sub_category[$j]->category_id == $category[$i]->id)
                  	{
                  ?>
                  <a href="<?php echo base_url();?>reports/report/getSubcategoryInfo/<?php echo $sub_category[$j]->id;?>"> 
                  <li><?php echo $sub_category[$j]->sub_category_name?></li>
                  </a>
                  <?php 
                  	} 
                  }
            ?>
                </ul>
              </div>
            <?php }?>
            </div>
          </div>
             <div class="widget">
            <h4>Featured Reports</h4>
                 <div class="featured">
              <ul>
			<?php 
			for($f = 0; $f< sizeof($featured); $f++)
			{
                ?>
                <li class="clearfix">
                  <figure> 
	                  <a href="<?php echo base_url();?>reports/report/index/<?php echo $featured[$f]->id;?>">
	                 <img src="<?php echo base_url();?>uploads/report_images/<?php echo $featured[$f]->report_image; ?>" alt="" />
	                 
	                  </a> 
                  </figure>
                  <div>
                  <a href="<?php echo base_url();?>reports/report/index/<?php echo $featured[$f]->id;?>" style="color:#737373;">
                    <h5><?php echo substr($featured[$f]->report_name,0,40).'..';?></h5>
                    </a>
                    <span>Rs&nbsp;<?php echo $featured[$f]->report_price; ?></span> </div>
                </li>
                <?php 
                }?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>