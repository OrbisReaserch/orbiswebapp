<header class="main-header">
    <div class="container">
        <h1 class="page-title">Privacy policy</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Privacy policy</li>
        </ol>
    </div>
</header>


<div class="container">
    <div class="row">
        <div class="col-md-6">
                
          <?php

			  $query = $this->db->query("SELECT * FROM content WHERE page_name LIKE '%Privacy policy%'");

				$result = $query->result();

				//print_r($result);

				if($result[0]->status==1)

				{

			  ?>

      <?php  

			for($i = 0; $i < count($result); $i++)

			{

			?>

      <p align="justify" style="color:black;"><?php echo str_replace("<ul>","<ul style='list-style-type: disc; color:black;'>",$result[$i]->page_description);?></p>

      <?php 

			}

			}

				else 

				{

					?>

					<p align="justify">

					

          <h3><a href="#">No data Available</a></h3>

      		</p>

					<?php 

				}

			?>

      
      </div>
      
      <div class="col-md-6">
        <h3 class="post-title">Information We Seek from Prospective Clients:</h3>
        
            <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading panel-heading-link">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                     We seek your personal information only under the following circumstances
                    </a>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                  <div class="panel-body">
                    <p>If and when you clearly express a desire to be given a preview of any specific product before placing an order for it.</p>
                    <p>If and when you seek to have your doubts cleared, queries answered or concerns addressed related to any of our products or services mentioned on our website.</p>
                    <p>When you place an order and purchase any of our products.</p>
                    <p>It is mandatory for all our clients to share such information with us under the circumstances mentioned above. Orbis applies this rule to one and all.</p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading panel-heading-link">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
                      Policy Pertaining to Private / Personal Particulars
                    </a>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Orbis Research is known for its professional ethics, insistence on best practices and compliance with the laws of the land. Therefore, any and every piece of personal or private information shared with us by any of clients (individual and institutional) is invariably kept classified and never shared with any third party for any manner of promotional activity. All such information is used by us to serve our clients better and is shared with external agencies only for verification purposes. </p>
                  
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading panel-heading-link">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
                    Here is what we do with the above mentioned information
                    </a>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>To ascertain the precise product sought by our client or try and produce such a product within a reasonable timeframe ensuring thereby that the specific information sought by the client is contained therein.</p>
                    <p>It is necessary to store such personal or private information at Orbis to complete the purchase procedure. Some information, such as your credit card information, has to inevitably be shared with external parties for verification.</p>
                    <p> Your personal or private information is also a reliable indicator to us of the kind of products you are likely to require in the future. This enables us to keep track of all new studies or reports we receive, which may be of interest to you, and keep you updated on their availability, as well as provide you with periodic lists of relevant report options.</p>
                  
                  </div>
                </div>
              </div>
            </div>
            
       </div>
      
      
    </div> <!-- row -->
   
</div>

      
      
<?php include 'includes/footer.php';?>
