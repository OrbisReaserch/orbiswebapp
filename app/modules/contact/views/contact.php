
      <header class="main-header">
         <div class="container">
            <h1 class="page-title">Contact Us</h1>
            <ol class="breadcrumb pull-right">
               <li><a href="<?php echo base_url(); ?>">Home</a></li>
               <li class="active">Contact Us</li>
            </ol>
         </div>
      </header>
      <div class="container">
         <div class="row">
            <div class="col-md-3">
               <div class="contactAddress">
                  <h3 class="post-title">US Office</h3>
                  <ul class="list-unstyled">
                    <li><i class="fa fa-home"></i> &nbsp; <?php echo $contact_info[0]->address;?></li>
                        <li><i class="fa fa-phone"></i> &nbsp; <?php echo $contact_info[0]->phone;?></li>
                          <!-- <li><i class="fa fa-phone"></i> &nbsp; +1 (210)-667-2421</li> -->
                        <li><i class="fa fa-envelope"></i>  &nbsp; <a href="mailto:<?php echo $contact_info[0]->email;?>"> <?php echo $contact_info[0]->email;?></a></li>  
                        <li><i class="fa fa-envelope"></i>  &nbsp; <a href="mailto:sales@orbisresearch.com">sales@orbisresearch.com</a></li>
                        <li><i class="fa fa-skype"></i>  &nbsp; Skype: <?php echo $contact_info[0]->skype;?></li>
                        <li><i class="fa fa-twitter"></i>  &nbsp; Twitter: @<?php echo $contact_info[0]->twitter;?></li>
                        <li><i class="fa fa-facebook"></i>  &nbsp; Facebook: <?php echo $contact_info[0]->facebook;?></li>
                  </ul>
               </div>
            </div>
            <div class="col-md-3">
               <div class="contactAddress">
                  <h3 class="post-title">Rest of the world</h3>
                  <ul class="list-unstyled">
                    <li><i class="fa fa-home"></i> &nbsp; <?php echo $contact_info[1]->address;?></li>
                        <li><i class="fa fa-phone"></i> &nbsp; <?php echo $contact_info[1]->phone;?></li>
                        <li><i class="fa fa-envelope"></i>  &nbsp; <a href="mailto:<?php echo $contact_info[1]->email;?>"> <?php echo $contact_info[0]->email;?></a></li>  
                        <li><i class="fa fa-envelope"></i>  &nbsp; <a href="mailto:sales@orbisresearch.com">sales@orbisresearch.com</a></li>
                        <li><i class="fa fa-skype"></i>  &nbsp; Skype: <?php echo $contact_info[1]->skype;?></li>
                        <li><i class="fa fa-twitter"></i>  &nbsp; Twitter: @<?php echo $contact_info[1]->twitter;?></li>
                        <li><i class="fa fa-facebook"></i>  &nbsp; Facebook: <?php echo $contact_info[1]->facebook;?></li>
                  </ul>
               </div>
            </div>
            <div class="col-md-6">
               <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d945.380542145091!2d73.7838461!3d18.5955691!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2b919c969729d%3A0x142737b241ac3f2e!2sORBIS+RESEARCH!5e0!3m2!1sen!2sin!4v1523015260625" width="100%" height="352" frameborder="0" style="border:solid 1px #eee" allowfullscreen></iframe>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-12 col-md-12">
               <div class="panel panel-default sendusqueryform">
                  <div class="panel-heading">
                     <h4>Send Us A Query !</h4>
                  </div>
								  <span style="margin-left: 0px; font-size:14px;">
							<?php
							  if($this->session->flashdata('success'))
								  {
								   echo "<center id='flashdata_success'><font style='color:green;'>".$this->session->flashdata('success')."</font></center> <br>";
								  
								 }
								   else if($this->session->flashdata('error'))
									{
									 echo "<center><font style='color:red;'>".$this->session->flashdata('errror')."</font></center><br>";
									 
									}
											?>
							</span>
                  <div class="panel-body">
                     <p>Please fill in the details with your queries and we will get back to you within 24 hours.</p>
                    
					  <form role="form" class="form-horizontal" method="post" action="<?php echo base_url();?>contact/send_contact">
                        <div class="row">
                           <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                 <label for="InputName" class="control-label col-xs-4"><span class="redstar">*</span> Your Name:</label>
                                 <div class="col-xs-8">
                                    <input class="form-control" id="name" name="name"    pattern="^[a-zA-Z ]+$" required >
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="InputEmail1" class="control-label col-xs-4"><span class="redstar">*</span> Your Email address:</label>
                                 <div class="col-xs-8">
                                    <input class="form-control" id="email" name="email"    required >
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="InputPhone" class="control-label col-xs-4"><span class="redstar">*</span> Your Phone Number :</label>
                                 <div class="col-xs-8">
                                      <input class="form-control" id="name" name="phone"   required >
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="InputJobTitle" class="control-label col-xs-4"><span class="redstar">*</span> Job Title:</label>
                                 <div class="col-xs-8">
                                     <input class="form-control" id="name" name="designation"   required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6 col-sm-6">
                              <div class="form-group">
                                 <label for="Categories" class="control-label col-xs-4">Select Country:</label>
                                 <div class="col-xs-8">
                                    <select name = "country"  class="form-control">
									<?php 
									for($c = 0; $c<count($countries); $c++)
									{
										?>
										<option value = "<?php echo $countries[$c]->country_name;?>"><?php echo $countries[$c]->country_name;?></option>
										<?php
									}
									?>
                                    </select>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="InputCompanyName" class="control-label col-xs-4"><span class="redstar">*</span> Your Company:</label>
                                 <div class="col-xs-8">
                                     <input class="form-control" id="name" name="company"    required>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label for="InputMessage" class="control-label col-xs-4"><span class="redstar">*</span> Queries on report :</label>
                                 <div class="col-xs-8">
                                    
									<textarea class="form-control"  id="message" name="comment" rows="4" required></textarea>
                                 </div>
                              </div>
                            
                                 <button type="submit" name = "submit" value = "submit" class="btn btn-primary pull-right"><i class="fa fa-send"></i>  Send</button>
                           
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script>
      $('#flashdata_success')[0].scrollIntoView(true);
    </script> 