
<header class="main-header">
   <div class="container">
      <h1 class="page-title">Faq's</h1>
      <ol class="breadcrumb pull-right">
         <li><a href="<?php echo base_url(); ?>">Home</a></li>
         <li class="active">Faq's</li>
      </ol>
   </div>
</header>
<div class="container">
   <div class="row">
      <div class="col-md-12">
         <div class="panel-group" id="accordion">
            <?php
			  $query = $this->db->query("SELECT * FROM faq WHERE status = '1'");
				$result = $query->result();
				for($f = 0; $f<count($result); $f++)
				{
			  ?>
            <div class="panel panel-default">
               <div class="panel-heading panel-heading-link"> 
					<a onclick = "ShowDiv(<?php echo $f; ?>);" data-toggle="collapse" data-parent="#accordion" href="<?php echo $f; ?>" class="collapsed"> <?php echo $result[$f]->question;?> </a> 
			   </div>
               <div id="<?php echo $f; ?>" class="panel-collapse collapse <?php if($f == 0){ echo "in"; } ?>">
                  <div class="panel-body">
                     <p><?php echo $result[$f]->answer;?> </p>
                  </div>
               </div>
            </div>
            <?php 
				}
				?>
         </div>
      </div>
   </div>
</div>
<script>
	function ShowDiv(id){
		
		$("#"+id).addClass("in");
		<?php for($j = 0; $j<count($result); $j++)
				{ ?>
			if(id != <?php echo $j; ?>){
				$("#"+<?php echo $j; ?>).removeClass("in");
			}
		<?php } ?>
			
		
	}
</script>
<!-- row --> 