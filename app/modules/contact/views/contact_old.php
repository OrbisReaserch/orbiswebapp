
 <header class="main-header">
    <div class="container">
        <h1 class="page-title">Contact Us</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Contact Us</li>
        </ol>
    </div>
</header>


<div class="container">
    
      <span style="margin-left: 0px; font-size:14px;">
            <?php
              if($this->session->flashdata('success'))
                  {
                   echo "<font style='color:green;'>".$this->session->flashdata('success')."</font>";
                   echo "<br><br>";
                 }
                   else if($this->session->flashdata('error'))
                    {
                     echo "<font style='color:red;'>".$this->session->flashdata('errror')."</font>";
                     echo "<br><br>";
                    }
                            ?>
            </span>
    <div class="row">
      
        <div class="col-sm-4 col-md-4">
        <div class="panel panel-default sendusqueryform">
           <div class="panel-heading"> <h4>Send Us A Query !</h4></div>
           <div class="panel-body">
               <p>Please fill in the details with your queries and we will get back to you within 24 hours.</p>

                <form role="form" id="contactform" method="post" action="<?php echo base_url();?>contact/send_contact">
                    <div class="form-group">
                        <label for="InputName">Your Name</label>
                        <input class="form-control" id="name" name="name" placeholder="Your Name"  pattern="^[a-zA-Z ]+$" required >
                    </div>
                    <div class="form-group">
                        <label for="InputEmail1">Your Email address</label>
                        <input class="form-control" id="email" name="email" placeholder="Your Email Address"  required >
                    </div>
                     <div class="form-group">
                        <label for="InputPhone">Your Phone Number</label>
                        <input class="form-control" id="name" name="phone" placeholder="Your Phone Number"  required >
                    </div>
                      <div class="form-group">
                        <label for="InputJobTitle">Job Title</label>
                        <input class="form-control" id="name" name="designation" placeholder="Job Title"  required>
                    </div>
                    <div class="form-group">
                          <label for="country">Select Country</label>
                          <select  name = "country"   class="form-control">
                             <?php 
			for($c = 0; $c<count($countries); $c++)
			{
				?>
				<option value = "<?php echo $countries[$c]->country_name;?>"><?php echo $countries[$c]->country_name;?></option>
				<?php
			}
			?>
                          </select>
                      </div>
                    <div class="form-group">
                        <label for="InputCompanyName">Your Company</label>
                        <input class="form-control" id="name" name="company" placeholder="Your Company"  required>
                    </div> 
                    <div class="form-group">
                        <label for="InputMessage">Mesagge</label>
                        
						<textarea class="form-control"  id="message" name="comment" rows="8" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Send</button>
                    <div class="clearfix"></div>
                </form>
           </div>
         
        </div>
        </div>
        
        
          <div class="col-sm-8 col-md-8">
           <div class="row">
            <div class="col-sm-6 col-md-6">
               <div class="contactAddress">
                  <h3 class="post-title">US Headquarters</h3>
                  <ul class="list-unstyled">
                        <li><i class="fa fa-home"></i> &nbsp; <?php echo $contact_info[0]->address;?></li>
                        <li><i class="fa fa-phone"></i> &nbsp; <?php echo $contact_info[0]->phone;?></li>
                        <li><i class="fa fa-envelope"></i>  &nbsp; <a href="mailto:<?php echo $contact_info[0]->email;?>"> <?php echo $contact_info[0]->email;?></a></li>  
                        <li><i class="fa fa-envelope"></i>  &nbsp; <a href="mailto:sales@orbisresearch.com">sales@orbisresearch.com</a></li>
                        <li><i class="fa fa-skype"></i>  &nbsp; Skype: <?php echo $contact_info[0]->skype;?></li>
                        <li><i class="fa fa-twitter"></i>  &nbsp; Twitter: @<?php echo $contact_info[0]->twitter;?></li>
                        <li><i class="fa fa-facebook"></i>  &nbsp; Facebook: <?php echo $contact_info[0]->facebook;?></li>
                  </ul>
              </div>
            </div>
             <div class="col-sm-6 col-md-6">
              <div class="contactAddress">
                      <h3 class="post-title">Rest of world</h3>
                      <ul class="list-unstyled">
                            <li><i class="fa fa-home"></i> &nbsp; <?php echo $contact_info[1]->address;?></li>
                        <li><i class="fa fa-phone"></i> &nbsp; <?php echo $contact_info[1]->phone;?></li>
                        <li><i class="fa fa-envelope"></i>  &nbsp; <a href="mailto:<?php echo $contact_info[1]->email;?>"> <?php echo $contact_info[0]->email;?></a></li>  
                        <li><i class="fa fa-envelope"></i>  &nbsp; <a href="mailto:sales@orbisresearch.com">sales@orbisresearch.com</a></li>
                        <li><i class="fa fa-skype"></i>  &nbsp; Skype: <?php echo $contact_info[1]->skype;?></li>
                        <li><i class="fa fa-twitter"></i>  &nbsp; Twitter: @<?php echo $contact_info[1]->twitter;?></li>
                        <li><i class="fa fa-facebook"></i>  &nbsp; Facebook: <?php echo $contact_info[1]->facebook;?></li>
                      </ul>
                  </div>
             </div>
           </div>
           <div class="row">
           <div class="col-md-12">
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62686408.44608889!2d42.68876705938636!3d16.471753055811206!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30635ff06b92b791%3A0xd78c4fa1854213a6!2sIndia!5e0!3m2!1sen!2sin!4v1481644939945" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
           </div>
           </div>
          </div>

       
      
      
      
      
    </div>
</div>
 