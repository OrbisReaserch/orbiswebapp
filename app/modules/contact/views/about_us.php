
 <header class="main-header">
    <div class="container">
        <h1 class="page-title">About Us</h1>

        <ol class="breadcrumb pull-right">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">About Us</li>
        </ol>
    </div>
</header>


<div class="container">
    <div class="row">
       
        <div class="col-md-7">
            
            
     <?php
			  $query = $this->db->query("SELECT * FROM content WHERE page_name LIKE '%About Us%'");
				$result = $query->result();
					if($result[0]->status==1)
				{
			 
					for($i = 0; $i < count($result); $i++)
					{
						?>
					  <p align="justify">
					  <?php $a = str_replace('<strong>About Us:</strong>',' <h2 class="post-title">About Orbis Research</h2>',$result[$i]->page_description);
					  $b = str_replace('<strong>Our Services:</strong>',' <h2 class="post-title">Our Services</h2>',$a);
					  $c = str_replace('<strong>For Academicians:</strong>',' <h2 class="post-title">For Academicians</h2>',$b);
					  echo $c;
					  ?></p>
						<?php 
					}
				}
				else 
				{
					?>
					<p align="justify">
          <h3><a href="#">No data Available</a></h3>
      		</p>
					<?php 
				}
			?>
        </div>
        <div class="col-md-5">
            <h2 class="post-title">For Academicians</h2>    
            <p>Elaborate studies are available on every conceivable aspect of markets in all geographies, which could be tapped into for the extensive study of trends, to make forecasts. Reliable intelligence (qualitative and quantitative), in-depth insights into the latest trends in a wide range of sectors, including pharmaceuticals, food and beverages, electronics, chemicals, nuclear technology, FMCGs, and the manufacturing industries, are all available on Orbis. This priceless research data is made available at a special discount, exclusively for students, wherever applicable.</p>
            <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading panel-heading-link">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                     <i class="fa fa-bullseye"></i>  Vision
                    </a>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                  <div class="panel-body">
                    <p>Our vision is to evolve into one of the world's premier market research data centres, at which, reliable data related to the latest market trends, based on exhaustive research, by individuals with impeccable credentials, would be made easily available to the researcher and businessman alike. Our vision is to emerge into a viable one-stop shop for every conceivable market-related research that serious seekers of such information would be able to turn to with confidence.</p>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading panel-heading-link">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" ><!-- class="collapsed" -->
                       <i class="fa fa-lightbulb-o"></i> Mission
                    </a>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse in">
                  <div class="panel-body">
                    <p>Our mission is to distribute comprehensive market-related data coupled with in-depth analyses, put together by researchers of repute and publishing houses of great standing, to any serious seeker of such critical information. We subject whatever is submitted to us to the most stringent scrutiny as it has always been our mission to ensure the highest standards of scholarship and probity. We make a conscientious effort to post on Orbis original research data, which is constantly replaced by the latest findings reflecting current trends at any given point in time. </p>
                    <p>Fundamentally, it is our mission to come to the aid of the busy businessman and the overworked academician alike. Meeting the research-related requirements of our global clientele is our mission and, quite frankly, that is why we exist.</p>
                  </div>
                </div>
              </div>
              
            </div>
        </div>
    </div> <!-- row -->
   
</div>


