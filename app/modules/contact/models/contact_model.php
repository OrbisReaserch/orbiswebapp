<?php
class Contact_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function getContactInfo()
	{
		$result=$this->db->query("select * from contact_info where status = '1'");
		return $result->result();
	}
	public function getReportById($id)
	{
		$result = $this->db->query("select * from report where id = '$id';");
		return $result->result();
	}
	public function getContactText()
	{
		$result=$this->db->query("select * from content where page_name='Contact Us' AND status = '1'");
		return $result->result();
	}
	public function getCategory()
	{
		$result=$this->db->query("select * from category where status='1'");
		return $result->result();
	}
	public function getSubCategory()
	{
		$result=$this->db->query("select * from sub_category where status='1'");
		return $result->result();
	}
	public function getFeaturedReports()
	{
		$result=$this->db->query("SELECT * FROM report AS r1 JOIN (SELECT CEIL(RAND() * (SELECT MAX(id)   FROM report)) AS id) AS r2 WHERE r1.id >= r2.id ORDER BY r1.id ASC LIMIT 0,3");
		return $result->result();
	}
  public function getTestimonials()
	{
		$result=$this->db->query("select * from testimonials where status='1' order by id DESC limit 1");
		return $result->result();
	}
	public function getCountryList()
	{
		$result=$this->db->query("select * from country where isContinent = '0' ORDER BY  `country_name` ; ");
		return $result->result();
	}
	public function send_contact_request()
	{
		$name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
		$country = $_POST['country'];
		$company = $_POST['company'];
		$designation = $_POST['designation'];
        $comment = mysql_real_escape_string($_POST['comment']);
        $d=date('y-m-d');
        $data = array('contact_name'=>$name,'contact_email'=>$email,'contact_phone'=>$phone,'designation'=>$designation,'country'=>$country,'company'=>$company,'contact_message'=>$comment,'contact_date'=>$d,'status'=>'1');
        //print_r($data);exit;
            $this->db->insert('contact_us',$data);
            return $this->db->insert_id();	
	}
	
public function getFooterNews()
	{
		$result=$this->db->query("select * from news where status='1' order by id desc limit 2");
		return $result->result();
	}
	public function getFooterReport()
	{
		$result=$this->db->query("select * from report where status='1' order by id desc limit 5");
		return $result->result();
	
	}
	
	/* public function getUsers()
	{
		$result=$this->db->query("select * from users");
		return $result->result();
	}
	public function getReports($report_id)
	{
		$result=$this->db->query("select * from report where id='$report_id'");
		return $result->result();
	}
	
	public function getProductImage()
	{
		$result=$this->db->query("select * from product_images where status='1' limit 1");
		return $result->result();	
	}
	public function getPublisher()
	{
		$result=$this->db->query("select * from users where status='1'");
		return $result->result();	
	}
	public function getReportList($report_id)
	{
		$result=$this->db->query("select * from report where sub_category_id='$report_id' order by id DESC");
		return $result->result();
	}
	//-------------- Repourt Sorting Start------------
	
	
	public function getReportListPriceDesc($subcategoryid)
	{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_price DESC");
		return $result->result();	
	}
	public function getReportListPriceAsc($subcategoryid)
	{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_price ASC");
		return $result->result();	
	}
	
	public function getReportListDateDesc($subcategoryid)
	{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_date DESC");
		return $result->result();	
	}
	public function getReportListDateAsc($subcategoryid)
	{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_date ASC");
		return $result->result();	
	}
	
	public function getReportListTitleDesc($subcategoryid)
	{
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_name DESC");
		return $result->result();	
	}
	public function getReportListTitleAsc($subcategoryid)
	{
		
		$result=$this->db->query("select * from report where sub_category_id='$subcategoryid' order by report_name ASC");
		return $result->result();	
	}
	//-------------- Repourt Sorting End------------
	
	*/

}
?>
