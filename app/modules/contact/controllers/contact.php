<?php
class Contact extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('contact/contact_model');
		$this->load->model('home/home_model');
		$this->load->model('register/register_model');
		$this->load->helper('form');
		$this->load->library('email');
	}
 
	
	public function purchase_old($id){
	// $id=$_POST['report_id'];
	
    if('' == $_POST['report_price']){
		$r = $this->contact_model->getReportById($id);
		// print_r($r); die();
		$report_price = $r[0]->report_price;
	}else{
		$report_price=$_POST['report_price'];
	}
			$data['clients'] = $this->home_model->getClients();
			 $data['product'] = $id;
			 $data['countries'] = $this->contact_model->getCountryList();
			 $data['report_price'] = $report_price;
			 $data['include'] = 'reports/checkout';
			$this->load->view('frontend/container',$data);
	}
	public function purchase($id){

 		$url=base_url()."contact/purchase-single-user/".$id;
 		redirect($url);
	}
	
	public function purchase_single_user($id){

			 //print_r($_POST);exit;
			 $r = $this->contact_model->getReportById($id);
			 $data['report_price'] =$r[0]->report_price;
			 $data['product'] = $id;
			 $data['countries'] = $this->contact_model->getCountryList();
			 $data['clients'] = $this->home_model->getClients();
			 $data['include'] = 'reports/checkout';
			 $data['price_type'] ='single';
			 //$message=array('status'=>'success','message'=>'Sign up successfully');
			 //echo json_encode($message);
			$this->load->view('frontend/container',$data);
	}
	public function purchase_multiple_user($id){

			 //print_r($_POST);exit;
			 $r = $this->contact_model->getReportById($id);
			 $data['report_price'] =$r[0]->multiple_price;
			 $data['product'] = $id;
			 $data['countries'] = $this->contact_model->getCountryList();
			 $data['clients'] = $this->home_model->getClients();
			 $data['include'] = 'reports/checkout';
			 $data['price_type'] ='multiple';
			 //$message=array('status'=>'success','message'=>'Sign up successfully');
			 //echo json_encode($message);
			$this->load->view('frontend/container',$data);
	}
	public function purchase_global_user($id){

			 //print_r($_POST);exit;
			 $r = $this->contact_model->getReportById($id);
			 $data['report_price'] =$r[0]->global_price;
			 $data['product'] = $id;
			 $data['countries'] = $this->contact_model->getCountryList();
			 $data['clients'] = $this->home_model->getClients();
			 $data['include'] = 'reports/checkout';
			  $data['price_type'] ='global';
			 //$message=array('status'=>'success','message'=>'Sign up successfully');
			 //echo json_encode($message);
			$this->load->view('frontend/container',$data);
	}
	public function purchase_corporate_user($id){

			 //print_r($_POST);exit;
			 $r = $this->contact_model->getReportById($id);
			 $data['report_price'] =$r[0]->corporate_price;
			 $data['product'] = $id;
			 $data['countries'] = $this->contact_model->getCountryList();
			 $data['clients'] = $this->home_model->getClients();
			 $data['include'] = 'reports/checkout';
			  $data['price_type'] ='corporate';
			 //$message=array('status'=>'success','message'=>'Sign up successfully');
			 //echo json_encode($message);
			$this->load->view('frontend/container',$data);
	}
	public function purchase2checkout($id){
	if('' == $_POST['report_price']){
		$r = $this->contact_model->getReportById($id);
		$report_price = $r[0]->report_price;
	}else{
		$report_price=$_POST['report_price'];
	}
	
			 $data['product'] = $id;
			 $data['countries'] = $this->contact_model->getCountryList();
			 $data['report_price'] = $report_price;
			 $data['include'] = 'reports/2checkout';
			$this->load->view('frontend/container',$data);
	}
	public function index()
	{
			
			
		//$data['publisher'] = $this->register_model->getPublisher();
		//$data['reports'] = $this->register_model->getReports($report_id);
		 
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->contact_model->getFooterNews();
		$data['footerreport'] = $this->contact_model->getFooterReport();
		$data['include'] = 'register/register';
		$this->load->view('frontend/container',$data);

		if(isset($_POST['submit']))
		{

			$k=$this->register_model->register();
			if($k)
			{

				$this->session->set_flashdata('success', 'Registration has been done successfully.');
				redirect(base_url().'register/register');
			}
			else
			{

				$this->session->set_flashdata('success', 'Registration not done successfully. Please Try Again.');
				$this->load->view('register/register',$data);
			}
				

		}
		//$this->load->view('register/register',$data);

		 


	}

	public function get_usernames()
	{
		$username = $_POST['username'];
		//echo $username;
		 
		 

		$result_user=$this->db->query("select * from users where username='".$username."'");
		if ($result_user->num_rows() > 0)
		{

			echo 'Username already exists';
		}
			
	}


	public function about_us()
	{
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->contact_model->getFooterNews();
		$data['footerreport'] = $this->contact_model->getFooterReport();
			
		$data['include'] = 'contact/about_us';
		$this->load->view('frontend/container',$data);

	}

	public function services()
	{
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->contact_model->getFooterNews();
		$data['footerreport'] = $this->contact_model->getFooterReport();
			
		$data['include'] = 'contact/services';
		$this->load->view('frontend/container',$data);

	}
	public function terms_and_conditions()
	{
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->contact_model->getFooterNews();
		$data['footerreport'] = $this->contact_model->getFooterReport();
			
		$data['include'] = 'contact/terms_and_conditions';
		$this->load->view('frontend/container',$data);

	}
	
	public function refund_cancellation_policy()
	{
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->contact_model->getFooterNews();
		$data['footerreport'] = $this->contact_model->getFooterReport();
			
		$data['include'] = 'contact/refund_cancellation_policy';
		$this->load->view('frontend/container',$data);

	}
	
	public function privacy_policy()
	{
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->contact_model->getFooterNews();
		$data['footerreport'] = $this->contact_model->getFooterReport();
			
		$data['include'] = 'contact/privacy_policy';
		$this->load->view('frontend/container',$data);

	}
	
	public function disclaimer()
	{
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->contact_model->getFooterNews();
		$data['footerreport'] = $this->contact_model->getFooterReport();
			
		$data['include'] = 'contact/disclaimer';
		$this->load->view('frontend/container',$data);

	}
	public function faq()
	{
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->contact_model->getFooterNews();
		$data['footerreport'] = $this->contact_model->getFooterReport();
			
		$data['include'] = 'contact/faq';
		$this->load->view('frontend/container',$data);

	}
public function payment_procedures()
	{
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->contact_model->getFooterNews();
		$data['footerreport'] = $this->contact_model->getFooterReport();
			
		$data['include'] = 'contact/payment_procedures';
		$this->load->view('frontend/container',$data);

	}
	
	
	public function contact_us()
	{
		$data['contact_info'] = $this->contact_model->getContactInfo();
		$data['category'] = $this->register_model->getCategory();
		$data['subcategory'] = $this->register_model->getSubCategory();
		$data['featured'] = $this->register_model->getFeaturedReports();
		$data['testimonials'] = $this->register_model->getTestimonials();
		$data['footernews'] = $this->contact_model->getFooterNews();
		$data['footerreport'] = $this->contact_model->getFooterReport();
		$data['contacttext'] = $this->contact_model->getContactText();
		$data['countries'] = $this->contact_model->getCountryList();
		$data['include'] = 'contact/contact';
		$this->load->view('frontend/container',$data);

	}
	/* public function send_contact()
	{
		if(isset($_POST['submit']))
		{
				
			$k=$this->contact_model->send_contact_request();
			 
			if($k)
			{
				$name = $_POST['name'];
				$email = $_POST['email'];
				$phone = $_POST['phone'];
				$job_titel= $_POST['job_titel'];
				$orginisation = $_POST['orginisation'];
				$country = $_POST['country'];
				$comment = mysql_real_escape_string($_POST['comment']);
				 

				//
				//                $this->email->from($email, $name);
				//                $this->email->to('arunjith@velociters.com', 'ArunJith');
				//               // $this->email->cc('sandeep@velociters.com');
				//               // $this->email->bcc('them@their-example.com');
				//                $this->email->subject('Orbis - Contact Us  Information ');
				//
				//             $this->email->message('Contact Us  Information Name : '.$name.'Email Id : '.$email.'Phone Number :'.$phone.'Message : '.$comment);
				//			//$this->email->message('hello friend');
				//                $this->email->send();

				//$to='arunjith@velociters.com';
				$to='customerservice@orbisresearch.com';
				$subject='Orbis - Contact Us  Information';
				//$message='<b>Contact Us  Information </b> <br> Name : '.$name.'<br> Email Id : '.$email.'<br> Phone Number :'.$phone.'<br> Job Title :'.$job_titel.'<br> Name of your Organisation :'.$orginisation.'<br> Country  : '.$country.'<br> <b> Message : </b><span align="justify"> <br>'.$comment.'</span>';
				$message .='<b>Contact Us  Information </b> <br> Name : '.$name.'<br> Email Id : '.$email.'<br> Phone Number : '.$phone;
			if($job_titel!='')
				{
				$message .= '<br> Job Title :'.$job_titel;
				}
			if($orginisation!='')
				{
				$message .= '<br> Organisation :'.$orginisation;
				}
			if($country!='')
				{
				$message .= '<br> Country  : '.$country;
				}
				$message .='<br> <b> Message : </b><span align="justify"> <br>'.$comment.'</span>';
//				echo $message;
//				die();
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				
				$headers .= "From:Orbis";
				//$subject = "News Letter From Orbis";
				//$from = "admin@orbis.com";
				$from=$email;
				$sent = mail($to,$subject,$message,$headers);


				//$this->email->print_debugger();
				$this->session->set_flashdata('success', 'Your message has been sent successfully.');
				redirect(base_url().'contact/contact_us');
			}
			else
			{

				$this->session->set_flashdata('success', 'Your message not send successfully. Please Try Again.');
				$this->load->view('contact/contact_us',$data);
			}
				

		}
	}
*/
	
	
	public function send_contact()
	{
		if(isset($_POST['submit']))
		{
			$k=$this->contact_model->send_contact_request();
			if($k)
			{ 
				 $name = $_POST['name'];
				 $email = $_POST['email'];
				 $phone = $_POST['phone'];
				 $country = $_POST['country'];
				 $comment = mysql_real_escape_string($_POST['comment']);
				 // $toemail  = array('rahul@orbisresearch.com','karan@orbisresearch.com','aditi@orbisresearch.com','mayuresh.sonawane@orbisresearch.com','ambika@orbisresearch.com','swara@orbisresearch.com','aslam@orbisresearch.com');
				 $toemail  = array('rahul@orbisresearch.com','aditi@orbisresearch.com','swara@orbisresearch.com','aslam@orbisresearch.com');
				$subject = 'Orbis - Contact Us  Information';
				$headers .= 'From: enquiry@orbisresearch.com' . "\r\n" .
								'Reply-To: enquiry@orbisresearch.com' . "\r\n" .
								'X-Mailer: PHP/' . phpversion();
									
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				$message ='';
				$message .='<b>Contact Person Information </b> <br> Name : '.$name.'<br> Email Id : '.$email.'<br> Phone Number :'.$phone.'<br> Country :'.$country;
				 $message .='<br> <b> Message : </b><span align="justify"> <br>'.$comment.'</span>';
				 foreach ($toemail as $key => $to) {
				 	
				 	mail($to, $subject, $message, $headers);
				 }
											 
				$this->session->set_flashdata('success', 'Your request has been received successfully, Our sales executive will get back to you shortly.');
				redirect(base_url().'contactus');

			}
			else
			{

				$this->session->set_flashdata('success', 'Your message not send successfully. Please Try Again.');
				$this->load->view('contact/contact_us',$data);
			}
				

		}
	}
	
	 
public function thanks()
{		
		$data['include'] = 'frontend/thankyou';
		$this->load->view('frontend/container');
}


}
