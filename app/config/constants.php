<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

 /*function valid_date($date){

        if($date =='0000-00-00'){

            $valid_date ='2019-18-03';
        }
        else{
            $valid_date=$date;
        }
        
        return $valid_date;

    }*/

    function getEmailsRegion($country,$region){

 ////Start Europe Countries 

        if($country=="Albania" || $country=="Andorra" || $country=="Belarus" ||
            $country=="Bosnia" || $country=="Croatia" || 
            $country=="European Union" || $country=="Faroe Islands" || 
            $country=="Gibraltar" || $country=="Iceland" || $country=="Jersey" ||
            $country=="Kosovo" || $country=="Liechtenstein" || 
            $country=="Macedonia" || $country=="Isle of Man" || 
            $country=="Moldova" || $country=="Monaco" ||
            $country=="Montenegro" || $country=="Norway" || $country=="Russia" ||
            $country=="San Marino" || $country=="Serbia" || 
            $country=="Svalbard and Jan Mayen Islands" || 
            $country=="Switzerland"  || 
            $country=="Ukraine" || $country=="Vatican City State" || 
            $country=="Austria" || $country=="Belgium" || $country=="Bulgaria" ||
            $country=="Cyprus" || $country=="Czech Republic" || 
            $country=="Denmark" || $country=="Estonia" || $country=="Finland" || 
            $country=="France" || $country=="Germany" ||  $country=="Greece" || 
            $country=="Hungary" || $country=="Ireland" || $country=="Italy" ||
            $country=="Latvia" || $country=="Lithuania" || 
            $country=="Luxembourg" || $country=="Malta" || 
            $country=="Netherlands" || $country=="Poland" ||
            $country=="Portugal" || $country=="Romania" || 
            $country=="Slovakia" || $country=="Slovenia" || $country=="Spain" || 
            $country=="Sweden" || $country=="United Kingdom" || 
            $country=="Burundi" || $country=="Comoros" || $country=="Djibouti" ||
            $country=="Eritrea" || $country=="Ethiopia" || $country=="Kenya" || 
            $country=="Madagascar" || $country=="Malawi" || 
            $country=="Mauritius" || $country=="Mayotte" || 
            $country=="Mozambique" || $country=="Reunion" || 
            $country=="Rwanda" || $country=="Seychelles" || 
            $country=="Somalia" || $country=="Tanzania" || $country=="Uganda" || 
            $country=="Zambia" || $country=="Zimbabwe" || $country=="Angola" || 
            $country=="Cameroon" || $country=="Central African Republic" || 
            $country=="Chad" || $country=="Congo (Brazzaville)" || 
            $country=="Congo" || $country=="Equatorial Guinea" || 
            $country=="Gabon" || $country=="Sao Tome and Principe" || 
            $country=="Algeria" || $country=="Libyan Arab Jamahiriya" || 
            $country=="Morocco" || $country=="South Sudan" || 
            $country=="Sudan" || $country=="Tunisia" || 
            $country=="Western Sahara" || $country=="Botswana" || 
            $country=="Lesotho" || $country=="Namibia" || 
            $country=="South Africa" || $country=="Swaziland" || 
            $country=="Benin" || $country=="Burkina Faso" || 
            $country=="Cape Verde" || $country=="Cote d'Ivoire (Ivory Coast)" || 
            $country=="Gambia" || $country=="Ghana" || $country=="Guinea" || 
            $country=="Liberia" || $country=="Mali" || $country=="Mauritania" ||
            $country=="Niger" || $country=="Nigeria" || $country=="Senegal" || 
            $country=="Sierra Leone" || $country=="Togo" )

             //End Europe Countries

            {
                if($region=='country'){

               $emailids=array('rahul@orbisresearch.com', 'hrishikesh@orbisresearch.com', 'prateek@orbisresearch.com', 'swara@orbisresearch.com' , 'anjali@orbisresearch.com', 'tressa@orbisresearch.com','shubham@orbisresearch.com'); 
                }
                else{
                    $emailids='EUROPE';
                }   
            } 

        //APAC Countries Start 

        elseif($country=="Afganistan" || $country=="Armenia" ||
            $country=="Azerbaijan" || $country=="Bangladesh" ||
            $country=="Bhutan" || $country=="Brunei Darussalam" ||
            $country=="Cambodia" || $country=="China" || $country=="Georgia" ||
            $country=="Hong Kong" || $country=="India" || 
            $country=="Indonesia" || $country=="Japan" || 
            $country=="Kazakhstan" || $country=="North Korea" ||
            $country=="South Korea" || $country=="Kyrgyzstan" ||
            $country=="Laos" || $country=="Macao" || $country=="Malaysia" ||
            $country=="Maldives" || $country=="Mongolia" || 
            $country=="Myanmar (ex-Burma)" || $country=="Nepal" ||
            $country=="Pakistan" || $country=="Philippines" || 
            $country=="Singapore" || $country=="Sri Lanka (ex-Ceilan)" || 
            $country=="Taiwan" || $country=="Tajikistan" || 
            $country=="Thailand" || $country=="Timor Leste (West)" ||           
            $country=="Turkmenistan" ||
            $country=="Uzbekistan" || $country=="Vietnam" || 
            $country=="Bahrain" || $country=="Iraq" || $country=="Iran" || 
            $country=="Israel" || $country=="Jordan" || $country=="Kuwait" || 
            $country=="Lebanon" || $country=="Oman" || $country=="Palestine" || 
            $country=="Qatar" || $country=="Saudi Arabia" || $country=="Syria" ||
            $country=="United Arab Emirates" || $country=="Yemen" || 
            $country=="Australia" || $country=="Fiji" || 
            $country=="French Polynesia" || $country=="Guam" || 
            $country=="Kiribati" || $country=="Marshall Islands" || 
            $country=="Micronesia" || $country=="New Caledonia" || 
            $country=="New Zealand" || $country=="Papua New Guinea" || 
            $country=="Samoa" || $country=="Samoa American" ||
            $country=="Solomon Islands" || $country=="Tonga" || 
            $country=="Vanuatu" || $country=="Egypt" || $country=="Turkey"  ) 

            // APACCountries End
            { 
                if($region=='country'){

                 $emailids=array('rahul@orbisresearch.com', 'hrishikesh@orbisresearch.com', 'prateek@orbisresearch.com', 'devashish@orbisresearch.com', 'shruti@orbisresearch.com', 'aditi@orbisresearch.com','tressa@orbisresearch.com','shubham@orbisresearch.com');
                //$emailids=array('pravin.chopade@orbisresearch.com', 'rutuja.kadam@orbisresearch.com'); 
                }
                else{
                    $emailids='APAC';
                }
            }  

       //Start US

        elseif($country=="Anguilla" || $country=="Antigua and Barbuda" || 
            $country=="Aruba" || $country=="Bahamas" || $country=="Barbados" ||
            $country=="Bonaire" ||  $country=="Saint Eustatius and Saba" ||
            $country=="British Virgin Islands" || $country=="Cayman Islands" ||
            $country=="Cuba" || $country=="Curaçao" || $country=="Dominica" ||
            $country=="Dominican Republic" || $country=="Grenada" ||
            $country=="Guadeloupe" || $country=="Haiti" || $country=="Jamaica" ||
            $country=="Martinique"|| $country=="Monserrat" || 
            $country=="Puerto Rico" || $country =="Saint-Barthélemy" || 
            $country=="St. Kitts and Nevis" || $country=="Saint Lucia" || 
            $country=="Saint Martin" || $country=="Sint Maarten" ||
            $country=="Saint Vincent and the Grenadines" || $country=="Belize" ||
            $country=="Trinidad and Tobago" || $country=="El Salvador" ||
            $country=="Turks and Caicos Islands" || $country=="Guatemala" ||
            $country=="Virgin Islands (US)" ||  $country=="Costa Rica"  || 
            $country=="Honduras" || $country=="Mexico" || $country=="Panama" ||
            $country=="Nicaragua" || $country=="Argentina" ||
            $country=="Bolivia" || $country=="Brazil" || $country=="Chile" ||
            $country=="Colombia" || $country=="Ecuador" || 
            $country=="Falkland Islands (Malvinas)" || $country=="Guyana" || 
            $country=="French Guiana" || $country=="Paraguay" ||
            $country=="Peru" || $country=="Suriname" || $country=="Uruguay" ||
            $country=="Venezuela" || $country=="Bermuda" || $country=="Canada" ||
            $country=="Greenland" || $country=="Saint Pierre and Miquelon" ||
            $country=="United States")

            //End US
            {   
                if($region=='country'){

                $emailids = array('rahul@orbisresearch.com','hrishikesh@orbisresearch.com', 'prateek@orbisresearch.com', 'aslam@orbisresearch.com','shariq@orbisresearch.com','ashwin@orbisresearch.com','shubham@orbisresearch.com');
                }
                else{
                    $emailids ='USA';
                }
            }
        else{

                if($region=='country'){

                $emailids = array('leads@orbisresearch.com');
                 }else{
                    $emailids ='ALL';
                 }


        }

        return $emailids;
}
//End getEmailRegion function

function getEmails($country){

 ////Start Europe Countries 

        if($country=="Albania" || $country=="Andorra" || $country=="Belarus" ||
            $country=="Bosnia" || $country=="Croatia" || 
            $country=="European Union" || $country=="Faroe Islands" || 
            $country=="Gibraltar" || $country=="Iceland" || $country=="Jersey" ||
            $country=="Kosovo" || $country=="Liechtenstein" || 
            $country=="Macedonia" || $country=="Isle of Man" || 
            $country=="Moldova" || $country=="Monaco" ||
            $country=="Montenegro" || $country=="Norway" || $country=="Russia" ||
            $country=="San Marino" || $country=="Serbia" || 
            $country=="Svalbard and Jan Mayen Islands" || 
            $country=="Switzerland" ||  
            $country=="Ukraine" || $country=="Vatican City State" || 
            $country=="Austria" || $country=="Belgium" || $country=="Bulgaria" ||
            $country=="Cyprus" || $country=="Czech Republic" || 
            $country=="Denmark" || $country=="Estonia" || $country=="Finland" || 
            $country=="France" || $country=="Germany" ||  $country=="Greece" || 
            $country=="Hungary" || $country=="Ireland" || $country=="Italy" ||
            $country=="Latvia" || $country=="Lithuania" || 
            $country=="Luxembourg" || $country=="Malta" || 
            $country=="Netherlands" || $country=="Poland" ||
            $country=="Portugal" || $country=="Romania" || 
            $country=="Slovakia" || $country=="Slovenia" || $country=="Spain" || 
            $country=="Sweden" || $country=="United Kingdom" || 
            $country=="Burundi" || $country=="Comoros" || $country=="Djibouti" ||
            $country=="Eritrea" || $country=="Ethiopia" || $country=="Kenya" || 
            $country=="Madagascar" || $country=="Malawi" || 
            $country=="Mauritius" || $country=="Mayotte" || 
            $country=="Mozambique" || $country=="Reunion" || 
            $country=="Rwanda" || $country=="Seychelles" || 
            $country=="Somalia" || $country=="Tanzania" || $country=="Uganda" || 
            $country=="Zambia" || $country=="Zimbabwe" || $country=="Angola" || 
            $country=="Cameroon" || $country=="Central African Republic" || 
            $country=="Chad" || $country=="Congo (Brazzaville)" || 
            $country=="Congo" || $country=="Equatorial Guinea" || 
            $country=="Gabon" || $country=="Sao Tome and Principe" || 
            $country=="Algeria" || $country=="Libyan Arab Jamahiriya" || 
            $country=="Morocco" || $country=="South Sudan" || 
            $country=="Sudan" || $country=="Tunisia" || 
            $country=="Western Sahara" || $country=="Botswana" || 
            $country=="Lesotho" || $country=="Namibia" || 
            $country=="South Africa" || $country=="Swaziland" || 
            $country=="Benin" || $country=="Burkina Faso" || 
            $country=="Cape Verde" || $country=="Cote d'Ivoire (Ivory Coast)" || 
            $country=="Gambia" || $country=="Ghana" || $country=="Guinea" || 
            $country=="Liberia" || $country=="Mali" || $country=="Mauritania" ||
            $country=="Niger" || $country=="Nigeria" || $country=="Senegal" || 
            $country=="Sierra Leone" || $country=="Togo" )

             //End Europe Countries

            {

                $$emailids=array('rahul@orbisresearch.com', 'hrishikesh@orbisresearch.com', 'prateek@orbisresearch.com', 'swara@orbisresearch.com' , 'anjali@orbisresearch.com', 'tressa@orbisresearch.com');  
            } 

        //APAC Countries Start 

        elseif($country=="Afganistan" || $country=="Armenia" ||
            $country=="Azerbaijan" || $country=="Bangladesh" ||
            $country=="Bhutan" || $country=="Brunei Darussalam" ||
            $country=="Cambodia" || $country=="China" || $country=="Georgia" ||
            $country=="Hong Kong" || $country=="India" || 
            $country=="Indonesia" || $country=="Japan" || 
            $country=="Kazakhstan" || $country=="North Korea" ||
            $country=="South Korea" || $country=="Kyrgyzstan" ||
            $country=="Laos" || $country=="Macao" || $country=="Malaysia" ||
            $country=="Maldives" || $country=="Mongolia" || 
            $country=="Myanmar (ex-Burma)" || $country=="Nepal" ||
            $country=="Pakistan" || $country=="Philippines" || 
            $country=="Singapore" || $country=="Sri Lanka (ex-Ceilan)" || 
            $country=="Taiwan" || $country=="Tajikistan" || 
            $country=="Thailand" || $country=="Timor Leste (West)" ||           
            $country=="Turkmenistan" ||
            $country=="Uzbekistan" || $country=="Vietnam" || 
            $country=="Bahrain" || $country=="Iraq" || $country=="Iran" || 
            $country=="Israel" || $country=="Jordan" || $country=="Kuwait" || 
            $country=="Lebanon" || $country=="Oman" || $country=="Palestine" || 
            $country=="Qatar" || $country=="Saudi Arabia" || $country=="Syria" ||
            $country=="United Arab Emirates" || $country=="Yemen" || 
            $country=="Australia" || $country=="Fiji" || 
            $country=="French Polynesia" || $country=="Guam" || 
            $country=="Kiribati" || $country=="Marshall Islands" || 
            $country=="Micronesia" || $country=="New Caledonia" || 
            $country=="New Zealand" || $country=="Papua New Guinea" || 
            $country=="Samoa" || $country=="Samoa American" ||
            $country=="Solomon Islands" || $country=="Tonga" || 
            $country=="Vanuatu" || $country=="Egypt" || $country=="Turkey") 

            // APACCountries End
            { 

                $emailids=array('rahul@orbisresearch.com', 'hrishikesh@orbisresearch.com', 'prateek@orbisresearch.com', 'devashish@orbisresearch.com', 'shruti@orbisresearch.com', 'aditi@orbisresearch.com','tressa@orbisresearch.com');
                //$emailids=array('pravin.chopade@orbisresearch.com', 'rutuja.kadam@orbisresearch.com'); 
            }  

       //Start US

        elseif($country=="Anguilla" || $country=="Antigua and Barbuda" || 
            $country=="Aruba" || $country=="Bahamas" || $country=="Barbados" ||
            $country=="Bonaire" ||  $country=="Saint Eustatius and Saba" ||
            $country=="British Virgin Islands" || $country=="Cayman Islands" ||
            $country=="Cuba" || $country=="Curaçao" || $country=="Dominica" ||
            $country=="Dominican Republic" || $country=="Grenada" ||
            $country=="Guadeloupe" || $country=="Haiti" || $country=="Jamaica" ||
            $country=="Martinique"|| $country=="Monserrat" || 
            $country=="Puerto Rico" || $country =="Saint-Barthélemy" || 
            $country=="St. Kitts and Nevis" || $country=="Saint Lucia" || 
            $country=="Saint Martin" || $country=="Sint Maarten" ||
            $country=="Saint Vincent and the Grenadines" || $country=="Belize" ||
            $country=="Trinidad and Tobago" || $country=="El Salvador" ||
            $country=="Turks and Caicos Islands" || $country=="Guatemala" ||
            $country=="Virgin Islands (US)" ||  $country=="Costa Rica"  || 
            $country=="Honduras" || $country=="Mexico" || $country=="Panama" ||
            $country=="Nicaragua" || $country=="Argentina" ||
            $country=="Bolivia" || $country=="Brazil" || $country=="Chile" ||
            $country=="Colombia" || $country=="Ecuador" || 
            $country=="Falkland Islands (Malvinas)" || $country=="Guyana" || 
            $country=="French Guiana" || $country=="Paraguay" ||
            $country=="Peru" || $country=="Suriname" || $country=="Uruguay" ||
            $country=="Venezuela" || $country=="Bermuda" || $country=="Canada" ||
            $country=="Greenland" || $country=="Saint Pierre and Miquelon" ||
            $country=="United States")

            //End US
            {
                 $emailids = array('rahul@orbisresearch.com','hrishikesh@orbisresearch.com', 'prateek@orbisresearch.com', 'aslam@orbisresearch.com','shariq@orbisresearch.com','ashwin@orbisresearch.com','shubham@orbisresearch.com');
            }
        else{

                $emailids = array('leads@orbisresearch.com');

        }

        return $emailids;
}
//End getEmail function

//Strat send_email_subject function
function send_email_subject($toemail,$msg,$username,$company_name,$request_type){

    //$rerurn=array('call_send_email');

    for($i = 0; $i < count($toemail); $i++){
                        $data = array(
                            'to_email' => $toemail[$i], 
                            'desc' => $msg,
                            'username'=>$username,
                            'company_name'=>$company_name,
                            'request_type'=>$request_type
                        );
                   //print_r($data);exit;
                        $handle = curl_init(base_url().'email/send_emails.php');
                        curl_setopt($handle, CURLOPT_POST, true);
                        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
                        $result = curl_exec($handle);
                        curl_close($handle);
                        //print_r($result);exit;

                   }
                    return true;


}

//End send_email_subject function
/* End of file constants.php */
/* Location: ./application/config/constants.php */