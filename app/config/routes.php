<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';
$route['contact/purchase-single-user/(:num)']= 'contact/purchase_single_user/$1';
$route['contact/purchase-multiple-user/(:num)']= 'contact/purchase_multiple_user/$1';
$route['contact/purchase-global-user/(:num)']= 'contact/purchase_global_user/$1';
$route['contact/purchase-corporate-user/(:num)']= 'contact/purchase_corporate_user/$1';

 $route['latest-market-research-reports.html'] = "reports/report/getLatestReportList"; 
 $route['market-research-reports-categories.html'] = "category"; 
 $route['market-research-reports-publishers.html'] = "publisher/all_publishers";
 $route['country-market-research-reports.html'] = "areas"; 
$route['continent-and-country-market-research-reports.html'] = "areas";
 $route['reports/thanks'] = "reports/reports/thanks";  
 $route['contactus'] = "contact/contact/contact_us"; 
 $route['aboutus'] = "contact/contact/about_us";
  $route['services'] = "contact/contact/services"; 
 
 $route['termsandconditions'] = "contact/contact/terms_and_conditions"; 
 $route['refundcancellationpolicy'] = "contact/contact/refund_cancellation_policy"; 
 $route['privacypolicy'] = "contact/contact/privacy_policy"; 
 $route['disclaimer'] = "contact/contact/disclaimer"; 
 $route['faq'] = "contact/contact/faq"; 
 $route['paymentprocedures'] = "contact/contact/payment_procedures"; 
 $route['becomeapublisher'] = "publisher/publisher"; 
 $route['register'] = "register/register"; 
 $route['login'] = "register/login";
 $route['our-services'] = "contact/contact/about_us"; 

 
 
 $route['contacts/enquiry-before-buying/(:num)']= 'reports/enquirybeforebuying/$1';
 $route['contacts/request-sample/(:num)']= 'reports/requestsample/$1';
 $route['contacts/discount/(:num)']= 'reports/discount/$1';
 $route['press-release'] = 'news/news/pressReleases';

//-------------------------------------------------Category-------------------------------------------
require_once( BASEPATH .'database/DB'. EXT );
$db =& DB();

$result_category=$db->query("select category_name,id from category");
$row_category=$result_category->result();

$result_sub_category=$db->query("select sc.id,sc.sub_category_name,c.category_name from sub_category sc INNER JOIN category c on (sc.category_id = c.id);");
$row_sub_category=$result_sub_category->result();

$result_publisher=$db->query("select display_name,id from users where user_type = 'Publisher';");
$row_publisher=$result_publisher->result();

$result_country=$db->query("select country_name,id from country;");
$row_country = $result_country->result();

$result_news=$db->query("select news_title,id from news where news_type='news';");
$row_news=$result_news->result();

$result_press_release=$db->query("select news_title,id from news where news_type='press_release';");
$row_press_release=$result_press_release->result();

/*
$result_country_rss=$db->query("select country_name,id from country;");
$result_publisher_rss=$db->query("select display_name,id from users where user_type = 'Publisher';");
$result_category_rss=$db->query("select category_name,id from category");
$result_sub_category_rss=$db->query("select sc.id,sc.sub_category_name,c.category_name from sub_category sc INNER JOIN category c on (sc.category_id = c.id);");
*/


for($i=0;$i<count($row_category);$i++)
{
    $category_name =  str_replace(" ","-",strtolower($row_category[$i]->category_name));
    $route['(market-reports)/'.$category_name.'.html'] = "reports/report/getCategoryReport/".$row_category[$i]->id;
	$route['(market-reports)/'.$category_name.'/feed'] = "rss/feed/category/".$row_category[$i]->id;
	/*
	$category_name_rss =  str_replace(" ","-",strtolower($row_category[$i]->category_name));
    $route['(market-reports)/'.$category_name_rss.'/feed'] = "rss/feed/category/".$row_category[$i]->id;
	*/
}

//-------------------------------------------------Sub Category------------------------------------------- 
 
for($i=0;$i<count($row_sub_category);$i++)
{
    $category_name =  str_replace(" ","-",strtolower($row_sub_category[$i]->category_name));
	$sub_category_name =  str_replace(" ","-",strtolower($row_sub_category[$i]->sub_category_name));
	$route['(market-reports)/'.$category_name.'/'.$sub_category_name.'.html'] = "reports/report/getSubcategoryInfo/".$row_sub_category[$i]->id;
	$route['(market-reports)/'.$category_name.'/'.$sub_category_name.'/feed'] = "rss/feed/sub_category/".$row_sub_category[$i]->id;
	
	/*$category_name_rss1 =  str_replace(" ","-",strtolower($row_sub_category[$i]->category_name));
	$sub_category_name_rss =  str_replace(" ","-",strtolower($row_sub_category[$i]->sub_category_name));
	$route['(market-reports)/'.$category_name_rss1.'/'.$sub_category_name_rss.'/feed'] = "rss/feed/sub_category/".$row_sub_category[$i]->id;*/
}
 
//-------------------------------------------------Publishers------------------------------------------- 
 
for($i=0;$i<count($row_publisher);$i++)
{
    $publisher_name =  str_replace(" ","-",strtolower($row_publisher[$i]->display_name));
	$route['(publisher)/'.$publisher_name.'.html'] = "reports/report/getPublisherList/".$row_publisher[$i]->id;
	$route['(publisher)/'.$publisher_name.'/feed'] = "rss/feed/publisher/".$row_publisher[$i]->id;
	/*
	$publisher_name_rss =  str_replace(" ","-",strtolower($row_publisher[$i]->display_name));
	$route['(publisher)/'.$publisher_name_rss.'/feed'] = "rss/feed/publisher/".$row_publisher[$i]->id;
	*/
}
 
//-------------------------------------------------Country------------------------------------------- 
 
for($i=0;$i<count($row_country);$i++)
{
    $country_name =  str_replace(" ","-",strtolower($row_country[$i]->country_name));
	$route[$country_name.'-market-reports.html'] = "reports/report/getCountryInfo/".$row_country[$i]->id;
	$route['(country)/'.$country_name."/feed"] = "rss/feed/country/".$row_country[$i]->id;
	/*
	$country_name_rss =  str_replace(" ","-",strtolower($row_country[$i]->country_name));
	$route['(country)/'.$country_name_rss."/feed"] = "rss/feed/country/".$row_country[$i]->id;
	*/
}
  
//-------------------------------------------------News------------------------------------------- 
 
for($i=0;$i<count($row_news);$i++)
{
    $news_name =  str_replace(" ","-",strtolower($row_news[$i]->news_title));
	$news_name_2 =  str_replace(",","-",strtolower($news_name));
	$route['(news)/'.$news_name_2] = "news/news/getNewsDetails/".$row_news[$i]->id;
}

for ($i=0; $i<count($row_press_release); $i++)
{
	$release_name =  str_replace(" ","-",strtolower($row_press_release[$i]->news_title));
	$release_name_2 =  str_replace(",","-",strtolower($release_name));
	$route['(press-release)/'.$release_name_2] = "news/news/getPressReleaseDetails/".$row_press_release[$i]->id;
}
    
 

 $route['allcategorysitemap.xml'] = "allcategorysitemap/index";
 $route['publishersitemap.xml'] = "publishersitemap/index";
 $route['newssitemap.xml'] = "newssitemap/index";
 $route['countrysitemap.xml'] = "countrysitemap/index";
 $route['sitemap.xml'] = "latestreportsitemap/sitemap";
 
$route['sitemap(:num).xml'] = "latestreportsitemap/latestreportsitemap$1";

 
 
 /*
//-------------------------------------------------Country------------------------------------------- 
 
for($i=0;$i<count($row_country_rss);$i++)
{
    $country_name_rss =  str_replace(" ","-",strtolower($row_country_rss[$i]->country_name));
	$route['(country)/'.$country_name_rss."/feed"] = "rss/feed/country/".$row_country_rss[$i]->id;
}
   
//-------------------------------------------------Publishers------------------------------------------- 
 
for($i=0;$i<count($row_publisher_rss);$i++)
{
    $publisher_name_rss =  str_replace(" ","-",strtolower($row_publisher_rss[$i]->display_name));
	$route['(publisher)/'.$publisher_name_rss.'/feed'] = "rss/feed/publisher/".$row_publisher_rss[$i]->id;
}

//-------------------------------------------------Category-------------------------------------------
 
for($i=0;$i<count($row_category_rss);$i++)
{
    $category_name_rss =  str_replace(" ","-",strtolower($row_category_rss[$i]->category_name));
    $route['(market-reports)/'.$category_name_rss.'/feed'] = "rss/feed/category/".$row_category_rss[$i]->id;
}
  
 //-------------------------------------------------Sub Category------------------------------------------- 
 
for($i=0;$i<count($row_sub_category_rss);$i++)
{
    $category_name_rss1 =  str_replace(" ","-",strtolower($row_sub_category_rss[$i]->category_name));
	$sub_category_name_rss =  str_replace(" ","-",strtolower($row_sub_category_rss[$i]->sub_category_name));
	$route['(market-reports)/'.$category_name_rss1.'/'.$sub_category_name_rss.'/feed'] = "rss/feed/sub_category/".$row_sub_category_rss[$i]->id;
}
*/
 
 
 
 
/* End of file routes.php */
/* Location: ./application/config/routes.php */
