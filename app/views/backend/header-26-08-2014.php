<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="images/favicon.html">
    
  
	
	
	
    <title><?php echo $this->config->item('site_name')?></title>
	
    <!--Core CSS -->
    <link href="<?php echo base_url();?>themes/backend/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>themes/backend/css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo base_url();?>themes/backend/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>themes/backend/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    
    <!--dynamic table-->
    <link href="<?php echo base_url();?>themes/backend/assets/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>themes/backend/assets/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url();?>themes/backend/assets/data-tables/DT_bootstrap.css" />
 	<link type="text/javascript" href="<?php echo base_url();?>themes/backend/assets/data-tables/DT_bootstrap.js" />
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>themes/backend/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>themes/backend/css/style-responsive.css" rel="stylesheet" />

    <script type="text/javascript" src="<?php echo base_url();?>themes/backend/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>themes/backend/tinymce/jscripts/tiny_mce/plugins/media/js/embed.js"></script>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="js/ie8/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	
	<script type="text/javascript">
		tinyMCE.init({
			// General options
			selector: "textarea",
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave,visualblocks",
			//plugins : "media",
			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,cut,copy,paste,pastetext,pasteword,undo,redo,link,unlink,anchor",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "center",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			content_css : "<?php echo base_url();?>themes/backend/tinymce/examples/css/content.css",
			image_advtab: true,
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "<?php echo base_url();?>themes/backend/tinymce/examples/lists/template_list.js",
			external_link_list_url : "<?php echo base_url();?>themes/backend/tinymce/examples/lists/link_list.js",
			external_image_list_url : "<?php echo base_url();?>themes/backend/tinymce/examples/lists/image_list.js",
			external_media_list_url : "<?php echo base_url();?>themes/backend/tinymce/examples/lists/media_list.js",

			// Style formats
			style_formats : [
				{title : 'Bold text', inline : 'b'},
				{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
				{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
				{title : 'Example 1', inline : 'span', classes : 'example1'},
				{title : 'Example 2', inline : 'span', classes : 'example2'},
				{title : 'Table styles'},
				{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
			],
			
			
		});
		
		
		
	</script>

</head>

<body>

<section id="container" >
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">

    <a href="#" class="logo">
        <img src="<?php echo base_url();?>themes/backend/images/logo.png" alt="">
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<!--logo end-->


<div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                <span class="username">John Doe</span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                       <li><a href="<?php echo base_url();?>siteadmin/change_password/change_password"><i class="fa fa-cog"></i> Change Password</a></li>

                <li><a href="<?php echo base_url();?>siteadmin/logout"><i class="fa fa-key"></i> Log Out</a></li>
            </ul>
        </li>
        <!-- user login dropdown end -->
    </ul>
    <!--search & user info end-->
</div>
</header>