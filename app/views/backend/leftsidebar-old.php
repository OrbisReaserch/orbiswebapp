<?php $page = $this->uri->segment(2); ?> 
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a <?php if($page=="dashboard"){ ?>class="active"<?php } ?> href="<?php echo base_url();?>siteadmin/dashboard">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="sub-menu">
                <a <?php if($page=="category"){ ?>class="active"<?php } ?> href="javascript:;">
                    <i class="fa fa-laptop"></i>
                    <span>Category</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo base_url();?>siteadmin/category">Manage Category</a></li>
                    <li><a href="<?php echo base_url();?>siteadmin/category/manage_sub_category">Manage Sub-Category</a></li>
                </ul>
            </li>
            <!-- <li class="sub-menu">
                <a <?php if($page=="product"){ ?>class="active"<?php } ?> href="javascript:;">
                    <i class="fa fa-laptop"></i>
                    <span>Report</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo base_url();?>siteadmin/product">Manage Reports</a></li>
                     <li><a href="<?php echo base_url();?>siteadmin/product/manage_product_reviews">Manage Report Reviews</a></li> 
                </ul>
            </li> -->
            
             <li class="sub-menu">
                <a <?php if($page=="user_management"){ ?>class="active"<?php } ?> href="javascript:;">
                    <i class="fa fa-laptop"></i>
                    <span>User</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo base_url();?>siteadmin/user_management">User Management</a></li>
                </ul>
            </li>
            
            <li class="sub-menu">
                <a <?php if($page=="content"){ ?>class="active"<?php } ?> href="javascript:;">
                    <i class="fa fa-laptop"></i>
                    <span>Website Content</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo base_url();?>siteadmin/content">Manage Website Content</a></li>
                </ul>
            </li>
			<li class="sub-menu">
                <a <?php if($page=="report_management"){ ?>class="active"<?php } ?> href="javascript:;">
                    <i class="fa fa-laptop"></i>
                    <span>Report Management</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo base_url();?>siteadmin/report">Report Management</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a <?php if($page=="request"){ ?>class="active"<?php } ?> href="javascript:;">
                    <i class="fa fa-laptop"></i>
                    <span>Report Requests</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo base_url();?>siteadmin/request">Request Management</a></li>
                </ul>
            </li>
			<li class="sub-menu">
                <a <?php if($page=="brand"){ ?>class="active"<?php } ?> href="javascript:;">
                    <i class="fa fa-laptop"></i>
                    <span>Client Section</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo base_url();?>siteadmin/brand">Manage Brand</a></li>
                </ul>
            </li>
			<li class="sub-menu">
                <a <?php if($page=="news_letter"){ ?>class="active"<?php } ?> href="javascript:;">
                    <i class="fa fa-laptop"></i>
                    <span>News Letter</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo base_url();?>siteadmin/news_letter">News Letter Management</a></li>
                </ul>
            </li>
            
            <li class="sub-menu">
                <a <?php if($page=="country"){ ?>class="active"<?php } ?> href="javascript:;">
                    <i class="fa fa-laptop"></i>
                    <span>Country</span>
                </a>
                <ul class="sub">
                    <li><a href="<?php echo base_url();?>siteadmin/country">Country Management</a></li>
                </ul>
            </li>
            
            
            
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>