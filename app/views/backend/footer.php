<!--footer    Sandeep Ramgiri  -->
<!--Core js-->
<script src="<?php echo base_url();?>themes/backend/js/lib/jquery.js"></script>
<script src="<?php echo base_url();?>themes/backend/bs3/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>themes/backend/js/lib/jquery-1.8.3.min.js"></script>
<script src="<?php echo base_url();?>themes/backend/js/lib/jquery-ui-1.9.2.custom.min.js"></script>

<!--Fileupload-->
<script src="<?php echo base_url();?>themes/backend/assets/bootstrap-switch-master/build/js/bootstrap-switch.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>

<!--Datepicker Colorpicker timepicker-->
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/bootstrap-daterangepicker/moment.min.js"></script>

<!--Daterangepicker multi-select js-->
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/jquery-multi-select/js/jquery.multi-select.js"></script>

<!--Quicksearch js-->
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/jquery-multi-select/js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
<script src="<?php echo base_url();?>themes/backend/assets/jquery-tags-input/jquery.tagsinput.js"></script>

<script class="include" type="text/javascript" src="<?php echo base_url();?>themes/backend/js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo base_url();?>themes/backend/js/scrollTo/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url();?>themes/backend/js/nicescroll/jquery.nicescroll.js" type="text/javascript"></script>

<!--Common script init for all pages-->
<script src="<?php echo base_url();?>themes/backend/js/scripts.js"></script>
<script src="<?php echo base_url();?>themes/backend/js/toggle-button/toggle-init.js"></script>
<script src="<?php echo base_url();?>themes/backend/js/advanced-form/advanced-form.js"></script>

<!--Easy Pie Chart-->
<script src="<?php echo base_url();?>themes/backend/assets/easypiechart/jquery.easypiechart.js"></script>

<!--Sparkline Chart-->
<script src="<?php echo base_url();?>themes/backend/assets/sparkline/jquery.sparkline.js"></script>

<!--jQuery Flot Chart-->
<script src="<?php echo base_url();?>themes/backend/assets/flot-chart/jquery.flot.js"></script>
<script src="<?php echo base_url();?>themes/backend/assets/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url();?>themes/backend/assets/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php echo base_url();?>themes/backend/assets/flot-chart/jquery.flot.pie.resize.js"></script>

<!--Dynamic table-->
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>themes/backend/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>themes/backend/assets/data-tables/DT_bootstrap.js"></script>

<!--dynamic table initialization -->
<script src="<?php echo base_url();?>themes/backend/js/dynamic_table/dynamic_table_init.js"></script>



</body>
</html>








