<header class="innerpages_serchheader">
    <div class="container">
      <div class="row">
          <div class="col-lg-12 col-sm-12 col-xs-12">
             <div class="SearchQueryBox">
              <form action="<?php echo base_url();?>search" method = "post" id="auto_submit" name="auto_submit" role="form" class="form-inline">
                <div class="form-group pull-left">
                   <label class="sr-only" for="SearchQuery">Search Query...</label>
                    <input id="auto_complete" type="text" placeholder="Search Keyword"   name="search_keyword" class="form-control"  autocomplete="off">
                    <div id="suggesstion-box">
                      <ul id="searchlist"></ul>
                    </div>
                </div> 
         
             <button type="submit" name="submit" value="Search" class="btn btn-ar btn-primary pull-right">Search</button>
            </form>
          </div>
         </div>
     </div>
  </div>
</header>
<style type="text/css">
/* mobile css*/ 
@media only screen and (min-width: 311px) and (max-width: 330px)
{
  #searchlist
  { 
  width: 163px !important;
  }
}
@media only screen and (min-width: 331px) and (max-width: 370px)
{
  #searchlist{
   width: 187px !important; 
  }
}
@media only screen and (min-width: 631px) and (max-width: 660px)
{
  #searchlist{
   width: 353px !important; 
  }
}
@media only screen and (min-width: 371px) and (max-width: 410px)
{
  #searchlist{
   width: 195px !important; 
  }
}
@media only screen and (min-width: 661px) and (max-width: 750px)
{
  #searchlist{
   width: 368px !important; 
  }
}
@media only screen and (min-width: 411px) and (max-width: 460px)
{
 #searchlist{
   width: 219px !important; 
  } 
}

@media only screen and (min-width: 667px) and (max-width: 750px)
{
  #searchlist{
   width: 369px !important; 
  } 
}
@media only screen and (min-width: 461px) and (max-width: 490px)
{
  #searchlist{
   width: 258px !important; 
  } 
}
@media only screen and (min-width: 491px) and (max-width: 570px)
{
  #searchlist{
   width: 310px !important; 
  } 
}
@media only screen and (min-width:736px) and (max-width: 750px)
{
  #searchlist{
   width: 410px !important; 
  } 
}
@media only screen and (min-width:533px) and (max-width: 550px)
{
  #searchlist{
   width: 290px !important; 
  } 
}
 
 /* tablet css*/ 
 @media only screen and (min-width : 951px) and (max-width : 1020px)
 {
  #searchlist{
   width: 273px !important; 
   
    }
}

@media only screen and (min-width: 781px) and (max-width: 860px)
{
  #searchlist{
   width: 200px !important; 
  } 
}

@media only screen and (min-width: 751px) and (max-width: 800px)
{
  #searchlist{
   width: 200px !important; 
  } 
}
@media only screen and (min-width: 600px) and (max-width: 810px)
{
 #searchlist{
   width: 330px !important; 
  }  
}
@media only screen and (min-width: 1001px) and (max-width: 1026px)
{
 #searchlist{
   width: 358px !important; 
  }  
} 
@media only screen and (min-width: 760px) and (max-width: 1000px)
{
 #searchlist{
   width: 275px !important; 
  } 
}
#auto_complete{padding: 10px;border: #a8d4b1 1px solid;border-radius:4px;}
#searchlist{float:left;list-style:none;margin-top:-3px;padding:0;width:483px;position: absolute;overflow: hidden;}
#searchlist li{padding: 10px; border-bottom: #ccc 1px solid;}
#searchlist li:hover{background:#eaf5ff;cursor: pointer;} 
#searchlist  {overflow-x: hidden;overflow: hidden; z-index: 4; max-height: 300px; overflow-y: scroll; box-shadow: 0px 5px 5px #ccc; font-size: 12px; background: #fff;}
#searchlist li a { text-decoration: none;}
.autolist{text-overflow: ellipsis;white-space: nowrap;overflow: hidden;}
</style>
<script type="text/javascript">
$(document).ready(function() 
{
    $("#searchlist" ).on("click","li.autolist", function(e) {    
      var selval=$(this).text();
      $("#auto_complete").val(selval);
     var geturl=$(this).data('pageurl');
     var pageurl=geturl.toLowerCase();
     window.location.href='<?php echo base_url() ?>reports/index/'+pageurl;
    });

   $("#auto_complete").keyup(function() {
      $("#searchlist").html('');
      $.ajax({
        type: "POST",
        url: '<?php echo base_url() ?>search/search/AutoComplete',
        dataType:'json',
        data: {
          'search_keyword': $(this).val()
        },
        success: function(data) {
            var content='';
            $.each( data.reports, function( index, value ) {
              content+='<li class="autolist" title="'+value.report_name+'" data-pageurl="'+value.page_urls+'">'+value.report_name+ '</li>';
            }); 
            $("#searchlist").html(content);
        }
      });
    });
});
</script>