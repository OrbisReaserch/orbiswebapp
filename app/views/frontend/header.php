<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="google-site-verification" content="B6ZKsPF2Fd5yPv1DJUJWqufPCnL5a_qSEVuSi5nJpGU" />

    <?php if($this->router->fetch_method()== 'getLatestReportList'){?>
<title>Browse All Latest Market Research Reports and Industry Analysis</title>
 

<?php }?>
<?php if($this->router->fetch_class()== 'category' && $this->router->fetch_method() == 'index') {?>
<title>Browse All Category Wise Market Research Reports and Industry Analysis</title>
<meta name="title" content="Browse All Category Wise Market Research Reports and Industry Analysis" />
 
<?php }?>
<?php if($this->router->fetch_class()== 'category' && $this->router->fetch_method() == 'publisher'){?>
<title>Browse All Publisher Wise Market Research Reports and Industry Analysis</title>
 
<?php }?> 
<?php if($this->router->fetch_class()== 'areas' && $this->router->fetch_method() == 'index'){ ?>
<meta name="title" content="Browse All Country Wise Market Research Reports and Industry Analysis" />
<title>Browse All Country Wise Market Research Reports and Industry Analysis</title>
 
<?php }?>
<?php if($this->router->fetch_class()== 'news' && $this->router->fetch_method() == 'index'){?>
<title>Browse All latest News By Orbis Research</title>
<meta name="title" content="Browse All latest News By Orbis Research" />
 
<?php }?>
<?php 
if($this->router->fetch_class()== 'news' && $this->router->fetch_method() == 'pressReleases'){?>
<title>Browse All latest Press Release By Orbis Research</title>
<meta name="title" content="Browse All latest Press Release By Orbis Research" />
 
<?php }?>
<?php if($this->router->fetch_method()== 'about_us'){?>

<title>About Us | OrbisResearch.com</title>
 
<?php }?>
<?php if($this->router->fetch_method()== 'contact_us'){?>
<title>Contact Us | OrbisResearch.com</title>
 
<?php }?>

    <link rel="shortcut icon" href="<?php echo theme_url();  ?>assets/img/favicon.png" />
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta name="language" content="English" />
<meta name="msvalidate.01" content="A03BA1EACB9EE9DA11EE90D3877302BD" />
<meta name="classification" content="market research report" />
<meta name="document-classification" content="Market Research Reports" />
<meta name="distribution" content="Global" />
<meta name="coverage" content="worldwide" />
<meta name="author" content="Orbis Research">
<meta name="rating" content="general" />
<meta name="abstract" content="Market Research Reports, Industry Analysis, Market Size, Share, Forecast" />
<meta name="document-type" content="Public" />
<meta name="URN.Identifier" content="Market Research Reports">
<meta name="Audience" content="All Market Reports, Business Indormation" />
<!--<meta name="revisit-after" content="1 days" />-->


<!----------------------------------------- nofollow noindex---------------------------------->



<?php if($this->router->fetch_class()== 'register' && $this->router->fetch_method() == 'login' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'register' && $this->router->fetch_method() == 'index' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'terms_and_conditions' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'refund_cancellation_policy' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'privacy_policy' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'disclaimer' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'about_us' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'faq' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'contact_us' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'payment_procedures' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'publisher' && $this->router->fetch_method() == 'publisher' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'purchase' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }if($this->router->fetch_class()== 'reports' && $this->router->fetch_method() == 'reportEnquiry' ){?>
<meta name="robots" content="nofollow,noindex" />
<?php }?>
<!----------------------------------------- nofollow noindex ends---------------------------------->
 
<?php 

  if(($this->router->fetch_class()== 'report' && $this->router->fetch_method() == 'getCategoryReport')) {
   	?>
    <title><?php if ('NULL' == $headerdetail[0]->page_title || '' == $headerdetail[0]->page_title){  echo $headerdetail[0]->meta_title; ?>
     
   <?php } 
      else{ echo $headerdetail[0]->page_title;}?></title>
    <meta name="title" content="<?php echo  $headerdetail[0]->meta_title; ?>" />
    <meta name="keywords" content="<?php echo  $headerdetail[0]->category_tag; ?>" />
    <meta name="description" content="<?php echo str_replace("&nbsp;"," ",stripslashes(strip_tags($headerdetail[0]->category_description)));?>"/> 
<?php }else if(($this->router->fetch_class()== 'report' && $this->router->fetch_method() == 'getSubcategoryInfo')) {?>
	<title><?php if ('NULL' == $headerdetail[0]->page_title || '' == $headerdetail[0]->page_title){echo $headerdetail[0]->meta_title;}else{ echo $headerdetail[0]->page_title;}?></title>
<meta name="title" content="<?php echo  $headerdetail[0]->meta_title; ?>" />

<meta name="keywords" content="<?php echo  $headerdetail[0]->sub_category_tag; ?>" />
<meta name="description" content="<?php echo  $headerdetail[0]->sub_category_description; ?>" />
 <?php } else if(($this->router->fetch_class()== 'report' && $this->router->fetch_method() == 'getCountryInfo')) { ?>
<meta name="keywords" content="<?php echo  $country_data['meta_tag']; ?>" />
<meta name="title" content="<?php echo  $country_data['meta_title']; ?>" /> 
<title><?php if ('NULL' == $country_data['page_title'] || '' == $country_data['page_title']){echo $country_data['meta_title'];}else{ echo $country_data['page_title'];}?></title>
<meta name="description" content="<?php echo  $country_data['meta_desc']; ?>" />
  <?php } ?>

<?php if(($this->router->fetch_class()== 'reports' && $this->router->fetch_method() == 'index') || ($this->router->fetch_class()== 'report' && $this->router->fetch_method() == 'getPublisherList') || ($this->router->fetch_class()== 'report' && $this->router->fetch_method() == 'getCategoryReport') || ($this->router->fetch_class()== 'report' && $this->router->fetch_method() == 'getSubcategoryInfo') || ($this->router->fetch_class()== 'report' && $this->router->fetch_method() == 'getCountryInfo')){
}else{
		  if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'terms_and_conditions' ){?>
		<title>Terms and Conditions | OrbisResearch.com</title>
		<?php }else if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'refund_cancellation_policy' ){?>
		<title>Refund or Cancellation policy | OrbisResearch.com</title>
		<?php }else if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'privacy_policy' ){?>
		<title>Privacy policy | OrbisResearch.com</title>
		<?php }else if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'disclaimer' ){?>
		<title>Disclaimer | OrbisResearch.com</title>
		<?php }else if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'faq' ){?>
		<title>FAQ's | OrbisResearch.com</title>
		<?php }else if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'payment_procedures' ){?>
		<title>Payment Procedures | OrbisResearch.com</title>
		<?php }else if($_SERVER['REQUEST_URI'] == "/becomeapublisher"){?>
		<title>Become a Publisher | OrbisResearch.com</title>
		<?php }else if($this->router->fetch_class()== 'news' && $this->router->fetch_method() == 'getNewsDetails'){?>
		 
		<?php }else if($this->router->fetch_class()== 'reports' && $this->router->fetch_method() == 'requestsample'){?>
		 
		<?php }else if($this->router->fetch_class()== 'reports' && $this->router->fetch_method() == 'enquirybeforebuying'){?>
		 
		<?php }else if($this->router->fetch_class()== 'reports' && $this->router->fetch_method() == 'discount'){?>
		 
		<?php }else if($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'purchase'){?>
		 
		<?php }elseif($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'about_us'){?>
		 
		<?php }elseif($this->router->fetch_class()== 'category' && $this->router->fetch_method() == 'index'){?>
		 
		<?php }elseif($this->router->fetch_class()== 'contact' && $this->router->fetch_method() == 'contact_us'){?>
		 
		<?php }elseif($this->router->fetch_class()== 'areas' && $this->router->fetch_method() == 'index'){?>
		 
		<?php }elseif($this->router->fetch_class()== 'report' && $this->router->fetch_method() == 'getLatestReportList'){?>
		 <?php }elseif($this->router->fetch_class()== 'news' && $this->router->fetch_method() == 'getPressReleaseDetails'){?>
		<?php }else{ ?>
			<title>OrbisResearch.com: Market Research Reports and Industry Analysis</title>
		<?php } ?>
<meta name="keywords" content="Orbis Research, Market Research Reports, Industry Analysis, Industry Trends, Industry Research Reports, Business Research, Market Analysis Reports, Market Size, Market Shares, Top Market Research Companies" />
<meta name="description" content="OrbisResearch.com is a leading market research reseller which offers market research reports/studies on products, services, companies, verticals, countries globally." />
<?php }?>
<!-----------Favicon--------------->
<link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>themes/frontend/images/orbisicon.png"/>
<!----------- end Favicon--------------->
<meta name="author" content="Orbis" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta charset="utf-8" />

   
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59019821-1', 'auto');
  ga('send', 'pageview');

</script>
	<link rel="canonical" href="<?php echo current_url(); ?>" />
  <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "https://www.orbisresearch.com/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "https://query.orbisresearch.com/search?q={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>
    

    <!-- CSS -->
    <link href="<?php echo theme_url();  ?>assets/css/preload.css" rel="stylesheet">
    <link href="<?php echo theme_url();  ?>assets/css/vendors.css" rel="stylesheet">
    <link href="<?php echo theme_url();  ?>assets/css/syntaxhighlighter/shCore.css" rel="stylesheet" >
    <link href="<?php echo theme_url();  ?>assets/css/style-blue.css" rel="stylesheet" title="default">
    <link href="<?php echo theme_url();  ?>assets/css/width-full.css" rel="stylesheet" title="default">
     <link href="<?php echo theme_url();  ?>assets/css/custom.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="<?php echo theme_url();  ?>assets/js/html5shiv.min.js"></script>
        <script src="<?php echo theme_url();  ?>assets/js/respond.min.js"></script>
    <![endif]-->
   
   	<!--Core js-->
	<script src="<?php echo base_url();?>themes/backend/js/lib/jquery.js"></script>
	<script src="<?php echo base_url();?>themes/backend/js/lib/jquery-1.8.3.min.js"></script>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-59019821-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-59019821-1');
</script>

</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>

<div class="sb-site-container">
<div class="boxed">
