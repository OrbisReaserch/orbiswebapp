<aside id="footer-widgets">
    <div class="container">
        <div class="row">
           <div class="col-md-2 col-sm-2">
                <div class="footer-widget">
                    <h3 class="footer-widget-title">Help:</h3>
                <ul class="list-unstyled three_cols">
                    <li><a href="<?php echo base_url(); ?>faq">FAQ's</a></li>
                    <li><a href="<?php echo base_url(); ?>paymentprocedures">Payment Procedures</a></li>
                    <li><a href="<?php echo base_url(); ?>reportindex">Sitemap</a></li>
               </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <h3 class="footer-widget-title">OrbisResearch:</h3>
                <ul class="list-unstyled three_cols">
                    
                   <li><a href="<?php echo base_url(); ?>aboutus">About Us</a></li>
                    <li><a href="<?php echo base_url(); ?>our-services">Our Services</a></li>
                    <li><a href="<?php echo base_url(); ?>contactus">Contact Us</a></li>
                    <li><a href="<?php echo base_url(); ?>becomeapublisher">Become a Publisher</a></li> 
                    
                </ul>
           </div>
            <div class="col-md-2 col-sm-2">
                <h3 class="footer-widget-title">Legal:</h3>
                <ul class="list-unstyled three_cols">
                    
                    
               
                    <li><a href="<?php echo base_url(); ?>termsandconditions">Terms and Conditions</a></li>
                    <li><a href="<?php echo base_url(); ?>refundcancellationpolicy">Refund or Cancellation policy</a></li>
                    <li><a href="<?php echo base_url(); ?>privacypolicy">Privacy policy</a></li>
                    <li><a href="<?php echo base_url(); ?>disclaimer">Disclaimer</a></li>
                    
                </ul>
           </div>
            <div class="col-md-3 col-sm-3">
                <div class="footer-widget footerSocialicons">
                    <h3 class="footer-widget-title">Connect With Us:</h3>
                    <a class="btn-social sm twitter" href="https://twitter.com/orbisresearch" target="_blank"><i class="fa fa-twitter"></i></a>
                    <a class="btn-social sm facebook" href="https://www.facebook.com/OrbisResearch" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a class="btn-social sm google-plus" href="https://plus.google.com/+Orbisresearch" target="_blank"><i class="fa fa-google-plus"></i></a>
                    <!-- <a class="btn-social sm linkedin" href="https://www.linkedin.com/company/orbisresearch" target="_blank"><i class="fa fa-linkedin"></i></a> -->
                    <a class="btn-social sm linkedin" href="https://in.linkedin.com/company/orbis-research" target="_blank"><i class="fa fa-linkedin"></i></a>
                     <a class="btn-social sm wordpress" href="https://orbisresearch.wordpress.com/" target="_blank"><i class="fa fa-wordpress"></i></a>
                  
                     <h3 class="footer-widget-title">We accept payments via:</h3>
                     <img src="<?php echo theme_url();  ?>assets/img/demo/paymentlogo.png">
                </div>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="footer-widget footeremailus">
                    <h3 class="footer-widget-title">Email Us:</h3>
                   <p>For all enquiries, send us an email on :<br/><a href="mailto:enquiry@orbisresearch.com"><strong>enquiry@orbisresearch.com</strong></a></p>
                </div>
            </div>
	    <div class="col-md-3 col-sm-3">
                <div class="footer-widget footeremailus">
                    
                   <p>For sales send us an email on :<br/><a href="mailto:sales@orbisresearch.com"><strong>sales@orbisresearch.com</strong></a></p>
                </div>
            </div>
			 
        </div> <!-- row -->
    </div> <!-- container -->
</aside> <!-- footer-widgets -->
<footer id="footer">
    <p>Copyright © <?php echo date('Y');?> Orbis Research. All rights reserved. <a href="<?php echo base_url();?>">www.orbisresearch.com</a></p>
</footer>



</div> <!-- boxed -->
</div> <!-- sb-site -->

<div class="sb-slidebar sb-right sb-style-overlay">

    <h2 class="slidebar-header no-margin-bottom">Useful Links</h2>
    <ul class="slidebar-menu">
                    <li><a href="<?php echo base_url(); ?>termsandconditions">Terms and Conditions</a></li>
                    <li><a href="<?php echo base_url(); ?>refundcancellationpolicy">Refund or Cancellation policy</a></li>
                    <li><a href="<?php echo base_url(); ?>privacypolicy">Privacy policy</a></li>
                    <li><a href="<?php echo base_url(); ?>disclaimer">Disclaimer</a></li>
                    <li><a href="<?php echo base_url(); ?>our-services">Our Services</a></li>
                    <li><a href="<?php echo base_url(); ?>reportindex">Sitemap</a></li>
    </ul>
    
     <h2 class="slidebar-header no-margin-bottom">Help</h2>
    <ul class="slidebar-menu">
                    <li><a href="<?php echo base_url(); ?>faq">FAQ's</a></li>
                    <li><a href="<?php echo base_url(); ?>aboutus">About Us</a></li>
                    <li><a href="<?php echo base_url(); ?>contactus">Contact Us</a></li>
                    <li><a href="<?php echo base_url(); ?>paymentprocedures">Payment Procedures</a></li>
                    <li><a href="<?php echo base_url(); ?>becomeapublisher">Become a Publisher</a></li> 
    </ul>
    
</div> <!-- sb-slidebar sb-right -->
<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>


<script src="<?php echo theme_url();  ?>assets/js/vendors.js"></script>

<!-- Syntaxhighlighter -->
<script src="<?php echo theme_url();  ?>assets/js/syntaxhighlighter/shCore.js"></script>
<script src="<?php echo theme_url();  ?>assets/js/syntaxhighlighter/shBrushXml.js"></script>
<script src="<?php echo theme_url();  ?>assets/js/syntaxhighlighter/shBrushJScript.js"></script>

<script src="<?php echo theme_url();  ?>assets/js/DropdownHover.js"></script>
<script src="<?php echo theme_url();  ?>assets/js/app.js"></script>
<!--<script src="<?php //echo theme_url();  ?>assets/js/home_services.js"></script>-->


<script src="<?php echo theme_url();  ?>assets/js/carousels.js"></script>


<script type="text/javascript">
  /* function callHref(link){
    var win=window.open(link, '_blank');
    win.focus();
  }

 if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) 
    {
        $('marquee').removeAttr("onMouseOver").attr("onMouseOver","this.stop();")
        $('marquee').removeAttr("onMouseOut").attr("onMouseOut","this.start();")
    }
    else if(navigator.userAgent.indexOf("Chrome") != -1 )
    {
        
        $('marquee').removeAttr("onMouseOver").attr("onMouseOver","this.stop();")
        $('marquee').removeAttr("onMouseOut").attr("onMouseOut","this.start();")
    }
    else if(navigator.userAgent.indexOf("Safari") != -1)
    {
       $('marquee').removeAttr("onMouseOver").attr("onMouseOver","this.stop();")
      $('marquee').removeAttr("onMouseOut").attr("onMouseOut","this.start();")
    }
    else if(navigator.userAgent.indexOf("Firefox") != -1 ) 
    {
        $('marquee').removeAttr("onMouseOver").attr("onMouseOver","this.setAttribute('scrollamount', 0, 0);")
        $('marquee').removeAttr("onMouseOut").attr("onMouseOut","this.setAttribute('scrollamount', 5, 0);")
    }
    else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
    {
      $('marquee').removeAttr("onMouseOver").attr("onMouseOver","this.stop();")
      $('marquee').removeAttr("onMouseOut").attr("onMouseOut","this.start();")
    }  
    else 
    {
       $('marquee').removeAttr("onMouseOver").attr("onMouseOver","this.stop();")
      $('marquee').removeAttr("onMouseOut").attr("onMouseOut","this.start();")
    }
*/
</script>
<script src="<?php echo base_url(); ?>themes/frontend/js/jquery.bootstrap.newsbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
		
        $(".demo1").bootstrapNews({
            newsPerPage: 4,
            autoplay: true,
			pauseOnHover:true,
            direction: 'down',
            newsTickerInterval: 4000,
            onToDo: function () {
                //console.log(this);
            }
        });
		
		  $(".demo2").bootstrapNews({
            newsPerPage: 4,
            autoplay: true,
			pauseOnHover:true,
            direction: 'down',
            newsTickerInterval: 4000,
            onToDo: function () {
                //console.log(this);
            }
        });
		
		
		  $(".demo3").bootstrapNews({
            newsPerPage: 4,
            autoplay: true,
			pauseOnHover:true,
            direction: 'down',
            newsTickerInterval: 4000,
            onToDo: function () {
                //console.log(this);
            }
        });
		
    });
</script>
		 
<script>
!function(d){
	var c="portfilter";
	var b=function(e){
		this.$element=d(e);this.stuff=d("[data-tag]");this.target=this.$element.data("target")||""};
		b.prototype.filter=function(g){var e=[],f=this.target;this.stuff.fadeOut("fast").promise().done(function()
		{d(this).each(function(){if(d(this).data("tag")==f||f=="all"){e.push(this)}});d(e).show()})};var a=d.fn[c];
		d.fn[c]=function(e){return this.each(function(){var g=d(this),f=g.data(c);if(!f){g.data(c,(f=new b(this)))}if(e=="filter"){f.filter()}})};
		d.fn[c].defaults={};d.fn[c].Constructor=b;d.fn[c].noConflict=function(){d.fn[c]=a;return this};
		d(document).on("click.portfilter.data-api","[data-toggle^=portfilter]",function(f){d(this).portfilter("filter")})}(window.jQuery);
		
		//clickAlpha();
</script>

<?php /* ?><!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-589023da2dbce537"></script> <?php */ ?>
</body>

</html>
