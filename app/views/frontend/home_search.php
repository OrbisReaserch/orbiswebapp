<header class="services-header">
         <div class="primary-dark-div">
            <div class="container">
               <div class="row">
                     <div class="col-md-12">
                        <div class="service-header-text text-center center-block">
                           <h1 class="animated fadeInDown animation-delay-2"> Search Market Research Reports with Industry Analysis and Forecasts</h1>
                        </div>
                     </div>
                  <div class="col-md-12 text-center search-box-index">
                     <div class="searchkeywordform animated fadeInDown animation-delay-7">
                           <form action="<?php echo base_url();?>search" method = "post" id="auto_submit" name="auto_submit" role="form" class="form-inline">
                           <div class="form-group text-center">
                              <label for="SearchKeyword" class="sr-only ">Search Keyword</label>
                              <input type="text" name="search_keyword"  placeholder="Search Keyword..." id="auto_complete" class="form-control " autocomplete="off">
                               <div id="suggesstion-box">
                               <ul id="searchlist"></ul>
                              </div>
                           </div>
                           
                           <div class="form-group">
                              <label for="exampleInputPassword2" class="sr-only">Select Catagory</label>
                                 <select name="category"  id="category">
                                       <option value="0">All</option>
                                          <?php
                     								$this->load->model('home/home_model');
                     								$result1 = $this->home_model->getAllCategory();
                     								for($cr = 0; $cr< count($result1); $cr++)
                  								   {
                  									   ?>
                  									<option value="<?php echo $result1[$cr]->id;?>"><?php echo ucfirst(strtolower($result1[$cr]->category_name));?>
                                              </option>
                  								 <?php 
                  								    }
                  								 ?>
                                 </select>
                           </div>
                           <button class="btn btn-ar btn-primary"  type="submit" name="submit" value="Search">Search</button>
                        </form>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="service-header-text text-center center-block">
                        <h4 class="animated fadeInDown animation-delay-2" style="color: #fff;"> We deal with world wide publishers and also deal in customized research reports </h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
 </header>

 <style type="text/css"> 
 /* mobile css*/
 @media only screen and (min-width: 311px) and (max-width: 330px)
{
  #searchlist
  { 
  width: 255px !important;
  }
}
@media only screen and (min-width: 331px) and (max-width: 370px)
{
  #searchlist{
   width: 256px !important; 
  }
}
@media only screen and (min-width: 631px) and (max-width: 660px)
{
  #searchlist{
   width: 256px !important; 
  }
}
@media only screen and (min-width: 371px) and (max-width: 410px)
{
  #searchlist{
   width: 256px !important; 
  }
}

@media only screen and (min-width: 411px) and (max-width: 460px)
{
 #searchlist{
   width: 256px !important; 
  } 
}

@media only screen and (min-width: 667px) and (max-width: 750px)
{
  #searchlist{
   width: 256px !important; 
  } 
}
@media only screen and (min-width: 461px) and (max-width: 490px)
{
  #searchlist{
   width: 258px !important; 
  } 
}
@media only screen and (min-width: 491px) and (max-width: 570px)
{
  #searchlist{
   width: 256px !important; 
  } 
}
@media only screen and (min-width:736px) and (max-width: 750px)
{
  #searchlist{
   width: 256px !important; 
  } 
}
@media only screen and (min-width:533px) and (max-width: 550px)
{
  #searchlist{
   width: 258px !important; 
  } 
}
/* tablet css*/
@media only screen and (min-width: 751px) and (max-width: 800px){
 #searchlist{
   width: 275px !important; 
  }   
}

@media only screen and (min-width : 951px) and (max-width : 1020px){
  #searchlist{
   width: 275px !important; 
  }    
}
@media only screen and (min-width: 600px) and (max-width: 810px)
{
 #searchlist{
   width: 255px !important; 
  }  
}
@media only screen and (min-width: 1001px) and (max-width: 1026px)
{
 #searchlist{
   width: 275px !important; 
  }  
}
@media only screen and (min-width: 800px) and (max-width: 900px){
 
 #searchlist{
   width: 275px !important; 
  }    
}
@media only screen and (min-width : 768px) and (max-width : 1024px){
 #searchlist{
   width: 275px !important; 
  }      
}
#auto_complete{padding: 10px;border: #a8d4b1 1px solid;border-radius:4px;}
#searchlist{float:left;list-style:none;margin-top:-3px;padding:0;width:475px;position: absolute;overflow: hidden;}
#searchlist li{padding: 10px; border-bottom: #ccc 1px solid;}
#searchlist li:hover{background:#eaf5ff;cursor: pointer;} 
#searchlist  {overflow-x: hidden;overflow: hidden; z-index: 1; max-height: 300px; overflow-y: scroll; box-shadow: 0px 5px 5px #ccc; font-size: 12px; background: #fff; color: #333;text-align: left;}
#searchlist li a { text-decoration: none;}
.autolist{text-overflow: ellipsis;white-space: nowrap;overflow: hidden;}
.search-box-index{z-index: 1;}
</style>
<script type="text/javascript">
$(document).ready(function() 
{
    $("#searchlist" ).on("click","li.autolist", function(e) {    
      var selval=$(this).text();
      $("#auto_complete").val(selval);
       var geturl=$(this).data('pageurl');
       var pageurl=geturl.toLowerCase();
      window.location.href='<?php echo base_url() ?>reports/index/'+pageurl;
    });
   $("#auto_complete").keyup(function() {
      $("#searchlist").html('');
      $.ajax({
        type: "POST",
        url: '<?php echo base_url() ?>search/search/AutoComplete',
        dataType:'json',
        data: {
          'search_keyword': $(this).val()
        },
        success: function(data) {
            var content='';
            $.each( data.reports, function( index, value ) {
            content+='<li class="autolist" title="'+value.report_name+'" data-pageurl="'+value.page_urls+'">'+value.report_name+ '</li>';
           });
           $("#searchlist").html(content);
         }
      });
    });
});
</script>