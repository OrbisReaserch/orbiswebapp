<?php
function getAllCategory($id)
{
    $CI =& get_instance();
    $CI->load->model('category/category_model');

    $r = $CI->category_model->getAllCategory();
    echo "<option value='0'>-Select Category-</option>";
    if(count($r))
    {
        for($i=0;$i<count($r);$i++)
        {
           ?>
            <option value='<?php echo $r[$i]->id?>' <?php if(isset($id) && $id!='' && $id==$r[$i]->id){ echo "selected"; } ?>><?php echo $r[$i]->category_name;?></option>
            <?php
        }  
    }
    
}
function CreateSitemap($fileName,$url)
{
         $current = file_get_contents($fileName);
	 $tmpFileData = str_replace("</urlset>","",$current);
	 $tmpFileData .= "<url><loc>".$url."</loc></url></urlset>";
         file_put_contents($fileName, $tmpFileData);
}
function CreateRss($fileName,$data)
{
          
		 $tmpFileData = "<?xml version='1.0' encoding='utf-8'?> <rss version='2.0' xmlns:content='http://purl.org/rss/1.0/modules/content/' xmlns:wfw='http://wellformedweb.org/CommentAPI/' xmlns:dc='http://purl.org/dc/elements/1.1/' xmlns:atom='http://www.w3.org/2005/Atom' xmlns:sy='http://purl.org/rss/1.0/modules/syndication/' xmlns:slash='http://purl.org/rss/1.0/modules/slash/' >";
		 $tmpFileData .= " <channel>".$data."</channel></rss>";
         file_put_contents($fileName, $tmpFileData);
		  
		 return true;
}
function allcategory($id)
{
    $CI = & get_instance();
    $CI->load->model('report/report_model');
    $r = $CI->report_model->allcategory();
    echo "<option value='#'>---Select Category----</option>";
    if (count($r)) {
        for ($i = 0; $i < count($r); $i++) {
            ?>
                        	<option value='<?php echo $r[$i]->id ?>' <?php
            if (isset($id) && $id != '' && $id == $r[$i]->id) {
                echo "selected";
            }
            ?>><?php echo $r[$i]->category_name; ?></option>
            <?php
        }
    }
}
	
function allvendor($id)
{
    $CI = & get_instance();
    $CI->load->model('report/report_model');

    $r = $CI->report_model->allvendor();
    echo "<option value='#'>---Select Publisher----</option>";
    if (count($r)) {
        for ($i = 0; $i < count($r); $i++) {
            ?>
                        		<option value='<?php echo $r[$i]->id ?>' <?php
            if (isset($id) && $id != '' && $id == $r[$i]->id) {
                echo "selected";
            }
            ?>><?php echo $r[$i]->display_name; ?></option>
            <?php
        }
    }
}
	
function allsubcategory($id)
{
    $CI = & get_instance();
    $CI->load->model('report/report_model');

    $r = $CI->report_model->allsubcategory();
    echo "<option value='#'>---Select Sub Category----</option>";
    if (count($r)) {
        for ($i = 0; $i < count($r); $i++) {
            ?>
            		<option value='<?php echo $r[$i]->id ?>' <?php if (isset($id) && $id != '' && $id == $r[$i]->id) {
                echo "selected";
            } ?>><?php echo $r[$i]->sub_category_name; ?></option>
            <?php
        }
    }
}
function allsubcategorybycatid($id,$cat_id){
  $CI = & get_instance();
    $CI->load->model('report/report_model');

    $r = $CI->report_model->subcategory_by_id($cat_id);
    echo "<option value='#'>---Select Sub Category----</option>";
    if (count($r)) {
        for ($i = 0; $i < count($r); $i++) {
            ?>
            		<option value='<?php echo $r[$i]->id ?>' <?php if (isset($id) && $id != '' && $id == $r[$i]->id) {
                echo "selected";
            } ?>><?php echo $r[$i]->sub_category_name; ?></option>
            <?php
        }
    }

}

function getVendors($id)
{
    $CI = & get_instance();
    $CI->load->model('user/user_model');

    $r = $CI->user_model->getVendors();
    echo "<option value='0'>-Select Vendor-</option>";
    if (count($r)) {
        for ($i = 0; $i < count($r); $i++) {
            ?>
                        <option value='<?php echo $r[$i]->id ?>' <?php if (isset($id) && $id != '' && $id == $r[$i]->id) {
                echo "selected";
            } ?>><?php echo $r[$i]->display_name; ?></option>
            <?php
        }
    }
}

function getAllSubCategory($id)
{
    $CI = & get_instance();
    $CI->load->model('siteadmin/category_model');

    $r = $CI->category_model->getAllSubCategory();
    echo "<option value='0'>-Select Sub Category-</option>";
    if (count($r)) {
        for ($i = 0; $i < count($r); $i++) {
            ?>
                        <option value='<?php echo $r[$i]->id ?>' <?php if (isset($id) && $id != '' && $id == $r[$i]->id) {
                echo "selected";
            } ?>><?php echo $r[$i]->sub_category_name; ?></option>
            <?php
        }
    }
}

function getAllSubSubCategory($id)
{
    $CI = & get_instance();
    $CI->load->model('category/category_model');

    $r = $CI->category_model->getsub_subcategory();
    echo "<option value='0'>-Select Category-</option>";
    if (count($r)) {
        for ($i = 0; $i < count($r); $i++) {
            ?>
                        <option value='<?php echo $r[$i]->id ?>' <?php if (isset($id) && $id != '' && $id == $r[$i]->id) {
                echo "selected";
            } ?>><?php echo $r[$i]->sub_sub_category_name; ?></option>
            <?php
        }
    }
}
// function to get a particular vendor type

function get_Country($id)
{
	$CI =& get_instance();
	$CI->load->model('emp/emp_model');
	
	$r = $CI->emp_model->get_Country($id);

	if(count($r))
	{
            echo $r[0]->country_name;
        }
        else
        {
            echo "---";
        }
}
function get_State($id)
{
	$CI =& get_instance();
	$CI->load->model('emp/emp_model');
	
	$r = $CI->emp_model->get_State($id);
        
	if(count($r))
	{
            echo $r[0]->state_name;
        }
        else
        {
            echo "---";
        }
}

function get_City($id)
{
	$CI =& get_instance();
	$CI->load->model('emp/emp_model');
	
	$r = $CI->emp_model->get_City($id);

	if(count($r))
	{
            echo $r[0]->city_name;
        }
        else
        {
            echo "---";
        }
}
function getAllCountry($id)
{
    $CI =& get_instance();
    $CI->load->model('emp/emp_model');

    $r = $CI->emp_model->getAllCountry();
    echo "<option value='0'>-Select Country-</option>";
    if(count($r))
    {
        for($i=0;$i<count($r);$i++)
        {
           ?>
            <option value='<?php echo $r[$i]->id?>' <?php if(isset($id) && $id!='' && $id==$r[$i]->id){ echo "selected"; } ?>><?php echo $r[$i]->country_name;?></option>
            <?php
        }
    }
    
}

function getAllState($id='')
{
    $CI =& get_instance();
    $CI->load->model('emp/emp_model');

    $r = $CI->emp_model->getAllState();
    echo "<option value='0'>-Select State-</option>";
    if(count($r))
    {
        for($i=0;$i<count($r);$i++)
        {
            ?>
            <option value='<?php echo $r[$i]->id;?>' <?php if(isset($id) && $id!='' && $id==$r[$i]->id){ echo "selected"; } ?>><?php echo $r[$i]->state_name;?></option>
            <?php
        }
    }
    
}

function getAllCity($id='')
{
    $CI =& get_instance();
    $CI->load->model('emp/emp_model');

    $r = $CI->emp_model->getAllCity();
    echo "<option value='0'>-Select City-</option>";
    if(count($r))
    {
        for($i=0;$i<count($r);$i++)
        {
            ?>
            <option value='<?php echo $r[$i]->id;?>' <?php if(isset($id) && $id!='' && $id==$r[$i]->id){ echo "selected"; } ?>><?php echo $r[$i]->city_name;?></option>
            <?php
        }
    }
    
}

function allcountry($id,$isContinent)
{ 
    $CI =& get_instance();
    $CI->load->model('report/report_model');

    $r = $CI->report_model->allcountry($isContinent);
    echo "<option value='0'>---Select--</option>";
    if(count($r))
    {
        for($i=0;$i<count($r);$i++)
        {
           ?>
            <option value='<?php echo $r[$i]->id?>' <?php if(isset($id) && $id!='' && $id == $r[$i]->id){ echo "selected"; } ?>><?php echo $r[$i]->country_name;?></option>
            <?php
        }
    }
    
}
function GetReportDetails($report_id){
	$CI =& get_instance();
    $CI->load->model('report/report_model');

    $r = $CI->report_model->getReports1($report_id);
	if(count($r))
    {
		return $r[0]->report_name;
	}
}
function ValidateLogin(){
	echo "got";
	die();
}
?>