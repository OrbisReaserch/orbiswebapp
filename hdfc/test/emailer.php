<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style>
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td {
margin: 0;
padding: 0;
border: 0;
outline: 0;
font-size: 100%;
background: transparent;
}

/* body */
body {
	padding: 0px auto;
	margin: 0px auto;
}

body, input, textarea {
	font: 14px 'Arial', sans-serif;
	font-weight: normal;
	font-style: normal;
	line-height: 23px;
	color: #323232;
}

/* Headings */
h1, 
h2,
h3,
h4,
h5,
h6 {
	font-family:Arial, Helvetica, sans-serif
	font-weight: normal;
	color: #1e1e1e;
}
h1 a, 
h2 a, 
h3 a, 
h4 a, 
h5 a, 
h6 a { 
	color: inherit;
}
h1 { 
	font-size: 40px; 
	line-height: 40px;
	margin-bottom: 30px;
	font-style: normal;
	font-weight: normal;
}
h1.white {
	color: #fff;
}

h2 { 
	font-size: 35px; 
	line-height: 35px;
	margin-bottom: 20px;
	font-style: normal;
	font-weight: normal;
}
h2.small { 
	font-size: 27px; 
	line-height: 32px;
}
h2.small2 { 
	font-size: 27px; 
	line-height: 32px;
	margin-bottom: 38px;
}
h2.white {
	color: #fff;
}

h3 { 
	font-size: 27px; 
	line-height: 30px;
	margin-bottom: 25px;
	font-style: normal;
	font-weight: normal;
}

h4 { 
	font-size: 21px; 
	line-height: 25px;
	margin-bottom: 2px;
	font-style: normal;
}

h5 { 
	font-size: 18px; 
	line-height: 25px;
	margin-bottom: 2px;
	font-style: normal;
}

h6 { 
	font-size: 16px; 
	line-height: 21px;
	margin-bottom: 6px;
	font-style: normal;
}



/* Text elements */
p {

	font-size: 14px;
	font-weight: normal;
	line-height: 23px;
	
}



ul, ol {
	margin: 0 0 20px 0;
	list-style-position: inside;
}
ul {
	list-style: none;
	padding: 3px 20px;
}
ul li {
	list-style: none;
	padding:2px 0px;
}


strong {
	font-weight: bold;
}
cite, em, i {
	font-style: italic;
}


table {
    border-collapse: collapse;
}

table, th {
    border: 1px solid black;
}

td{padding:3px 20px;
border-right:1px solid #000;
}
.emailer_container
{
width:810px;
margin:0 auto;
}

.emailer
{
border:1px solid #333;
width:100%;
}

.emailer tr {
border-bottom: 1px solid black;
}

.emailer td ul li span
{
width:155px;
display:block;
float:left;
}

.noborder
{
border-bottom:none !important; 
}

.capital
{
text-transform:uppercase;
}

.rght
{
float:right;
}

.amount
{
border-bottom:1px solid #000;
padding: 0px 15px;
font-weight:bold;

}

.heading
{
color:#000;
font-weight:bold;
}
</style>
</head>

<body>
<div class="emailer_container">
<center><h3 style="margin-top: 5%;">Success!! Your order has been processed</h3><br>
<h5 style="margin-top: -5%;">Transaction details</h5> </center> 
<br>
<center>
<table class="emailer">
 
 
 <tr>
 <td align="center"><span class="heading">Sr No</span></td>
 <td><span class="heading">Title</span></td>

 <td ><span class="heading">Description</span></td>
 </tr>
 
  <?php 
  $arraykeys = array_keys($_REQUEST);
 /* for($i = 0; $i < count($arraykeys); $i++){ 
  if($_REQUEST[$arraykeys[$i]] != ""){
	?>
 
  <tr>
 <td align="center"><?php echo ($i + 1); ?></td>
 <td><?php echo $arraykeys[$i]; ?></td>

 <td><?php echo $_REQUEST[$arraykeys[$i]]; ?></td>
 </tr>
  <?php }
  } */ ?>

<tr>
 <td align="center"><?php echo '1'; ?></td>
 <td><?php echo 'txnid'; ?></td>

 <td><?php echo $_REQUEST['txnid']; ?></td>
 </tr>
 <tr>
 <td align="center"><?php echo '2'; ?></td>
 <td><?php echo 'amount'; ?></td>

 <td><?php echo $_REQUEST['amount']; ?></td>
 </tr>
</table>
<center>
</div>
</body>
</html>
