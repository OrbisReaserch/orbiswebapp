<?php
// echo "<pre>";
// print_r($_GET);
// exit;
require('config.php');
require('razorpay-php/Razorpay.php');
session_start();

// Create the Razorpay Order

use Razorpay\Api\Api;

$api = new Api($keyId, $keySecret);

$_SESSION['order_amount'] = base64_decode($_GET['amount']);
$merchant_order_id=base64_decode($_GET['merchant_order_id']);
$full_name=base64_decode($_GET['full_name']);
$description=base64_decode($_GET['description']);
$email=base64_decode($_GET['email']);
$phone=base64_decode($_GET['phone']);
$address=base64_decode($_GET['address']);
$full_name=base64_decode($_GET['full_name']);
//
// We create an razorpay order using orders api
// Docs: https://docs.razorpay.com/docs/orders
//
$orderData = [
    'receipt'         => $merchant_order_id,
    'amount'          => $_SESSION['order_amount'] * 100, // 2000 rupees in paise
    'currency'        => $displayCurrency,
    'payment_capture' => 1 // auto capture
];

$razorpayOrder = $api->order->create($orderData);

$razorpayOrderId = $razorpayOrder['id'];

$_SESSION['razorpay_order_id'] = $razorpayOrderId;

$displayAmount = $amount = $orderData['amount'] / 100;

// if ($displayCurrency !== 'INR')
// {
//      //$url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
//     $url="http://data.fixer.io/api/latest?access_key=bbd79da9eda80d67f49e1d0d40285110&symbols=$displayCurrency";
//     $exchange = json_decode(file_get_contents($url), true);

//     $displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
// }

$checkout = 'automatic';

$data = [
    "key"               => $keyId,
    "amount"            => $_SESSION['order_amount'],
    "name"              => 'Orbis Research',
    "description"       => $description,
    "image"             => $base_url."themes/frontend/assets/img/logo_orbis.jpg",
    "prefill"           => [
    "name"              => $full_name,
    "email"             => $email,
    "contact"           => $phone,
    ],
    "notes"             => [
    "address"           => $address,
    "merchant_order_id" => $merchant_order_id,
    ],
    "theme"             => [
    "color"             => "#F37254"
    ],
    "order_id"          => $razorpayOrderId,
];

if ($displayCurrency !== 'INR')
{
    $data['display_currency']  = $displayCurrency;
    $data['display_amount']    = $displayAmount;
}

$json = json_encode($data);

//require("checkout/{$checkout}.php");

?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<style>
    table {
  border-collapse: separate;
  border-spacing: 4px;
}
table,
th,
td {
  border: 1px solid #cecfd5;
}
th,
td {
  padding: 10px 15px;
  color:#1062AC;
}

    input[type="submit"]
{
    margin-top:30px;
    margin-left:127%;
    border:1px solid #1062AC;
    text-decoration:none;
    padding:15px;
    width:70%;
    color:#fff;

    background-color:#1062AC;
}
/*.vl {
  border-left: 6px solid #1062AC;
  height: 400px;
  box-shadow: 10px 10px 5px grey;
}*/
.col-md-1 {
    width:3.33% !important;
    }
</style>
<div class="container">
    <div class="col-md-12" style="margin-top:6%;">
        <div class="col-md-12">
            <img src="<?php echo $base_url; ?>themes/frontend/assets/img/logo_orbis.jpg" style="width:20%;">
<table>
  <caption><h3 style="color:#1062AC;text-align:center;"><b>ORDER SUMMARY</b></h3></caption>
  <thead>
    <tr>
      <th scope="col" style="text-transform: uppercase;">REPPORT NAME</th>
      
      <th style="width:30%;" scope="col" style="text-transform: uppercase;">Price</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="width:90%;text-transform: uppercase;"><b><?php echo $description;?></b></td>
       <td style="width:106px;">USD <?php echo $_SESSION['order_amount'];?></td>
    </tr>
     <tr>
      <td style="height:110px;"></td>
       <td style="height:110px;width:40%;"></td>
    </tr>
    <tr>
      <td style="width:90%;text-transform: uppercase;text-align:right"><b>Total</b></td>
       <td style="width:106px;"><b>USD <?php echo $_SESSION['order_amount'];?></b></td>
    </tr>
  </tbody>
  
</table>

</div>

<div class="col-md-6">
    <div class="row" >
        
<!-- <h1 style="text-align:center;">Welcome to Orbis Research</h1> -->
<!-- <p style="text-align:left;font-size:22px;margin-left:4%;">Place your Order Here ....</p> -->
<!-- <p style="text-align:left;font-size:22px;margin-left:4%;">Welcome to Orbis Research</p> -->
<form action="<?php echo $base_url.'razorpaypg/verify.php'?>" method="POST">
  <script
    src="https://checkout.razorpay.com/v1/checkout.js"
    data-key="<?php echo $data['key']?>"
    
    data-amount="<?php echo $data['amount']?>"
    data-currency="<?php echo $displayCurrency?>"
    data-name="<?php echo $data['name']?>"
    data-image="<?php echo $data['image']?>"
    data-description="<?php echo $data['description']?>"
    data-prefill.name="<?php echo $data['prefill']['name']?>"
    data-prefill.email="<?php echo $data['prefill']['email']?>"
    data-prefill.contact="<?php echo $data['prefill']['contact']?>"
    data-notes.shopping_order_id="<?php echo $data['notes']['merchant_order_id']?>"
    data-order_id="<?php echo $data['order_id']?>"
    <?php if ($displayCurrency !== 'INR') { ?> data-display_amount="<?php echo $data['display_amount']?>" <?php } ?>
    <?php if ($displayCurrency !== 'INR') { ?> data-display_currency="<?php echo $data['display_currency']?>" <?php } ?>
  >
  </script>
  
  <!-- Any extra fields to be submitted with the form but not sent to Razorpay -->
  <input type="hidden" name="shopping_order_id" value="<?php echo $data['notes']['merchant_order_id']?>">
</form>
</div>
</div>
</div>
</div>