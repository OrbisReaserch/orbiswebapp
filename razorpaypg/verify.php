<?php
//include('conn.php');
require('config.php');

session_start();

require('razorpay-php/Razorpay.php');
use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;

$success = true;

$error = "Payment Failed";

if (empty($_POST['razorpay_payment_id']) === false)
{
    $api = new Api($keyId, $keySecret);

    try
    {
        // Please note that the razorpay order ID must
        // come from a trusted source (session here, but
        // could be database or something else)
        $attributes = array(
            'razorpay_order_id' => $_SESSION['razorpay_order_id'],
            'razorpay_payment_id' => $_POST['razorpay_payment_id'],
            'razorpay_signature' => $_POST['razorpay_signature']
        );

        $api->utility->verifyPaymentSignature($attributes);
    }
    catch(SignatureVerificationError $e)
    {
        $success = false;
        $error = 'Razorpay Error : ' . $e->getMessage();
    }
}
$result="Failed";
$shopping_order_id="";
$razorpay_payment_id="";
$razorpay_order_id="";


if ($success === true)
{
    // echo "<pre>";
    // print_r($_POST);exit;
    if(!empty($_POST)){

          if(!empty($_POST['razorpay_payment_id']) &&  !empty($_POST['razorpay_order_id']) ){

                $result="SUCCESS";
                $shopping_order_id=$_POST['shopping_order_id'];
                $razorpay_payment_id=$_POST['razorpay_payment_id'];
                $razorpay_order_id=$_POST['razorpay_order_id'];
          }
          else{
                if(!empty($error)){
                $error="{$error}";
              }
              else{
                $error="Empty Razorpay Payment Id or Razorpay Order Id";
              }

          }
              
     // $html = "<p>Your payment was successful</p>
     //          <p>Shopping Order ID: {$_POST['shopping_order_id']}</p>
     //          <p>Payment ID: {$_POST['razorpay_payment_id']}
     //          <p>Razorpay Order ID: {$_POST['razorpay_order_id']}";

    


    }else{

        //$html="<p>Invalid Transaction Attempted";
        $error="Invalid Transaction";
    }

}
else
{
    // $html = "<p>Your payment failed</p>
    //          <p>{$error}</p>";
    $error="{$error}";
}
$url=$base_url.'reports/report/updatePaymentResponse';
$curl_data=array(
  'shopping_order_id'=>$shopping_order_id,
  'razorpay_payment_id'=>$_POST['razorpay_payment_id'],
  'razorpay_order_id'=>$razorpay_order_id,
  'amount'=>$_SESSION['order_amount'],
  'transaction_result'=>$result,
  'error'=>$error

);
      $handle = curl_init($url);
      curl_setopt($handle, CURLOPT_POST, true);
      curl_setopt($handle, CURLOPT_POSTFIELDS, $curl_data);
      curl_exec($handle);
      curl_close($handle);
//echo $html;
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
table{
    width: 494px;
    height: 150px;
}
tr:nth-child(odd) {
  background: #fff;
}

tr:nth-child(even) {
  background: #ccc;
}
td{
    height:40px;
}
</style>
<div class="container">
    <div class="row text-center">
     <div class="col-md-12">
        <img src="<?php echo $base_url;?>themes/frontend/assets/img/logo_orbis.jpg" style="width:200px;height:90px;box-shadow: 10px 10px 5px #aaaaaa;text-align:center">
        </div>
        <div class="col-sm-6 col-sm-offset-3">
        <br>
          <?php 
         if($result == 'SUCCESS'){?>
          <h2 style="color:#ffffff;padding:22px;background-image: linear-gradient(#057dc2, #0894d6);"><b>Transaction Successful</b></h2>
          <img src="images/successful.png" style="margin-top: 5px; margin-bottom: 25px;">

        
      <?php  }
            else { ?>
        <h2 style="color:#ffffff;padding:22px;background-image: linear-gradient(#d33c54, #e0687b);"><b>Transaction Unsuccessful</b></h2>
          <img src="images/unsuccessful.png" style="margin-top: 7px;margin-bottom: 25px;">
            
        <?php }?>
        <div class="col-md-12">
            
        <div class="col-md-7 ">
        <table border="0">
            <thead>
            <tr style="background-color:#057dc2;"><td colspan="2" style="padding:10px;color:#fff;font-size:20px;text-align: center"><b>Trasaction Details</b></td></tr></thead>
            <tbody><tr ><td>
                <b>Transaction Status:</b>
            </td>

                <td style="padding-left: 10px;"><?php echo $result;?> <td>
            </tr>
            
             <tr><td>
               <b> Transaction Amount:</b>
            </td>

                <td style="padding-left: 10px;"> <?php echo "USD ". $_SESSION['order_amount'];?></td>
            </tr>
             <tr><td>
               <b> Payment Id:</b>
            </td>

                <td style="padding-left: 10px;"> <?php echo $razorpay_payment_id;?></td>
            </tr>
            
             <tr><td>
               <b> Razorpay Order Id:</b>
            </td>

               <td style="padding-left: 10px;"><?php echo $razorpay_order_id;?></td>
            </tr>
            <tr><td>
               <b> Shopping Order Id</b>
            </td>

               <td style="padding-left: 10px;"><?php echo $shopping_order_id;?></td>
            </tr>

            <?php if($result != 'SUCCESS' ){ ?>
            <tr>
                <td><b>Transaction Error:</b></td>              
                <td style="padding-left: 10px;"><?php echo $error;?> </td>       

            </tr>

    <?php
    session_destroy();
     } ?>
        </tbody>
        </table>
    </div>

   <div class="col-md-12" style="margin-bottom:20px;">
      <div class="col-md-offset-2 col-md-4">

          <a href="<?php echo $base_url;?>" class="btn " style="margin-top:30px;background-color:#057dc2;color:#fff;">Go To Home Page</a>
        </div>    
      
      
      <div class="col-md-4" style="margin-left: -34px;">
          <button class="btn "style="margin-top:30px;background-color:#057dc2;color:#fff;" onClick="window.print();">Print Details</button>

          
        </div>    
      </div>
    
    </div>
        
    </div>
</div>